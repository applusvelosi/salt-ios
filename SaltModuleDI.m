
//
//  SaltModuleDI.m
//  Salt
//
//  Created by Rick Royd Aban on 16/02/2017.
//  Copyright © 2017 Applus Velosi. All rights reserved.
//

#import "SaltModuleDI.h"
#import "VCClaimNotPaidByCC.h"
#import "ClaimManager.h"

@implementation SaltModuleDI

- (void)configure {
    [self bindClass:[ClaimManager class] toProtocol:@protocol(ClaimHeaderDelegate)];
}

@end
