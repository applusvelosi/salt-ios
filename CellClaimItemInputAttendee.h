//
//  CellClaimInputAttendee.h
//  Salt
//
//  Created by Rick Royd Aban on 2/29/16.
//  Copyright © 2016 Applus Velosi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CellClaimItemInputAttendee : UITableViewCell

@property (strong, nonatomic) IBOutlet UILabel *propLabelName;
@property (strong, nonatomic) IBOutlet UILabel *propLabelJobDesc;
@property (strong, nonatomic) IBOutlet UILabel *propLabelNotes;

@end
