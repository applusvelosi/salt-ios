//
//  CapexHeader.h
//  Salt
//
//  Created by Rick Royd Aban on 10/1/15.
//  Copyright © 2015 Applus Velosi. All rights reserved.

#define NO_ATTACHMENT @"NO ATTACHMENT"
#import <Foundation/Foundation.h>
#import "OnlineGateway.h"
#import "OfflineGatewaySavable.h"

typedef NS_ENUM(int, CapexStatusId) {
    CapexStatusIdOpen = 40,
    CapexStatusIdSubmitted,
    CapexStatusIdApprovedByCm,
    CapexStatusIdRejectedByCm,
    CapexStatusIdApprovedByRm,
    CapexStatusIdRejectedByRm,
    CapexStatusIdReturned,
    CapexStatusIdCancelled,
    CapexStatusIdApprovedByCfo,
    CapexStatusIdRejectedByCfo,
    CapexStatusIdApprovedByCeo,
    CapexStatusIdRejectedByCeo,
    CapexStatusIdApprovedByRfm = 144,
    CapexStatusIdRejectedByRfm = 145
};

@interface CapexHeader : NSObject<OfflineGatewaySavable>

- initWithDictionary:(NSDictionary *)jsonCapexHeader onlineGateway:(OnlineGateway *)onlineGateway;
- (CapexHeader *)initWithDictionary:(NSMutableDictionary *)jsonCapexHeader;
-(NSString *)getCapexJson;
- (NSMutableArray *)propDocuments;
- (NSString *)propAttachedCer;
- (int)propCapexID;
- (NSArray *)propCapexLineItems;
- (NSString *)propApproverNote;
- (NSString *)propCapexNumber;
- (int)propCostCenterID;
- (NSString *)propCostCenterName;
- (int)propCMID;
- (NSString *)propCMName;
- (NSString *)propCMEmail;
- (NSString *)propDateCreated:(AppDelegate *)app;
- (NSString *)propDateProcessedByCM:(AppDelegate *)app;
- (NSString *)propDateProcessedByRFM:(AppDelegate *)app;
- (NSString *)propDateProcessedByRM:(AppDelegate *)app;
- (NSString *)propDateSubmitted:(AppDelegate *)app;
- (NSString *)propDateProcessedByCFO:(AppDelegate *)app;
- (NSString *)propDateProcessedByCEO:(AppDelegate *)app;
- (int)propDepartmentID;
- (NSString *)propDepartmentName;
- (int)propInvestmentTypeID;
- (NSString *)propInvestmentTypeName;
- (NSString *)propNote;
- (int)propOfficeID;
- (NSString *)propOfficeName;
- (int)propRFMID;
- (int)propRMID;
- (NSString *)propRMEmail;
- (NSString *)propRMName;
- (NSString *)propRequesterEmail;
- (int)propRequesterID;
- (NSString *)propRequesterName;
- (int)propStatusID;
- (NSString *)propStatusName;
- (float)propTotal;
- (float)propTotalAmountInUSD;

- (NSString *)JSONForUpdatingCapexHeaderWithStatusID:(CapexStatusId)statusID keyForUpdatableDate:(NSString *)keyForUpdatableDate withApproverNote:(NSString *)note appDelegate:(AppDelegate *)appDelegate;
- (NSString *)jsonizeWithappDelegate:(AppDelegate *)appDelegate;

@end
