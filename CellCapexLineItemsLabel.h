//
//  CellCapexLineItemDetail.h
//  Salt
//
//  Created by Rick Royd Aban on 10/19/15.
//  Copyright © 2015 Applus Velosi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CellCapexLineItemsLabel : UITableViewCell

@property (strong, nonatomic) IBOutlet UILabel *propCapexNumber;


@end
