//
//  VCBusinessAdvance.m
//  Salt
//
//  Created by Rick Royd Aban on 2015/07/31.
//  Copyright (c) 2015年 Applus Velosi. All rights reserved.
//

#import "MBProgressHUD.h"
#import "VCBusinessAdvance.h"
#import "CellClaimDetailOverview.h"
#import "CellMinifiedDetailAmount.h"
#import "CellClaimDetailProcess.h"
#import "VCClaimItems.h"
#import "Document.h"
#import "VCClaimsForApproval.h"
#import <Objection/Objection.h>
#import "VelosiColors.h"
#import "ClaimItem.h"
#import "VCClaimForApprovalLineItems.h"
#import "CellForBAOutstanding.h"
#import "BAOutstandingDialogVC.h"

@interface VCBusinessAdvance ()<UIDocumentInteractionControllerDelegate> {
    UITapGestureRecognizer *_attachmentRecognizer;
    UIDocumentInteractionController *_docInterCtrl;
    ClaimHeader *myClaim;
    NSMutableString *_stringNotes, *_approversNote;
    NSArray *claimItems;
    CABasicAnimation *animation;
    
    int processedItemFlag, approvedItemCtr, rejectedItemCtr, returnedItemCtr;
    NSMutableArray *_baOutstanding;
    UIAlertController *_alertBADialogController;
    BAOutstandingDialogVC *_alert;
    
}

@property (nonatomic, weak) IBOutlet UIButton *processButton;
@property (nonatomic, weak) IBOutlet UILabel *propFieldClaimID;
@property (nonatomic, weak) IBOutlet UILabel *propFieldStaff;
@property (nonatomic, weak) IBOutlet UILabel *propFieldCostCenter;
@property (nonatomic, weak) IBOutlet UILabel *propFieldCostCenterNum;
@property (nonatomic, weak) IBOutlet UILabel *propFieldApprover;
@property (nonatomic, weak) IBOutlet UILabel *propFieldType;
@property (nonatomic, weak) IBOutlet UILabel *propFieldStatus;
@property (nonatomic, weak) IBOutlet UILabel *propFieldCompanyCard;
@property (nonatomic, weak) IBOutlet UILabel *propFieldBAFReference;
@property (nonatomic, weak) IBOutlet UILabel *propFieldParentClaim;
@property (nonatomic, weak) IBOutlet UILabel *propFieldAttachment;
@property (nonatomic, weak) IBOutlet UILabel *propFieldLineItem;

@property (nonatomic, weak) IBOutlet UILabel *propFieldTotalClaimAmt;
@property (nonatomic, weak) IBOutlet UILabel *propFieldApprovedAmt;
@property (nonatomic, weak) IBOutlet UILabel *propFieldRejectedAmt;
@property (nonatomic, weak) IBOutlet UILabel *propFieldForPaymentAmt;
@property (nonatomic, weak) IBOutlet UILabel *propFieldForDeduction;

@property (nonatomic, weak) IBOutlet UILabel *propFieldDateSubmitted;
@property (nonatomic, weak) IBOutlet UILabel *propFieldDateProcessedByApprover;
@property (nonatomic, weak) IBOutlet UILabel *propFieldDateProcessedByCM;
@property (nonatomic, weak) IBOutlet UILabel *propFieldDateProcessedByAccount;
@property (nonatomic, weak) IBOutlet UILabel *propFieldDatePaymentOn;
@property (nonatomic, weak) IBOutlet UILabel *propFieldDateSAPPosting;
@property (nonatomic, weak) IBOutlet UILabel *propFieldApproversNote;

@property (nonatomic, weak) IBOutlet UINavigationItem *propFieldNavTitle;

@property (nonatomic, strong) id<ClaimHeaderDelegate> delegate;
@end

@implementation VCBusinessAdvance

objection_requires(@"delegate");

#pragma mark -
#pragma mark === View Life Cycle Management ===
#pragma mark -
- (void)viewDidLoad {
    [super viewDidLoad];
    
    dispatch_group_t grpQueue = dispatch_group_create();
    [[JSObjection defaultInjector] injectDependencies:self];
    animation = [CABasicAnimation animationWithKeyPath:@"opacity"];
    [animation setFromValue:@0.0f];
    [animation setToValue:@1.0f];
    [animation setDuration:0.5f];
    [animation setRepeatCount:HUGE_VALF];
    [animation setAutoreverses:YES];
    [animation setRemovedOnCompletion:NO];
    self.definesPresentationContext = YES;
    _baOutstanding = [[NSMutableArray alloc] init];
    if ([self.baLiquidation propTypeID] == CLAIMTYPEID_ADVANCES)
        self.propFieldNavTitle.title = @"Business Advance";
    else
        self.propFieldNavTitle.title = @"Liquidation";
    
    [AppDelegate showGlogalHUDWithView:self.view];
    dispatch_group_enter(grpQueue);
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
        id claimResult = [self.propAppDelegate.propGatewayOnline claimByID:[self.baLiquidation propClaimID] andStaffID:[self.baLiquidation propStaffID]];
        dispatch_group_async(grpQueue, dispatch_get_main_queue(), ^{
//            [AppDelegate hideGlogalHUDWithView:self.view];
            if([claimResult isKindOfClass:[NSString class]])
                [self viewAlertDialogWithTitle:@"" message:claimResult];
            else {
                _processButton.enabled = NO;
                myClaim = [[ClaimHeader alloc] initWithJSONDictionary:claimResult withAppDelegate:_propAppDelegate];
                claimItems = [myClaim dictForWebServices];
                
                dispatch_group_async(grpQueue, dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                    id claimsResult = [self.propAppDelegate.propGatewayOnline myClaimsWithStaffID:[myClaim propStaffID] requestingPerson:[[_propAppDelegate getStaff] propStaffID]];
                    dispatch_group_async(grpQueue, dispatch_get_main_queue(), ^{
                        if (![claimsResult isKindOfClass:[NSString class]]) {
//                            [AppDelegate hideGlogalHUDWithView:self.view];
                            
                            if ([myClaim propTypeID] == CLAIMTYPEID_ADVANCES) {
                                for (ClaimHeader *myBA in claimsResult) {
                                    if ([myBA propTypeID] == CLAIMTYPEID_ADVANCES) {
                                        if ([myClaim propClaimID] != [myBA propClaimID]) {
                                            if ([myClaim propStatusID] == CLAIMSTATUSID_SUBMITTED || [myClaim propStatusID] == CLAIMSTATUSID_APPROVEDBYAPPROVER || [myClaim propStatusID] == CLAIMSTATUSID_APPROVEDBYCM || [myClaim propStatusID] == CLAIMSTATUSID_SENTTOSAP || [myClaim propStatusID] == CLAIMSTATUSID_APPROVEDBYACCOUNTS)
                                                [_baOutstanding addObject:myBA];
                                        }
                                    }
                                }

                                BAOutstandingDialogVC *alertVC = [[BAOutstandingDialogVC alloc] init];
                                CGRect frame;
                                if (_baOutstanding.count < 4) {
                                    frame = CGRectMake(0, 0, 272, 100);
                                    [alertVC setPreferredContentSize:frame.size];
                                }
                                else if (_baOutstanding.count < 6) {
                                    frame = CGRectMake(0, 0, 272, 150);
                                    [alertVC setPreferredContentSize:frame.size];
                                }
                                else if (_baOutstanding.count < 8) {
                                    frame = CGRectMake(0, 0, 272, 200);
                                    [alertVC setPreferredContentSize:frame.size];
                                } else {
                                    frame = CGRectMake(0, 0, 272, 250);
                                    [alertVC setPreferredContentSize:frame.size];
                                }
                                alertVC.propTableFrame = frame;
                                
                                alertVC.numberOfRowsInTableAlert = ^(BAOutstandingDialogVC *alertView){
                                    return _baOutstanding.count;
                                };
                                
                                alertVC.cellForRow = ^(BAOutstandingDialogVC *alertView, NSIndexPath *indexPath) {
                                    if (indexPath.row == 0) {
                                        static NSString *cellIdentifier = @"outstandingheadercell";
                                        CellForBAOutstanding *cell = [alertView.alertTableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
                                        return cell;
                                    }else {
                                        ClaimHeader *baContent = [_baOutstanding objectAtIndex:indexPath.row];
                                        static NSString *cellIdentifier = @"outstandingcell";
                                        CellForBAOutstanding *cell = [alertView.alertTableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
                                        cell.propClaimNum.text = [baContent propClaimNumber];
                                        cell.propAmount.text = [NSString stringWithFormat:@"%@ %@",[baContent propCurrencyThree], [self.propAppDelegate.decimalFormat stringFromNumber:[NSNumber numberWithFloat:[baContent propTotalClaim]]]];;
                                        cell.propStatus.text = [baContent propStatusName];
                                        cell.propStaff.text = [baContent propStaffName];
                                        
                                        cell.bounds = CGRectMake(0, 0, CGRectGetWidth(alertView.alertTableView.bounds), 99999);
                                        cell.contentView.bounds = cell.bounds;
                                        [cell layoutIfNeeded];
                                        
                                        cell.propStaff.preferredMaxLayoutWidth = CGRectGetWidth(cell.propStaff.frame);
                                        
                                        return cell;
                                    }
                                };
                                
                                @try {
                                    _alertBADialogController = [UIAlertController alertControllerWithTitle:@"OUTSTANDING" message:NSLocalizedString(@"BAOutstandingNote", @"") preferredStyle:UIAlertControllerStyleAlert];
                                    [_alertBADialogController setValue:alertVC forKey:@"contentViewController"];
                                    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDestructive handler:^(UIAlertAction *action) {
                                        
                                    }];
                                    UIAlertAction *processAction = [UIAlertAction actionWithTitle:@"Process"
                                                                                            style:UIAlertActionStyleDefault
                                                                                          handler:^(UIAlertAction *action) {
                                                                                              [self processBAWithoutOutstanding];
                                                                                          }];
                                    [_alertBADialogController addAction:cancelAction];
                                    [_alertBADialogController addAction:processAction];
                                    
                                } @catch (NSException *exception) {
                                    NSLog(@"Failed setting content view controller: %@", exception);
                                }
                                
                                if (_baOutstanding.count > 0) {
                                    [self.processButton addTarget:self action:@selector(processBAWithOutstanding) forControlEvents:UIControlEventTouchUpInside];
                                } else {
                                    [self.processButton addTarget:self action:@selector(processBAWithoutOutstanding) forControlEvents:UIControlEventTouchUpInside];
                                }
                                
                            }
                        } else {
                            [self viewAlertDialogWithTitle:@"" message:claimsResult];
                        }
                    });
                });
                
                _propFieldClaimID.text = [NSString stringWithFormat:@"%@", [myClaim propClaimNumber]];
                _propFieldStaff.text = [NSString stringWithFormat:@"%@", [myClaim propStaffName]];
                
                _propFieldCostCenter.text = [NSString stringWithFormat:@"%@", [myClaim propCostCenterName]];
                //                _propFieldCostCenterNum.text = [NSString stringWithFormat:@"%d", [myClaim propCostCenterID]];
                _propFieldApprover.text = [myClaim propApproverName];
                _propFieldType.text = [myClaim propTypeName];
                _propFieldStatus.text = [myClaim propStatusName];
                _propFieldCompanyCard.text = ([myClaim propIsPaidByCC])?@"YES":@"NO";
                _propFieldParentClaim.text = [myClaim propParentClaimNumber];
                _docInterCtrl = [[UIDocumentInteractionController alloc] init];
                _docInterCtrl.delegate = self;
                if([myClaim propDocuments].count > 0){
                    _propFieldAttachment.text = [[[myClaim propDocuments] objectAtIndex:0] propDocName];
                    _propFieldAttachment.textColor = [VelosiColors orangeVelosi];
                    _propFieldAttachment.font = [UIFont fontWithName:@"TrebuchetMS-Bold" size:13];
                    _attachmentRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onAttachmentClicked)];
                    [_propFieldAttachment addGestureRecognizer:_attachmentRecognizer];
                }else{
                    _propFieldAttachment.text = [myClaim propAttachedCert];
                    _propFieldAttachment.textColor = [VelosiColors blue];
                }
                NSString *msg = ([claimItems count] > 1)?@"Items":@"Item";
                _propFieldLineItem.text = [NSString stringWithFormat:@"%ld %@", claimItems.count, msg];
                _propFieldDateSubmitted.text = [myClaim propDateSubmitted:_propAppDelegate];
                _propFieldDateProcessedByApprover.text = [myClaim propDateApprovedByApprover:_propAppDelegate];
                _propFieldDateProcessedByAccount.text = [myClaim propDateApprovedByAccount:_propAppDelegate];
                _propFieldDatePaymentOn.text = [myClaim propDatePaid:_propAppDelegate];
                
                _stringNotes = [[NSMutableString alloc] init];
                _approversNote = [[NSMutableString alloc] init];
                
                if ([myClaim approversNote].length > 0) {
                    NSArray *tempStr = [[myClaim approversNote] componentsSeparatedByString:@";"];
                    for (NSString *str in tempStr) {
                        [_stringNotes appendString:str];
                        [_stringNotes appendString:@"\n"];
                    }
                    for (NSString *str in tempStr)
                        [_approversNote appendString:str];
                    [_approversNote appendString:@";"];
                } else {
                    [_stringNotes appendString:[myClaim approversNote]];
                    [_approversNote appendString:[myClaim approversNote]];
                }
                _propFieldApproversNote.text = _stringNotes;
            }
        });
        dispatch_group_leave(grpQueue);
    });
    dispatch_group_notify(grpQueue, dispatch_get_main_queue(), ^{
        [AppDelegate hideGlogalHUDWithView:self.view];
    });
}

- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:YES];
    float totalLC, totalApproved, totalRejected, totalForDeduction, totalForPayment;
    totalLC = totalApproved = totalRejected = totalForDeduction = totalForPayment = 0;
    processedItemFlag = approvedItemCtr = rejectedItemCtr = returnedItemCtr = 0;
    
    
    for ( ClaimItem *claimItem in [_propAppDelegate forApprovalClaimItemsForClaimID:[self.baLiquidation propClaimID]] ) {
        if ([claimItem statusID] != [self.baLiquidation propStatusID])
            processedItemFlag++;
        if ( [claimItem statusID] == CLAIMSTATUSID_APPROVEDBYAPPROVER || [claimItem statusID] == CLAIMSTATUSID_APPROVEDBYCM )
            approvedItemCtr++;
        else if ( [claimItem statusID] == CLAIMSTATUSID_REJECTEDBYAPPROVER || [claimItem statusID] == CLAIMSTATUSID_REJECTEDBYCM )
            rejectedItemCtr++;
        else if ( [claimItem statusID] == CLAIMSTATUSID_RETURN )
            returnedItemCtr++;
    }
    
    if (claimItems.count > 0) {
        if(processedItemFlag == claimItems.count) {
            _processButton.enabled = YES;
            [_processButton setTitleColor:[VelosiColors greenAcceptance] forState:UIControlStateNormal];
            UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"" message:@"All items are evaluated, you may now endorse the claim by clicking the Process button." preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction *defaultAction = [UIAlertAction actionWithTitle:@"Dismiss" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action){
                NSLog(@"Cancelled.");
            }];
            [alertController addAction:defaultAction];
            [self presentViewController:alertController animated:YES completion:nil];
            [self.processButton.layer addAnimation:animation forKey:@"animation"];
        }
        
    }
    
//    if (_baOutstanding.count > 0) {
//        [self.processButton addTarget:self action:@selector(processBAWithOutstanding) forControlEvents:UIControlEventTouchUpInside];
//    } else {
//        [self.processButton addTarget:self action:@selector(processBAWithoutOutstanding) forControlEvents:UIControlEventTouchUpInside];
//    }
    
    if (self.lcSumValuesProvider != nil) {
        self.lcSumValuesProvider(&totalLC, &totalApproved, &totalRejected, &totalForDeduction, &totalForPayment);
    }
    
    self.propFieldTotalClaimAmt.text = [NSString stringWithFormat:@"%@ %@",[self.propAppDelegate.decimalFormat stringFromNumber:[NSNumber numberWithFloat:totalLC]], [self.baLiquidation propCurrencyThree]];
    self.propFieldApprovedAmt.text = [NSString stringWithFormat:@"%@ %@",[self.propAppDelegate.decimalFormat stringFromNumber:[NSNumber numberWithFloat:totalApproved]], [self.baLiquidation propCurrencyThree]];
    self.propFieldRejectedAmt.text = [NSString stringWithFormat:@"%@ %@",[self.propAppDelegate.decimalFormat stringFromNumber:[NSNumber numberWithFloat:totalRejected]], [self.baLiquidation propCurrencyThree]];
    self.propFieldForPaymentAmt.text = [NSString stringWithFormat:@"%@ %@",[self.propAppDelegate.decimalFormat stringFromNumber:[NSNumber numberWithFloat:totalForPayment]], [self.baLiquidation propCurrencyThree]];
    self.propFieldForDeduction.text = [NSString stringWithFormat:@"%@ %@",[self.propAppDelegate.decimalFormat stringFromNumber:[NSNumber numberWithFloat:totalForDeduction]], [self.baLiquidation propCurrencyThree]];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    VCClaimForApprovalLineItems *lineItemsVC = (VCClaimForApprovalLineItems*)[segue destinationViewController];
    lineItemsVC.propClaimHeader = self.baLiquidation;
    lineItemsVC.propParentVC = self;
}


#pragma mark -
#pragma mark === UI Event Handler Management ===
#pragma mark -
- (void)processBAWithOutstanding {
    if (_alertBADialogController)
        [self presentViewController:_alertBADialogController animated:YES completion:nil];
}

- (void)processBAWithoutOutstanding {
    NSString *alertTitle = @"Approver's Note";
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:alertTitle
                                                                             message:nil
                                                                      preferredStyle:UIAlertControllerStyleAlert];
    [alertController addTextFieldWithConfigurationHandler:^(UITextField *textField)
     {
         textField.placeholder = @"Note";
         [textField addTarget:nil action:@selector(self) forControlEvents:UIControlEventValueChanged];
     }];
    void(^approveAlertHandler)(UIAlertAction *action);
    approveAlertHandler = ^(UIAlertAction *action) {
        UITextField *approveNote = alertController.textFields[0];
        NSLog(@"APPROVERS: %@",_stringNotes);
        if ([myClaim claimStatus] == CLAIMSTATUSID_SUBMITTED) {
            if (approveNote.text.length > 0)
                [_approversNote appendFormat:@"Approver: %@;",approveNote.text];
            else
                [_approversNote appendString:@"Approver: Approved;"];
            [self updateClaimHeaderWithStatusID:CLAIMSTATUSID_APPROVEDBYAPPROVER withKeyForUpdatableDate:@"DateApprovedByApprover" statusDesc:CLAIMSTATUSDESC_APPROVEDBYAPPROVER approversNote:_approversNote];
        } else if ([myClaim claimStatus] == CLAIMSTATUSID_APPROVEDBYAPPROVER) {
            if (approveNote.text.length > 0) {
                if ([self.baLiquidation officeID] != 41)
                    [_approversNote appendFormat:@"CM: %@", approveNote.text];
                else
                    [_approversNote appendFormat:@"CM: %@", approveNote.text];
            } else {
                if ([self.baLiquidation officeID] != 41)
                    [_approversNote appendString:@"CM: Approved"];
                else
                    [_approversNote appendString:@"CFO: Approved"];
            }
            [_approversNote appendString:@";"];
            [self updateClaimHeaderWithStatusID:CLAIMSTATUSID_APPROVEDBYCM withKeyForUpdatableDate:@"DateApprovedByDirector" statusDesc:CLAIMSTATUSDESC_APPROVEDBYCM approversNote:_approversNote];
        }
    };
    [alertController addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action){}]];
    [alertController addAction:[UIAlertAction actionWithTitle:@"Proceed" style:UIAlertActionStyleDefault handler:approveAlertHandler]];
    [self presentViewController:alertController animated:YES completion:nil];
}


#pragma mark -
#pragma mark === Helper Method ===
#pragma mark -
- (void)updateClaimHeaderWithStatusID:(int)statusID withKeyForUpdatableDate:(NSString *)keyForUpdatableDate statusDesc:(NSString*)description approversNote:(NSString *)approversNote {
    
    NSString *processMsg = [[NSString alloc] init];
    if (approvedItemCtr == [self.propAppDelegate forApprovalClaimItemsForClaimID:[self.baLiquidation propClaimID]].count)
        processMsg = @"Approved Successfully!";
    else if (rejectedItemCtr == [self.propAppDelegate forApprovalClaimItemsForClaimID:[self.baLiquidation propClaimID]].count)
        processMsg = @"Rejected Successfully";
    else if (returnedItemCtr == [self.propAppDelegate forApprovalClaimItemsForClaimID:[self.baLiquidation propClaimID]].count)
        processMsg = @"Returned Successfully";
    else
        processMsg = @"Processed Successfully";
    
    [self.delegate claimViewController:self willProcessClaimSplit:self.baLiquidation statusID:statusID withKeyForUpdatableDate:keyForUpdatableDate statusDesc:description approversNote:approversNote processMessage:processMsg appDelegate:self.propAppDelegate];
}

- (void)onAttachmentClicked {
    [AppDelegate showGlogalHUDWithView:self.tableView.superview];
    if ([[myClaim propDocuments] count] > 0) {
        Document *doc = [[myClaim propDocuments] objectAtIndex:0];
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
            NSString *urlString = [NSString stringWithFormat:@"%@?dID=%d&refID=%d&obTypeID=%d", NSLocalizedString(@"ImageViewerLink", @""),[doc propDocID], [doc propRefID], [doc propObjectTypeID]];
            id urlData = [_propAppDelegate.propGatewayOnline makeWebServiceCall:urlString requestMethod:GET postBody:nil];
            dispatch_async(dispatch_get_main_queue(), ^{
                [AppDelegate hideGlogalHUDWithView:self.tableView.superview];
                if (![urlData isKindOfClass:[NSString class]]) {
                    NSString *filePath = [NSTemporaryDirectory() stringByAppendingPathComponent:[doc propDocName]];
                    [urlData writeToFile:filePath atomically:YES];
                    NSURL *filePathURL = [NSURL fileURLWithPath:filePath];
                    _docInterCtrl.URL = filePathURL;
                    [_docInterCtrl presentPreviewAnimated:YES];
                } else {
                    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"" message:urlData preferredStyle:UIAlertControllerStyleAlert];
                    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"Dismiss" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
                        [self.navigationController popViewControllerAnimated:YES];
                    }];
                    [alertController addAction:cancelAction];
                    [self presentViewController:alertController animated:YES completion:nil];
                }
            });
        });
    }else{
        [self viewAlertDialogWithTitle:@"" message:@"Attached document is invalid."];
    }
}

- (void)viewAlertDialogWithTitle:(NSString*)title message:(NSString*)msg{
    [AppDelegate hideGlogalHUDWithView:self.view];
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:title message:msg preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"Dismiss" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
        [self.navigationController popViewControllerAnimated:YES];
    }];
    [alertController addAction:cancelAction];
    [self presentViewController:alertController animated:YES completion:nil];
}

#pragma mark -
#pragma mark === UIDocument Interaction Controller Delegate ===
#pragma mark -
- (UIViewController *)documentInteractionControllerViewControllerForPreview:(UIDocumentInteractionController *)controller{
    return self;
}

@end
