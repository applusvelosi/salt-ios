//
//  BusinessAdvance.m
//  Salt
//
//  Created by Rick Royd Aban on 2015/07/29.
//  Copyright (c) 2015年 Applus Velosi. All rights reserved.
//

#import "BusinessAdvance.h"

@implementation BusinessAdvance

- (ClaimHeader *)initWithCostCenterID:(int)costCenterID costCenterName:(NSString *)costCenterName claimTypeID:(int)claimTypeID isPaidByCC:(BOOL)isPaidByCC baIDCharged:(int)baIDCharged bacNumber:(NSString *)bacNumber appDelegate:(AppDelegate *)appDelegate{
    
    return [super initWithCostCenterID:costCenterID costCenterName:costCenterName claimTypeID:CLAIMTYPEID_ADVANCES isPaidByCC:YES baIDCharged:0 bacNumber:@"" appDelegate:appDelegate];
}

@end
