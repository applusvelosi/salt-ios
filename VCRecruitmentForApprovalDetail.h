//
//  VCRecruitmentForApprovalDetail.h
//  Salt
//
//  Created by Rick Royd Aban on 10/2/15.
//  Copyright © 2015 Applus Velosi. All rights reserved.
//

#import "VCPage.h"
#import "Recruitment.h"

@interface VCRecruitmentForApprovalDetail : VCPage<UITableViewDataSource, UITableViewDelegate, UIAlertViewDelegate>

@property (assign, nonatomic) int propRecruitmentID;

@end
