//
//  VCRecruitmentForApprovalDetail.m
//  Salt
//
//  Created by Rick Royd Aban on 10/2/15.
//  Copyright © 2015 Applus Velosi. All rights reserved.
//

#import "VCRecruitmentForApprovalDetail.h"
#import "CellRFADetailEmploymentStatus.h"
#import "CellRFADetailHiringSupervisor.h"
#import "CellRFADetailPositionInfo.h"
#import "CellRFADetailVacancy.h"
#import "VCRFADetailOtherBenefits.h"
#import "VCRFADetailAttachments.h"
#import "MBProgressHUD.h"
#import "CellRFADetailApproversNote.h"

@interface VCRecruitmentForApprovalDetail() {
    int _staffId;
    IBOutlet UITableView *_propLV;
    Recruitment *_propRecruitment;
    NSMutableString *_stringNote;
}
@end

@implementation VCRecruitmentForApprovalDetail

#pragma mark -
#pragma mark === View Life Cycle Management ===
#pragma mark -
- (void)viewDidLoad{
    [super viewDidLoad];
    
    _propLV.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    _propLV.estimatedRowHeight = 140;
    _propLV.rowHeight = UITableViewAutomaticDimension;
    _propLV.dataSource = self;
    _propLV.delegate = self;
    _staffId = [[self.propAppDelegate getStaff] propStaffID];
    
    [self fetchRecruitmentForApprovalByID:^(Recruitment *recruitment){
        _propRecruitment = recruitment;
        [_propLV reloadData];
        NSLog(@"NOTES: %@",[_propRecruitment propApproverNote]);
        _stringNote = [[NSMutableString alloc] init];
        NSString *tempStr;
        NSScanner *stringScanner = [NSScanner scannerWithString:[_propRecruitment propApproverNote]];
        NSCharacterSet *separator = [NSCharacterSet newlineCharacterSet];
        while ([stringScanner isAtEnd] == NO) {
            if ([stringScanner scanUpToCharactersFromSet:separator intoString:&tempStr])
                [_stringNote appendString:tempStr];
            [_stringNote appendFormat:@"\n"];
        }
    }];
}

-(void) fetchRecruitmentForApprovalByID:(void(^)(Recruitment *))blockPtr{
    [AppDelegate showGlogalHUDWithView:self.view];
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
        id result = [self.propAppDelegate.propGatewayOnline recruitmentForID:_propRecruitmentID];
        __block Recruitment *tempRecruitment = nil;
        dispatch_async(dispatch_get_main_queue(), ^{
            [AppDelegate hideGlogalHUDWithView:self.view];
            if([result isKindOfClass:[NSString class]])
                [[[UIAlertView alloc] initWithTitle:@"" message:result delegate:nil cancelButtonTitle:@"Dismiss" otherButtonTitles:nil, nil] show];
            else {
                tempRecruitment = [[Recruitment alloc] initWithDictionary:result onlineGateway:self.propAppDelegate.propGatewayOnline];
                blockPtr(tempRecruitment);
            }
        });
    });
    
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    if(_propRecruitment != nil){
        if([segue.identifier isEqualToString:@"vcrfadetailtootherbenefits"])
            ((VCRFADetailOtherBenefits *)segue.destinationViewController).propRecruitment = _propRecruitment;
        else if([segue.identifier isEqualToString:@"vcrfadetailtodocuments"])
            ((VCRFADetailAttachments *)segue.destinationViewController).propRecruitment = _propRecruitment;
    }
}

#pragma mark -
#pragma mark - Table view data source delegate
#pragma mark -


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    switch(indexPath.section){
        case 0: { //Hiring Supervisor
            CellRFADetailHiringSupervisor *cell = [tableView dequeueReusableCellWithIdentifier:@"cell_hiringsupervisor"];
            cell.propFieldName.text = [_propRecruitment propRequesterName];
            cell.propFieldEmail.text = [_propRecruitment propEmail];
            cell.propFieldOffice.text = [_propRecruitment propRequesterOfficeName];
            cell.propFieldDepartment.text = [_propRecruitment propRequesterDepartmentName];
            cell.propFieldPhoneNumber.text = [_propRecruitment propRequesterPhoneNumber];
            cell.propFieldCMName.text = [_propRecruitment propCMName];
            cell.propFieldDateRequested.text = [_propRecruitment propDateRequested:self.propAppDelegate];
            cell.propFieldProcessedByCM.text = [_propRecruitment propDateProcessedByCM:self.propAppDelegate];
            cell.propFieldProcessedByRHM.text = [_propRecruitment propDateProcessedByRHM:self.propAppDelegate];
            cell.propFieldProcessedByRM.text = [_propRecruitment propDateProcessedByRM:self.propAppDelegate];
            cell.propFieldProcessedByHRM.text = [_propRecruitment propDateProcessedByHR:self.propAppDelegate];
            cell.propFieldProcessedByCEO.text = [_propRecruitment propDateProcessedByCEO:self.propAppDelegate];
            
            cell.bounds = CGRectMake(0, 0, CGRectGetWidth(tableView.bounds), 99999);
            cell.contentView.bounds = cell.bounds;
            [cell layoutIfNeeded];
            cell.propFieldName.preferredMaxLayoutWidth = CGRectGetWidth(cell.propFieldName.frame);
            cell.propFieldOffice.preferredMaxLayoutWidth = CGRectGetWidth(cell.propFieldOffice.frame);
            return cell;
        }
            break;
        case 1:{ //Reason for Vacancy
            CellRFADetailVacancy *cell = [tableView dequeueReusableCellWithIdentifier:@"cell_vacancy"];
            cell.propFieldRequestType.text = [_propRecruitment propPositionTypeName];
            cell.propFieldReasonForPosition.text = [_propRecruitment propReason];
            if ([_propRecruitment propPositionTypeID] == RecruitmentPositionTypeIdReplacement) {
                cell.propFieldYes.image = [UIImage imageNamed:([_propRecruitment propIsRequiredReplacement])?@"icon_checkbox_sel":@"icon_checkbox_normal"];
                cell.propFieldNo.image = [UIImage imageNamed:(![_propRecruitment propIsRequiredReplacement])?@"icon_checkbox_sel":@"icon_checkbox_normal"];
            }else{
                cell.propFieldYes.image = [UIImage imageNamed:@"icon_checkbox_normal"];
                cell.propFieldNo.image = [UIImage imageNamed:@"icon_checkbox_normal"];
                if ([_propRecruitment propPositionTypeID] == RecruitmentPositionTypeIdDismissal){
                    cell.propReasonLabel.text = @"Reason for Dismissal";
                }
            }
            cell.bounds = CGRectMake(0, 0, CGRectGetWidth(tableView.bounds), 99999);
            cell.contentView.bounds = cell.bounds;
            [cell layoutIfNeeded];
            cell.propFieldReasonForPosition.preferredMaxLayoutWidth = CGRectGetWidth(cell.propFieldReasonForPosition.frame);
            
            return cell;
        }
            break;
        case 2:{ //Attachment
            return [tableView dequeueReusableCellWithIdentifier:@"cell_attachments"];
        }
            break;
        case 3: { //Position Information
            CellRFADetailPositionInfo *cell = [tableView dequeueReusableCellWithIdentifier:@"cell_positioninformation"];
            cell.propFieldOffice.text = [_propRecruitment propOfficeOfDeploymentName];
            cell.propFieldDepartment.text = [_propRecruitment propDepartmentToBeAssignedName];
            cell.propFieldStartDate.text = [_propRecruitment propTargettedStartDate];
            cell.propFieldJobTitle.text = [_propRecruitment propJobTitle];
            cell.propFieldEmpCategory.text = [_propRecruitment propEmployeeCategoryName];
            cell.propFieldAnnualRevenue.text = [NSString stringWithFormat:@"%.2f",[_propRecruitment propAnnualRevenue]];
            cell.propFieldSalaryRange.text = [NSString stringWithFormat:@"%.2f - %.2f",[_propRecruitment propSalaryRangeFrom], [_propRecruitment propSalaryRangeTo]];
            cell.propCboxIsBudgettedCost.image = [UIImage imageNamed:([_propRecruitment propIsBudgetedCost])?@"icon_checkbox_sel":@"icon_checkbox_normal"];
            cell.propCboxIsSpecificPerson.image = [UIImage imageNamed:([_propRecruitment propIsSpecificPerson])?@"icon_checkbox_sel":@"icon_checkbox_normal"];
            cell.propServeranceLabel.hidden = YES;
            cell.propFieldSeveranceDetails.hidden = YES;
            if ([_propRecruitment propPositionTypeID] == RecruitmentPositionTypeIdDismissal) {
                cell.propServeranceLabel.hidden = NO;
                cell.propFieldSeveranceDetails.hidden = NO;
                cell.propFieldSeveranceDetails.text = [_propRecruitment propSeverancePayment];
            }
            cell.bounds = CGRectMake(0, 0, CGRectGetWidth(tableView.bounds), 99999);
            cell.contentView.bounds = cell.bounds;
            [cell layoutIfNeeded];
            cell.propFieldOffice.preferredMaxLayoutWidth = CGRectGetWidth(cell.propFieldOffice.frame);
            cell.propFieldDepartment.preferredMaxLayoutWidth = CGRectGetWidth(cell.propFieldDepartment.frame);
            cell.propFieldJobTitle.preferredMaxLayoutWidth = CGRectGetWidth(cell.propFieldJobTitle.frame);
            cell.propFieldEmpCategory.preferredMaxLayoutWidth = CGRectGetWidth(cell.propFieldEmpCategory.frame);
            return cell;
        }
            break;
        case 4: { //Employment Status & Time Base
            CellRFADetailEmploymentStatus * cell = [tableView dequeueReusableCellWithIdentifier:@"cell_employmentstatus"];
            cell.propFIeldTimeBase.text = [_propRecruitment propTimeBaseTypeName];
            cell.propFieldEmploymentType.text = [_propRecruitment propEmploymentTypeName];
            cell.propFieldHrsPrWk.text = [NSString stringWithFormat:@"%.2f",[_propRecruitment propHoursPerWeek]];
            cell.propCboxIsPositionMaybecomePermanent.image = [UIImage imageNamed:([_propRecruitment propIsPositionMayBePermanent])?@"icon_checkbox_sel":@"icon_checkbox_normal"];
            return cell;
        }
            break;
        case 5: { //Approver's Note
            CellRFADetailApproversNote *cell = [tableView dequeueReusableCellWithIdentifier:@"cell_approversnote"];
            cell.propFieldApproversNote.text = [_propRecruitment propApproverNote];
            cell.bounds = CGRectMake(0, 0, CGRectGetWidth(tableView.bounds), 99999);
            cell.contentView.bounds = cell.bounds;
            [cell layoutIfNeeded];
            cell.propFieldApproversNote.preferredMaxLayoutWidth = CGRectGetWidth(cell.propFieldApproversNote.frame);
            return cell;
        }
            break;
        case 6: //Other Benefits
            return [tableView dequeueReusableCellWithIdentifier:@"cell_otherbenefits"];
            break;
        default:
            return nil;
            break;
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 1;
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 7;
}

-(NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    switch (section) {
        case 0:
            return @"HIRING SUPERVISOR";
            break;
        case 1:
            return @"REASON FOR VACANCY";
            break;
        case 2:
            return @"";
            break;
        case 3:
            return @"POSITION INFORMATION";
            break;
        case 4:
            return @"TIME BASE & EMPLOYMENT STATUS";
            break;
        case 5:
            return @"APPROVER'S NOTE";
            break;
        case 6:
            return @"BENEFITS";
            break;
        default:
            return nil;
            break;
    }
}


#pragma mark -
#pragma mark === UI Event Handler Management ===
#pragma mark -
- (IBAction)returnRecruitment:(id)sender {
    NSString *alertTitle = @"Reason for Returning";
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:alertTitle
                                                                             message:nil
                                                                      preferredStyle:UIAlertControllerStyleAlert];
    [alertController addTextFieldWithConfigurationHandler:^(UITextField *textField)
     {
         textField.placeholder = @"Required";
         [textField addTarget:self
                       action:@selector(alertTextFieldDidChange:)
             forControlEvents:UIControlEventEditingChanged];
     }];
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"Cancel"
                                                           style:UIAlertActionStyleCancel
                                                         handler:^(UIAlertAction *action) {
                                                             NSLog(@"Canceled");
                                                         }];
    UIAlertAction *returnAction = [UIAlertAction actionWithTitle:@"Return"
                                                           style:UIAlertActionStyleDefault
                                                         handler:^(UIAlertAction *action){
                                                             UITextField *returnNote = alertController.textFields.firstObject;
                                                             int statusId = [_propRecruitment propStatusID];
                                                             NSString *dateProcessedBy = @"NA";
                                                             
                                                             if (statusId == RECRUITMENT_STATUSID_SUBMITTED) {
                                                                 [_stringNote appendFormat:@"CM: %@", returnNote.text];
                                                                 dateProcessedBy = @"DateProcessedByCountryManager";
                                                             } else if (statusId == RECRUITMENT_STATUSID_APPROVEDBYCM && _propRecruitment.propRegionalHRManager == _staffId) {
                                                                 [_stringNote appendFormat:@"RHM: %@", returnNote.text];
                                                                 dateProcessedBy = @"DateProcessedByRegionalHR";
                                                             } else if ((statusId == RECRUITMENT_STATUSID_APPROVEDBYCM && _propRecruitment.propRegionalHRManager == 0) || statusId == RECRUITMENT_STATUSID_APPROVEDBYRHM) {
                                                                 [_stringNote insertString:@"\n" atIndex:0];
                                                                 [_stringNote appendFormat:@"RM: %@", returnNote.text];
                                                                 dateProcessedBy = @"DateProcessedByRegionalManager";
                                                             } else if (statusId == RECRUITMENT_STATUSID_APPROVEDBYMHR) {
                                                                 [_stringNote insertString:@"\n" atIndex:0];
                                                                 NSString *stringFormat = [NSString stringWithFormat:@"EVP: %@",returnNote.text];
                                                                 [_stringNote insertString:stringFormat atIndex:0];
                                                                 dateProcessedBy = @"DateProcessedByCEO";
                                                             }
                                                             [self changeApprovalStatus:RECRUITMENT_STATUSID_OPEN keyForUpdatableDate:dateProcessedBy approverNotes:_stringNote];
                                                         }];
    returnAction.enabled = NO;
    [alertController addAction:cancelAction];
    [alertController addAction:returnAction];
    [self presentViewController:alertController animated:YES completion:nil];
}

- (IBAction)rejectRecruitment:(id)sender {
    NSString *alertTitle = @"Reason for Rejection";
    
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:alertTitle
                                                                             message:nil
                                                                      preferredStyle:UIAlertControllerStyleAlert];
    [alertController addTextFieldWithConfigurationHandler:^(UITextField *textField)
     {
         textField.placeholder = @"Required";
         [textField addTarget:self
                       action:@selector(alertTextFieldDidChange:)
             forControlEvents:UIControlEventEditingChanged];
     }];
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"Cancel"
                                                           style:UIAlertActionStyleCancel
                                                         handler:^(UIAlertAction *action) {
                                                             NSLog(@"Canceled");
                                                         }];
    UIAlertAction *rejectAction = [UIAlertAction actionWithTitle:@"Reject"
                                                           style:UIAlertActionStyleDefault
                                                         handler:^(UIAlertAction *action){
                                                             UITextField *rejectNote = alertController.textFields.firstObject;
                                                             int statusId = [_propRecruitment propStatusID];
                                                             
                                                             if (statusId == RECRUITMENT_STATUSID_SUBMITTED) {
                                                                 [_stringNote appendFormat:@"CM: %@", rejectNote.text];
                                                                 [self changeApprovalStatus:RECRUITMENT_STATUSID_REJECTEDBYCM keyForUpdatableDate:@"DateProcessedByCountryManager" approverNotes:_stringNote];
                                                             } else if (statusId == RECRUITMENT_STATUSID_APPROVEDBYCM && _propRecruitment.propRegionalHRManager != 0) {
                                                                 [_stringNote insertString:@"\n" atIndex:0];
                                                                 [_stringNote appendFormat:@"RHM: %@", rejectNote.text];
                                                                 [self changeApprovalStatus:RECRUITMENT_STATUSID_REJECTEDBYRHM keyForUpdatableDate:@"DateProcessedByRegionalHR" approverNotes:_stringNote];
                                                             } else if ((statusId == RECRUITMENT_STATUSID_APPROVEDBYCM && _propRecruitment.propRegionalHRManager == 0) || statusId == RECRUITMENT_STATUSID_APPROVEDBYRHM) {
                                                                 [_stringNote insertString:@"\n" atIndex:0];
                                                                 [_stringNote appendFormat:@"RM: %@", rejectNote.text];
                                                                 [self changeApprovalStatus:RECRUITMENT_STATUSID_REJECTEDBYRM keyForUpdatableDate:@"DateProcessedByRegionalManager" approverNotes:_stringNote];
                                                             } else if (statusId == RECRUITMENT_STATUSID_APPROVEDBYMHR) {
                                                                 [_stringNote insertString:@"\n" atIndex:0];
                                                                 [_stringNote appendFormat:@"EVP: %@", rejectNote.text];
                                                                 [self changeApprovalStatus:RECRUITMENT_STATUSID_REJECTEDBYCEO keyForUpdatableDate:@"DateProcessedByCEO" approverNotes:_stringNote];
                                                             }
                                                         }];
    rejectAction.enabled = NO;
    [alertController addAction:cancelAction];
    [alertController addAction:rejectAction];
    
    [self presentViewController:alertController animated:YES completion:nil];
}

- (IBAction)approveRecruitment:(id)sender {
    NSString *alertTitle = @"Approver's Note";
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:alertTitle
                                                                             message:nil
                                                                      preferredStyle:UIAlertControllerStyleAlert];
    
    [alertController addTextFieldWithConfigurationHandler:^(UITextField *textField)
     {
         textField.placeholder = @"Note";
         [textField addTarget:nil
                       action:@selector(self)
             forControlEvents:UIControlEventEditingChanged];
     }];
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"Cancel"
                                                           style:UIAlertActionStyleCancel
                                                         handler:^(UIAlertAction *action) {
                                                             NSLog(@"Canceled");
                                                         }];
    UIAlertAction *approveAction = [UIAlertAction actionWithTitle:@"Proceed"
                                                            style:UIAlertActionStyleDefault
                                                          handler:^(UIAlertAction *action){
                                                              UITextField *approveNote = alertController.textFields[0];
                                                              int statusId = [_propRecruitment propStatusID];
                                                              
                                                              if (statusId == RECRUITMENT_STATUSID_SUBMITTED) {
                                                                  if (approveNote.text.length > 0)
                                                                      [_stringNote appendFormat:@"CM: %@",approveNote.text];
                                                                  else
                                                                      [_stringNote appendString:@"CM: Approved"];
                                                                  [self changeApprovalStatus:RECRUITMENT_STATUSID_APPROVEDBYCM keyForUpdatableDate:@"DateProcessedByCountryManager" approverNotes:_stringNote];
                                                              } else if (statusId == RECRUITMENT_STATUSID_APPROVEDBYCM && _propRecruitment.propRegionalHRManager != 0) {
                                                                  if (approveNote.text.length > 0) {
                                                                      [_stringNote insertString:@"\n" atIndex:0];
                                                                      [_stringNote insertString:[NSString stringWithFormat:@"RHM: %@",approveNote.text] atIndex:0];
                                                                  }else{
                                                                      [_stringNote insertString:@"\n" atIndex:0];
                                                                      [_stringNote insertString:@"RHM: Approved" atIndex:0];
                                                                  }
                                                                  [self changeApprovalStatus:RECRUITMENT_STATUSID_APPROVEDBYRHM keyForUpdatableDate:@"DateProcessedByRegionalHR" approverNotes:_stringNote];
                                                              } else if ((statusId == RECRUITMENT_STATUSID_APPROVEDBYCM && _propRecruitment.propRegionalHRManager == 0) || statusId == RECRUITMENT_STATUSID_APPROVEDBYRHM) {
                                                                  [_stringNote insertString:@"\n" atIndex:0];
                                                                  if (approveNote.text.length > 0)
                                                                      [_stringNote insertString:[NSString stringWithFormat:@"RM: %@",approveNote.text] atIndex:0];
                                                                  else
                                                                      [_stringNote insertString:@"RM: Approved" atIndex:0];
                                                                  [self changeApprovalStatus:RECRUITMENT_STATUSID_APPROVEDBYRM keyForUpdatableDate:@"DateProcessedByRegionalManager" approverNotes:_stringNote];
                                                              } else if (statusId == RECRUITMENT_STATUSID_APPROVEDBYMHR) {
                                                                  if (approveNote.text.length > 0) {
                                                                      [_stringNote insertString:@"\n" atIndex:0];
                                                                      [_stringNote insertString:[NSString stringWithFormat:@"CEO: %@",approveNote.text] atIndex:0];
                                                                  }else{
                                                                      [_stringNote insertString:@"\n" atIndex:0];
                                                                      [_stringNote insertString:@"CEO: Approved" atIndex:0];
                                                                  }
                                                                  NSLog(@"approvers note: %@",_stringNote);
                                                                  [self changeApprovalStatus:RECRUITMENT_STATUSID_APPROVEDBYCEO keyForUpdatableDate:@"DateProcessedByCEO" approverNotes:_stringNote];
                                                              }
                                                              
                                                          }];
    [alertController addAction:cancelAction];
    [alertController addAction:approveAction];
    
    [self presentViewController:alertController animated:YES completion:nil];
}

#pragma mark -
#pragma mark === Helper methods ===
#pragma mark -
- (void)alertTextFieldDidChange:(UITextField *)sender
{
    UIAlertController *alertController = (UIAlertController *)self.presentedViewController;
    if (alertController)
    {
        UITextField *noteField = alertController.textFields.firstObject;
        UIAlertAction *confirmAction = alertController.actions.lastObject;
        confirmAction.enabled = ([[noteField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]] length] == 0)?NO:YES;
    }
}

- (void)changeApprovalStatus:(int)statusID keyForUpdatableDate:(NSString *)keyForUpdatableDate approverNotes:(NSString *)approverNotes{
    [AppDelegate showGlogalHUDWithView:self.view];
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
        id result = [self.propAppDelegate.propGatewayOnline saveRecruitment:[_propRecruitment JSONForUpdatingRecruitmentWithStatusID:statusID keyForUpdatableDate:keyForUpdatableDate approverNotes:approverNotes appDelegate:self.propAppDelegate] oldRecruitmentJSON:[_propRecruitment jsonizeWithAppDelegate:self.propAppDelegate]];
        NSLog(@"result %@",result);
        dispatch_async(dispatch_get_main_queue(), ^{
            [AppDelegate hideGlogalHUDWithView:self.view];
            UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"" message:([result isEqualToString:@"OK"])?@"Updated Successfully":result preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"Dismiss" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action){
                [self.navigationController popViewControllerAnimated:YES];
            }];
            [alertController addAction:cancelAction];
            [self presentViewController:alertController animated:YES completion:nil];
        });
    });
}

//#pragma mark -
//#pragma mark === Alert View Delegate ===
//#pragma mark -x
//- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
//    //this delegate is only intended for listening button clicked after leave submission success message will be dismissed
//    [self.navigationController popViewControllerAnimated:YES];
//}

@end
