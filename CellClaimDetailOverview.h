//
//  CellClaimPaidByCCOverview.h
//  Salt
//
//  Created by Rick Royd Aban on 2015/07/27.
//  Copyright (c) 2015年 Applus Velosi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CellClaimDetailOverview : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *propClaimNumber;
@property (weak, nonatomic) IBOutlet UILabel *propFieldPaidByCC;
@property (weak, nonatomic) IBOutlet UILabel *propFieldClaimant;
@property (weak, nonatomic) IBOutlet UILabel *propFieldDateExpensed;
@property (weak, nonatomic) IBOutlet UILabel *propFieldStatus;
@end
