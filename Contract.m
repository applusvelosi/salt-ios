//
//  Contract.m
//  Salt
//
//  Created by Rick Royd Aban on 07/06/2017.
//  Copyright © 2017 Applus Velosi. All rights reserved.
//

#import "Contract.h"
#import "AppDelegate.h"
#import "Document.h"

@interface Contract () {
    NSMutableDictionary *_contract;
}

@end

@implementation Contract

-(instancetype)initWithJSONDictionary:(NSDictionary *)dictionary withOnlineGateway:(OnlineGateway *)onlineGateway {
    
    self = [super init];
    if (self) {
        _contract = [[NSMutableDictionary dictionaryWithDictionary:dictionary] mutableCopy];
        _propAdditionalMembership = [_contract objectForKey:@"AdditionalMembership"];
        _propAgentCommission = [_contract objectForKey:@"AgentCommission"];
        _propAgentName = [_contract objectForKey:@"AgentName"];
        _propApproverNote = [_contract objectForKey:@"ApproverNote"];
        _propBankPerformanceGuarantee = [_contract objectForKey:@"BankPerformanceGuarantee"];
        _propBillingCurrency = [_contract objectForKey:@"BillingCurrency"];
        _propBusinessOrigDocName = [_contract objectForKey:@"BusinessOrigDocName"];
        _propCmEmail = [_contract objectForKey:@"CMEmail"];
        _propCmName = [_contract objectForKey:@"CMName"];
        _propCertificatesValid = [_contract objectForKey:@"CertificatesValid"];
        _propCfoName = [_contract objectForKey:@"CfoName"];
        _propCommercialTermsOther = [_contract objectForKey:@"CommercialTermsOther"];
        _propCompany = [_contract objectForKey:@"Company"];
        _propCompanyCurrency = [_contract objectForKey:@"CompanyCurrency"];
        _propCompetingGrounds = [_contract objectForKey:@"CompetingGrounds"];
        _propCompetingAdvantage = [_contract objectForKey:@"CompetitiveAdvantage"];
        _propCompetitorOffers = [_contract objectForKey:@"CompetitorOffers"];
        _propCompetitors = [_contract objectForKey:@"Competitors"];
        _propConsequentialLoss = [_contract objectForKey:@"ConsequentialLoss"];
        _propContractingStrategy = [_contract objectForKey:@"ContractingStrategy"];
        _propDealBreakers = [_contract objectForKey:@"DealBreakers"];
        _propEndClient = [_contract objectForKey:@"EndClient"];
        _propEquipmentRequired = [_contract objectForKey:@"EquipmentRequired"];
        _propEvpEmail = [_contract objectForKey:@"EvpEmail"];
        _propEvpName = [_contract objectForKey:@"EvpName"];
        _propExistingOrNew = [_contract objectForKey:@"ExistingOrNew"];
        _propHsqe = [_contract objectForKey:@"Hsqe"];
        _propImportExport = [_contract objectForKey:@"ImportExport"];
        _propInsurance = [_contract objectForKey:@"Insurances"];
        _propIntellectualPropertyRights = [_contract objectForKey:@"IntellectualPropertyRights"];
        _propInvoicing = [_contract objectForKey:@"Invoicing"];
        _propJobStaff = [_contract objectForKey:@"JobStaff"];
        _propJustification = [_contract objectForKey:@"Justification"];
        _propLmEmail = [_contract objectForKey:@"LMEmail"];
        _propLmName = [_contract objectForKey:@"LMName"];
        _propLegalOther2 = [_contract objectForKey:@"LegalOthers2"];
        _propLimitationLiability = [_contract objectForKey:@"LimitationLiability"];
        _propLiquidatedDamage = [_contract objectForKey:@"LimitationLiability"];
        _propMainOrigDocName = [_contract objectForKey:@"MainOrigDocName"];
        _propNegotiations = [_contract objectForKey:@"Negotiations"];
        _propNotes = [_contract objectForKey:@"Notes"];
        _propOperational = [_contract objectForKey:@"Operational"];
        _propOtherLocalTaxes = [_contract objectForKey:@"OtherLocalTaxes"];
        _propPaymentTerms = [_contract objectForKey:@"PaymentTerms"];
        _propPermanentEstablishment = [_contract objectForKey:@"PermanentEstablishment"];
        _propPricing = [_contract objectForKey:@"Pricing"];
        _propPricingAdditional = [_contract objectForKey:@"PricingAdditional"];
        _propPrimaryResponsibility = [_contract objectForKey:@"PrimaryResponsibility"];
        _propProjectExecutionLocation = [_contract objectForKey:@"ProjectExecutionLocation"];
        _propProjectManagerCriteria = [_contract objectForKey:@"ProjectManagerCriteria"];
        _propTeamBuildup = [_contract objectForKey:@"ProjectTeamBuildUp"];
        _propRmEmail = [_contract objectForKey:@"RMEmail"];
        _propRmName = [_contract objectForKey:@"RMName"];
        _propReferenceNumber = [_contract objectForKey:@"ReferenceNumber"];
        _propRequestStatusName = [_contract objectForKey:@"RequestStatusName"];
        _propRequestor = [_contract objectForKey:@"Requestor"];
        _propRfmId = [[_contract objectForKey:@"RfmId"] intValue];
        _propRfmEmail = [_contract objectForKey:@"RfmEmail"];
        _propRfmName = [_contract objectForKey:@"RfmName"];
        _propRiskAssessmentOthers = [_contract objectForKey:@"RiskAssessmentOthers"];
        _propSpecificTraining = [_contract objectForKey:@"SpecificTraining"];
        _propStaffAware = [_contract objectForKey:@"StaffAware"];
        _propStatusName = [_contract objectForKey:@"StatusName"];
        _propTechnicalAnalysisOthers = [_contract objectForKey:@"TechnicalAnalysisOthers"];
        _propVisa = [_contract objectForKey:@"Visa"];
        _propAgentApproved = [[_contract objectForKey:@"AgentApproved"] intValue];
        _propCmId = [[_contract objectForKey:@"CMId"] intValue];
        _propCfoId = [[_contract objectForKey:@"CfoID"] intValue];
        _propCompetentPersonnel = [[_contract objectForKey:@"CompetentPersonnel"] intValue];
        _propContractId = [[_contract objectForKey:@"ContractId"] intValue];
        _propContractType = [[_contract objectForKey:@"ContractType"] intValue];
        _propCreditRisk = [[_contract objectForKey:@"CreditRisk"] intValue];
        _propEquipment = [[_contract objectForKey:@"Equipment"] intValue];
        _propEquipmentAvailable = [[_contract objectForKey:@"EquipmentAvailable"] intValue];
        _propEquipmentCost = [[_contract objectForKey:@"EquipmentCost"] intValue];
        _propEvpId = [[_contract objectForKey:@"EvpID"] intValue];
        _propInfrastructure = [[_contract objectForKey:@"Infrastructure"] intValue];
        _propLmId = [[_contract objectForKey:@"LMId"] intValue];
        _propLegalOthers = [[_contract objectForKey:@"LegalOthers"] intValue];
        _propOfficeId = [[_contract objectForKey:@"OfficeId"] intValue];
        _propPaymentBehavior = [[_contract objectForKey:@"PaymentBehavior"] intValue];
        _propPersonnel = [[_contract objectForKey:@"Personnel"] intValue];
        _propProbabilityOfAward = [[_contract objectForKey:@"ProbabilityOfAward"] intValue];
        _propProjectDuration = [[_contract objectForKey:@"ProjectDuration"] intValue];
        _propProjectDurationRange = [[_contract objectForKey:@"ProjectDurationRange"] intValue];
        _propRmId = [[_contract objectForKey:@"RMId"] intValue];
        _propRequestStatus = [[_contract objectForKey:@"RequestStatus"] intValue];
        _propServiceLines = [[_contract objectForKey:@"ServiceLines"] intValue];
        _propServices = [[_contract objectForKey:@"Services"] intValue];
        _propStaffId = [[_contract objectForKey:@"StaffId"] intValue];
        _propStatus = [[_contract objectForKey:@"Status"] intValue];
        _propIsAgentRequired = [[_contract objectForKey:@"AgentRequired"] boolValue];
        _propCapexRequired = [[_contract objectForKey:@"CapexRequired"] intValue];
        _propCapexRequiredEquiv = [[_contract objectForKey:@"CapexRequiredEquiv"] intValue];
        _propGrossMargin = [[_contract objectForKey:@"GrossMargin"] floatValue];
        _propProjectValue = [[_contract objectForKey:@"ProjectValue"] intValue];
        _propProjectValueEquiv = [[_contract objectForKey:@"ProjectValueEquiv"] intValue];
        _propRateToEuro = [[_contract objectForKey:@"RateToEUR"] floatValue];
        [_contract setObject:[onlineGateway deserializeJsonDateString:[_contract objectForKey:@"DateProcessedByCFO"]] forKey:@"DateProcessedByCFO"];
        [_contract setObject:[onlineGateway deserializeJsonDateString:[_contract objectForKey:@"DateProcessedByCM"]] forKey:@"DateProcessedByCM"];
        [_contract setObject:[onlineGateway deserializeJsonDateString:[_contract objectForKey:@"DateProcessedByEVP"]] forKey:@"DateProcessedByEVP"];
        [_contract setObject:[onlineGateway deserializeJsonDateString:[_contract objectForKey:@"DateProcessedByLM"]] forKey:@"DateProcessedByLM"];
        [_contract setObject:[onlineGateway deserializeJsonDateString:[_contract objectForKey:@"DateProcessedByRM"]] forKey:@"DateProcessedByRM"];
        [_contract setObject:[onlineGateway deserializeJsonDateString:[_contract objectForKey:@"RequestDate"]] forKey:@"RequestDate"];
        [_contract setObject:[onlineGateway deserializeJsonDateString:[_contract objectForKey:@"StartDate"]] forKey:@"StartDate"];
        [_contract setObject:[onlineGateway deserializeJsonDateString:[_contract objectForKey:@"TenderSubmission"]] forKey:@"TenderSubmission"];
    }
    return self;
}

-(instancetype)initWithJSONDictionary:(NSMutableDictionary *)contractDict {
    if (self = [super init])
        _contract = [NSMutableDictionary dictionaryWithDictionary:contractDict];
    return self;
}

#pragma mark -
#pragma mark === Helper methods ===
#pragma mark -
-(NSString *)formatDateProcessed:(NSString *)dateFromStr appDelegate:(AppDelegate *)app{
    NSInteger janOneTwoThousand = 946684800000/1000; //Jan 1, 2000 GMT in epoch time
    NSDate *jan1 = [NSDate dateWithTimeIntervalSince1970:janOneTwoThousand];
    NSDate *dateFromString = [app.propDateFormatByProcessed dateFromString:dateFromStr];
    if ([dateFromString compare:jan1] == NSOrderedDescending || [dateFromString compare:jan1] == NSOrderedSame) //Date Before Jan 1, 2000 must not be viewed
        return [app.propDateFormatByProcessed stringFromDate:dateFromString];
    return @"";
}

- (NSString *)getDateStart:(AppDelegate *)appDelegate {
    NSDate *stringDate = [appDelegate.propDateFormatByProcessed dateFromString:[_contract objectForKey:@"StartDate"]];
    return [appDelegate.propDateFormatLeaveDuration stringFromDate:stringDate];
}

- (NSString *)getDateTenderSubmission:(AppDelegate *)appDelegate {
    NSDate *stringDate = [appDelegate.propDateFormatByProcessed dateFromString:[_contract objectForKey:@"TenderSubmission"]];
    return [appDelegate.propDateFormatLeaveDuration stringFromDate:stringDate];
}

- (NSString *)processedDateSubmitted:(AppDelegate *)appdelegate {
    return [self formatDateProcessed:[_contract objectForKey:@"RequestDate"] appDelegate:appdelegate];
}

- (NSString *)processedDateByCM:(AppDelegate *)appdelegate {
    return [self formatDateProcessed:[_contract objectForKey:@"DateProcessedByCM"] appDelegate:appdelegate];
}

- (NSString *)processedDateByRM:(AppDelegate *)appdelegate {
    return [self formatDateProcessed:[_contract objectForKey:@"DateProcessedByRM"] appDelegate:appdelegate];
}

- (NSString *)processedDateByLM:(AppDelegate *)appdelegate {
    return [self formatDateProcessed:[_contract objectForKey:@"DateProcessedByLM"] appDelegate:appdelegate];
}

- (NSString *)processedDateByCFO:(AppDelegate *)appdelegate {
    return [self formatDateProcessed:[_contract objectForKey:@"DateProcessedByCFO"] appDelegate:appdelegate];
}

- (NSString *)processedDateByEVP:(AppDelegate *)appdelegate {
    return [self formatDateProcessed:[_contract objectForKey:@"DateProcessedByEVP"] appDelegate:appdelegate];
}

- (NSString *) updateJSONGiftWithStatusId:(ContractStatusId) statusId keyForUpdatableDate:(NSString *)keyUpdatableDate withApproverNote:(NSString *)note appDelegate:(AppDelegate *)appdelegate {
    
    NSMutableDictionary *tempDic = [NSMutableDictionary dictionaryWithDictionary:_contract];
    [tempDic setObject:[appdelegate.propGatewayOnline convertToUnixDate:[appdelegate.propDateFormatByProcessed dateFromString:[tempDic objectForKey:@"StartDate"]]] forKey:@"StartDate"];
    [tempDic setObject:[appdelegate.propGatewayOnline convertToUnixDate:[appdelegate.propDateFormatByProcessed dateFromString:[tempDic objectForKey:@"RequestDate"]]] forKey:@"RequestDate"];
    [tempDic setObject:[appdelegate.propGatewayOnline convertToUnixDate:[appdelegate.propDateFormatByProcessed dateFromString:[tempDic objectForKey:@"TenderSubmission"]]] forKey:@"TenderSubmission"];
    [tempDic setObject:[appdelegate.propGatewayOnline convertToUnixDate:[appdelegate.propDateFormatByProcessed dateFromString:[tempDic objectForKey:@"DateProcessedByCM"]]] forKey:@"DateProcessedByCM"];
    [tempDic setObject:[appdelegate.propGatewayOnline convertToUnixDate:[appdelegate.propDateFormatByProcessed dateFromString:[tempDic objectForKey:@"DateProcessedByRM"]]] forKey:@"DateProcessedByRM"];
    [tempDic setObject:[appdelegate.propGatewayOnline convertToUnixDate:[appdelegate.propDateFormatByProcessed dateFromString:[tempDic objectForKey:@"DateProcessedByLM"]]] forKey:@"DateProcessedByLM"];
    [tempDic setObject:[appdelegate.propGatewayOnline convertToUnixDate:[appdelegate.propDateFormatByProcessed dateFromString:[tempDic objectForKey:@"DateProcessedByCFO"]]] forKey:@"DateProcessedByCFO"];
    [tempDic setObject:[appdelegate.propGatewayOnline convertToUnixDate:[appdelegate.propDateFormatByProcessed dateFromString:[tempDic objectForKey:@"DateProcessedByEVP"]]] forKey:@"DateProcessedByEVP"];
    [tempDic setObject:@(statusId) forKey:@"RequestStatus"];
    [tempDic setObject:note forKey:@"ApproverNote"];
    switch (statusId) {
        case ContractStatusIdApprovedByCm:
            [tempDic setObject:@"Approved by CM" forKey:@"RequestStatusName"];
            break;
        case ContractStatusIdApprovedByRm:
            [tempDic setObject:@"Approved by CM" forKey:@"RequestStatusName"];
            break;
        case ContractStatusIdApprovedByLm:
            [tempDic setObject:@"Approved by CM" forKey:@"RequestStatusName"];
            break;
        case ContractStatusIdApprovedByCfo:
            [tempDic setObject:@"Approved by CM" forKey:@"RequestStatusName"];
            break;
        case ContractStatusIdApprovedByEvp:
            [tempDic setObject:@"Approved by CM" forKey:@"RequestStatusName"];
            break;
        case ContractStatusIdRejectedByCm:
            [tempDic setObject:@"Approved by CM" forKey:@"RequestStatusName"];
            break;
        case ContractStatusIdRejectedByRm:
            [tempDic setObject:@"Approved by CM" forKey:@"RequestStatusName"];
            break;
        case ContractStatusIdRejectedByLm:
            [tempDic setObject:@"Approved by CM" forKey:@"RequestStatusName"];
            break;
        case ContractStatusIdRejectedByCfo:
            [tempDic setObject:@"Approved by CM" forKey:@"RequestStatusName"];
            break;
        case ContractStatusIdRejectedByEvp:
            [tempDic setObject:@"Approved by CM" forKey:@"RequestStatusName"];
            break;
        default:
            break;
    }
    if (![keyUpdatableDate isEqualToString:@"NA"]) {
        [tempDic setObject:[appdelegate.propGatewayOnline epochizeDate:[NSDate date]] forKey:keyUpdatableDate];
    }
    return [[NSString alloc] initWithData:[NSJSONSerialization dataWithJSONObject:tempDic options:0 error:nil] encoding:NSUTF8StringEncoding];
}

-(NSMutableArray *)propBusinessCaseDoc {
    NSMutableArray *businessDocArray = [NSMutableArray array];
    NSArray *jsonAttachments = [_contract objectForKey:@"BusinessCaseDocument"];
    for (NSDictionary *jsonDoc in jsonAttachments)
        [businessDocArray addObject:[[Document alloc] initWithDictionary:jsonDoc]];
    return businessDocArray;
}

- (NSString *)getContractType:(int) key {
    NSDictionary *contractTypeHash = @{@1:@"Backlog Fixed Lumpsum (Contracted-committed)",
                                       @2:@"Backlog Project Contract - Single (Contracted-Committed)",
                                       @3:@"Orderbook MSA (Contracted not-committed)",
                                       @4:@"Others"};
    return [contractTypeHash objectForKey:[NSNumber numberWithInt:key]];
}

- (NSString *)getServiceLine:(int) key {
    NSDictionary *serviceLineHash = @{@1 : @"Asset Integrity & HSE",
                                      @2 : @"Certification",
                                      @3 : @"Vendor Surveillance",
                                      @4 : @"Technical Staffing",
                                      @5 : @"Training",
                                      @6 : @"Premium Services",
                                      @7 : @"NDT & Corrosion Monitoring",
                                      @8 : @"Site Inspection",
                                      @9 : @"Power",
                                      @10 : @"Contruction",
                                      @11 : @"Others"};
    return [serviceLineHash objectForKey:[NSNumber numberWithInt:key]];
}

- (NSString *)getStatus:(int) key {
    NSDictionary *statusHash = @{@1 : @"Awaiting award",
                                 @2 : @"Awarded",
                                 @3 : @"Tender preparation"};
    return [statusHash objectForKey:[NSNumber numberWithInt:key]];
}

- (NSString *)getDurationRange:(int) key {
    NSDictionary *durationRangeHash = @{@1 : @"Year/s",
                                        @2 : @"Month/s",
                                        @3 : @"Week/s",
                                        @4 : @"Day/s"};
    return [durationRangeHash objectForKey:[NSNumber numberWithInt:key]];
}

- (NSString *)getCreditRisk:(int) key {
    NSDictionary *creditRishHash = @{@1 : @"High",
                                     @2 : @"Low",
                                     @3 : @"Medium",
                                     @4 : @"Don't know"};
    return [creditRishHash objectForKey:[NSNumber numberWithInt:key]];
}

- (NSString *)getPaymentBehavior:(int) key {
    NSDictionary *paymentBehaviorHash = @{@1 : @"Good",
                                          @2 : @"Bad",
                                          @3 : @"Don't know"};
    return [paymentBehaviorHash objectForKey:[NSNumber numberWithInt:key]];
}

@end









