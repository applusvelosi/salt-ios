//
//  VCGiftsHospitalityForApproval.h
//  Salt
//
//  Created by Rick Royd Aban on 27/04/2017.
//  Copyright © 2017 Applus Velosi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "VCGiftsHospitalityForApprovalList.h"

@interface VCGiftsHospitalityForApproval : UITableViewController

@property(nonatomic,weak) VCGiftsHospitalityForApprovalList *parentVC;

@end
