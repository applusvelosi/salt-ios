//
//  Claim.h
//  Salt
//
//  Created by Rick Royd Aban on 2015/07/29.
//  Copyright (c) 2015年 Applus Velosi. All rights reserved.
//
#import "ClaimHeader.h"

@interface Claim : ClaimHeader

- (instancetype)initClaimWithJSONDict:(NSDictionary *)dictionary withAppDelegate:(AppDelegate *)appDelegate;
- (BOOL)isPaidByCompanyCard;
- (void)updateIsClaimPaidByCC:(BOOL)isPaidByCC;

@end
