//
//  VCClaimHeaderLiquidation.h
//  Salt
//
//  Created by Rick Royd Aban on 2015/08/12.
//  Copyright (c) 2015年 Applus Velosi. All rights reserved.
//

#import "VCClaimHeader.h"
#import "Liquidation.h"
#import "CostCenter.h"
#import "BusinessAdvance.h"

@interface VCClaimHeaderLiquidation : VCClaimHeader

@property (strong, nonatomic) Liquidation *propLiquidation;
@property (strong, nonatomic) NSMutableArray *propRefBAs;

- (void)updateRefBA:(BusinessAdvance *)ba;

@end
