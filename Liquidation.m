//
//  Liquidation.m
//  Salt
//
//  Created by Rick Royd Aban on 2015/07/29.
//  Copyright (c) 2015年 Applus Velosi. All rights reserved.
//

#import "Liquidation.h"

@implementation Liquidation

- (ClaimHeader *)initWithCostCenterID:(int)costCenterID costCenterName:(NSString *)costCenterName claimTypeID:(int)claimTypeID isPaidByCC:(BOOL)isPaidByCC baIDCharged:(int)baIDCharged bacNumber:(NSString *)bacNumber appDelegate:(AppDelegate *)appDelegate{
    return [super initWithCostCenterID:costCenterID costCenterName:costCenterName claimTypeID:CLAIMTYPEID_LIQUIDATION isPaidByCC:NO baIDCharged:baIDCharged bacNumber:bacNumber appDelegate:appDelegate];
}

- (int)propBusinessAdvanceID{
    return [[[super claimHeader] objectForKey:@"BusinessAdvanceIDCharged"] intValue];;
}

- (NSString *)propBACNumber{
    return [[super claimHeader] objectForKey:@"BACNumber"];
}

- (float)propForDeductionAmount{
    return [[[super claimHeader] objectForKey:@"TotalComputedForDeductionInLC"] floatValue];
}

- (float)propForPaymentAmount{
    return [[[super claimHeader] objectForKey:@"TotalComputedForPaymentInLC"] floatValue];
}

@end
