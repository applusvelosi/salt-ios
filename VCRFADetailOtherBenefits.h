//
//  VCRFADetailOtherBenefits.h
//  Salt
//
//  Created by Rick Royd Aban on 10/5/15.
//  Copyright © 2015 Applus Velosi. All rights reserved.
//

#import "VCPage.h"
#import "Recruitment.h"

@interface VCRFADetailOtherBenefits : VCPage<UITableViewDelegate, UITableViewDataSource>

@property (strong, nonatomic) Recruitment* propRecruitment;

@end
