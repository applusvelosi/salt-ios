//
//  CellClaimItemDetailMileage.h
//  Salt
//
//  Created by Rick Royd Aban on 2/10/16.
//  Copyright © 2016 Applus Velosi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CellClaimItemDetailMileage : UITableViewCell

@property (strong, nonatomic) IBOutlet UILabel *propLabelMileageFrom;
@property (strong, nonatomic) IBOutlet UILabel *propLabelMileageTo;
@property (strong, nonatomic) IBOutlet UILabel *propLabelMileageMileage;
@property (strong, nonatomic) IBOutlet UILabel *propLabelMileageRate;
@property (strong, nonatomic) IBOutlet UIImageView *propCboxReturn;

@end
