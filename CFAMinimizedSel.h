//
//  CFAMinimizedSel.h
//  Salt
//
//  Created by Rick Royd Aban on 2015/09/07.
//  Copyright (c) 2015年 Applus Velosi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CFAMinimizedSel : UITableViewCell

@property (strong, nonatomic) IBOutlet UIImageView *propCbox;
@property (strong, nonatomic) IBOutlet UILabel *propFieldCategory;
@property (strong, nonatomic) IBOutlet UILabel *propFieldLocalCurrency;
@property (strong, nonatomic) IBOutlet UILabel *propFieldStatus;

@end
