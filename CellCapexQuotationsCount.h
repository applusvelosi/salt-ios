//
//  CellCapexQuotationsCount.h
//  Salt
//
//  Created by Rick Royd Aban on 15/09/2016.
//  Copyright © 2016 Applus Velosi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CellCapexQuotationsCount : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *propQuotations;
@end
