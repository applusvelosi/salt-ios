//
//  VCGiftsHospitalityForApproval.m
//  Salt
//
//  Created by Rick Royd Aban on 27/04/2017.
//  Copyright © 2017 Applus Velosi. All rights reserved.
//

#import "VCGiftsHospitalityForApproval.h"
#import "AppDelegate.h"
#import "CellGiftsForApproval.h"
#import "GiftHospitality.h"
#import "VelosiColors.h"
#import "LoaderDelegate.h"
#import "VCGiftForApprovalDetail.h"

@interface VCGiftsHospitalityForApproval () <UISearchResultsUpdating, LoaderDelegate, UIScrollViewDelegate, UISearchBarDelegate>{
    UISearchController *_searchController;
    NSMutableArray *_gifts;
    NSString *_filterString;
    BOOL _searchWasCancelled;
}

@property (nonatomic, strong) AppDelegate *appDelegate;

@end

@implementation VCGiftsHospitalityForApproval
#pragma mark -
#pragma mark === View Life Cycle Management ===
#pragma mark -
- (void)viewDidLoad {
    [super viewDidLoad];
    _gifts = [NSMutableArray array];
    _appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    _filterString = @"";
    self.parentVC.delegate = self;
    _searchWasCancelled = NO;
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(didChangePrefeeredContentSize:)
                                                 name:UIContentSizeCategoryDidChangeNotification object:nil];
    
    // No search results controller to display the search results in the current view
    _searchController = [[UISearchController alloc] initWithSearchResultsController:nil];
    _searchController.searchResultsUpdater = self;
    _searchController.dimsBackgroundDuringPresentation = NO;
    _searchController.hidesNavigationBarDuringPresentation = NO;
    
    //    self.searchController.searchBar.delegate = self;
    _searchController.searchBar.tintColor = [UIColor blueColor];
    _searchController.searchBar.placeholder = @"Requestor name or office";
    
    self.tableView.tableHeaderView = _searchController.searchBar;
    self.definesPresentationContext = YES;
    [_searchController.searchBar sizeToFit];
    _searchController.searchBar.delegate = self;
    
    self.tableView.rowHeight = UITableViewAutomaticDimension;
    self.tableView.estimatedRowHeight = 140;
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    NSIndexPath *indexPath = [self.tableView indexPathForCell:sender];
    GiftHospitality *gift = (indexPath)?[_gifts objectAtIndex:indexPath.row]:nil;
    if ([segue.identifier isEqualToString:@"embedVCGifts"]) {
        VCGiftForApprovalDetail *destVC = segue.destinationViewController;
        destVC.propAppDelegate = _appDelegate;
        destVC.propGiftHeaderId = gift.propGiftHospitalityId;
    }
}

#pragma mark -
#pragma mark - === scroll view delegate ===
#pragma mark -
-(void)scrollViewWillBeginDragging:(UIScrollView *)scrollView {
    [_searchController.searchBar setShowsCancelButton:NO animated:YES];
    [_searchController.searchBar resignFirstResponder];
}

#pragma mark -
#pragma mark - search results delegate
#pragma mark -
- (void)updateSearchResultsForSearchController:(UISearchController *)searchController {
    if (searchController.active)
        [searchController.searchBar setShowsCancelButton:YES animated:YES];
}

#pragma mark -
#pragma mark === Search bar delegate ===
#pragma mark -
- (void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar {
    _searchWasCancelled = NO;
}

-(void)searchBarCancelButtonClicked:(UISearchBar *)searchBar {\
    _searchWasCancelled = YES;
}

-(void)searchBarTextDidEndEditing:(UISearchBar *)searchBar {
    if (_searchWasCancelled)
        searchBar.text = _filterString;
    else
        _filterString = searchBar.text;
}

-(void)searchBarSearchButtonClicked:(UISearchBar *)searchBar {
    [searchBar setShowsCancelButton:NO animated:YES];
}

-(void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText {
    _filterString = searchText;
    [self reloadGiftsForApprovalWithOfficeOrRequestor:_filterString];
}

#pragma mark -
#pragma mark === Helper methods ===
#pragma mark -
-(void)didChangePrefeeredContentSize:(NSNotification*)notification{
    [self.tableView reloadData];
}

- (void)reloadGiftsForApprovalWithOfficeOrRequestor:(NSString *)searchString {
    [_gifts removeAllObjects];
    if (searchString.length > 0) {
        for (GiftHospitality *giftForApproval in [_appDelegate giftsForApproval]) {
            if ( [giftForApproval.propStaffName localizedCaseInsensitiveContainsString:searchString] || [giftForApproval.propOfficeName localizedCaseInsensitiveContainsString:searchString] || searchString.length < 1 )
                [_gifts addObject:giftForApproval];
        }
    }else {
        [_gifts addObjectsFromArray:[_appDelegate giftsForApproval]];
    }
    [self.tableView reloadData];
}

#pragma mark -
#pragma mark === Loader delegate ===
#pragma mark -
-(void)loadFinished {
    [self reloadGiftsForApprovalWithOfficeOrRequestor:_filterString];
}

-(void)loadFailedWithError:(NSString *)error {
    [[[UIAlertView alloc] initWithTitle:@"" message:error delegate:nil cancelButtonTitle:@"Dismiss" otherButtonTitles:nil, nil] show];
}

#pragma mark -
#pragma mark - Table view data source delegate
#pragma mark -
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _gifts.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellId = @"giftcell";
    CellGiftsForApproval *cell = [tableView dequeueReusableCellWithIdentifier:cellId];
    if (cell == nil)
        cell = [[CellGiftsForApproval alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellId];
    
    GiftHospitality *gift = [_gifts objectAtIndex:indexPath.row];
    if(gift.propStatusId == GiftStatusIdApprovedByCm){
        cell.propFieldStatus.textColor = [VelosiColors greenAcceptance];
        cell.propFieldStatus.text = @"Approved by CM";
    }else if(gift.propStatusId == GiftStatusIdRejectedByCm){
        cell.propFieldStatus.textColor = [VelosiColors redRejection];
        cell.propFieldStatus.text = @"Rejected by CM";
    }else if(gift.propStatusId == GiftStatusIdApprovedByRm){
        cell.propFieldStatus.textColor = [VelosiColors greenAcceptance];
        cell.propFieldStatus.text = @"Approved by RM";
    }else if(gift.propStatusId == GiftStatusIdRejectedByRm){
        cell.propFieldStatus.textColor = [VelosiColors redRejection];
        cell.propFieldStatus.text = @"Rejected by RM";
    }else if(gift.propStatusId == GiftStatusIdApprovedByCeo){
        cell.propFieldStatus.textColor = [VelosiColors greenAcceptance];
        cell.propFieldStatus.text = @"Approved by EVP";
    }else if(gift.propStatusId == GiftStatusIdRejectedByCeo){
        cell.propFieldStatus.textColor = [VelosiColors redRejection];
        cell.propFieldStatus.text = @"Rejected by EVP";
    }else if(gift.propStatusId == GiftStatusIdApprovedByApplus){
        cell.propFieldStatus.textColor = [VelosiColors greenAcceptance];
        cell.propFieldStatus.text = @"Approved by Applus CSR";
    }else if(gift.propStatusId == GiftStatusIdRejectedByApplus){
        cell.propFieldStatus.textColor = [VelosiColors redRejection];
        cell.propFieldStatus.text = @"Rejected by Applus CSR";
    }else {
        cell.propFieldStatus.textColor = [UIColor lightGrayColor];
        cell.propFieldStatus.text = gift.propStatusName;
    }
    
    cell.propFieldOffice.text = gift.propOfficeName;
    cell.propFieldLocalCurrency.text = [NSString stringWithFormat:@"%@ %@",gift.localCurrencySymbol, [_appDelegate.decimalFormat stringFromNumber:[NSNumber numberWithFloat:gift.propAmountInLc]]];
    cell.propFieldRequestorName.text = gift.propStaffName;
    
    cell.bounds = CGRectMake(0, 0, CGRectGetWidth(tableView.bounds), 99999);
    cell.contentView.bounds = cell.bounds;
    [cell layoutIfNeeded];
    
    cell.propFieldOffice.preferredMaxLayoutWidth = CGRectGetWidth(cell.propFieldOffice.frame);
    cell.propFieldRequestorName.preferredMaxLayoutWidth = CGRectGetWidth(cell.propFieldRequestorName.frame);
    
    return cell;
}

@end
