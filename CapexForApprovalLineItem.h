//
//  CapexForApprovalLineItem.h
//  Salt
//
//  Created by Rick Royd Aban on 10/6/15.
//  Copyright © 2015 Applus Velosi. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "OfflineGatewaySavable.h"
#import "OnlineGateway.h"

@interface CapexForApprovalLineItem : NSObject<OfflineGatewaySavable>

- (CapexForApprovalLineItem *)initWithDictionary:(NSDictionary *)jsonCapexForApprovalLineItem onlineGateway:(OnlineGateway *)onlineGateway;

- (float)propAmount;
- (float)propAmountInUSD;
- (int)propBaseCurrencyID;
- (NSString *)propBaseCurrencyName;
- (NSString *)propBaseCurrencySymbol;
- (float)propBaseExchangeRate;
- (int)propCapexID;
- (int)propCapexLineItemID;
- (NSString *)propCapexLineItemNumber;
- (NSString *)propCapexNumber;
- (int)propCategoryID;
- (NSString *)propCategoryName;
- (int)propCurrencyID;
- (NSString *)propCurrencyName;
- (NSString *)propCurrencySymbol;
- (NSString *)propDateCreated;
- (NSString *)propDescription;
- (int)propQuantity;
- (int)propRequesterID;
- (NSString *)propRequesterName;
- (float)propUnitCost;
- (float)propUSDExchangeRate;
- (float)propUsefulLife;

@end
