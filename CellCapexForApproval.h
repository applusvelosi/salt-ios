//
//  CellCapexForApproval.h
//  Salt
//
//  Created by Rick Royd Aban on 10/5/15.
//  Copyright © 2015 Applus Velosi. All rights reserved.
//

#import "VCPage.h"

@interface CellCapexForApproval : UITableViewCell

@property (strong, nonatomic) IBOutlet UILabel *propFieldOffice;
@property (strong, nonatomic) IBOutlet UILabel *propFieldCostCenter;
@property (strong, nonatomic) IBOutlet UILabel *propFieldStatus;
@property (nonatomic, strong) IBOutlet UILabel *propFieldName;
@end
