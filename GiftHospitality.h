//
//  GiftHospitality.h
//  Salt
//
//  Created by Rick Royd Aban on 26/04/2017.
//  Copyright © 2017 Applus Velosi. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "OnlineGateway.h"

typedef NS_ENUM(int, GiftStatusId) {
    GiftStatusIdOpen = 60,
    GiftStatusIdSubmitted,
    GiftStatusIdCancelled,
    GiftStatusIdApprovedByCm,
    GiftStatusIdRejectedByCm,
    GiftStatusIdApprovedByRm,
    GiftStatusIdRejectedByRm,
    GiftStatusIdApprovedByCeo,
    GiftStatusIdRejectedByCeo,
    GiftStatusIdApprovedByApplus,
    GiftStatusIdRejectedByApplus
};

@interface GiftHospitality : NSObject

@property (nonatomic, assign) float propAmountInEuro, propAmountInLc, propEuroRate;
@property (nonatomic, assign) int propStatusId, propGiftHospitalityId, propRequestorOfficeId;
@property (nonatomic, strong) NSString *propStaffName, *propOfficeName, *localCurrencySymbol, *propStatusName, *propReferenceNum;
@property (nonatomic, strong) NSString *propCountryManagerName, *propRegionalManagerName, *propCeoName, *propCsrName;
@property (nonatomic, strong) NSString *propCurrencyThree;
@property (nonatomic, strong) NSString *propGiftType, *propCountry, *propCompanyName, *propCity;
@property (nonatomic, assign) BOOL propIsReceivedGiven, propIsDonation, propIsPublicOfficial;
@property (nonatomic, strong) NSString *propDescription, *propReason, *propApproverNote;
@property (nonatomic, assign) int propProcessedByCmId, propProcessedByRmId, propProcessedByCeoId, propApplusCsrId;

- (instancetype)initWithJSONDictionary:(NSDictionary *)dictionary withOnlineGateway:(OnlineGateway *)onlineGateway;
- (instancetype)initWithDictionary:(NSMutableDictionary *)giftHeaderDictionary;
- (NSString *)processedDateSubmitted:(AppDelegate *)appdelegate;
- (NSString *)processedDateByCM:(AppDelegate *)appdelegate;
- (NSString *)processedDateByRM:(AppDelegate *)appdelegate;
- (NSString *)processedDateByCEO:(AppDelegate *)appdelegate;
- (NSString *)processedDateByCSR:(AppDelegate *)appdelegate;
- (NSString *)processedDateIsReceived:(AppDelegate *)appdelegate;
- (NSString *)jSONForUpdatingGiftWithStatusId:(GiftStatusId)statusId keyForUpdatableDate:(NSString *)keyForUpdatableDate withApproverNote:(NSString *)note appDelegate:(AppDelegate *)appDelegate;
- (NSString *)jsonizeWithappDelegate:(AppDelegate *)appDelegate;
@end
