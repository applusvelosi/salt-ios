//
//  CellForBAOutstandingHeader.m
//  Salt
//
//  Created by Rick Royd Aban on 24/03/2017.
//  Copyright © 2017 Applus Velosi. All rights reserved.
//

#import "CellForBAOutstandingHeader.h"

@implementation CellForBAOutstandingHeader

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
