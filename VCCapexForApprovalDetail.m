//
//  CapexForApprovalDetail.m
//  Salt
//
//  Created by Rick Royd Aban on 10/6/15.
//  Copyright © 2015 Applus Velosi. All rights reserved.
//

#import "VCCapexForApprovalDetail.h"
#import "AppDelegate.h"
#import "VelosiColors.h"
#import "VCCapexForApprovalLineItems.h"
#import "Document.h"

@interface VCCapexForApprovalDetail() {
    int _staffId;
}
    
@property (weak, nonatomic) IBOutlet UILabel *_propFieldCapexNumber;
@property (weak, nonatomic) IBOutlet UILabel *_propFieldInvestmentType;
@property (weak, nonatomic) IBOutlet UILabel *_propFieldAttachment;
@property (weak, nonatomic) IBOutlet UILabel *_propFieldStatusName;
@property (weak, nonatomic) IBOutlet UILabel *_propFieldTotal;
    
@property (weak, nonatomic) IBOutlet UILabel *_propFieldRequester;
@property (weak, nonatomic) IBOutlet UILabel *_propFieldOffice;
@property (weak, nonatomic) IBOutlet UILabel *_propFieldDepartment;
@property (weak, nonatomic) IBOutlet UILabel *_propFieldCostCenter;
@property (weak, nonatomic) IBOutlet UILabel *_propFieldCM;
@property (weak, nonatomic) IBOutlet UILabel *_propFieldRM;
    
@property (weak, nonatomic) IBOutlet UILabel *_propFieldDateSubmitted;
@property (weak, nonatomic) IBOutlet UILabel *_propFieldDateProcessedByCM;
@property (weak, nonatomic) IBOutlet UILabel *_propFieldDateProcessedByRFM;
@property (weak, nonatomic) IBOutlet UILabel *_propFieldDateProcessByRM;
@property (weak, nonatomic) IBOutlet UILabel *_propFieldDateProcessByCFO;
@property (weak, nonatomic) IBOutlet UILabel *_propFieldDateProcessByCEO;
@property (weak, nonatomic) IBOutlet UILabel *_propFieldCapexItemNumber;
@property (weak, nonatomic) IBOutlet UILabel *_propApproverNote;
    
@property (strong, nonatomic) UITapGestureRecognizer *_attachmentTapRecognizer;
@property (strong, nonatomic) AppDelegate *_appdelegate;
@property (strong, nonatomic) CapexHeader *_propCapexHeader;
@property (strong, nonatomic) UIDocumentInteractionController *_dic;
@property (strong, nonatomic) NSMutableString *_stringNotes;
@property (strong, nonatomic) NSMutableArray *_capexLineItems;

@end


@implementation VCCapexForApprovalDetail

#pragma mark -
#pragma mark === View Life Cycle Management ===
#pragma mark -
- (void)viewDidLoad {
    [super viewDidLoad];
    self._appdelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    [AppDelegate showGlogalHUDWithView:self.view];
    _staffId = [[self._appdelegate getStaff] propStaffID];
//    self.tableView.scrollEnabled = NO;
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0) , ^{
        id result = [self._appdelegate.propGatewayOnline capexHeaderForID:_capexHeaderID];
        dispatch_async(dispatch_get_main_queue(), ^{
            if([result isKindOfClass:[NSString class]])
                [[[UIAlertView alloc] initWithTitle:@"" message:result delegate:self cancelButtonTitle:@"Dismiss" otherButtonTitles:nil, nil] show];
            else {
                self._propCapexHeader = [[CapexHeader alloc] initWithDictionary:result onlineGateway:self._appdelegate.propGatewayOnline];
                self._propFieldCapexNumber.text = [self._propCapexHeader propCapexNumber];
                self._propFieldInvestmentType.text = [self._propCapexHeader propInvestmentTypeName];
                self._propFieldAttachment.text = [self._propCapexHeader propAttachedCer];
                if(![[self._propCapexHeader propAttachedCer] isEqualToString:NO_ATTACHMENT]){
                    self._propFieldAttachment.textColor = [VelosiColors orangeVelosi];
                    self._propFieldAttachment.font = [UIFont fontWithName:@"TrebuchetMS-Bold" size:13];
                    self._attachmentTapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onAttachmentClicked)];
                    [self._propFieldAttachment addGestureRecognizer:self._attachmentTapRecognizer];
                }
                self._propFieldStatusName.text = [self._propCapexHeader propStatusName];
                self._propFieldTotal.text = [self._appdelegate.decimalFormat stringFromNumber:[NSNumber numberWithFloat:[self._propCapexHeader propTotalAmountInUSD] /*@(((NSInteger)[_propCapexHeader propTotalAmountInUSD])) */ ]];
                
                self._propFieldRequester.text = [self._propCapexHeader propRequesterName];
                self._propFieldOffice.text = [self._propCapexHeader propOfficeName];
                self._propFieldDepartment.text = [self._propCapexHeader propDepartmentName];
                self._propFieldCostCenter.text = [self._propCapexHeader propCostCenterName];
                self._propFieldCM.text = [self._propCapexHeader propCMName];
                self._propFieldRM.text = [self._propCapexHeader propRMName];
                self._propApproverNote.text = [self._propCapexHeader propApproverNote];
                
                self._propFieldDateSubmitted.text = [self._propCapexHeader propDateSubmitted:self._appdelegate];
                self._propFieldDateProcessedByCM.text = [self._propCapexHeader propDateProcessedByCM:self._appdelegate];
                self._propFieldDateProcessedByRFM.text = [self._propCapexHeader propDateProcessedByRFM:self._appdelegate];
                self._propFieldDateProcessByRM.text = [self._propCapexHeader propDateProcessedByRM:self._appdelegate];
                self._propFieldDateProcessByCFO.text = [self._propCapexHeader propDateProcessedByCFO:self._appdelegate];
                self._propFieldDateProcessByCEO.text = [self._propCapexHeader propDateProcessedByCEO:self._appdelegate];
                
                self._dic = [[UIDocumentInteractionController alloc] init];
                self._dic.delegate = self;
                
                self._stringNotes = [[NSMutableString alloc] init];
                NSString *tempString;
                NSScanner *stringScanner = [NSScanner scannerWithString:[self._propCapexHeader propApproverNote]];
                NSCharacterSet *separator = [NSCharacterSet newlineCharacterSet];
                while ([stringScanner isAtEnd] == NO) {
                    if([stringScanner scanUpToCharactersFromSet:separator intoString:&tempString])
                        [self._stringNotes appendString:tempString];
                    [self._stringNotes appendFormat:@"\n"];
                }
            }
        });
    });
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
        self._capexLineItems = [[NSMutableArray alloc] init];
        id result = [self._appdelegate.propGatewayOnline capexLineItemsForCapexID:self.capexHeaderID];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [AppDelegate hideGlogalHUDWithView:self.view];
//            self.tableView.scrollEnabled = YES;
            if([result isKindOfClass:[NSString class]])
                [[[UIAlertView alloc] initWithTitle:@"" message:result delegate:nil cancelButtonTitle:@"Dismiss" otherButtonTitles:nil, nil] show];
            else {
                [self._capexLineItems removeAllObjects];
                [self._capexLineItems addObjectsFromArray:result];
                NSString *msg = (self._capexLineItems.count > 1)?@"Line Items":@"Line Item";
                self._propFieldCapexItemNumber.text = [NSString stringWithFormat:@"%ld %@", self._capexLineItems.count, msg];
            }
        });
    });
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    ((VCCapexForApprovalLineItems*)segue.destinationViewController).fieldCapexHeaderID = [self._propCapexHeader propCapexID];
}


#pragma mark -
#pragma mark === Private Method ===
#pragma mark -
- (void)onAttachmentClicked {
    
    [AppDelegate showGlogalHUDWithView:self.view];
    Document *doc;
    if ([[self._propCapexHeader propDocuments] count] > 0)
        doc = [[self._propCapexHeader propDocuments] objectAtIndex:0];
    else
        [[[UIAlertView alloc] initWithTitle:@"" message:@"Attached document is invalid." delegate:nil cancelButtonTitle:@"Dismiss" otherButtonTitles:nil, nil] show];
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
        NSString *urlString = [NSString stringWithFormat:@"%@?dID=%d&refID=%d&obTypeID=%d", NSLocalizedString(@"ImageViewerLink", @""),[doc propDocID], [doc propRefID], [doc propObjectTypeID]];
        id urlData = [self._appdelegate.propGatewayOnline makeWebServiceCall:urlString requestMethod:GET postBody:nil];
        if (![urlData isKindOfClass:[NSString class]]) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [AppDelegate hideGlogalHUDWithView:self.view];
                NSString *filePath = [NSTemporaryDirectory() stringByAppendingPathComponent:[doc propDocName]];
                
                [urlData writeToFile:filePath atomically:YES];
                NSURL *filePathURL = [NSURL fileURLWithPath:filePath];
                self._dic.URL = filePathURL;
                [self._dic presentPreviewAnimated:YES];
            });
        } else {
            dispatch_async(dispatch_get_main_queue(), ^{
                [AppDelegate hideGlogalHUDWithView:self.view];
                UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"" message:urlData preferredStyle:UIAlertControllerStyleAlert];
                UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"Dismiss" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
                    
                }];
                [alertController addAction:cancelAction];
                [self presentViewController:alertController animated:YES completion:nil];
            });
        }
    });
}

- (void)changeCapexStatus:(CapexStatusId)capexStatusID keyForUpdatableDate:(NSString *)keyForUpdatableDate withApproversNote:(NSString *)approverNote{
    [AppDelegate showGlogalHUDWithView:self.view];
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
        id result = [self._appdelegate.propGatewayOnline saveCapexHeader:[self._propCapexHeader JSONForUpdatingCapexHeaderWithStatusID:capexStatusID keyForUpdatableDate:keyForUpdatableDate withApproverNote:approverNote appDelegate:self._appdelegate] oldCapexHeaderJSON:[self._propCapexHeader jsonizeWithappDelegate:self._appdelegate]];
        dispatch_async(dispatch_get_main_queue(), ^{
            [AppDelegate hideGlogalHUDWithView:self.view];
            [[[UIAlertView alloc] initWithTitle:@"" message:([result isEqualToString:@"OK"])?@"Updated Successfully":result delegate:self cancelButtonTitle:@"Dismiss" otherButtonTitles:nil, nil] show];
        });
    });
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    //this delegate is only intended for listening button clicked after leave submission success message will be dismissed
    [self.navigationController popViewControllerAnimated:YES];
}


#pragma mark -
#pragma mark === UIDocument Interaction Controller Delegate ===
#pragma mark -
- (UIViewController *)documentInteractionControllerViewControllerForPreview:(UIDocumentInteractionController *)controller{
    return self;
}


#pragma mark -
#pragma mark === Helper Method ===
#pragma mark -
- (void)alertTextFieldDidChange:(UITextField *)sender
{
    UIAlertController *alertController = (UIAlertController *)self.presentedViewController;
    if (alertController)
    {
        UITextField *noteField = alertController.textFields.firstObject;
        UIAlertAction *confirmAction = alertController.actions.lastObject;
        confirmAction.enabled = ([[noteField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]] length] == 0)?NO:YES;
    }
}


#pragma mark -
#pragma mark === UI Event Handler Management ===
#pragma mark -
- (IBAction)approveCapex:(id)sender {
    NSString *alertTitle = @"Approver's Note";
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:alertTitle
                                                                             message:nil
                                                                      preferredStyle:UIAlertControllerStyleAlert];
    
    [alertController addTextFieldWithConfigurationHandler:^(UITextField *textField)
     {
         textField.placeholder = @"Note";
         [textField addTarget:nil
                       action:@selector(self)
             forControlEvents:UIControlEventEditingChanged];
     }];
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"Cancel"
                                                           style:UIAlertActionStyleCancel
                                                         handler:^(UIAlertAction *action) {
                                                             NSLog(@"Canceled");
                                                         }];
    UIAlertAction *approveAction = [UIAlertAction actionWithTitle:@"Proceed"
                                                            style:UIAlertActionStyleDefault
                                                          handler:^(UIAlertAction *action) {
                                                              UITextField *approveNote = alertController.textFields[0];
                                                              int capexStatus = [self._propCapexHeader propStatusID];
                                                              if (capexStatus == CapexStatusIdSubmitted) {
                                                                  if (approveNote.text.length > 0) {
                                                                      if (self._propCapexHeader.propOfficeID != 41)
                                                                          [self._stringNotes appendFormat:@"CM: %@",approveNote.text];
                                                                      else
                                                                          [self._stringNotes appendFormat:@"CFO: %@",approveNote.text];
                                                                  } else {
                                                                      if (self._propCapexHeader.propOfficeID != 41)
                                                                          [self._stringNotes appendString:@"CM: Approved"];
                                                                      else
                                                                          [self._stringNotes appendString:@"CFO: Approved"];
                                                                  }
                                                                  NSLog(@"approvers note: %@",self._stringNotes);
                                                                  [self changeCapexStatus:CapexStatusIdApprovedByCm
                                                                      keyForUpdatableDate:@"DateProcessedByCountryManager"
                                                                        withApproversNote:self._stringNotes];
                                                              } else if (capexStatus == CapexStatusIdApprovedByCm && [self._propCapexHeader propRFMID] == _staffId) {
                                                                  if (approveNote.text.length > 0) {
                                                                      [self._stringNotes insertString:@"\n" atIndex:0];
                                                                      [self._stringNotes insertString:[NSString stringWithFormat:@"CFO: %@",approveNote.text] atIndex:0];
                                                                  } else {
                                                                      [self._stringNotes insertString:@"\n" atIndex:0];
                                                                      [self._stringNotes insertString:@"CFO: Approved" atIndex:0];
                                                                  }
                                                                  [self changeCapexStatus:CapexStatusIdApprovedByRfm
                                                                      keyForUpdatableDate:@"DateProcessedByRFM"
                                                                        withApproversNote:self._stringNotes];
                                                              } else if ((capexStatus == CapexStatusIdApprovedByCm && [self._propCapexHeader propRFMID] == 0) || capexStatus == CapexStatusIdApprovedByRfm) {
                                                                  if (approveNote.text.length > 0) {
                                                                      [self._stringNotes insertString:@"\n" atIndex:0];
                                                                      if (self._propCapexHeader.propOfficeID != 41)
                                                                          [self._stringNotes insertString:[NSString stringWithFormat:@"RM: %@",approveNote.text] atIndex:0];
                                                                      else
                                                                          [self._stringNotes insertString:[NSString stringWithFormat:@"CEO: %@",approveNote.text] atIndex:0];
                                                                      
                                                                  } else {
                                                                      if (self._propCapexHeader.propOfficeID != 41)
                                                                          [self._stringNotes insertString:@"CM: Approved" atIndex:0];
                                                                      else
                                                                          [self._stringNotes insertString:@"CFO: Approved" atIndex:0];
                                                                  }
                                                                  [self changeCapexStatus:CapexStatusIdApprovedByRm
                                                                      keyForUpdatableDate:@"DateProcessedByRegionalManager"
                                                                        withApproversNote:self._stringNotes];
                                                              } else if (capexStatus == CapexStatusIdApprovedByRm) {
                                                                  if (approveNote.text.length > 0) {
                                                                      [self._stringNotes insertString:@"\n" atIndex:0];
                                                                      [self._stringNotes insertString:[NSString stringWithFormat:@"CFO: %@",approveNote.text] atIndex:0];
                                                                  }else{
                                                                      [self._stringNotes insertString:@"\n" atIndex:0];
                                                                      [self._stringNotes insertString:@"CFO: Approved" atIndex:0];
                                                                  }
                                                                  [self changeCapexStatus:CapexStatusIdApprovedByCfo
                                                                      keyForUpdatableDate:@"DateProcessedByCFO"
                                                                        withApproversNote:self._stringNotes];
                                                              } else if (capexStatus == CapexStatusIdApprovedByCfo) {
                                                                  if (approveNote.text.length > 0) {
                                                                      [self._stringNotes insertString:@"\n" atIndex:0];
                                                                      [self._stringNotes insertString:[NSString stringWithFormat:@"CEO: %@",approveNote.text] atIndex:0];
                                                                  }else{
                                                                      [self._stringNotes insertString:@"\n" atIndex:0];
                                                                      [self._stringNotes insertString:@"CEO: Approved" atIndex:0];
                                                                  }
                                                                  [self changeCapexStatus:CapexStatusIdApprovedByCeo
                                                                      keyForUpdatableDate:@"DateProcessedByCEO"
                                                                        withApproversNote:self._stringNotes];
                                                              }
                                                              
                                                          }];
    [alertController addAction:cancelAction];
    [alertController addAction:approveAction];
    
    [self presentViewController:alertController animated:YES completion:nil];
}


- (IBAction)rejectCapex:(id)sender {
    NSString *alertTitle = @"Reason for Rejection";
    
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:alertTitle
                                                                             message:nil
                                                                      preferredStyle:UIAlertControllerStyleAlert];
    [alertController addTextFieldWithConfigurationHandler:^(UITextField *textField)
     {
         textField.placeholder = @"Required";
         [textField addTarget:self
                       action:@selector(alertTextFieldDidChange:)
             forControlEvents:UIControlEventEditingChanged];
     }];
    
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"Cancel"
                                                           style:UIAlertActionStyleCancel
                                                         handler:^(UIAlertAction *action) {
                                                             NSLog(@"Canceled");
                                                         }];
    UIAlertAction *rejectAction = [UIAlertAction actionWithTitle:@"Reject"
                                                           style:UIAlertActionStyleDefault
                                                         handler:^(UIAlertAction *action){
                                                             UITextField *rejectNote = alertController.textFields.firstObject;
                                                             NSLog(@"Reject Text Value: %@",rejectNote.text);
                                                             int capexStatusId = [self._propCapexHeader propStatusID];
                                                             
                                                             if (capexStatusId == CapexStatusIdSubmitted) {
                                                                 if (self._propCapexHeader.propStatusID != 41 ) //office 41 is the headquarter
                                                                     [self._stringNotes appendFormat:@"CM: %@", rejectNote.text];
                                                                 else
                                                                     [self._stringNotes appendFormat:@"CFO: %@", rejectNote.text];
                                                                 [self changeCapexStatus:CapexStatusIdRejectedByCm keyForUpdatableDate:@"DateProcessedByCountryManager"
                                                                       withApproversNote:self._stringNotes];
                                                             } else if (capexStatusId == CapexStatusIdApprovedByCm && self._propCapexHeader.propRFMID == _staffId) {
                                                                 [self._stringNotes insertString:@"\n" atIndex:0];
                                                                 NSString *stringFormat = [NSString stringWithFormat:@"CFO: %@",rejectNote.text];
                                                                 [self._stringNotes insertString:stringFormat atIndex:0];
                                                                 [self changeCapexStatus:CapexStatusIdRejectedByRfm keyForUpdatableDate:@"DateProcessedByRFM"
                                                                       withApproversNote:self._stringNotes];
                                                             } else if ((capexStatusId == CapexStatusIdApprovedByCm && [self._propCapexHeader propRFMID] == 0) || capexStatusId == CapexStatusIdApprovedByRfm) {
                                                                 [self._stringNotes insertString:@"\n" atIndex:0];
                                                                 if (self._propCapexHeader.propStatusID != 41 )
                                                                     [self._stringNotes appendFormat:@"RM: %@", rejectNote.text];
                                                                 else
                                                                     [self._stringNotes appendFormat:@"CEO: %@", rejectNote.text];
                                                                 [self changeCapexStatus:CapexStatusIdRejectedByRm keyForUpdatableDate:@"DateProcessedByRegionalManager"
                                                                       withApproversNote:self._stringNotes];
                                                             } else if (capexStatusId == CapexStatusIdApprovedByRm) {
                                                                 [self._stringNotes insertString:@"\n" atIndex:0];
                                                                 NSString *stringFormat = [NSString stringWithFormat:@"CFO: %@",rejectNote.text];
                                                                 [self._stringNotes insertString:stringFormat atIndex:0];
                                                                 [self changeCapexStatus:CapexStatusIdRejectedByCfo keyForUpdatableDate:@"NA"
                                                                       withApproversNote:self._stringNotes];
                                                             } else if (capexStatusId == CapexStatusIdApprovedByCfo) {
                                                                 [self._stringNotes insertString:@"\n" atIndex:0];
                                                                 NSString *stringFormat = [NSString stringWithFormat:@"CEO: %@",rejectNote.text];
                                                                 [self._stringNotes insertString:stringFormat atIndex:0];
                                                                 [self changeCapexStatus:CapexStatusIdRejectedByCeo keyForUpdatableDate:@"NA"
                                                                       withApproversNote:self._stringNotes];
                                                             }
                                                             
                                                         }];
    rejectAction.enabled = NO;
    [alertController addAction:cancelAction];
    [alertController addAction:rejectAction];
    
    [self presentViewController:alertController animated:YES completion:nil];
}


- (IBAction)returnCapex:(id)sender {
    NSString *alertTitle = @"Reason for Returning";
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:alertTitle
                                                                             message:nil
                                                                      preferredStyle:UIAlertControllerStyleAlert];
    [alertController addTextFieldWithConfigurationHandler:^(UITextField *textField)
     {
         textField.placeholder = @"Required";
         [textField addTarget:self
                       action:@selector(alertTextFieldDidChange:)
             forControlEvents:UIControlEventEditingChanged];
     }];
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"Cancel"
                                                           style:UIAlertActionStyleCancel
                                                         handler:^(UIAlertAction *action) {
                                                             NSLog(@"Canceled");
                                                         }];
    UIAlertAction *returnAction = [UIAlertAction actionWithTitle:@"Return"
                                                           style:UIAlertActionStyleDefault
                                                         handler:^(UIAlertAction *action){
                                                             UITextField *returnNote = alertController.textFields.firstObject;
                                                             int capexStatusId = [self._propCapexHeader propStatusID];
                                                             
                                                             if (capexStatusId == CapexStatusIdSubmitted) {
                                                                 if ([self._propCapexHeader propOfficeID] != 41)
                                                                     [self._stringNotes appendFormat:@"CM: %@", returnNote.text];
                                                                 else
                                                                     [self._stringNotes appendFormat:@"CFO: %@", returnNote.text];
                                                             } else if (capexStatusId == CapexStatusIdApprovedByCm && self._propCapexHeader.propRFMID == _staffId) {
                                                                 [self._stringNotes insertString:@"\n" atIndex:0];
                                                                 NSString *stringFormat = [NSString stringWithFormat:@"RFM: %@",returnNote.text];
                                                                 [self._stringNotes insertString:stringFormat atIndex:0];
                                                             } else if ((capexStatusId == CapexStatusIdApprovedByCm && self._propCapexHeader.propRFMID == 0) || capexStatusId == CapexStatusIdApprovedByRfm) {
                                                                 [self._stringNotes insertString:@"\n" atIndex:0];
                                                                 if ([self._propCapexHeader propOfficeID] != 41)
                                                                     [self._stringNotes appendFormat:@"RM: %@", returnNote.text];
                                                                 else
                                                                     [self._stringNotes appendFormat:@"CEO: %@", returnNote.text];
                                                             } else if (capexStatusId == CapexStatusIdApprovedByRm) {
                                                                 [self._stringNotes insertString:@"\n" atIndex:0];
                                                                 NSString *stringFormat = [NSString stringWithFormat:@"CFO: %@",returnNote.text];
                                                                 [self._stringNotes insertString:stringFormat atIndex:0];
                                                             } else if (capexStatusId == CapexStatusIdApprovedByCfo) {
                                                                 [self._stringNotes insertString:@"\n" atIndex:0];
                                                                 NSString *stringFormat = [NSString stringWithFormat:@"CEO: %@",returnNote.text];
                                                                 [self._stringNotes insertString:stringFormat atIndex:0];
                                                             }
                                                             [self changeCapexStatus:CapexStatusIdOpen keyForUpdatableDate:@"NA" withApproversNote:self._stringNotes];
                                                         }];
    returnAction.enabled = NO;
    [alertController addAction:cancelAction];
    [alertController addAction:returnAction];
    [self presentViewController:alertController animated:YES completion:nil];
}

@end
