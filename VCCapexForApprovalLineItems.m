//
//  VCCapexForApprovalLineItemNew.m
//  Salt
//
//  Created by Rick Royd Aban on 4/4/16.
//  Copyright © 2016 Applus Velosi. All rights reserved.
//

#import "VCCapexForApprovalLineItems.h"
#import "MBProgressHUD.h"
#import "AppDelegate.h"
#import "CapexForApprovalLineItem.h"
#import "VCCapexItemsDetailsAndQuotation.h"
#import "CellCapexQuotationsCount.h"
#import "CellCapexLineItemsLabel.h"
#import "CapexLineItemQoutation.h"

@interface VCCapexForApprovalLineItems ()
{
    NSMutableArray *propClaimItems,*_propQuotations;
    AppDelegate *_appDelegate;
}
@end

@implementation VCCapexForApprovalLineItems


#pragma mark -
#pragma mark === View Life Cycle Management ===
#pragma mark -

- (void)viewDidLoad {
    [super viewDidLoad];
    _appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    self.tableView.estimatedRowHeight = 140;
    self.tableView.rowHeight = UITableViewAutomaticDimension;
    propClaimItems = [NSMutableArray array];
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self refresh];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    NSIndexPath *indexPath = [self.tableView indexPathForCell:sender];
    CapexForApprovalLineItem *capexItem = [self capexLineItemIndexPath:indexPath];
    ((VCCapexItemsDetailsAndQuotation *)segue.destinationViewController).capexLineItem = capexItem;
}

#pragma mark -
#pragma mark === Private Method ===
#pragma mark -

-(CapexForApprovalLineItem *) capexLineItemIndexPath:(NSIndexPath *)indexPath{
    CapexForApprovalLineItem *capexLineItem = nil;
    if (indexPath)
        capexLineItem = [propClaimItems objectAtIndex:indexPath.row];
    return capexLineItem;
}

-(void)refresh{
    [AppDelegate showGlogalHUDWithView:self.view];
    self.tableView.scrollEnabled = NO;
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
        id result = [_appDelegate.propGatewayOnline capexLineItemsForCapexID:self.fieldCapexHeaderID];
        dispatch_async(dispatch_get_main_queue(), ^{
            [AppDelegate hideGlogalHUDWithView:self.view];
            self.tableView.scrollEnabled = YES;
            if([result isKindOfClass:[NSString class]])
                [[[UIAlertView alloc] initWithTitle:@"" message:result delegate:nil cancelButtonTitle:@"Dismiss" otherButtonTitles:nil, nil] show];
            else{
                [propClaimItems removeAllObjects];
                [propClaimItems addObjectsFromArray:result];
            }
            [self.tableView reloadData];
        });
    });
}

//-(void)fetchLineItemsQuotation:(int)capexLineItemID  lineItemsQuotations:(void(^)(NSMutableArray *))quotations {
//    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
//    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
//        id result = [_appDelegate.propGatewayOnline capexLineItemQoutationForLineItemID:capexLineItemID];
//        __block NSMutableArray *tempQuotations = [NSMutableArray array];
//        dispatch_async(dispatch_get_main_queue(), ^{
//            [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
//            if([result isKindOfClass:[NSString class]]) //failure in retrieving
//                [[[UIAlertView alloc] initWithTitle:@"" message:result delegate:nil cancelButtonTitle:@"Dismiss" otherButtonTitles:nil, nil] show];
//            else{
////                [tempQuotations removeAllObjects];
//                [tempQuotations addObjectsFromArray:result];
//                quotations(tempQuotations);
//            }
//        });
//    });
//}

#pragma mark -
#pragma mark === UITableViewDataSource Delegate Methods ===
#pragma mark -


-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return propClaimItems.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *cellItem = @"cellItem";
    
    CellCapexLineItemsLabel *cell = [tableView dequeueReusableCellWithIdentifier:cellItem];
    if (cell == nil)
        cell = [[CellCapexLineItemsLabel alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellItem];
    CapexForApprovalLineItem *lineItem = [propClaimItems objectAtIndex:indexPath.row];
//    [self fetchLineItemsQuotation:[lineItem propCapexLineItemID] lineItemsQuotations:^(NSMutableArray *quotations){
//        cell.propItem.text = [NSString stringWithFormat:@"%ld", [quotations count] ] ;
//    }];
    cell.propCapexNumber.text = [lineItem propCapexNumber];
    cell.bounds = CGRectMake(0, 0, CGRectGetWidth(tableView.bounds), 99999);
    cell.contentView.bounds = cell.bounds;
    [cell layoutIfNeeded];
    
    cell.propCapexNumber.preferredMaxLayoutWidth = CGRectGetWidth(cell.propCapexNumber.frame);
    
    return cell;
}

@end
