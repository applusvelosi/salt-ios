//
//  VCClaimItemInputGeneral.m
//  Salt
//
//  Created by Rick Royd Aban on 2015/09/10.
//  Copyright (c) 2015年 Applus Velosi. All rights reserved.
//

#define HEADER_PROJECT @"-Project-"
#define HEADER_BILLABLE @"-Company Code-"

#import "VCClaimItemInputGeneral.h"
#import "VCClaimItemAttendees.h"
#import "VelosiDatePicker.h"
#import "VelosiCustomPicker.h"
#import "MBProgressHUD.h"
#import "Project.h"
#import "Office.h"
#import "ClaimCategory.h"
#import "ClaimItemAttendee.h"

@interface VCClaimItemInputGeneral()<UIAlertViewDelegate>{
    
    IBOutlet UITextField *_propFieldDate;
    IBOutlet UITextField *_propFieldLocalAmount;
    IBOutlet UITextField *_propFieldLocalCurrency;
    IBOutlet UITextField *_propFieldForeignAmount;
    IBOutlet UITextField *_propFieldForeignCurrency;
    IBOutlet UITextField *_propFieldExRate;
    IBOutlet UITextField *_propFieldTaxAmount;
    IBOutlet UISwitch *_propSwitchIsTaxable;
    
    IBOutlet UITextField *_propFieldProjectName;
    IBOutlet UITextField *_propFieldBillTo;
    IBOutlet UISwitch *_propSwitchIsBillable;
    IBOutlet UITextField *_propFieldNotesToClient;
    
    IBOutlet UITableViewCell *_propCellAttendees;
    IBOutlet UITextView *_propFieldDesc;
    
    IBOutlet UIView *_propContainerBrowseImage;
    IBOutlet UIView *_propContainerTakePhoto;
    IBOutlet UILabel *_propLabelAttachment;
    
    UITapGestureRecognizer *_recognizerBrowse, *_recognizerTakePhoto, *_recognizerShowAttachments;
    UIImage *_attachedImage;

    VelosiDatePicker *_datePicker;
    UIPickerView *_pickerForeignCurrency, *_pickerProject, *_pickerBillTo;
    NSArray *_foreignCurrencies, *_projects, *_billables;
    NSMutableArray *_projectNames, *_billableNames, *_claimitemAttendees;
    NSDateFormatter *_dateFormatClaimItemDocDate;
    
    UIImagePickerController *_imagePicker;
    UIPopoverController *_popoverCameraRoll;
    UIDocumentInteractionController *_dic;
    
}
@end

@implementation VCClaimItemInputGeneral

- (void)viewDidLoad{
    [super viewDidLoad];
    
    _claimitemAttendees = [NSMutableArray array];
    _dateFormatClaimItemDocDate = [[NSDateFormatter alloc] init];
    _dateFormatClaimItemDocDate.dateFormat = @"yyyy-MM-dd-HH-mm-ss-S";
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{ //get all offices
        id resultAllOffice = [self.propAppDelegate.propGatewayOnline allOffices];
        
        if([resultAllOffice isKindOfClass:[NSString class]]){
            dispatch_async(dispatch_get_main_queue(), ^{
                [AppDelegate showGlogalHUDWithView:self.view];
                [[[UIAlertView alloc] initWithTitle:@"" message:resultAllOffice delegate:self cancelButtonTitle:@"Dismiss" otherButtonTitles:nil, nil] show];
            });
        }else{
            id resultAllProjects = [self.propAppDelegate.propGatewayOnline projectsByCostCenterID:_propClaimHeader.propCostCenterID];

            dispatch_async(dispatch_get_main_queue(), ^{
                [AppDelegate hideGlogalHUDWithView:self.view];
                if([resultAllProjects isKindOfClass:[NSString class]]){
                    [[[UIAlertView alloc] initWithTitle:@"" message:resultAllProjects delegate:self cancelButtonTitle:@"Dismiss" otherButtonTitles:nil, nil] show];
                }else{
                    _dic = [[UIDocumentInteractionController alloc] init];
                    _dic.delegate = self;
                    _recognizerBrowse = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(browsePhotos)];
                    _recognizerTakePhoto = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(takePhoto)];
                    _recognizerShowAttachments = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(showAttachment)];
                    
                    [_propContainerBrowseImage addGestureRecognizer:_recognizerBrowse];
                    [_propContainerTakePhoto addGestureRecognizer:_recognizerTakePhoto];
                    [_propLabelAttachment addGestureRecognizer:_recognizerShowAttachments];
                    
                    _billables = resultAllOffice;
                    _projects = resultAllProjects;
                    _billableNames = [NSMutableArray array];
                    _projectNames = [NSMutableArray array];
                    [_billableNames addObject:HEADER_BILLABLE];
                    [_projectNames addObject:HEADER_PROJECT];
                    for(Project *project in _projects)
                        [_projectNames addObject:[project propCode]];
                    for(Office *office in _billables)
                        [_billableNames addObject:[office officeName]];
                    
                    NSDate *expenseDate;
                    NSString *selectedForeignCurrencyThree;
                    NSString *selectedForeignCurrencyName;
                    NSString *selectedProject;
                    NSString *selectedBillTo;

                    if(_propClaimItem == nil){ //set to default values
                        expenseDate = [NSDate date];
                        selectedForeignCurrencyThree = [self.propAppDelegate.office baseCurrencyThree];
                        selectedForeignCurrencyName = [self.propAppDelegate.office baseCurrencyName];
                        
                        _propFieldLocalCurrency.text = selectedForeignCurrencyThree;
                        _propFieldForeignCurrency.text = selectedForeignCurrencyThree;
                        _propFieldTaxAmount.text = [NSString stringWithFormat:@"%.2f", [self.propAppDelegate.office defaultTax]/100];
                        selectedBillTo = HEADER_BILLABLE;
                        selectedProject = HEADER_PROJECT;
                        _propFieldProjectName.text = selectedProject;
                        _propFieldBillTo.text = selectedBillTo;
                    }else{
                        _propFieldTaxAmount.text = [NSString stringWithFormat:@"%.2f", [_propClaimItem propTaxedAmount]];
                        expenseDate = [self.propAppDelegate.propFormatVelosiDate dateFromString:[_propClaimItem propDateExpensedWithAppDelegate:self.propAppDelegate]];
                        selectedForeignCurrencyThree = [_propClaimItem propForeignCurrencyName];
                        selectedProject = [_propClaimItem propProjectName];
                        selectedBillTo = [_propClaimItem propCompanyChargeToName];
                    }

                    if([selectedForeignCurrencyThree isEqualToString:[self.propAppDelegate.office baseCurrencyThree]])
                        _propFieldExRate.text = @"1.00";
                    else
                        [self forexValueForCurrThree:[_propClaimItem propForeignCurrencyName]];
                    
                    _datePicker = [[VelosiDatePicker alloc] initWithDate:expenseDate minimumDate:nil viewController:self action:@selector(onDateSelected)];
                    _pickerForeignCurrency = [[VelosiCustomPicker alloc] initWithArray:[self.propAppDelegate.propCurrencies propCurrencyNames] rowSelectionDelegate:self selectedItem:[[self.propAppDelegate.propCurrencies propCurrencyThrees] objectAtIndex:[[self.propAppDelegate.propCurrencies propCurrencyThrees] indexOfObject:selectedForeignCurrencyThree]]];
                    _pickerBillTo = [[VelosiCustomPicker alloc] initWithArray:_billableNames rowSelectionDelegate:self selectedItem:selectedBillTo];
                    _pickerProject = [[VelosiCustomPicker alloc] initWithArray:_projectNames rowSelectionDelegate:self selectedItem:selectedProject];
                    
                    [_propSwitchIsTaxable addTarget:self action:@selector(updateTaxability:) forControlEvents:UIControlEventValueChanged];
                    _propFieldDate.delegate = self;
                    _propFieldForeignCurrency.delegate = self;
                    _propFieldForeignAmount.delegate = self;
                    _propFieldTaxAmount.delegate = self;
                    _propFieldExRate.delegate = self;
                    _propFieldProjectName.delegate = self;
                    _propFieldBillTo.delegate = self;
                }
                
                
            });
        }
    });
}

- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    _propCellAttendees.detailTextLabel.text = [NSString stringWithFormat:@"%lu",_claimitemAttendees.count];
}

- (void)showAttachment{
    if(_propLabelAttachment.text.length > 0){
        [_dic presentPreviewAnimated:YES];
    }
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    //if range's lenght > 0 then user is deleting
    //replacementstring is the string added per action
    @try{
        NSString *newStr = [_propFieldForeignAmount.text stringByReplacingCharactersInRange:range withString:string];
        float amt = [newStr floatValue];
        if(textField == _propFieldForeignAmount){
            if(_propClaimItem.propCategoryTypeID == [ClaimCategory TYPE_BUSINESSADVANCE] && amt>0){
                textField.delegate = nil;
                textField.text = [NSString stringWithFormat:@"-%.2f",amt];
                textField.delegate = self;
            }
        }

        [self updateLocalAmountFromForeignAmt:amt];
        if(textField == _propFieldTaxAmount && [[_propFieldForeignAmount.text stringByReplacingCharactersInRange:range withString:string] floatValue]>=1)
            return NO;
        else
            return YES;
    }@catch(NSException *e){
        NSLog(@"Exception %@",e);
        return NO;
    }
}

- (void)updateLocalAmountFromForeignAmt:(float)amt{
    float totalLC = amt * [_propFieldExRate.text floatValue];
    if(_propSwitchIsTaxable.isOn)
        totalLC = totalLC-(totalLC*[_propFieldTaxAmount.text floatValue]);
    
    _propFieldLocalAmount.text = [NSString stringWithFormat:@"%.2f",totalLC];
}

- (void)onDateSelected{
    _propFieldDate.text = [self.propAppDelegate.propFormatVelosiDate stringFromDate:_datePicker.date];
}

- (IBAction)updateTaxability:(id)sender {
    if(_propSwitchIsTaxable.isOn){
        _propFieldTaxAmount.text = [NSString stringWithFormat:@"%.2f", (_propClaimItem == nil)?[self.propAppDelegate.office defaultTax]/100:_propClaimItem.propTaxedAmount];

    }else
        _propFieldTaxAmount.text = @"0.00";


    [self updateLocalAmountFromForeignAmt:[_propFieldForeignAmount.text floatValue]];
}

- (IBAction)updateBillability:(id)sender {
    _propFieldBillTo.userInteractionEnabled = (_propSwitchIsBillable.isOn)?YES:NO;
    [self.tableView reloadData];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    switch (section) {
        case 0: return 1;
        case 1: return 4;
        case 2: return 1;
        case 3: return (_propSwitchIsBillable.isOn)?3:2;
        case 4: return 1;
        case 5: return 1;
        default: return 0;
    }
}

- (void)textFieldDidBeginEditing:(UITextField *)textField{
    [super textFieldDidBeginEditing:textField];
    
    if(textField == _propFieldForeignCurrency){
        textField.inputView = _pickerForeignCurrency;
        [_pickerForeignCurrency selectRow:[[self.propAppDelegate.propCurrencies propCurrencyThrees] indexOfObject:_propFieldForeignCurrency.text] inComponent:0 animated:NO];
    }else if(textField == _propFieldProjectName)
        textField.inputView = _pickerProject;
    else if(textField == _propFieldBillTo)
        textField.inputView = _pickerBillTo;
    else if(textField == _propFieldDate){
        if(_propFieldDate.text.length < 1)
            _propFieldDate.text = [self.propAppDelegate.propFormatVelosiDate stringFromDate:[NSDate date]];
        
        textField.inputView = _datePicker;
    }
}

- (void)pickerSelection:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component{
    if(pickerView == _pickerForeignCurrency){
        NSString *currThree = [[self.propAppDelegate.propCurrencies propCurrencyThrees] objectAtIndex:row];
        if([currThree isEqualToString:[self.propAppDelegate.office baseCurrencyThree]]){
            _propFieldExRate.text = @"1.00";
            _propFieldForeignCurrency.text = currThree;
            [self updateLocalAmountFromForeignAmt:[_propFieldForeignAmount.text floatValue]];
        }else{
            [self forexValueForCurrThree:currThree];
        }
    }
    
    if(pickerView == _pickerProject)
        _propFieldProjectName.text = [_projectNames objectAtIndex:row];
    else if(pickerView == _pickerBillTo)
        _propFieldBillTo.text = [_billableNames objectAtIndex:row];
}

- (void)forexValueForCurrThree:(NSString *)currThree{
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
        id result = [self.propAppDelegate.propGatewayOnline forexRateFrom:currThree to:_propFieldLocalCurrency.text];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
            if([result isKindOfClass:[NSString class]]){
                [[[UIAlertView alloc] initWithTitle:@"" message:result delegate:self cancelButtonTitle:@"Dismiss" otherButtonTitles:nil, nil] show];
            }else{
                _propFieldExRate.text = [NSString stringWithFormat:@"%.2f",[result floatValue]];
                _propFieldForeignCurrency.text = currThree;
                [self updateLocalAmountFromForeignAmt:[_propFieldForeignAmount.text floatValue]];
            }
        });
    });
}

- (void)browsePhotos{
    if(_imagePicker == nil){
        _imagePicker = [[UIImagePickerController alloc] init];
        _imagePicker.delegate = self;
    }
    
    _imagePicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    [self presentViewController:_imagePicker animated:YES completion:nil];
}

- (void)takePhoto{
    if(_imagePicker == nil){
        _imagePicker = [[UIImagePickerController alloc] init];
        _imagePicker.delegate = self;
    }
    _imagePicker.sourceType = UIImagePickerControllerSourceTypeCamera;
    _imagePicker.allowsEditing =  NO;
    [self presentViewController:_imagePicker animated:YES completion:nil];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    [self.navigationController dismissViewControllerAnimated:YES completion:nil];
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<NSString *,id> *)info{
    _attachedImage = [info objectForKey:@"UIImagePickerControllerOriginalImage"];

    NSLog(@"size %lu",UIImageJPEGRepresentation(_attachedImage, 0.0).length);
    if(UIImageJPEGRepresentation(_attachedImage, 0.0).length < 95000000){
        NSString *attachmentName = [NSString stringWithFormat:@"img_%@.jpg",[_dateFormatClaimItemDocDate stringFromDate:[NSDate date]]];
        NSString *attachedImageURL = [NSTemporaryDirectory() stringByAppendingPathComponent:attachmentName];
        _propLabelAttachment.text = attachmentName;
        
        [UIImageJPEGRepresentation(_attachedImage, 0.0) writeToFile:attachedImageURL atomically:YES];
        _dic.URL = [NSURL fileURLWithPath:attachedImageURL];
        [_imagePicker dismissViewControllerAnimated:YES completion:nil];
    }else
        [[[UIAlertView alloc] initWithTitle:@"" message:@"Please use a file less than 10mb" delegate:nil cancelButtonTitle:@"Dismiss" otherButtonTitles:nil, nil] show];
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker{
    [_imagePicker dismissViewControllerAnimated:YES completion:nil];
}

- (UIViewController *)documentInteractionControllerViewControllerForPreview:(UIDocumentInteractionController *)controller{
    return self;
}

#pragma clang diagnostic ignored "-Wnonnull"
- (IBAction)submit:(id)sender {
    if(_propFieldDate.text.length > 0){
        if(_propFieldForeignAmount.text.length > 0 || [_propFieldForeignAmount.text floatValue]!=0){
            if([_pickerProject selectedRowInComponent:0] > 0){
                if(!(_propSwitchIsBillable.isOn && [_pickerBillTo selectedRowInComponent:0] == 0)){
                    if(_propFieldDesc.text.length > 0){
                        [AppDelegate showGlogalHUDWithView:self.view];
                        self.view.userInteractionEnabled = NO;
                        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
                            ClaimItem *newClaimItem = [[ClaimItem alloc] initWithClaimHeader:_propClaimHeader];
                            [newClaimItem propAmount:[_propFieldForeignAmount.text floatValue]];
                            [newClaimItem propAmounLC:[_propFieldLocalAmount.text floatValue]];
                            [newClaimItem propCategory:_propClaimCategory];
                            [newClaimItem propCompanyChargeTo:(_propSwitchIsBillable.isOn)?[_billables objectAtIndex:[_pickerBillTo selectedRowInComponent:0]-1]:nil];
                            [newClaimItem propCurrency:[self.propAppDelegate.propCurrencies.propCurrencies objectAtIndex:[_pickerForeignCurrency selectedRowInComponent:0]]];
                            [newClaimItem setDesc:_propFieldDesc.text];
                            [newClaimItem propForex:[_propFieldExRate.text floatValue]];
                            [newClaimItem propExpenseDate:[self.propAppDelegate.propGatewayOnline epochizeDate:[self.propAppDelegate.propFormatVelosiDate dateFromString:_propFieldDate.text]]];
                            [newClaimItem propIsRechargeable:_propSwitchIsBillable.isOn];
                            [newClaimItem propIsTaxRated:_propSwitchIsTaxable.isOn];
                            [newClaimItem propHasReceipt:(_propLabelAttachment.text.length > 0)];
                            [newClaimItem propLocalCurrency:[[Currency alloc] initWithID:[self.propAppDelegate.office baseCurrencyID] name:[self.propAppDelegate.office baseCurrencyName] currThree:[self.propAppDelegate.office baseCurrencyThree]]];
                            [newClaimItem propNotes:_propFieldNotesToClient.text];
                            [newClaimItem propProject:[_projects objectAtIndex:[_pickerProject selectedRowInComponent:0]-1]];
                            [newClaimItem propDateCreated:[self.propAppDelegate.propGatewayOnline epochizeDate:[NSDate date]]];
                            if(_claimitemAttendees.count > 0)
                                [newClaimItem clearAndUpdateAttendees:_claimitemAttendees];
                            NSString *base64;
                            if(_propLabelAttachment.text.length > 0){
                                base64 = [UIImageJPEGRepresentation(_attachedImage, 1.0) base64EncodedStringWithOptions:NSDataBase64EncodingEndLineWithLineFeed];
                                [newClaimItem addAttachment:[[Document alloc] initWithFileName:_propLabelAttachment.text dateCreated:[self.propAppDelegate.propGatewayOnline epochizeDate:[NSDate date]] fileSize:base64.length claimHeader:_propClaimHeader nameDate:[_dateFormatClaimItemDocDate stringFromDate:[NSDate date]] objectTypeID:[Document objecttype_CLAIMLINEITEMID]]];
                            }
                            
                            NSString *claimItemJSON = [newClaimItem jsonForWebServicesWithAppDelegate:self.propAppDelegate];
                            NSString *attachment = ([newClaimItem propAttachments] != nil) ? [[[newClaimItem propAttachments] objectForKey:0] jsonObject]: [newClaimItem propAttachments];
                            id result = [self.propAppDelegate.propGatewayOnline saveNewClaimItemJSON:claimItemJSON oldClaimItemID:0 document:attachment base64:base64];
                            dispatch_async(dispatch_get_main_queue(), ^{
                                self.view.userInteractionEnabled = YES;
                                [AppDelegate hideGlogalHUDWithView:self.view];
                                if([result isKindOfClass:[NSString class]])
                                    [[[UIAlertView alloc] initWithTitle:@"" message:result delegate:nil cancelButtonTitle:@"Cancel" otherButtonTitles:nil, nil] show];
                                else
                                    [[[UIAlertView alloc] initWithTitle:@"" message:@"Claim Item Added Successfully" delegate:self cancelButtonTitle:@"Dismiss" otherButtonTitles:nil, nil] show];
                            });
                        });
                    }else
                        [[[UIAlertView alloc] initWithTitle:@"" message:@"Description is required" delegate:nil cancelButtonTitle:@"Dismiss" otherButtonTitles:nil, nil] show];
                }else
                    [[[UIAlertView alloc] initWithTitle:@"" message:@"Please select billable company" delegate:nil cancelButtonTitle:@"Dismiss" otherButtonTitles:nil, nil] show];
            }else
                [[[UIAlertView alloc] initWithTitle:@"" message:@"Project is required" delegate:nil cancelButtonTitle:@"Dismiss" otherButtonTitles:nil, nil] show];
        }else
            [[[UIAlertView alloc] initWithTitle:@"" message:@"Amount is required" delegate:nil cancelButtonTitle:@"Dismiss" otherButtonTitles:nil, nil] show];
    }else
        [[[UIAlertView alloc] initWithTitle:@"" message:@"Expense date is required" delegate:nil cancelButtonTitle:@"Dismiss" otherButtonTitles:nil, nil] show];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    if([segue.identifier isEqualToString:@"claimiteminputtoclaimitemattendees"]){
        ((VCClaimItemAttendees *)segue.destinationViewController).claimitemAttendees = _claimitemAttendees;
    }
}

@end
