//
//  HomeOverviewItem.m
//  Salt iOS
//
//  Created by Rick Royd Aban on 5/20/15.
//  Copyright (c) 2015 Rick Royd Aban. All rights reserved.
//

#import "HomeOverviewItem.h"

@interface HomeOverviewItem(){
    NSString *_name, *_label1, *_label2, *_label3;
    HomeOverviewItemCellType _cellType;
}
@end

@implementation HomeOverviewItem

- (HomeOverviewItem *)initWithName:(NSString *)name label1:(NSString *)label1 label2:(NSString *)label2 label3:(NSString *)label3 cellType:(HomeOverviewItemCellType)cellType{
    self = [super init];
    
    if(self) {
        _name = name;
        _label1 = label1;
        _label2 = label2;
        _label3 = label3;
        _cellType = cellType;
    }
    
    return self;
}

- (NSString *)propName{
    return _name;
}

- (NSString *)propLabel1{
    return _label1;
}

- (NSString *)propLabel2{
    return _label2;
}

- (NSString *)propLabel3{
    return _label3;
}

- (HomeOverviewItemCellType)propCellType{
    return _cellType;
}


@end
