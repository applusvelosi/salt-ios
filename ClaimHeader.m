//
//  ClaimHeader.m
//  Salt
//
//  Created by Rick Royd Aban on 2015/07/27.
//  Copyright (c) 2015 Applus Velosi. All rights reserved.
//

#import "ClaimHeader.h"
#import "AppDelegate.h"
#import "OnlineGateway.h"
#import "Staff.h"
#import "Office.h"
#import "Document.h"

@implementation ClaimHeader

+ (NSArray *)claimTypeDescs{
    return @[CLAIMTYPEDESC_CLAIMS, CLAIMTYPEDESC_ADVANCES, CLAIMTYPEDESC_LIQUIDATION];
}

+ (NSString *)typeDescForTypeKey:(int)key {
    if(key == CLAIMTYPEID_CLAIMS) return CLAIMTYPEDESC_CLAIMS;
    else if(key == CLAIMTYPEID_ADVANCES) return CLAIMTYPEDESC_ADVANCES;
    else return CLAIMTYPEDESC_LIQUIDATION;
}

+ (NSString *)statusDescForStatusKey:(int)statusID {
    switch (statusID) {
        case CLAIMSTATUSID_OPEN:
            return CLAIMSTATUSDESC_OPEN;
            break;
        case CLAIMSTATUSID_SUBMITTED:
            return CLAIMSTATUSDESC_SUBMITTED;
            break;
        case CLAIMSTATUSID_APPROVEDBYAPPROVER:
            return CLAIMSTATUSDESC_APPROVEDBYAPPROVER;
            break;
        case CLAIMSTATUSID_REJECTEDBYAPPROVER:
            return CLAIMSTATUSDESC_REJECTEDBYAPPROVER;
            break;
        case CLAIMSTATUSID_CANCELLED:
            return CLAIMSTATUSDESC_CANCELLED;
            break;
        case CLAIMSTATUSID_APPROVEDBYCM:
            return CLAIMSTATUSDESC_APPROVEDBYCM;
            break;
        case CLAIMSTATUSID_REJECTEDBYCM:
            return CLAIMSTATUSDESC_REJECTEDBYCM;
            break;
        case CLAIMSTATUSID_APPROVEDBYACCOUNTS:
            return CLAIMSTATUSDESC_APPROVEDBYACCOUNTS;
            break;
        case CLAIMSTATUSID_REJECTEDBYACCOUNTS:
            return CLAIMSTATUSDESC_REJECTEDBYACCOUNTS;
            break;
        case CLAIMSTATUSID_RETURN:
            return CLAIMSTATUSDESC_RETURN;
            break;
        case CLAIMSTATUSID_LIQUIDATED:
            return CLAIMSTATAUSDESC_LIQUIDATED;
            break;
        case CLAIMSTATUSID_PAIDUNDERCOMPANYCARD:
            return CLAIMSTATUSDESC_PAIDUNDERCOMPANYCARD;
            break;
        case CLAIMSTATUSID_REJECTEDFORSALARYDEDUCTION:
            return CLAIMSTATUSDESC_REJECTEDFORSALARYDEDUCTION;
            break;
        case CLAIMSTATUSID_PAID:
            return CLAIMSTATUSDESC_PAID;
            break;
        case CLAIMSTATUSID_SENTTOSAP:
            return CLAIMSTATUSDESC_SENTTOSAP;
            break;
        default:
            return @"";
            break;
    }
}


+ (int)typeKeyForTypeDesc:(NSString *)desc{
    if([desc isEqualToString:CLAIMTYPEDESC_CLAIMS]) return CLAIMTYPEID_CLAIMS;
    else if([desc isEqualToString:CLAIMTYPEDESC_ADVANCES]) return CLAIMTYPEID_ADVANCES;
    else return CLAIMTYPEID_LIQUIDATION;
}

+ (int)statusKeyForStatusDesc:(NSString *)desc{
    if([desc isEqualToString:CLAIMSTATUSDESC_OPEN]) return CLAIMSTATUSID_OPEN;
    else if([desc isEqualToString:CLAIMSTATUSDESC_SUBMITTED]) return CLAIMSTATUSID_SUBMITTED;
    else if([desc isEqualToString:CLAIMSTATUSDESC_APPROVEDBYAPPROVER]) return CLAIMSTATUSID_APPROVEDBYAPPROVER;
    else if([desc isEqualToString:CLAIMSTATUSDESC_REJECTEDBYAPPROVER]) return CLAIMSTATUSID_REJECTEDBYAPPROVER;
    else if([desc isEqualToString:CLAIMSTATUSDESC_CANCELLED]) return CLAIMSTATUSID_CANCELLED;
    else if([desc isEqualToString:CLAIMSTATUSDESC_PAID]) return CLAIMSTATUSID_PAID;
    else if([desc isEqualToString:CLAIMSTATUSDESC_APPROVEDBYCM]) return CLAIMSTATUSID_APPROVEDBYCM;
    else if([desc isEqualToString:CLAIMSTATUSDESC_REJECTEDBYCM]) return CLAIMSTATUSID_REJECTEDBYCM;
    else if([desc isEqualToString:CLAIMSTATUSDESC_REJECTEDBYACCOUNTS]) return CLAIMSTATUSID_REJECTEDBYACCOUNTS;
    else if([desc isEqualToString:CLAIMSTATUSDESC_APPROVEDBYACCOUNTS]) return CLAIMSTATUSID_APPROVEDBYACCOUNTS;
    else if([desc isEqualToString:CLAIMSTATUSDESC_RETURN]) return CLAIMSTATUSID_RETURN;
    else return CLAIMSTATUSID_PAIDUNDERCOMPANYCARD;
}

//Constructor for copying from another claim or deserializing from offline gateway
- (instancetype)initWithDictionary:(NSMutableDictionary *)claimHeaderDictionary {
    if(self = [super init])
        _claimHeader = [NSMutableDictionary dictionaryWithDictionary:claimHeaderDictionary];
    return self;
}

//Constructor for fetching claims from web service
- (instancetype)initWithJSONDictionary:(NSDictionary *)dictionary withAppDelegate:(AppDelegate *)appDelegate {
    self = [super init];
    if(self) {
        _claimHeader = [NSMutableDictionary dictionaryWithDictionary:dictionary];
        [self setApproversNote:[_claimHeader objectForKey:@"ApproversNote"]];
        [self setClaimStatus:[[_claimHeader objectForKey:@"ClaimStatus"] intValue]];
        [self setClaimCMId:[[_claimHeader objectForKey:@"ClaimCountryManagerId"] intValue]];
        [self setStatusName:[_claimHeader objectForKey:@"StatusName"]];
        [self setOfficeID:[[_claimHeader objectForKey:@"OfficeID"] intValue]];
        [self setClaimLineItems:[NSArray arrayWithArray:[_claimHeader objectForKey:@"ClaimLineItems"]]];
        [_claimHeader setObject:([_claimHeader objectForKey:@"PaymentMethod"] == [NSNull null])?@"":[_claimHeader objectForKey:@"PaymentMethod"] forKey:@"PaymentMethod"];
        [_claimHeader setObject:[appDelegate.propGatewayOnline deserializeJsonDateString:[_claimHeader objectForKey:@"DateCreated"]] forKey:@"DateCreated"];
        [_claimHeader setObject:[appDelegate.propGatewayOnline deserializeJsonDateString:[_claimHeader objectForKey:@"DateSubmitted"]] forKey:@"DateSubmitted"];
        [_claimHeader setObject:[appDelegate.propGatewayOnline deserializeJsonDateString:[_claimHeader objectForKey:@"DateApprovedByApprover"]] forKey:@"DateApprovedByApprover"];
        [_claimHeader setObject:[appDelegate.propGatewayOnline deserializeJsonDateString:[_claimHeader objectForKey:@"DateApprovedByDirector"]] forKey:@"DateApprovedByDirector"];
        [_claimHeader setObject:[appDelegate.propGatewayOnline deserializeJsonDateString:[_claimHeader objectForKey:@"DateApprovedByAccount"]] forKey:@"DateApprovedByAccount"];
        [_claimHeader setObject:[appDelegate.propGatewayOnline deserializeJsonDateString:[_claimHeader objectForKey:@"DateModified"]] forKey:@"DateModified"];
        [_claimHeader setObject:[appDelegate.propGatewayOnline deserializeJsonDateString:[_claimHeader objectForKey:@"DatePaid"]] forKey:@"DatePaid"];
        [_claimHeader setObject:[appDelegate.propGatewayOnline deserializeJsonDateString:[_claimHeader objectForKey:@"DateCancelled"]] forKey:@"DateCancelled"];
        [_claimHeader setObject:[appDelegate.propGatewayOnline deserializeJsonDateString:[_claimHeader objectForKey:@"DateRejected"]] forKey:@"DateRejected"];
        [_claimHeader setObject:[_claimHeader objectForKey:@"Documents"] forKey:@"Documents"];
    }
    return self;
}

//Constructor for creating new claim header
- (instancetype)initWithCostCenterID:(int)costCenterID costCenterName:(NSString *)costCenterName claimTypeID:(int)claimTypeID isPaidByCC:(BOOL)isPaidByCC baIDCharged:(int)baIDCharged bacNumber:(NSString *)bacNumber appDelegate:(AppDelegate *)appDelegate{
    if(self = [super init]){
        _claimHeader = [NSMutableDictionary dictionary];
        [_claimHeader setObject:@"" forKey:@"AccountManagerEmail"];
        [_claimHeader setObject:[appDelegate.getStaff propAccountName] forKey:@"AccountName"];
        [_claimHeader setObject:@"" forKey:@"AccountsEmail"]; //Server will do the email retriaval
        [_claimHeader setObject:@([appDelegate.getStaff propAccountID]) forKey:@"AccountsID"];
        [_claimHeader setObject:[NSNumber numberWithBool:true] forKey:@"Active"];
        [_claimHeader setObject:@([appDelegate.getStaff propExpenseApproverID]) forKey:@"ApproverID"];
        [_claimHeader setObject:[appDelegate.getStaff propExpenseApproverName] forKey:@"ApproverName"];
        [_claimHeader setObject:@"" forKey:@"ApproversEmail"]; //Server will do the email retriaval
        [_claimHeader setObject:@"" forKey:@"ApproversNote"];
        [_claimHeader setObject:@"" forKey:@"AttachedCER"];
        [_claimHeader setObject:bacNumber forKey:@"BACNumber"];
        [_claimHeader setObject:@(baIDCharged) forKey:@"BusinessAdvanceIDCharged"];
        [_claimHeader setObject:@(0) forKey:@"ClaimID"];
        //save this key's object as a serialized string since it will be deserialize in the claim item retrieval
        [_claimHeader setObject:[[NSString alloc] initWithData:[NSJSONSerialization dataWithJSONObject:[NSArray array] options:0 error:nil] encoding:NSUTF8StringEncoding] forKey:@"ClaimLineItems"];
        [_claimHeader setObject:@"" forKey:@"ClaimNumber"];
        [_claimHeader setObject:@(CLAIMSTATUSID_OPEN) forKey:@"ClaimStatus"];
        [_claimHeader setObject:@(claimTypeID) forKey:@"ClaimTypeID"]; //must be overriden by its children classes
        [_claimHeader setObject:@"" forKey:@"ClaimTypeName"];
        [_claimHeader setObject:@(costCenterID) forKey:@"CostCenterID"];
        [_claimHeader setObject:costCenterName forKey:@"CostCenterName"];
        [_claimHeader setObject:@(0) forKey:@"CountryManager"];
        [_claimHeader setObject:@"" forKey:@"CountryManagerEmail"];
        [_claimHeader setObject:@"" forKey:@"CountryManagerName"];
        [_claimHeader setObject:@([appDelegate.getStaff propStaffID]) forKey:@"CreatedBy"];
        [_claimHeader setObject:[appDelegate.getStaff propFullName] forKey:@"CreatedByName"];
        [_claimHeader setObject:@"" forKey:@"CurrencySymbol"];
        [_claimHeader setObject:@"\/Date(-2208988800000+0000)\/" forKey:@"DateApprovedByAccount"];
        [_claimHeader setObject:@"\/Date(-2208988800000+0000)\/" forKey:@"DateApprovedByApprover"];
        [_claimHeader setObject:@"\/Date(-2208988800000+0000)\/" forKey:@"DateApprovedByDirector"];
        [_claimHeader setObject:@"\/Date(-2208988800000+0000)\/" forKey:@"DateCancelled"];
        [_claimHeader setObject:[self epochizeDate:[appDelegate.propFormatVelosiDate stringFromDate:[NSDate date]]] forKey:@"DateCreated"];
        [_claimHeader setObject:@"\/Date(-2208988800000+0000)\/" forKey:@"DateModified"];
        [_claimHeader setObject:@"\/Date(-2208988800000+0000)\/" forKey:@"DatePaid"];
        [_claimHeader setObject:@"\/Date(-2208988800000+0000)\/" forKey:@"DateRejected"];
        [_claimHeader setObject:[self epochizeDate:[appDelegate.propFormatVelosiDate stringFromDate:[NSDate date]]]  forKey:@"DateSubmitted"];
        [_claimHeader setObject:@"" forKey:@"DocName"];
        [_claimHeader setObject:[[NSString alloc] initWithData:[NSJSONSerialization dataWithJSONObject:[NSArray array] options:0 error:nil] encoding:NSUTF8StringEncoding] forKey:@"Documents"];
        [_claimHeader setObject:@"" forKey:@"HasYesNoDoc"];
        [_claimHeader setObject:@"" forKey:@"HROfficerEmail"];
        [_claimHeader setObject:[NSNumber numberWithBool:true] forKey:@"IsPaidByCompanyCC"];
        [_claimHeader setObject:@([appDelegate.getStaff propStaffID]) forKey:@"ModifiedBy"];
        [_claimHeader setObject:[appDelegate.getStaff propFullName] forKey:@"ModifiedByName"];
        [_claimHeader setObject:@([appDelegate.office officeID]) forKey:@"OfficeID"];
        [_claimHeader setObject:[appDelegate.office officeName] forKey:@"OfficeName"];
        [_claimHeader setObject:@([appDelegate.office officeID]) forKey:@"OfficeToCharge"];
        [_claimHeader setObject:@(0) forKey:@"ParentClaimID"];
        [_claimHeader setObject:@"" forKey:@"ParentClaimNumber"];
        [_claimHeader setObject:@(0) forKey:@"RejectedBy"];
        [_claimHeader setObject:@"" forKey:@"RejectedByName"];
        [_claimHeader setObject:@"" forKey:@"SAPExpenseID"];
        [_claimHeader setObject:@"" forKey:@"SAPPaymentID"];
        [_claimHeader setObject:[appDelegate.getStaff propEmail] forKey:@"StaffEmail"];
        [_claimHeader setObject:@([appDelegate.getStaff propStaffID]) forKey:@"StaffID"];
        [_claimHeader setObject:[appDelegate.getStaff propFullName] forKey:@"StaffName"];
        [_claimHeader setObject:CLAIMSTATUSDESC_OPEN forKey:@"StatusName"];
        [_claimHeader setObject:@(0) forKey:@"TotalAmount"];
        [_claimHeader setObject:@(0) forKey:@"TotalAmountInLC"];
        [_claimHeader setObject:@(0) forKey:@"TotalComputedApprovedInLC"];
        [_claimHeader setObject:@(0) forKey:@"TotalComputedForDeductionInLC"];
        [_claimHeader setObject:@(0) forKey:@"TotalComputedForPaymentInLC"];
        [_claimHeader setObject:@(0) forKey:@"TotalÇomputedInLC"];
        [_claimHeader setObject:@(0) forKey:@"TotalComputedRejectedInLC"];
    }
    return self;
}

//for creating existing claim header
- (instancetype)initWithParentClaimID:(int)parentClaimID parentClaimNumber:(NSString*)parentNumber isPaidByCC:(BOOL)isPaid appDelegate:(AppDelegate *)appDelegate{
    if(self = [super init]){
        NSMutableDictionary *tempClaimHeader = [NSMutableDictionary dictionaryWithDictionary:_claimHeader];
        [tempClaimHeader setObject:@"" forKey:@"AccountManagerEmail"];
        [tempClaimHeader setObject:[appDelegate.getStaff propAccountName] forKey:@"AccountName"];
        [tempClaimHeader setObject:@"" forKey:@"AccountsEmail"];
        [tempClaimHeader setObject:@([appDelegate.getStaff propAccountID]) forKey:@"AccountsID"];
        [tempClaimHeader setObject:[NSNumber numberWithBool:true] forKey:@"Active"];
        [tempClaimHeader setObject:@([appDelegate.getStaff propExpenseApproverID]) forKey:@"ApproverID"];
        [tempClaimHeader setObject:[appDelegate.getStaff propExpenseApproverName] forKey:@"ApproverName"];
        [tempClaimHeader setObject:[tempClaimHeader objectForKey:@"ApproversEmail"] forKey:@"ApproversEmail"];
        [tempClaimHeader setObject:@"" forKey:@"ApproversNote"];
        [tempClaimHeader setObject:@"" forKey:@"AttachedCER"];
        [tempClaimHeader setObject:[tempClaimHeader objectForKey:@"BACNumber"] forKey:@"BACNumber"];
        [tempClaimHeader setObject:@([[tempClaimHeader objectForKey:@"BusinessAdvanceIDCharged"] intValue]) forKey:@"BusinessAdvanceIDCharged"];
        [tempClaimHeader setObject:@([[tempClaimHeader objectForKey:@"ClaimID"] intValue]) forKey:@"ClaimID"];
        [tempClaimHeader setObject:[[tempClaimHeader objectForKey:@"ClaimLineItems"] array] forKey:@"ClaimLineItems"];
        [tempClaimHeader setObject:[tempClaimHeader objectForKey:@"ClaimNumber"] forKey:@"ClaimNumber"];
        [tempClaimHeader setObject:@([[tempClaimHeader objectForKey:@"ClaimStatus"] intValue]) forKey:@"ClaimStatus"];
        [tempClaimHeader setObject:@([[tempClaimHeader objectForKey:@"ClaimTypeID"] intValue]) forKey:@"ClaimTypeID"];
        [tempClaimHeader setObject:[tempClaimHeader objectForKey:@"ClaimTypeName"] forKey:@"ClaimTypeName"];
        [tempClaimHeader setObject:@([[tempClaimHeader objectForKey:@"CostCenterID"] intValue]) forKey:@"CostCenterID"];
        [tempClaimHeader setObject:[tempClaimHeader objectForKey:@"CostCenterName"] forKey:@"CostCenterName"];
        [tempClaimHeader setObject:@([[tempClaimHeader objectForKey:@"CountryManager"] intValue]) forKey:@"CountryManager"];
        [tempClaimHeader setObject:[tempClaimHeader objectForKey:@"CountryManagerEmail"] forKey:@"CountryManagerEmail"];
        [tempClaimHeader setObject:[tempClaimHeader objectForKey:@"CountryManagerName"] forKey:@"CountryManagerName"];
        [tempClaimHeader setObject:@([appDelegate.getStaff propStaffID]) forKey:@"CreatedBy"];
        [tempClaimHeader setObject:[appDelegate.getStaff propFullName] forKey:@"CreatedByName"];
        [tempClaimHeader setObject:[tempClaimHeader objectForKey:@"CurrencySymbol"] forKey:@"CurrencySymbol"];
        [tempClaimHeader setObject:[appDelegate.propGatewayOnline convertToUnixDate:[appDelegate.propDateFormatByProcessed dateFromString:[tempClaimHeader objectForKey:@"DateApprovedByAccount"]]] forKey:@"DateApprovedByAccount"];
        [tempClaimHeader setObject:[appDelegate.propGatewayOnline convertToUnixDate:[appDelegate.propDateFormatByProcessed dateFromString:[tempClaimHeader objectForKey:@"DateApprovedByApprover"]]] forKey:@"DateApprovedByApprover"];
        [tempClaimHeader setObject:[appDelegate.propGatewayOnline convertToUnixDate:[appDelegate.propDateFormatByProcessed dateFromString:[tempClaimHeader objectForKey:@"DateApprovedByDirector"]]] forKey:@"DateApprovedByDirector"];
        [tempClaimHeader setObject:[appDelegate.propGatewayOnline convertToUnixDate:[appDelegate.propDateFormatByProcessed dateFromString:[tempClaimHeader objectForKey:@"DateCancelled"]]] forKey:@"DateCancelled"];
        [tempClaimHeader setObject:[appDelegate.propGatewayOnline convertToUnixDate:[appDelegate.propDateFormatByProcessed dateFromString:[tempClaimHeader objectForKey:@"DateCreated"]]] forKey:@"DateCreated"];
        [tempClaimHeader setObject:[appDelegate.propGatewayOnline convertToUnixDate:[appDelegate.propDateFormatByProcessed dateFromString:[tempClaimHeader objectForKey:@"DateModified"]]] forKey:@"DateModified"];
        [tempClaimHeader setObject:[appDelegate.propGatewayOnline convertToUnixDate:[appDelegate.propDateFormatByProcessed dateFromString:[tempClaimHeader objectForKey:@"DatePaid"]]] forKey:@"DatePaid"];
        [tempClaimHeader setObject:[appDelegate.propGatewayOnline convertToUnixDate:[appDelegate.propDateFormatByProcessed dateFromString:[tempClaimHeader objectForKey:@"DateRejected"]]] forKey:@"DateRejected"];
        [tempClaimHeader setObject:[appDelegate.propGatewayOnline convertToUnixDate:[appDelegate.propDateFormatByProcessed dateFromString:[tempClaimHeader objectForKey:@"DateSubmitted"]]]  forKey:@"DateSubmitted"];
        [tempClaimHeader setObject:@"" forKey:@"DocName"];
        [tempClaimHeader setObject:[[tempClaimHeader objectForKey:@"Documents"] array] forKey:@"Documents"];
        [tempClaimHeader setObject:@([[tempClaimHeader objectForKey:@"HasYesNoDoc"] intValue]) forKey:@"HasYesNoDoc"];
        [tempClaimHeader setObject:[tempClaimHeader objectForKey:@"HROfficerEmail"] forKey:@"HROfficerEmail"];
        [tempClaimHeader setObject:[NSNumber numberWithBool:isPaid] forKey:@"IsPaidByCompanyCC"];
        [tempClaimHeader setObject:@([[tempClaimHeader objectForKey:@"StaffID"] intValue]) forKey:@"ModifiedBy"];
        [tempClaimHeader setObject:[tempClaimHeader objectForKey:@"ModifiedByName"] forKey:@"ModifiedByName"];
        [tempClaimHeader setObject:@([appDelegate.office officeID]) forKey:@"OfficeID"];
        [tempClaimHeader setObject:[appDelegate.office officeName] forKey:@"OfficeName"];
        [tempClaimHeader setObject:@([appDelegate.office officeID]) forKey:@"OfficeToCharge"];
        [tempClaimHeader setObject:@(parentClaimID) forKey:@"ParentClaimID"];
        [tempClaimHeader setObject:parentNumber forKey:@"ParentClaimNumber"];
        [tempClaimHeader setObject:[tempClaimHeader objectForKey:@"PaymentMethod"] forKey:@"PaymentMethod"];
        [tempClaimHeader setObject:@([[tempClaimHeader objectForKey:@"RejectedBy"] intValue]) forKey:@"RejectedBy"];
        [tempClaimHeader setObject:[tempClaimHeader objectForKey:@"RejectedByName"] forKey:@"RejectedByName"];
        [tempClaimHeader setObject:[tempClaimHeader objectForKey:@"SAPExpenseID"] forKey:@"SAPExpenseID"];
        [tempClaimHeader setObject:[tempClaimHeader objectForKey:@"SAPPaymentID"] forKey:@"SAPPaymentID"];
        [tempClaimHeader setObject:[appDelegate.getStaff propEmail] forKey:@"StaffEmail"];
        [tempClaimHeader setObject:@([[tempClaimHeader objectForKey:@"StaffID"] intValue]) forKey:@"StaffID"];
        [tempClaimHeader setObject:[tempClaimHeader objectForKey:@"StaffName"] forKey:@"StaffName"];
        [tempClaimHeader setObject:[tempClaimHeader objectForKey:@"StatusName"] forKey:@"StatusName"];
        [tempClaimHeader setObject:@([[tempClaimHeader objectForKey:@"TotalAmount"] intValue]) forKey:@"TotalAmount"];
        [tempClaimHeader setObject:@([[tempClaimHeader objectForKey:@"TotalAmountInLC"] intValue]) forKey:@"TotalAmountInLC"];
        [tempClaimHeader setObject:@([[tempClaimHeader objectForKey:@"TotalComputedApprovedInLC"] intValue]) forKey:@"TotalComputedApprovedInLC"];
        [tempClaimHeader setObject:@([[tempClaimHeader objectForKey:@"TotalComputedApprovedInLC"] intValue]) forKey:@"TotalComputedForDeductionInLC"];
        [tempClaimHeader setObject:@([[tempClaimHeader objectForKey:@"TotalComputedForPaymentInLC"] intValue]) forKey:@"TotalComputedForPaymentInLC"];
        [tempClaimHeader setObject:@([[tempClaimHeader objectForKey:@"TotalÇomputedInLC"] intValue]) forKey:@"TotalÇomputedInLC"];
        [tempClaimHeader setObject:@([[tempClaimHeader objectForKey:@"TotalComputedRejectedInLC"] intValue]) forKey:@"TotalComputedRejectedInLC"];
    }
    return self;
}

#pragma mark -
#pragma mark === Helper methods ===
#pragma mark -
-(NSString *)formatDateToBeViewed:(NSString *)dateFromStr appDelegate:(AppDelegate *)app{
    NSInteger janOneTwoThousand = 946684800000/1000; //Jan 1, 2000 GMT in epoch time
    NSDate *jan1 = [NSDate dateWithTimeIntervalSince1970:janOneTwoThousand];
    NSDate *dateFromString = [app.propDateFormatByProcessed dateFromString:dateFromStr];
    if ([dateFromString compare:jan1] == NSOrderedDescending || [dateFromString compare:jan1] == NSOrderedSame) //Date Before Jan 1, 2000 must not be viewed
        return [app.propDateFormatByProcessed stringFromDate:dateFromString];
    return @"";
}

#pragma mark -
#pragma mark === Accessor methods ===
#pragma mark -
- (NSString *)propApproversNote {
    return [_claimHeader objectForKey:@"ApproversNote"];
}

- (int)propCMId {
    return [[_claimHeader objectForKey:@"CountryManager"] intValue];
}

- (NSString *)propDateCreated:(AppDelegate *)appdelegate {
    return [self formatDateToBeViewed:[_claimHeader objectForKey:@"DateCreated"] appDelegate:appdelegate];
}

- (NSString *)propDateSubmitted:(AppDelegate *)appdelegate {
    return [self formatDateToBeViewed:[_claimHeader objectForKey:@"DateSubmitted"] appDelegate:appdelegate];
}

- (NSString *)propDateApprovedByApprover:(AppDelegate *)appdelegate {
    return [self formatDateToBeViewed:[_claimHeader objectForKey:@"DateApprovedByApprover"] appDelegate:appdelegate];
}

- (NSString *)propDateApprovedByAccount:(AppDelegate *)appdelegate {
    return [self formatDateToBeViewed:[_claimHeader objectForKey:@"DateApprovedByAccount"] appDelegate:appdelegate];
}

-(int)propClaimID {
    return [[_claimHeader objectForKey:@"ClaimID"] intValue];
}

- (NSString *)propDateApprovedByDirector:(AppDelegate *)appdelegate {
    return [self formatDateToBeViewed:[_claimHeader objectForKey:@"DateApprovedByDirector"] appDelegate:appdelegate];
}
- (NSString *)propDateModified:(AppDelegate *)appdelegate {
    return [self formatDateToBeViewed:[_claimHeader objectForKey:@"DateModified"] appDelegate:appdelegate];
}
- (NSString *)propDateCancelled:(AppDelegate *)appdelegate {
    return [self formatDateToBeViewed:[_claimHeader objectForKey:@"DateCancelled"] appDelegate:appdelegate];
}
- (NSString *)propDateRejected:(AppDelegate *)appdelegate {
    return [self formatDateToBeViewed:[_claimHeader objectForKey:@"DateRejected"] appDelegate:appdelegate];
}
- (NSString *)propDatePaid:(AppDelegate *)appdelegate {
    return [self formatDateToBeViewed:[_claimHeader objectForKey:@"DatePaid"] appDelegate:appdelegate];
}
- (NSMutableArray *)propDocuments{
    NSMutableArray *docArray = [NSMutableArray array];
    NSArray *jsonAttachment = [_claimHeader objectForKey:@"Documents"];
    for (NSDictionary *jsonDoc in jsonAttachment)
        [docArray addObject:[[Document alloc] initWithDictionary:jsonDoc]];
    return docArray;
}
- (NSDictionary *)savableDictionary {
    return _claimHeader;
}
-(int)propHasYesNoDoc{
    return [[_claimHeader objectForKey:@"HasYesNoDoc"] intValue];
}
- (void)updateCostCenter:(CostCenter *)costCenter{
    [_claimHeader setObject:@(costCenter.propCostCenterID) forKey:@"CostCenterID"];
    [_claimHeader setObject:costCenter.propCostCenterName forKey:@"CostCenterName"];
}
- (NSString *)propAttachedCert {
    if( [self propDocuments].count > 0)
        return [[[self propDocuments] objectAtIndex:0] propOrigDocName];
    else
        return NO_ATTACHMENT;
}

- (NSString *)propClaimNumber { return [_claimHeader objectForKey:@"ClaimNumber"];
}
- (int)propStaffID{ return [[_claimHeader objectForKey:@"StaffID"] intValue]; }
- (NSString *)propStaffName{ return [_claimHeader objectForKey:@"StaffName"]; }
//- (int)propOfficeID{ return [[_claimHeader objectForKey:@"OfficeID"] intValue]; }
- (NSString *)propOfficeName{ return [_claimHeader objectForKey:@"OfficeName"]; }
- (int)propCostCenterID{ return [[_claimHeader objectForKey:@"CostCenterID"] intValue]; }
- (NSString *)propCostCenterName{ return [_claimHeader objectForKey:@"CostCenterName"]; }
- (int)propApproverID{ return [[_claimHeader objectForKey:@"ApproverID"] intValue]; }
- (NSString *)propApproverName{
    return [_claimHeader objectForKey:@"ApproverName"];
}

- (NSString *)propRequesterName{
    return [_claimHeader objectForKey:@"CreatedByName"];
}
- (BOOL)propIsPaidByCC{
    return ([[_claimHeader objectForKey:@"IsPaidByCompanyCC"] boolValue]);
}
- (NSString *)propApproverEmail {
    return [_claimHeader objectForKey:@"ApproversEmail"];
}
- (int)propAccountID {
    return [[_claimHeader objectForKey:@"AccountsID"] intValue];
}
- (NSString *)propAccountEmail {
    return [_claimHeader objectForKey:@"AccountsEmail"];
}
- (int)propTypeID{ return [[_claimHeader objectForKey:@"ClaimTypeID"] intValue]; }
- (NSString *)propTypeName{ return [ClaimHeader typeDescForTypeKey:[self propTypeID]]; }
- (int)propStatusID{ return [[_claimHeader objectForKey:@"ClaimStatus"] intValue]; }

- (NSString *)propStatusName {
    return [ClaimHeader statusDescForStatusKey:[self propStatusID]];
}

- (NSString *)propCurrencyThree {
    return [_claimHeader objectForKey:@"CurrencySymbol"];
}

- (int)propTotalClaim {
    return [[_claimHeader objectForKey:@"TotalAmount"] intValue];
}

-(float)propTotalAmountInLC{
    return [[_claimHeader objectForKey:@"TotalAmountInLC"] floatValue];
}

- (int)propApprovedAmount {
    return [[_claimHeader objectForKey:@"TotalComputedApprovedInLC"] intValue];
}

- (int)propRejectedAmount {
    return [[_claimHeader objectForKey:@"TotalComputedRejectedInLC"] intValue];
}

-(int)propParentClaimID {
    return [[_claimHeader objectForKey:@"ParentClaimID"] intValue];
}
- (NSString *)propParentClaimNumber{
    return [_claimHeader objectForKey:@"ParentClaimNumber"];
}
//- (void)updateStatusWithAppdelegate:(AppDelegate *)appdelegate statusID:(int)statusID updatableDate:(NSString *)updatableDate{
//    int staffID = appdelegate.getStaff.propStaffID;
//    NSString *staffName = appdelegate.getStaff.propFullName;
//    NSString *latestDate = [appdelegate.propGatewayOnline epochizeDate:[NSDate date]];
//    [_claimHeader setObject:@(statusID) forKey:@"ClaimStatus"];
//    [_claimHeader setObject:[ClaimHeader statusDescForStatusKey:statusID] forKey:@"StatusName"];
//    [_claimHeader setObject:@(staffID) forKey:@"ModifiedBy"];
//    [_claimHeader setObject:staffName forKey:@"ModifiedByName"];
//    [_claimHeader setObject:latestDate forKey:updatableDate];
//}

- (NSArray *)dictForWebServices{ //the claim item must be converted into its array form instead of our default string form
//    NSMutableDictionary *tempDictionary = [NSMutableDictionary dictionaryWithDictionary:_claimHeader];
//    [tempDictionary setObject:[NSArray array] forKey:@"ClaimLineItems"];
    NSArray *tempClaimItems = [NSArray arrayWithArray:[_claimHeader objectForKey:@"ClaimLineItems"]];
    return tempClaimItems;
}

- (NSString *)epochizeDate:(NSString *)dateStr{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    return [appDelegate.propGatewayOnline convertToUnixDate:[appDelegate.propFormatVelosiDate dateFromString:dateStr]];
}

//- (NSString *)jsonClaimChildForApprovalWebServicesWithParentWithStatusID:(int)statusID parentClaimID:(int)parentID parentClaimNumber:(NSString *)parentNumber statusDesc:(NSString*)statusDesc keyForUpdatableDate:(NSString *)keyForUpdatableDate approversNote:(NSString *)approverNote withAppDelegate:(AppDelegate *)appDelegate {}

//for creating parent/existing claim header
- (NSString *)jsonClaimParentForApprovalWebServicesWithStatusID:(int)statusID statusDesc:(NSString*)description keyForUpdatableDate:(NSString *)keyForUpdatableDate approversNote:(NSString *)approverNote withAppDelegate:(AppDelegate*)appDelegate {
    NSMutableString *json = [NSMutableString string];
    [json appendString:@"{"];
    [json appendString:[NSString stringWithFormat:@"\"AccountManagerEmail\":\"%@\",", [_claimHeader objectForKey:@"AccountManagerEmail"]]];
    [json appendString:[NSString stringWithFormat:@"\"AccountName\":\"%@\",", [_claimHeader objectForKey:@"AccountName"]]];
    [json appendString:[NSString stringWithFormat:@"\"AccountsEmail\":\"%@\",", [_claimHeader objectForKey:@"AccountsEmail"]]];
    [json appendString:[NSString stringWithFormat:@"\"AccountsID\":%d,", [[_claimHeader objectForKey:@"AccountsID"] intValue]]];
    [json appendString:@"\"Active\":true,"];
    [json appendString:[NSString stringWithFormat:@"\"ApproverID\":%d,",[[_claimHeader objectForKey:@"ApproverID"] intValue]]];
    [json appendString:[NSString stringWithFormat:@"\"ApproverName\":\"%@\",",[_claimHeader objectForKey:@"ApproverName"]]];
    [json appendString:[NSString stringWithFormat:@"\"ApproversEmail\":\"%@\",",[_claimHeader objectForKey:@"ApproversEmail"]]];
    [json appendString:[NSString stringWithFormat:@"\"ApproversNote\":\"%@\",", approverNote]];
    [json appendString:[NSString stringWithFormat:@"\"AttachedCER\":\"%@\",",[_claimHeader objectForKey:@"AttachedCER"]]];
    [json appendString:[NSString stringWithFormat:@"\"BACNumber\":\"%@\",",[_claimHeader objectForKey:@"BACNumber"]]];
    [json appendString:[NSString stringWithFormat:@"\"BusinessAdvanceIDCharged\":%d,",[[_claimHeader objectForKey:@"BusinessAdvanceIDCharged"] intValue]]];
    [json appendString:[NSString stringWithFormat:@"\"ClaimID\":%d,", [[_claimHeader objectForKey:@"ClaimID"] intValue]]];
    [json appendString:@"\"ClaimLineItems\":[],"];
    [json appendFormat:@"\"ClaimNumber\":\"%@\",", [_claimHeader objectForKey:@"ClaimNumber"]];
    [json appendString:[NSString stringWithFormat:@"\"ClaimStatus\":%d,", statusID]];
    [json appendString:[NSString stringWithFormat:@"\"ClaimTypeID\":%d,",[[_claimHeader objectForKey:@"ClaimTypeID"] intValue]]];
    [json appendString:[NSString stringWithFormat:@"\"ClaimTypeName\":\"%@\",", [_claimHeader objectForKey:@"ClaimTypeName"]]];
    [json appendString:[NSString stringWithFormat:@"\"CostCenterID\":%d,", [[_claimHeader objectForKey:@"CostCenterID"] intValue]]];
    [json appendString:[NSString stringWithFormat:@"\"CostCenterName\":\"%@\",", [_claimHeader objectForKey:@"CostCenterName"]]];
    [json appendString:[NSString stringWithFormat:@"\"CountryManager\":%d,", [[_claimHeader objectForKey:@"CountryManager"] intValue]]];
    [json appendString:[NSString stringWithFormat:@"\"CountryManagerEmail\":\"%@\",", [_claimHeader objectForKey:@"CountryManagerEmail"]]];
    [json appendString:[NSString stringWithFormat:@"\"CountryManagerName\":\"%@\",", [_claimHeader objectForKey:@"CountryManagerName"]]];
    [json appendString:[NSString stringWithFormat:@"\"CreatedBy\":%d,", [[_claimHeader objectForKey:@"CreatedBy"] intValue]]];
    [json appendString:[NSString stringWithFormat:@"\"CreatedByName\":\"%@\",", [_claimHeader objectForKey:@"CreatedByName"]]];
    [json appendString:[NSString stringWithFormat:@"\"DateApprovedByAccount\":\"%@\",", [appDelegate.propGatewayOnline convertToUnixDate:[appDelegate.propDateFormatByProcessed dateFromString:[_claimHeader objectForKey:@"DateApprovedByAccount"]]]]];
    if ([_claimHeader objectForKey:keyForUpdatableDate]) {
        if ([keyForUpdatableDate isEqualToString:@"DateApprovedByApprover"]) {
            [json appendString:[NSString stringWithFormat:@"\"DateApprovedByDirector\":\"%@\",", [appDelegate.propGatewayOnline convertToUnixDate:[appDelegate.propDateFormatByProcessed dateFromString:[_claimHeader objectForKey:@"DateApprovedByDirector"]]]]];
            [json appendFormat:@"\"DateApprovedByApprover\":\"%@\",",[appDelegate.propGatewayOnline epochizeDate:[NSDate date]]];
        }else if([keyForUpdatableDate isEqualToString:@"DateApprovedByDirector"]) {
            [json appendString:[NSString stringWithFormat:@"\"DateApprovedByApprover\":\"%@\",", [appDelegate.propGatewayOnline convertToUnixDate:[appDelegate.propDateFormatByProcessed dateFromString:[_claimHeader objectForKey:@"DateApprovedByApprover"]]]]];
            [json appendFormat:@"\"DateApprovedByDirector\":\"%@\",",[appDelegate.propGatewayOnline epochizeDate:[NSDate date]]];
        }
    }
    [json appendString:[NSString stringWithFormat:@"\"DateCancelled\":\"%@\",", [appDelegate.propGatewayOnline convertToUnixDate:[appDelegate.propDateFormatByProcessed dateFromString:[_claimHeader objectForKey:@"DateCancelled"]]]]];
    NSLog(@"Date created %@",[_claimHeader objectForKey:@"DateCreated"]);
    [json appendString:[NSString stringWithFormat:@"\"DateCreated\":\"%@\",", [appDelegate.propGatewayOnline convertToUnixDate:[appDelegate.propDateFormatByProcessed dateFromString:[_claimHeader objectForKey:@"DateCreated"]]]]];
    [json appendString:[NSString stringWithFormat:@"\"DateModified\":\"%@\",", [appDelegate.propGatewayOnline convertToUnixDate:[appDelegate.propDateFormatByProcessed dateFromString:[_claimHeader objectForKey:@"DateModified"]]]]];
    [json appendString:[NSString stringWithFormat:@"\"DatePaid\":\"%@\",", [appDelegate.propGatewayOnline convertToUnixDate:[appDelegate.propDateFormatByProcessed dateFromString:[_claimHeader objectForKey:@"DatePaid"]]]]];
    [json appendString:[NSString stringWithFormat:@"\"DateRejected\":\"%@\",", [appDelegate.propGatewayOnline convertToUnixDate:[appDelegate.propDateFormatByProcessed dateFromString:[_claimHeader objectForKey:@"DateRejected"]]]]];
    [json appendString:[NSString stringWithFormat:@"\"DateSubmitted\":\"%@\",", [appDelegate.propGatewayOnline convertToUnixDate:[appDelegate.propDateFormatByProcessed dateFromString:[_claimHeader objectForKey:@"DateSubmitted"]]]]];
    [json appendString:[NSString stringWithFormat:@"\"DocName\":\"%@\",", [_claimHeader objectForKey:@"DocName"]]];
    [json appendString:[NSString stringWithFormat:@"\"Documents\":%@,", [[NSString alloc] initWithData:[NSJSONSerialization dataWithJSONObject:[_claimHeader objectForKey:@"Documents"] options:0 error:nil] encoding:NSUTF8StringEncoding]]];
     [json appendString:[NSString stringWithFormat:@"\"HasYesNoDoc\":%d,", [[_claimHeader objectForKey:@"HasYesNoDoc"] intValue]]];
    [json appendString:[NSString stringWithFormat:@"\"HROfficerEmail\":\"%@\",", [_claimHeader objectForKey:@"HROfficerEmail"]]];
    [json appendString:([[_claimHeader objectForKey:@"IsPaidByCompanyCC"] boolValue])?@"\"IsPaidByCompanyCC\":true,":@"\"IsPaidByCompanyCC\":false,"];
    [json appendString:[NSString stringWithFormat:@"\"ModifiedBy\":%d,", [[_claimHeader objectForKey:@"ModifiedBy"] intValue]]];
    [json appendString:[NSString stringWithFormat:@"\"ModifiedByName\":\"%@\",", [_claimHeader objectForKey:@"ModifiedByName"]]];
    [json appendString:[NSString stringWithFormat:@"\"OfficeID\":%d,", [[_claimHeader objectForKey:@"OfficeID"] intValue]]];
    [json appendString:[NSString stringWithFormat:@"\"OfficeName\":\"%@\",", [_claimHeader objectForKey:@"OfficeName"]]];
    [json appendString:[NSString stringWithFormat:@"\"OfficeToCharge\":%d,", [[_claimHeader objectForKey:@"OfficeToCharge"] intValue]]];
    [json appendString:[NSString stringWithFormat:@"\"ParentClaimID\":%d,", [[_claimHeader objectForKey:@"ParentClaimID"] intValue]]];
    [json appendString:[NSString stringWithFormat:@"\"ParentClaimNumber\":\"%@\",", [_claimHeader objectForKey:@"ParentClaimNumber"]]];
    [json appendString:[NSString stringWithFormat:@"\"PaymentMethod\":\"%@\",", [_claimHeader objectForKey:@"PaymentMethod"]]];
    [json appendString:[NSString stringWithFormat:@"\"RejectedBy\":%d,", [[_claimHeader objectForKey:@"RejectedBy"] intValue]]];
    [json appendString:[NSString stringWithFormat:@"\"RejectedByName\":\"%@\",", [_claimHeader objectForKey:@"RejectedByName"]]];
    [json appendString:[NSString stringWithFormat:@"\"SAPExpenseID\":\"%@\",", [_claimHeader objectForKey:@"SAPExpenseID"]]];
    [json appendString:[NSString stringWithFormat:@"\"SAPPaymentID\":\"%@\",", [_claimHeader objectForKey:@"SAPPaymentID"]]];
    [json appendString:[NSString stringWithFormat:@"\"SecondaryApproverEmail\":\"%@\"", [_claimHeader objectForKey:@"SecondaryApproverEmail"]]];
    [json appendString:[NSString stringWithFormat:@"\"SecondaryApproverId\":%d", [[_claimHeader objectForKey:@"SecondaryApproverId"] intValue]]];
    [json appendString:[NSString stringWithFormat:@"\"SecondaryApproverName\":\"%@\"", [_claimHeader objectForKey:@"SecondaryApproverName"]]];
    [json appendString:[NSString stringWithFormat:@"\"StaffEmail\":\"%@\",", [_claimHeader objectForKey:@"StaffEmail"]]];
    [json appendString:[NSString stringWithFormat:@"\"StaffID\":%d,", [[_claimHeader objectForKey:@"StaffID"] intValue]]];
    [json appendString:[NSString stringWithFormat:@"\"StaffName\":\"%@\",", [_claimHeader objectForKey:@"StaffName"]]];
    [json appendString:[NSString stringWithFormat:@"\"StatusName\":\"%@\",", description]];
    [json appendString:[NSString stringWithFormat:@"\"TotalAmount\":%d,", 0]];
    [json appendString:[NSString stringWithFormat:@"\"TotalAmountInLC\":%d,", 0]];
    [json appendString:[NSString stringWithFormat:@"\"TotalComputedApprovedInLC\":%d,", 0]];
    [json appendString:[NSString stringWithFormat:@"\"TotalComputedForDeductionInLC\":%d,", 0]];
    [json appendString:[NSString stringWithFormat:@"\"TotalComputedForPaymentInLC\":%d,", 0]];
    [json appendString:[NSString stringWithFormat:@"\"TotalComputedInLC\":%d,", 0]];
    [json appendString:[NSString stringWithFormat:@"\"TotalComputedRejectedInLC\":%d", 0]];
    [json appendString:@"}"];
    return json;
}

//for creating a splitted claim header and generating a claimID
- (NSString *)jsonClaimChildForApprovalWebServicesWithStatusID:(int)statusID
                                                       claimId:(int)claimId
                                                   claimNumber:(NSString*)claimNum
                                                 parentClaimID:(int)parentID
                                             parentClaimNumber:(NSString *)parentNumber
                                                     claimCMId:(int)nextApproverId
                                                    statusDesc:(NSString*)statusDesc
                                           keyForUpdatableDate:(NSString *)keyForUpdatableDate
                                                 approversNote:(NSString *)approverNote
                                               withAppDelegate:(AppDelegate *)appDelegate
{
    
    NSMutableString *json = [NSMutableString stringWithString:@"{"];
    [json appendString:[NSString stringWithFormat:@"\"AccountManagerEmail\":\"%@\",", [_claimHeader objectForKey:@"AccountManagerEmail"]]];
    [json appendString:[NSString stringWithFormat:@"\"AccountName\":\"%@\",", [_claimHeader objectForKey:@"AccountName"]]];
    [json appendString:[NSString stringWithFormat:@"\"AccountsEmail\":\"%@\",", [_claimHeader objectForKey:@"AccountsEmail"]]];
    [json appendString:[NSString stringWithFormat:@"\"AccountsID\":%d,", [[_claimHeader objectForKey:@"AccountsID"] intValue]]];
    [json appendString:@"\"Active\":true,"];
    [json appendString:[NSString stringWithFormat:@"\"ApproverID\":%d,",[[_claimHeader objectForKey:@"ApproverID"] intValue]]];
    [json appendString:[NSString stringWithFormat:@"\"ApproverName\":\"%@\",",[_claimHeader objectForKey:@"ApproverName"]]];
    [json appendString:[NSString stringWithFormat:@"\"ApproversEmail\":\"%@\",",[_claimHeader objectForKey:@"ApproversEmail"]]];
    [json appendString:[NSString stringWithFormat:@"\"ApproversNote\":\"%@\",", approverNote]];
    [json appendString:[NSString stringWithFormat:@"\"AttachedCER\":\"%@\",",[_claimHeader objectForKey:@"AttachedCER"]]];
    [json appendString:[NSString stringWithFormat:@"\"BACNumber\":\"%@\",",[_claimHeader objectForKey:@"BACNumber"]]];
    [json appendString:[NSString stringWithFormat:@"\"BusinessAdvanceIDCharged\":%d,",[[_claimHeader objectForKey:@"BusinessAdvanceIDCharged"] intValue]]];
    [json appendString:[NSString stringWithFormat:@"\"ClaimID\":%d,", claimId]];
    [json appendString:[NSString stringWithFormat:@"\"ClaimCountryManagerId\":%d,",nextApproverId]];
    [json appendString:@"\"ClaimLineItems\":[],"];
    [json appendString:[NSString stringWithFormat:@"\"ClaimNumber\":\"%@\",", claimNum]];
    [json appendString:[NSString stringWithFormat:@"\"ClaimStatus\":%d,", statusID]];
    [json appendString:[NSString stringWithFormat:@"\"ClaimTypeID\":%d,",[[_claimHeader objectForKey:@"ClaimTypeID"] intValue]]];
    [json appendString:[NSString stringWithFormat:@"\"ClaimTypeName\":\"%@\",", [_claimHeader objectForKey:@"ClaimTypeName"]]];
    [json appendString:[NSString stringWithFormat:@"\"CostCenterID\":%d,", [[_claimHeader objectForKey:@"CostCenterID"] intValue]]];
    [json appendString:[NSString stringWithFormat:@"\"CostCenterName\":\"%@\",", [_claimHeader objectForKey:@"CostCenterName"]]];
    [json appendString:[NSString stringWithFormat:@"\"CountryManager\":%d,", [[_claimHeader objectForKey:@"CountryManager"] intValue]]];
    [json appendString:[NSString stringWithFormat:@"\"CountryManagerEmail\":\"%@\",", [_claimHeader objectForKey:@"CountryManagerEmail"]]];
    [json appendString:[NSString stringWithFormat:@"\"CountryManagerName\":\"%@\",", [_claimHeader objectForKey:@"CountryManagerName"]]];
    [json appendString:[NSString stringWithFormat:@"\"CreatedBy\":%d,", [[_claimHeader objectForKey:@"CreatedBy"] intValue]]];
    [json appendString:[NSString stringWithFormat:@"\"CreatedByName\":\"%@\",", [_claimHeader objectForKey:@"CreatedByName"]]];
    [json appendString:[NSString stringWithFormat:@"\"CurrencySymbol\":\"%@\",", [_claimHeader objectForKey:@"CurrencySymbol"]]];
    if ([_claimHeader objectForKey:keyForUpdatableDate]) {
        if ([keyForUpdatableDate isEqualToString:@"DateApprovedByApprover"]) {
            [json appendString:[NSString stringWithFormat:@"\"DateApprovedByDirector\":\"%@\",", [appDelegate.propGatewayOnline convertToUnixDate:[appDelegate.propDateFormatByProcessed dateFromString:[_claimHeader objectForKey:@"DateApprovedByDirector"]]]]];
            [json appendFormat:@"\"DateApprovedByApprover\":\"%@\",",[appDelegate.propGatewayOnline epochizeDate:[NSDate date]]];
        }else if([keyForUpdatableDate isEqualToString:@"DateApprovedByDirector"]) {
            [json appendString:[NSString stringWithFormat:@"\"DateApprovedByApprover\":\"%@\",", [appDelegate.propGatewayOnline convertToUnixDate:[appDelegate.propDateFormatByProcessed dateFromString:[_claimHeader objectForKey:@"DateApprovedByApprover"]]]]];
            [json appendFormat:@"\"DateApprovedByDirector\":\"%@\",",[appDelegate.propGatewayOnline epochizeDate:[NSDate date]]];
        }
    }
    [json appendString:[NSString stringWithFormat:@"\"DateApprovedByAccount\":\"%@\",", [appDelegate.propGatewayOnline convertToUnixDate:[appDelegate.propDateFormatByProcessed dateFromString:[_claimHeader objectForKey:@"DateApprovedByAccount"]]]]];
    [json appendString:[NSString stringWithFormat:@"\"DateCancelled\":\"%@\",", [appDelegate.propGatewayOnline convertToUnixDate:[appDelegate.propDateFormatByProcessed dateFromString:[_claimHeader objectForKey:@"DateCancelled"]]]]];
    [json appendString:[NSString stringWithFormat:@"\"DateCreated\":\"%@\",", [appDelegate.propGatewayOnline epochizeDate:[NSDate date]]]];
    [json appendString:[NSString stringWithFormat:@"\"DateModified\":\"%@\",", [appDelegate.propGatewayOnline convertToUnixDate:[appDelegate.propDateFormatByProcessed dateFromString:[_claimHeader objectForKey:@"DateModified"]]]]];
    [json appendString:[NSString stringWithFormat:@"\"DatePaid\":\"%@\",", [appDelegate.propGatewayOnline convertToUnixDate:[appDelegate.propDateFormatByProcessed dateFromString:[_claimHeader objectForKey:@"DatePaid"]]]]];
    [json appendString:[NSString stringWithFormat:@"\"DateRejected\":\"%@\",", [appDelegate.propGatewayOnline convertToUnixDate:[appDelegate.propDateFormatByProcessed dateFromString:[_claimHeader objectForKey:@"DateRejected"]]]]];
    [json appendString:[NSString stringWithFormat:@"\"DateSubmitted\":\"%@\",", [appDelegate.propGatewayOnline convertToUnixDate:[appDelegate.propDateFormatByProcessed dateFromString:[_claimHeader objectForKey:@"DateSubmitted"]]]]];
    [json appendString:[NSString stringWithFormat:@"\"DocName\":\"%@\",", [_claimHeader objectForKey:@"DocName"]]];
    NSString *jsonizeAttachment = [[NSString alloc] initWithData:[NSJSONSerialization dataWithJSONObject:[_claimHeader objectForKey:@"Documents"] options:0 error:nil] encoding:NSUTF8StringEncoding];
    [json appendFormat:@"\"Documents\":%@,", jsonizeAttachment];
    [json appendString:[NSString stringWithFormat:@"\"HasYesNoDoc\":%d,", [[_claimHeader objectForKey:@"HasYesNoDoc"] intValue]]];
    [json appendString:[NSString stringWithFormat:@"\"HROfficerEmail\":\"%@\",", [_claimHeader objectForKey:@"HROfficerEmail"]]];
    [json appendString:([[_claimHeader objectForKey:@"IsPaidByCompanyCC"] boolValue])?@"\"IsPaidByCompanyCC\":true,":@"\"IsPaidByCompanyCC\":false,"];
    [json appendString:[NSString stringWithFormat:@"\"ModifiedBy\":%d,", [[_claimHeader objectForKey:@"ModifiedBy"] intValue]]];
    [json appendString:[NSString stringWithFormat:@"\"ModifiedByName\":\"%@\",", [_claimHeader objectForKey:@"ModifiedByName"]]];
    [json appendString:[NSString stringWithFormat:@"\"OfficeID\":%d,", [[_claimHeader objectForKey:@"OfficeID"] intValue]]];
    [json appendString:[NSString stringWithFormat:@"\"OfficeName\":\"%@\",", [_claimHeader objectForKey:@"OfficeName"]]];
    [json appendString:[NSString stringWithFormat:@"\"OfficeToCharge\":%d,", [[_claimHeader objectForKey:@"OfficeToCharge"] intValue]]];
    [json appendString:[NSString stringWithFormat:@"\"ParentClaimID\":%d,", parentID]];
    [json appendString:[NSString stringWithFormat:@"\"ParentClaimNumber\":\"%@\",", parentNumber]];
    [json appendString:[NSString stringWithFormat:@"\"PaymentMethod\":\"%@\",", [_claimHeader objectForKey:@"PaymentMethod"]]];
    [json appendString:[NSString stringWithFormat:@"\"RejectedBy\":%d,", [[_claimHeader objectForKey:@"RejectedBy"] intValue]]];
    [json appendString:[NSString stringWithFormat:@"\"RejectedByName\":\"%@\",", [_claimHeader objectForKey:@"RejectedByName"]]];
    [json appendString:[NSString stringWithFormat:@"\"SAPExpenseID\":\"%@\",", [_claimHeader objectForKey:@"SAPExpenseID"]]];
    [json appendString:[NSString stringWithFormat:@"\"SAPPaymentID\":\"%@\",", [_claimHeader objectForKey:@"SAPPaymentID"]]];
    [json appendString:[NSString stringWithFormat:@"\"SecondaryApproverEmail\":\"%@\",", [_claimHeader objectForKey:@"SecondaryApproverEmail"]]];
    [json appendString:[NSString stringWithFormat:@"\"SecondaryApproverId\":%d,", [[_claimHeader objectForKey:@"SecondaryApproverId"] intValue]]];
    [json appendString:[NSString stringWithFormat:@"\"SecondaryApproverName\":\"%@\",", [_claimHeader objectForKey:@"SecondaryApproverName"]]];
    [json appendString:[NSString stringWithFormat:@"\"StaffEmail\":\"%@\",", [_claimHeader objectForKey:@"StaffEmail"]]];
    [json appendString:[NSString stringWithFormat:@"\"StaffID\":%d,", [[_claimHeader objectForKey:@"StaffID"] intValue]]];
    [json appendString:[NSString stringWithFormat:@"\"StaffName\":\"%@\",", [_claimHeader objectForKey:@"StaffName"]]];
    [json appendString:[NSString stringWithFormat:@"\"StatusName\":\"%@\",", statusDesc]];
    [json appendString:[NSString stringWithFormat:@"\"TotalAmount\":%.1f,", 0.0f]];
    [json appendString:[NSString stringWithFormat:@"\"TotalAmountInLC\":%.1f,", 0.0f]];
    [json appendString:[NSString stringWithFormat:@"\"TotalComputedApprovedInLC\":%.1f,", 0.0f]];
    [json appendString:[NSString stringWithFormat:@"\"TotalComputedForDeductionInLC\":%.1f,", 0.0f]];
    [json appendString:[NSString stringWithFormat:@"\"TotalComputedForPaymentInLC\":%.1f,", 0.0f]];
    [json appendString:[NSString stringWithFormat:@"\"TotalComputedInLC\":%.1f,", 0.0f]];
    [json appendString:[NSString stringWithFormat:@"\"TotalComputedRejectedInLC\":%.1f", 0.0f]];
    [json appendString:@"}"];
    return json;
}

+ (NSString *)jsonFromNewEmptyClaimHeader{
    NSMutableDictionary *dict = [NSMutableDictionary dictionary];
    [dict setObject:@"" forKey:@"AccountManagerEmail"];
    [dict setObject:@"" forKey:@"AccountName"];
    [dict setObject:@"" forKey:@"AccountsEmail"]; //Server will do the email retriaval
    [dict setObject:@(0) forKey:@"AccountsID"];
    [dict setObject:[NSNumber numberWithBool:true] forKey:@"Active"];
    [dict setObject:@(0) forKey:@"ApproverID"];
    [dict setObject:@"" forKey:@"ApproverName"];
    [dict setObject:@"" forKey:@"ApproversEmail"]; //Server will do the email retriaval
    [dict setObject:@"" forKey:@"ApproversNote"];
    [dict setObject:@"" forKey:@"AttachedCER"];
    [dict setObject:@"" forKey:@"BACNumber"];
    [dict setObject:@(0) forKey:@"BusinessAdvanceIDCharged"];
    [dict setObject:@(0) forKey:@"ClaimID"];
    [dict setObject:[NSArray array] forKey:@"ClaimLineItems"]; //save this key's object as a serialized string since it will be deserialize in the claim item retrieval
    [dict setObject:@"" forKey:@"ClaimNumber"];
    [dict setObject:@(0) forKey:@"ClaimStatus"];
    [dict setObject:@(0) forKey:@"ClaimTypeID"]; //must be overriden by its children classes
    [dict setObject:@"" forKey:@"ClaimTypeName"];
    [dict setObject:@(0) forKey:@"CostCenterID"];
    [dict setObject:@"" forKey:@"CostCenterName"];
    [dict setObject:@(0) forKey:@"CountryManager"];
    [dict setObject:@"" forKey:@"CountryManagerEmail"];
    [dict setObject:@"" forKey:@"CountryManagerName"];
    [dict setObject:@(0) forKey:@"CreatedBy"];
    [dict setObject:@"" forKey:@"CreatedByName"];
    [dict setObject:@"" forKey:@"CurrencySymbol"];
    [dict setObject:@"/Date(-2208988800000+0000)/" forKey:@"DateApprovedByAccount"];
    [dict setObject:@"/Date(-2208988800000+0000)/" forKey:@"DateApprovedByApprover"];
    [dict setObject:@"/Date(-2208988800000+0000)/" forKey:@"DateApprovedByDirector"];
    [dict setObject:@"/Date(-2208988800000+0000)/" forKey:@"DateCancelled"];
    [dict setObject:@"/Date(-2208988800000+0000)/" forKey:@"DateCreated"];
    [dict setObject:@"/Date(-2208988800000+0000)/" forKey:@"DateModified"];
    [dict setObject:@"/Date(-2208988800000+0000)/" forKey:@"DatePaid"];
    [dict setObject:@"/Date(-2208988800000+0000)/" forKey:@"DateRejected"];
    [dict setObject:@"/Date(-2208988800000+0000)/"  forKey:@"DateSubmitted"];
    [dict setObject:@"" forKey:@"DocName"];
    [dict setObject:[NSArray array] forKey:@"Documents"];
    [dict setObject:@(0) forKey:@"HasYesNoDoc"];
    [dict setObject:@"" forKey:@"HROfficerEmail"];
    [dict setObject:[NSNumber numberWithBool:false] forKey:@"IsPaidByCompanyCC"];
    [dict setObject:@(0) forKey:@"ModifiedBy"];
    [dict setObject:@"" forKey:@"ModifiedByName"];
    [dict setObject:@(0) forKey:@"OfficeID"];
    [dict setObject:@"" forKey:@"OfficeName"];
    [dict setObject:@(0) forKey:@"OfficeToCharge"];
    [dict setObject:@(0) forKey:@"ParentClaimID"];
    [dict setObject:@"" forKey:@"ParentClaimNumber"];
    [dict setObject:@"" forKey:@"PaymentMethod"];
    [dict setObject:@(0) forKey:@"RejectedBy"];
    [dict setObject:@"" forKey:@"RejectedByName"];
    [dict setObject:@"" forKey:@"SAPExpenseID"];
    [dict setObject:@"" forKey:@"SAPPaymentID"];
    [dict setObject:@"" forKey:@"StaffEmail"];
    [dict setObject:@(0) forKey:@"StaffID"];
    [dict setObject:@"" forKey:@"StaffName"];
    [dict setObject:CLAIMSTATUSDESC_OPEN forKey:@"StatusName"];
    [dict setObject:@(0) forKey:@"TotalAmount"];
    [dict setObject:@(0) forKey:@"TotalAmountInLC"];
    [dict setObject:@(0) forKey:@"TotalComputedApprovedInLC"];
    [dict setObject:@(0) forKey:@"TotalComputedForDeductionInLC"];
    [dict setObject:@(0) forKey:@"TotalComputedForPaymentInLC"];
    [dict setObject:@(0) forKey:@"TotalÇomputedInLC"];
    [dict setObject:@(0) forKey:@"TotalComputedRejectedInLC"];
    return [[NSString alloc] initWithData:[NSJSONSerialization dataWithJSONObject:dict options:0 error:nil] encoding:NSUTF8StringEncoding];
}

- (NSString *)jsonClaimForApprovalWithStatusID:(int)statusID withKeyForUpdatableDate:(NSString *)keyForUpdatableDate approversNote:(NSString*)note withAppDelegate:(AppDelegate*)appDelegate {

    NSMutableDictionary *tempDict = [NSMutableDictionary dictionaryWithDictionary:self.claimHeader];
    NSString *currentDate = [appDelegate.propGatewayOnline epochizeDate:[NSDate date]];
    NSString *staffName = [[appDelegate getStaff] propFullName];
    
    [tempDict setObject:[NSNumber numberWithInteger:statusID] forKey:@"ClaimStatus"];
    [tempDict setObject:staffName forKey:@"ModifiedByName"];
    [tempDict setObject:@([[appDelegate getStaff] propStaffID]) forKey:@"ModifiedBy"];
    [tempDict setObject:note forKey:@"ApproversNote"];
    
    [tempDict setObject:[appDelegate.propGatewayOnline convertToUnixDate:[appDelegate.propDateFormatByProcessed dateFromString:[tempDict objectForKey:@"DateCreated"]]]  forKey:@"DateCreated"];
    [tempDict setObject:[appDelegate.propGatewayOnline convertToUnixDate:[appDelegate.propDateFormatByProcessed dateFromString:[tempDict objectForKey:@"DateSubmitted"]]] forKey:@"DateSubmitted"];
    [tempDict setObject:[appDelegate.propGatewayOnline convertToUnixDate:[appDelegate.propDateFormatByProcessed dateFromString:[tempDict objectForKey:@"DateApprovedByApprover"]]] forKey:@"DateApprovedByApprover"];
    [tempDict setObject:[appDelegate.propGatewayOnline convertToUnixDate:[appDelegate.propDateFormatByProcessed dateFromString:[tempDict objectForKey:@"DateApprovedByDirector"]]] forKey:@"DateApprovedByDirector"];
    [tempDict setObject:[appDelegate.propGatewayOnline convertToUnixDate:[appDelegate.propDateFormatByProcessed dateFromString:[tempDict objectForKey:@"DateApprovedByAccount"]]] forKey:@"DateApprovedByAccount"];
    [tempDict setObject:[appDelegate.propGatewayOnline convertToUnixDate:[appDelegate.propDateFormatByProcessed dateFromString:[tempDict objectForKey:@"DateModified"]]] forKey:@"DateModified"];
    [tempDict setObject:[appDelegate.propGatewayOnline convertToUnixDate:[appDelegate.propDateFormatByProcessed dateFromString:[tempDict objectForKey:@"DatePaid"]]] forKey:@"DatePaid"];
    [tempDict setObject:[appDelegate.propGatewayOnline convertToUnixDate:[appDelegate.propDateFormatByProcessed dateFromString:[tempDict objectForKey:@"DateCancelled"]]] forKey:@"DateCancelled"];
    [tempDict setObject:[appDelegate.propGatewayOnline convertToUnixDate:[appDelegate.propDateFormatByProcessed dateFromString:[tempDict objectForKey:@"DateRejected"]]] forKey:@"DateRejected"];
    if(![keyForUpdatableDate isEqualToString:@"NA"])
        [tempDict setObject:currentDate forKey:keyForUpdatableDate];
    [tempDict setObject:[ClaimHeader statusDescForStatusKey:statusID] forKey:@"StatusName"];
    
    return [[NSString alloc] initWithData:[NSJSONSerialization dataWithJSONObject:tempDict options:0 error:nil] encoding:NSUTF8StringEncoding];
    
}

@end
