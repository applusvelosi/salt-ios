//
//  Staff.m
//  Salt iOS
//
//  Created by Rick Royd Aban on 5/18/15.
//  Copyright (c) 2015 Rick Royd Aban. All rights reserved.
//

#import "Staff.h"

#define STAFF_SECURITYLEVEL_USER 1
#define STAFF_SECURITYLEVEL_MANAGER 2
#define STAFF_SECURITYLEVEL_ACCOUNT 3
#define STAFF_SECURITYLEVEL_ADMIN 4
#define STAFF_SECURITYLEVEL_COUNTRYMANAGER  5
#define STAFF_SECURITYLEVEL_ACCOUNTMANAGER 6

#define STAFF_GENDER_MALE @"Male"
#define STAFF_GENDER_FEMALE @"Female"

@interface Staff(){
    NSMutableDictionary *_staff;
}
@end

@implementation Staff

- (Staff *)initWithOnlineGateway:(OnlineGateway *)onlineGateway staffDictionary:(NSDictionary *)staffDictionary{
    
    self = [super init];
    if(self){
        _staff = [staffDictionary mutableCopy];
    }
    
    return self;
}

- (Staff *)initWithOfflineGateway:(OfflineGateway *)key staffDictionary:(NSDictionary *)staffDictionary{
    if([super init]){
        _staff = [staffDictionary mutableCopy];
    }
    
    return self;
}

- (NSMutableDictionary *)staffDictionary{
    return _staff;
}

- (NSString *)propAccountName{
    return [_staff objectForKey:@"AccountPersonName"]; }
- (int)propAccountID{ return [[_staff objectForKey:@"AccountsPerson"] intValue]; }
- (BOOL)propIsActive{ return [[_staff objectForKey:@"Active"] boolValue]; }
- (NSString *)propActiveDirectoryName{ return [_staff objectForKey:@"ActiveDirectoryUsername"]; }
- (float)propApproverLimit{ return [[_staff objectForKey:@"ApproverLimit"] floatValue]; }
- (float)propCarryOverLeave{ return [[_staff objectForKey:@"CarryOverLeave"] floatValue]; }
- (int)propCostCenterID{ return [[_staff objectForKey:@"CostCenterID"] intValue]; }
- (NSString *)propCostCenterName{ return [_staff objectForKey:@"CostCenterName"]; }
- (NSString *)propDateCreated{ return [_staff objectForKey:@"DateCreated"]; }
- (NSString *)propDateModified{ return [_staff objectForKey:@"DateModified"]; }
- (NSString *)propBirthdate{ return [_staff objectForKey:@"DateOfBirth"]; }
- (int)propDepartmentID{ return [[_staff objectForKey:@"DepartmentID"] intValue]; }
- (NSString *)propDepartmentName{ return [_staff objectForKey:@"DepartmentName"]; }
- (NSString *)propEmail{ return [_staff objectForKey:@"Email"]; }
- (NSString *)propEmploymentEndDate{ return [_staff objectForKey:@"EmploymentEndDate"]; }
- (NSString *)propEmploymentStartDate{ return [_staff objectForKey:@"EmploymentStartDate"]; }
- (int)propExpenseApproverID{ return [[_staff objectForKey:@"ExpApprover"] intValue]; }
- (NSString *)propExpenseApproverName{ return [_staff objectForKey:@"ExpenseApproverName"]; }
- (NSString *)propFirstName{ return [_staff objectForKey:@"FirstName"]; }
- (BOOL)propHasFriday{ return [[_staff objectForKey:@"Friday"] boolValue]; }
- (NSString *)propGender{ return [_staff objectForKey:@"GenderSex"]; }
- (BOOL)propISADAuth{ return [[_staff objectForKey:@"IsADAuth"] boolValue]; }
- (BOOL)propISAllowProxySubmission{ return [[_staff objectForKey:@"IsAllowProxySubmission"] boolValue]; }
- (BOOL)propIsApprover{ return [[_staff objectForKey:@"IsApprover"] boolValue]; }
- (BOOL)propIsCorporateApprover{ return [[_staff objectForKey:@"IsCorporateApprover"] boolValue]; }
- (BOOL)propIsRegional{ return [[_staff objectForKey:@"IsRegional"] boolValue]; }
- (NSString *)propJobTitle{ return [_staff objectForKey:@"JobTitle"]; }
- (NSString *)propLastName{ return [_staff objectForKey:@"LastName"]; }
- (int)propLeaveApprover1ID{ return [[_staff objectForKey:@"LeaveApprover1"] intValue]; }
- (NSString *)propLeaveApprover1Email{ return [_staff objectForKey:@"LeaveApprover1Email"]; }
- (NSString *)propLeaveApprover1Name{ return [_staff objectForKey:@"LeaveApprover1Name"]; }
- (int)propLeaveApprover2ID{ return [[_staff objectForKey:@"LeaveApprover2"] intValue]; }
- (NSString *)propLeaveApprover2Email{ return [_staff objectForKey:@"LeaveApprover2Email"]; }
- (NSString *)propLeaveApprover2Name{ return [_staff objectForKey:@"LeaveApprover2Name"]; }
- (int)propLeaveApprover3ID{ return [[_staff objectForKey:@"LeaveApprover3"] intValue]; }
- (NSString *)propLeaveApprover3Email{ return [_staff objectForKey:@"LeaveApprover3Email"]; }
- (NSString *)propLeaveApprover3Name{ return [_staff objectForKey:@"LeaveApprover3Name"]; }
- (NSString *)propLocation{ return [_staff objectForKey:@"Location"]; }
- (NSString *)propLockDate{ return [_staff objectForKey:@"LockDate"]; }
- (int)propMaxConsecutiveDays{ return [[_staff objectForKey:@"MaxConsecutiveDays"] intValue]; }
- (NSString *)propMobile{ return [_staff objectForKey:@"Mobile"]; }
- (BOOL)propHasMonday{ return [[_staff objectForKey:@"Monday"] boolValue]; }
- (NSString *)propNotes{ return [_staff objectForKey:@"Notes"]; }
- (NSString *)propOfficeAddress{ return [_staff objectForKey:@"OfficeAddress"]; }
- (int)propOfficeID{ return [[_staff objectForKey:@"OfficeID"] intValue]; }
- (NSString *)propOfficeName{ return [_staff objectForKey:@"OfficeName"]; }
- (NSString *)propPassword{ return [_staff objectForKey:@"Password"]; }
- (NSString *)propPayrollID{ return [_staff objectForKey:@"PayrollID"]; }
- (NSString *)propPhone{ return [_staff objectForKey:@"Phone"]; }
- (NSString *)propExt{ return [_staff objectForKey:@"PhoneExt"]; }
- (int)propPrefCurrencyID{ return [[_staff objectForKey:@"PrefCurrency"] intValue]; }
- (NSString *)propPrefCurrencyName{ return [_staff objectForKey:@"PrefCurrencyName"]; }
- (int)propProxyStaffID{ return [[_staff objectForKey:@"ProxyStaffID"] intValue]; }
- (NSString *)propProxyStaffName{ return [_staff objectForKey:@"ProxyStaffName"]; }
- (BOOL)propHasSaturday{ return [[_staff objectForKey:@"Saturday"] boolValue]; }
- (int)propSecurityLevel{ return [[_staff objectForKey:@"SecurityLevel"] intValue]; }
- (NSString *)propSecurityLevelName{ return [_staff objectForKey:@"SecurityLevelName"]; }
- (float)propSickLeaveAllowance{ return [[_staff objectForKey:@"SickLeaveAllowance"] floatValue]; }
- (int)propStaffID{ return [[_staff objectForKey:@"StaffID"] intValue]; }
- (NSString *)propStaffNumber{ return [_staff objectForKey:@"StaffNumber"]; }
- (int)propStatusID{ return [[_staff objectForKey:@"StatusID"] intValue]; }
- (NSString *)propStatusName{ return [_staff objectForKey:@"Status"]; }
- (BOOL)propHasSunday{
    return [[_staff objectForKey:@"Sunday"] boolValue];
}
- (BOOL)propHasThursday{ return [_staff objectForKey:@"Thursday"]; }
- (NSString *)propTitle{ return [_staff objectForKey:@"Title"]; }
- (float)propTotalVacationLeave{ return [[_staff objectForKey:@"TotalVacationLeave"] floatValue]; }
- (BOOL)propHasTuesday{ return [[_staff objectForKey:@"Tuesday"] boolValue]; }
- (float)propVacationLeaveAllowance{ return [[_staff objectForKey:@"VacationLeaveAllowance"] floatValue]; }
- (BOOL)propHasWednesday{ return [[_staff objectForKey:@"Wednesday"] boolValue]; }
- (int)propWorkingDays{ return [[_staff objectForKey:@"WorkingDays"] intValue]; }

- (NSString *)propFullName{ return [NSString stringWithFormat:@"%@ %@",[self propFirstName], [self propLastName]]; }
- (void)setUserPosition:(USERPOSITION)userPosition {
    [_staff setObject:@(userPosition) forKey:@"UserPosition"];
}
- (USERPOSITION)getUserPosition{ return
    [[_staff objectForKey:@"UserPosition"] intValue];
}
- (BOOL)isUser{
    return ([self propSecurityLevel] == STAFF_SECURITYLEVEL_USER);
}
- (BOOL)isManager{ return ([self propSecurityLevel] == STAFF_SECURITYLEVEL_MANAGER); }
- (BOOL)isAccount{ return ([self propSecurityLevel] == STAFF_SECURITYLEVEL_ACCOUNT); }
- (BOOL)isAdmin{ return ([self propSecurityLevel] == STAFF_SECURITYLEVEL_ADMIN); }
- (BOOL)isCM{ return ([self propSecurityLevel] == STAFF_SECURITYLEVEL_COUNTRYMANAGER); }
- (BOOL)isAM{ return ([self propSecurityLevel] == STAFF_SECURITYLEVEL_ACCOUNTMANAGER); }

@end
