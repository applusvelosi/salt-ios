//
//  VCClaimHeaderAdvances.h
//  Salt
//
//  Created by Rick Royd Aban on 2015/08/12.
//  Copyright (c) 2015年 Applus Velosi. All rights reserved.
//

#import "VCClaimHeader.h"
#import "BusinessAdvance.h"
#import "CostCenter.h"

@interface VCClaimHeaderAdvances : VCClaimHeader

@property (strong, nonatomic) BusinessAdvance *propBusinessAdvance;

@end
