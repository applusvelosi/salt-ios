//
//  CellLFAPending.h
//  Salt iOS
//
//  Created by Rick Royd Aban on 6/11/15.
//  Copyright (c) 2015 Rick Royd Aban. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "VCLeavesForApprovalPending.h"

@interface CellLFAPending :UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *propLabelType;
@property (weak, nonatomic) IBOutlet UILabel *propLabelDuration;
@property (weak, nonatomic) IBOutlet UILabel *propLabelStaff;

@property (weak, nonatomic) IBOutlet UIButton *propButtonApprove;
@property (weak, nonatomic) IBOutlet UIButton *propButtonReject;

- (void)assignVCLeavesForApprovalPending:(VCLeavesForApprovalPending *)vcLeavesForApprovalPending;

@end
