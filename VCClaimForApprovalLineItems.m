//
//  VCClaimsForApprovalItems.m
//  Salt
//
//  Created by Rick Royd Aban on 2015/08/25.
//  Copyright (c) 2015年 Applus Velosi. All rights reserved.


#import "VCClaimForApprovalLineItems.h"
#import "VCMileageClaimItemDetail.h"
#import "MileageClaimItem.h"
#import "ClaimCategory.h"
#import "ClaimItem.h"
#import "CFAMaximized.h"
#import "CFAMaximizedSel.h"
#import "CFAMinimized.h"
#import "CFAMinimizedSel.h"
#import "VCGeneralClaimItemsDetail.h"
#import "VCClaimPaidByCC.h"
#import "VCClaimsForApproval.h"
#import <Objection/Objection.h>

@interface VCClaimForApprovalLineItems (){
    
    IBOutlet UIButton *_propBarbuttonSelect;
    IBOutlet UIButton *_propBarbuttonCancel;
    IBOutlet UIButton *_propBarbuttonReject;
    IBOutlet UIButton *_propBarbuttonReturn;
    IBOutlet UIButton *_propBarbuttonApprove;
    IBOutlet UITableView *_propLV;
    ClaimItem *_selectedClaimItem;
    
    NSMutableDictionary *_cellTypes;
    NSMutableArray *_selectedItems;
    __block int statusCtr;
    int submittedStatus;
    BOOL _isSelectMode;
    NSMutableString *_note;
}
@property (nonatomic, strong) id<ClaimHeaderDelegate> delegate;
@end

@implementation VCClaimForApprovalLineItems

objection_requires(@"delegate");

#pragma mark -
#pragma mark === View Life Cycle Management ===
#pragma mark -
- (void)viewDidLoad{
    [super viewDidLoad];
    [[JSObjection defaultInjector] injectDependencies:self];
     statusCtr = 0;
    _isSelectMode = false;
    _cellTypes = [NSMutableDictionary dictionary];
    _selectedItems = [NSMutableArray array];
    _propLV.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    _propLV.delegate = self;
    _propLV.dataSource = self;
    _propLV.allowsMultipleSelection = YES;
    _propLV.rowHeight = UITableViewAutomaticDimension;
    _propLV.estimatedRowHeight = 100;
    _note = [[NSMutableString alloc] init];

//    [self refreshWithBlock:^(NSArray *claimItems) {
//        for (ClaimItem *item in claimItems) {
//            if ([item statusID] != [self.propClaimHeader propStatusID])
//                statusCtr++;
//        }
//        if (statusCtr == claimItems.count)
//            _propBarbuttonSelect.enabled = NO;
//        
//    }];
}

- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    statusCtr = 0;
    [self refreshWithBlock:^(NSArray *claimItems){
        for (ClaimItem *item in claimItems) {
            if ([item statusID] != [self.propClaimHeader propStatusID])
                statusCtr++;
        }
        if (statusCtr == claimItems.count)
            _propBarbuttonSelect.enabled = NO;
        if (self.propParentVC != nil) {
            [(VCClaimPaidByCC*)self.propParentVC setLcSumValuesProvider:^(float *lcTotal, float *approvedTotal, float *rejectedTotal, float *deductionTotal, float *paymentTotal) {
                
                float positiveTotal, negativeTotal;
                positiveTotal = negativeTotal = 0;
                if (claimItems.count > 0) {
                    for (ClaimItem *lineItem in claimItems) {
                        int itemStatus = [lineItem statusID];
                        if (itemStatus == CLAIMSTATUSID_REJECTEDBYAPPROVER || itemStatus == CLAIMSTATUSID_REJECTEDBYCM || itemStatus == CLAIMSTATUSID_REJECTEDFORSALARYDEDUCTION)
                            *rejectedTotal += [lineItem amountLocal];
                        if (itemStatus == CLAIMSTATUSID_APPROVEDBYAPPROVER || itemStatus == CLAIMSTATUSID_APPROVEDBYCM || itemStatus == CLAIMSTATUSID_APPROVEDBYACCOUNTS || itemStatus == CLAIMSTATUSID_PAIDUNDERCOMPANYCARD || itemStatus == CLAIMSTATUSID_SENTTOSAP)
                            *approvedTotal += [lineItem amountLocal];
                        
                        if ([self.propClaimHeader propTypeID] == CLAIMTYPEID_ADVANCES || self.propClaimHeader.propTypeID == CLAIMTYPEID_LIQUIDATION ) {
                            if (itemStatus != CLAIMSTATUSID_RETURN && lineItem.amountLocal < 0)
                                *lcTotal += lineItem.amountLocal;
                        }else{
                            if (itemStatus != CLAIMSTATUSID_RETURN) {
                                *lcTotal += lineItem.amountLocal;
                            }
                        }
                        if ([self.propClaimHeader propTypeID] == CLAIMTYPEID_CLAIMS) {
                            if (itemStatus == CLAIMSTATUSID_REJECTEDFORSALARYDEDUCTION) {
                                *deductionTotal += lineItem.amountLocal;
                            }
                        }
                        float resultNegative = 0.0f;
                        if ([self.propClaimHeader propTypeID] == CLAIMTYPEID_LIQUIDATION) {
                            if (itemStatus != CLAIMSTATUSID_RETURN && lineItem.amountLocal > 0)
                                positiveTotal += lineItem.amountLocal;
                            if (itemStatus != CLAIMSTATUSID_RETURN && lineItem.amountLocal < 0)
                                negativeTotal += lineItem.amountLocal;
                        }else if ([self.propClaimHeader propTypeID] == CLAIMTYPEID_ADVANCES){
                            if (resultNegative < *approvedTotal)
                                *paymentTotal = *approvedTotal - *lcTotal / -1;
                            else
                                *paymentTotal = *approvedTotal;
                        }else{
                            *paymentTotal = 0;
                        }
                    }
                }
                
            }];
        }
    }];
    [self onCancelButtonClicked:nil];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    NSIndexPath *selectedIndexPath = [_propLV indexPathForSelectedRow];
    if([segue.identifier isEqualToString:@"cfaitemstomileagecfaitemdetail"])
        ((VCMileageClaimItemDetail *)segue.destinationViewController).propClaimItem = [[MileageClaimItem alloc] initWithDictionary:[_selectedClaimItem savableDictionary]];
    else{
        VCGeneralClaimItemsDetail *generalClaimItemsVC = [segue destinationViewController];
        generalClaimItemsVC.propClaimHeader = _propClaimHeader;
        generalClaimItemsVC.pageIndex = selectedIndexPath.row;
    }
}


#pragma mark -
#pragma mark - Table view data source delegate
#pragma mark -
//- (BOOL)tableView:(UITableView *)tableView shouldHighlightRowAtIndexPath:(NSIndexPath *)indexPath {
//    ClaimItem *claimItem = [[self.propAppDelegate forApprovalClaimItemsForClaimID:[_propClaimHeader propClaimID]] objectAtIndex:indexPath.row];
//    if(_isSelectMode && [claimItem statusID] == [self.propClaimHeader propStatusID])
//        return YES;
//    else
//        return NO;
//}
//
//- (void)tableView:(UITableView *)tableView didHighlightRowAtIndexPath:(NSIndexPath *)indexPath {
//    // Add your Colour.
//    CFAMinimizedSel *cell = (CFAMinimizedSel *)[tableView cellForRowAtIndexPath:indexPath];
//    [self setCellColor:[UIColor blueColor] ForCell:cell];  //highlight colour
//}
//
//- (void)tableView:(UITableView *)tableView didUnhighlightRowAtIndexPath:(NSIndexPath *)indexPath {
//    // Reset Colour.
//    CFAMinimized *cell = (CFAMinimized *)[tableView cellForRowAtIndexPath:indexPath];
//    [self setCellColor:[UIColor colorWithWhite:0.961 alpha:1.000] ForCell:cell]; //normal color
//    
//}
//
//- (void)setCellColor:(UIColor *)color ForCell:(UITableViewCell *)cell {
//    cell.contentView.backgroundColor = color;
//    cell.backgroundColor = color;
//}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    ClaimItem *claimItem = [[self.propAppDelegate forApprovalClaimItemsForClaimID:[_propClaimHeader propClaimID]] objectAtIndex:indexPath.row];
    
    if([[_cellTypes objectForKey:[NSString stringWithFormat:@"%d",(int)indexPath.row]] intValue] == 0){ //minimized
        if(_isSelectMode && [claimItem statusID] == [self.propClaimHeader propStatusID]) {
            CFAMinimizedSel *cellMinimizedSel = [tableView dequeueReusableCellWithIdentifier:@"cell_minimize_sel"];
            cellMinimizedSel.propCbox.image = [UIImage imageNamed:([_selectedItems containsObject:indexPath])?@"icon_checkbox_sel":@"icon_checkbox_normal"];
            cellMinimizedSel.propFieldCategory.text = [claimItem propCategoryName];
            cellMinimizedSel.propFieldStatus.text = [claimItem propStatusName];
            cellMinimizedSel.propFieldLocalCurrency.text = [NSString stringWithFormat:@"%@ %.2f",[claimItem propLocalCurrencyName], [claimItem propLocalAmount]];
            return cellMinimizedSel;
        }else {
            CFAMinimized *cellMinimized = [tableView dequeueReusableCellWithIdentifier:@"cell_minimize"];
            cellMinimized.propFieldType.text = [claimItem propCategoryName];
            cellMinimized.propFieldStatus.text = [claimItem propStatusName];
            cellMinimized.propFieldTotal.text = [NSString stringWithFormat:@"%@ %.2f",[claimItem propLocalCurrencyName], [claimItem propLocalAmount]];
            return cellMinimized;
        }
    }else{ //maximized
        if(_isSelectMode && [claimItem statusID] == [self.propClaimHeader propStatusID]) {
            CFAMaximizedSel *cellMaximizedSel = [tableView dequeueReusableCellWithIdentifier:@"cell_maximize_sel"];
            cellMaximizedSel.propCbox.image = [UIImage imageNamed:([_selectedItems containsObject:indexPath])?@"icon_checkbox_sel":@"icon_checkbox_normal"];
            cellMaximizedSel.propFieldCategory.text = [claimItem propCategoryName];
            cellMaximizedSel.propFieldStatus.text = [claimItem propStatusName];
            cellMaximizedSel.propFieldDate.text = [claimItem propDateExpensedWithAppDelegate:self.propAppDelegate];
            cellMaximizedSel.propFieldForeignAmount.text = [NSString stringWithFormat:@"%.2f",[claimItem propForeignAmount]];
            cellMaximizedSel.propFieldVAT.text = [NSString stringWithFormat:@"%.2f",[claimItem propTaxedAmount]];
            cellMaximizedSel.propFieldTax.text = [NSString stringWithFormat:@"%.2f",[claimItem propOnActualConversionExchangeRate]];
            cellMaximizedSel.propFieldLocalAmount.text = [NSString stringWithFormat:@"%.2f",[claimItem propLocalAmount]];
            cellMaximizedSel.propFieldDesc.text = [claimItem description];
            cellMaximizedSel.propImageReceipt.image = [UIImage imageNamed:([claimItem propHasReceipt])?@"icon_attachment":@"icon_attachment_none"];
            return cellMaximizedSel;
        }else{
            CFAMaximized *cellMaximized = [tableView dequeueReusableCellWithIdentifier:@"cell_maximize"];
            cellMaximized.propFieldCategory.text = [claimItem propCategoryName];
            cellMaximized.propFieldStatus.text = [claimItem propStatusName];
//            cellMaximized.propFieldExpenseDate.text = [claimItem propDateExpensedWithAppDelegate:self.propAppDelegate];
            cellMaximized.propFieldAmountForeign.text = [NSString stringWithFormat:@"%.2f",[claimItem propForeignAmount]];
            cellMaximized.propFieldVat.text = [NSString stringWithFormat:@"%.2f", [claimItem propTaxedAmount]];
            cellMaximized.propFieldTax.text = [NSString stringWithFormat:@"%.2f", [claimItem propOnActualConversionExchangeRate]];
            cellMaximized.propFieldAmountLocal.text = [NSString stringWithFormat:@"%.2f", [claimItem propLocalAmount]];
            
            cellMaximized.propFieldDescription.text = [claimItem description];
            cellMaximized.propImageReceipt.image = [UIImage imageNamed:([claimItem propHasReceipt])?@"icon_attachment":@"icon_attachment_none"];

            return cellMaximized;
        }
    }
}

- (void)tableView:(UITableView *)tableView accessoryButtonTappedForRowWithIndexPath:(NSIndexPath *)indexPath{
    [_cellTypes setValue:([[_cellTypes objectForKey:[NSString stringWithFormat:@"%d",(int)indexPath.row]] intValue] == 0)?@(1):@(0) forKey:[NSString stringWithFormat:@"%d",(int)indexPath.row]];
    [_propLV reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
    
    if([_selectedItems containsObject:indexPath])
        [tableView selectRowAtIndexPath:indexPath animated:NO scrollPosition:UITableViewScrollPositionNone];
    [tableView scrollToRowAtIndexPath:indexPath atScrollPosition:UITableViewScrollPositionTop animated:YES];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    submittedStatus = 0;
    _selectedClaimItem = [[self.propAppDelegate forApprovalClaimItemsForClaimID:[_propClaimHeader propClaimID]] objectAtIndex:indexPath.row];
    if(_isSelectMode && [_selectedClaimItem statusID] == [self.propClaimHeader propStatusID]) {
        if([_selectedItems containsObject:indexPath])
            [_selectedItems removeObject:indexPath];
        else
            [_selectedItems addObject:indexPath];
        [_propLV reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
        [self updateControlbar];
    }else
        [self performSegueWithIdentifier:[_selectedClaimItem propCategoryID]== [ClaimCategory TYPE_MILEAGE]?@"cfaitemstomileagecfaitemdetail":@"cfaitemstogeneralcfaitemdetail" sender:[tableView cellForRowAtIndexPath:indexPath]];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [self.propAppDelegate forApprovalClaimItemsForClaimID:[_propClaimHeader propClaimID]].count;
}


#pragma mark -
#pragma mark === Helper methods ===
#pragma mark -
- (void)updateControlbar{
    NSLog(@"\nSELECTED ITEMS: %ld",_selectedItems.count);
    
    if(_selectedItems.count == 0) {
        _propBarbuttonReturn.hidden = YES;
        _propBarbuttonReject.hidden = YES;
        _propBarbuttonApprove.hidden = YES;
    }else{
        _propBarbuttonSelect.enabled = (_selectedItems.count == [self.propAppDelegate forApprovalClaimItemsForClaimID:[_propClaimHeader propClaimID]].count)?NO:YES;
        _propBarbuttonReturn.hidden = NO;
        [_propBarbuttonReturn setEnabled:(!self.propClaimHeader.propIsPaidByCC)?YES:NO];
        _propBarbuttonReject.hidden = NO;
        _propBarbuttonApprove.hidden = NO;
    }
}


#pragma mark -
#pragma mark -UI Event handler Management ===
#pragma mark -
- (IBAction)onSelectAllButtonClicked:(id)sender {
    if(_isSelectMode){
        _propBarbuttonSelect.enabled = NO;
        submittedStatus = 0;
        NSIndexPath *currIndexPath;
        for(int i=0; i<[self.propAppDelegate forApprovalClaimItemsForClaimID:[_propClaimHeader propClaimID]].count; i++) {
            currIndexPath = [NSIndexPath indexPathForRow:i inSection:0];
            if( [[[self.propAppDelegate forApprovalClaimItemsForClaimID:[_propClaimHeader propClaimID]] objectAtIndex:i] statusID] == [self.propClaimHeader propStatusID]){
                if(![_selectedItems containsObject:currIndexPath]) {
                    submittedStatus++;
                    [_selectedItems addObject:currIndexPath];
                    [_propLV selectRowAtIndexPath:currIndexPath animated:NO scrollPosition:UITableViewScrollPositionNone];
                }
            }
        }
        [self updateControlbar];
    } else {
        [_propBarbuttonSelect setTitle:@"Select All" forState:UIControlStateNormal];
        _isSelectMode = YES;
        _propBarbuttonCancel.hidden = NO;
    }
    [_propLV reloadSections:[NSIndexSet indexSetWithIndex:0] withRowAnimation:UITableViewRowAnimationFade];
}

- (IBAction)onCancelButtonClicked:(id)sender {
    [_selectedItems removeAllObjects];
    [_propBarbuttonSelect setTitle:@"Select" forState:UIControlStateNormal];
    _propBarbuttonSelect.enabled = YES;
    _isSelectMode = NO;
    _propBarbuttonCancel.hidden = YES;
    _propBarbuttonReject.hidden = YES;
    _propBarbuttonReturn.hidden = YES;
    _propBarbuttonApprove.hidden = YES;
    [_propLV reloadData];
}

- (IBAction)onApproveButtonClicked:(id)sender {
    [AppDelegate showGlogalHUDWithView:self.view];
    for (int i = 0; i < _selectedItems.count; i++) {
        NSInteger pos = [[_selectedItems objectAtIndex:i] row];
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            ClaimItem *newClaimItem = [[self.propAppDelegate forApprovalClaimItemsForClaimID:[_propClaimHeader propClaimID]] objectAtIndex:pos];
            if ([newClaimItem statusID] == CLAIMSTATUSID_SUBMITTED)
                [self updateClaimItem:newClaimItem statusID:CLAIMSTATUSID_APPROVEDBYAPPROVER updatableDate:ClaimItemUpdatableDateApprovedByApprover approverNote:@"Approver: Approved"];
            else if ([newClaimItem statusID] == CLAIMSTATUSID_APPROVEDBYAPPROVER)
                [self updateClaimItem:newClaimItem statusID:CLAIMSTATUSID_APPROVEDBYCM updatableDate:ClaimItemUpdatableDateApprovedByDirector approverNote:@"CM: Approved"];
        });
    }
}

- (IBAction)onRejectButtonClicked:(id)sender {
    NSString *alertTitle = @"Reason for Rejection";
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:alertTitle message:nil preferredStyle:UIAlertControllerStyleAlert];
    [alertController addTextFieldWithConfigurationHandler:^(UITextField *textField)
     {
         textField.placeholder = @"Required";
         [textField addTarget:self
                       action:@selector(alertTextFieldDidChange:)
             forControlEvents:UIControlEventEditingChanged];
     }];
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"Cancel"
                                                           style:UIAlertActionStyleCancel
                                                         handler:^(UIAlertAction *action) {
                                                             NSLog(@"Canceled");
                                                         }];
    UIAlertAction *rejectAction = [UIAlertAction actionWithTitle:@"Reject"
                                                           style:UIAlertActionStyleDefault
                                                         handler:^(UIAlertAction *action){
                                                             UITextField *rejectNote = alertController.textFields.firstObject;
                                                             [AppDelegate showGlogalHUDWithView:self.view];
                                                             if (self.propClaimHeader.propIsPaidByCC) {
                                                                 [_note appendFormat:@"APPROVER: %@;", rejectNote.text];
                                                                 for (int i = 0; i < _selectedItems.count; i++) {
                                                                     NSInteger pos = [[_selectedItems objectAtIndex:i] row];
                                                                     dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                                                                         ClaimItem *newClaimItem = [[self.propAppDelegate forApprovalClaimItemsForClaimID:[_propClaimHeader propClaimID]] objectAtIndex:pos];
                                                                         [self updateClaimItem:newClaimItem statusID:CLAIMSTATUSID_REJECTEDFORSALARYDEDUCTION updatableDate:ClaimItemUpdatableDateApprovedByApprover approverNote:_note];
                                                                     });
                                                                 }
                                                             } else {
                                                                 for (int i = 0; i < _selectedItems.count; i++) {
                                                                     NSInteger pos = [[_selectedItems objectAtIndex:i] row];
                                                                     dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                                                                         ClaimItem *newClaimItem = [[self.propAppDelegate forApprovalClaimItemsForClaimID:[_propClaimHeader propClaimID]] objectAtIndex:pos];
                                                                         if (self.propAppDelegate.getStaff.getUserPosition == USERPOSITION_CM || self.propAppDelegate.getStaff.getUserPosition == USERPOSITION_CFO) {
                                                                             [_note appendFormat:@"CM: %@;", rejectNote.text];
                                                                             [self updateClaimItem:newClaimItem statusID:CLAIMSTATUSID_REJECTEDBYCM updatableDate:ClaimItemUpdatableDateApprovedByApprover approverNote:_note];
                                                                         }else {
                                                                             [_note appendFormat:@"APPROVER: %@;", rejectNote.text];
                                                                             [self updateClaimItem:newClaimItem statusID:CLAIMSTATUSID_REJECTEDBYAPPROVER updatableDate:ClaimItemUpdatableDateApprovedByDirector approverNote:_note];
                                                                         }
                                                                     });
                                                                 }
                                                             }
                                                         }];
    rejectAction.enabled = NO;
    [alertController addAction:cancelAction];
    [alertController addAction:rejectAction];
    [self presentViewController:alertController animated:YES completion:nil];
    
}

- (IBAction)onReturnButtonClicked:(id)sender {
    NSString *alertTitle = @"Reason for Returning";
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:alertTitle message:nil preferredStyle:UIAlertControllerStyleAlert];
    [alertController addTextFieldWithConfigurationHandler:^(UITextField *textField)
     {
         textField.placeholder = @"Required";
         [textField addTarget:self
                       action:@selector(alertTextFieldDidChange:)
             forControlEvents:UIControlEventEditingChanged];
     }];
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"Cancel"
                                                           style:UIAlertActionStyleCancel
                                                         handler:^(UIAlertAction *action) {
                                                             NSLog(@"Canceled");
                                                         }];
    UIAlertAction *returnAction = [UIAlertAction actionWithTitle:@"Reject"
                                                           style:UIAlertActionStyleDefault
                                                         handler:^(UIAlertAction *action){
                                                             UITextField *returnNote = alertController.textFields.firstObject;
                                                             [AppDelegate showGlogalHUDWithView:self.view];
                                                             for (int i = 0; i < _selectedItems.count; i++) {
                                                                 NSInteger pos = [[_selectedItems objectAtIndex:i] row];
                                                                 dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                                                                     ClaimItem *newClaimItem = [[self.propAppDelegate forApprovalClaimItemsForClaimID:[_propClaimHeader propClaimID]] objectAtIndex:pos];
                                                                     if (self.propAppDelegate.getStaff.getUserPosition == USERPOSITION_CM || self.propAppDelegate.getStaff.getUserPosition == USERPOSITION_CFO)
                                                                         [_note appendFormat:@"CM: %@;", returnNote.text];
                                                                     else
                                                                         [_note appendFormat:@"APPROVER: %@;", returnNote.text];
                                                                     
                                                                     [self updateClaimItem:newClaimItem statusID:CLAIMSTATUSID_RETURN updatableDate:ClaimItemUpdatableDateApprovedByDirector approverNote:_note];

                                                                 });
                                                             }
                                                         }];
    returnAction.enabled = NO;
    [alertController addAction:cancelAction];
    [alertController addAction:returnAction];
    [self presentViewController:alertController animated:YES completion:nil];
}


#pragma mark -
#pragma mark === Helper method ===
#pragma mark -
#pragma clang diagnostic ignored "-Wnonnull"
- (void)updateClaimItem:(ClaimItem *)newClaimItem statusID:(int)statusID updatableDate:(ClaimItemUpdatableDate)updatableDate approverNote:(NSString *)approversNote{
    [newClaimItem setClaimStatus:statusID];
    [newClaimItem setStatusName:[ClaimHeader statusDescForStatusKey:statusID]];
    [newClaimItem updatableDate:updatableDate withAppDelegate:self.propAppDelegate];
    [newClaimItem setNotes:approversNote];
    [newClaimItem setModifiedBy:[[self.propAppDelegate getStaff] propStaffID]];
    NSString *claimItemJSON = [newClaimItem jsonForWebServicesWithAppDelegate:self.propAppDelegate];
    NSString *attachment = ([newClaimItem propAttachments] != nil) ? [[[newClaimItem propAttachments] objectAtIndex:0] jsonObject]: [newClaimItem propAttachments];
    id result = [self.propAppDelegate.propGatewayOnline saveNewClaimItemJSON:claimItemJSON oldClaimItemID:0 document:attachment base64:nil];
    dispatch_async(dispatch_get_main_queue(), ^{
        [AppDelegate hideGlogalHUDWithView:self.view];
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"" message:((![result isKindOfClass:[NSString class]]) ? @"Updated Successfully" : result) preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"Dismiss" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action){
            [self onCancelButtonClicked:nil];
            [self viewDidAppear:YES];
        }];
        [alertController addAction:cancelAction];
        [self presentViewController:alertController animated:YES completion:nil];
    });
}

- (void)refreshWithBlock:(void(^)(NSArray*))refreshBlk{
    [AppDelegate showGlogalHUDWithView:self.view];
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
        id result = [self.propAppDelegate.propGatewayOnline claimItemsForClaimID:[self.propClaimHeader propClaimID]];
        dispatch_async(dispatch_get_main_queue(), ^{
            [AppDelegate hideGlogalHUDWithView:self.view];
            if([result isKindOfClass:[NSString class]])
                [[[UIAlertView alloc] initWithTitle:@"" message:result delegate:nil cancelButtonTitle:@"Dismiss" otherButtonTitles:nil, nil] show];
            else
                [self.propAppDelegate updateForApprovalClaimItems:result forClaimID:[_propClaimHeader propClaimID]];
            [_cellTypes removeAllObjects];
            int i=0;
            for(; i<[self.propAppDelegate forApprovalClaimItemsForClaimID:[_propClaimHeader propClaimID]].count; i++)
                [_cellTypes setValue:@"0" forKey:[NSString stringWithFormat:@"%d",i]];
            refreshBlk([self.propAppDelegate forApprovalClaimItemsForClaimID:[_propClaimHeader propClaimID]]);
            [_propLV reloadData];
        });
    });
}

- (void)alertTextFieldDidChange:(UITextField *)sender
{
    UIAlertController *alertController = (UIAlertController *)self.presentedViewController;
    if (alertController)
    {
        UITextField *noteField = alertController.textFields.firstObject;
        UIAlertAction *confirmAction = alertController.actions.lastObject;
        confirmAction.enabled = ([[noteField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]] length] == 0)?NO:YES;
    }
}

@end
