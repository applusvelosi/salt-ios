//
//  UserPageNavigators.h
//  Jobs
//
//  Created by Rick Royd Aban on 10/2/14.
//  Copyright (c) 2014 applusvelosi. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
@class VCHomeLeaves;
#import "VelosiPickerRowSelectionDelegate.h"

@interface PageNavigatorFactory : NSObject


+ (PageNavigatorFactory *)sharedNavigators;

- (void)logout;
- (UINavigationController *)vcLogin;
- (UINavigationController *)vcHome;
- (UINavigationController *)vcMyLeaves;
- (UINavigationController *)vcLeaveInput;
- (UINavigationController *)vcLeavesForApproval;
- (UINavigationController *)vcWeeklyHolidays;
- (UINavigationController *)vcMonthlyHolidays;
- (UINavigationController *)vcLocalHolidays;
- (UINavigationController *)vcMyCalendar;
- (UINavigationController *)vcMyClaims;
- (UINavigationController *)vcClaimsForApproval;
- (UINavigationController *)vcCapexForApproval;
- (UINavigationController *)vcRecruitmentForApproval;
- (UINavigationController *)vcGiftsForApproval;
- (UINavigationController *)vcContractForApproval;

@end
