//
//  Document.h
//  Salt
//
//  Created by Rick Royd Aban on 2/10/16.
//  Copyright © 2016 Applus Velosi. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AppDelegate.h"

@interface Document : NSObject

- (Document *)initWithAppDelegate:(AppDelegate *)appDelegate;
- (Document *)initWithDictionary:(NSDictionary *)dictionary;
- (Document *)initWithFileName:(NSString *)fileName dateCreated:(NSString *)dateCreated fileSize:(NSInteger)fileSize claimHeader:(ClaimHeader *)claimHeader nameDate:(NSString *)nameDate objectTypeID:(int)objectTypeID;
- (int)propDocID;
- (int)propRefID;
- (int)propObjectTypeID;
- (NSString *)propDocName;
- (NSString *)propOrigDocName;
- (void)propRefID:(int)refID;
- (NSString *)jsonObject;

+ (int)objecttype_CLAIMID;
+ (int)objecttype_LEAVEID;
+ (int)objecttype_CLAIMLINEITEMID;
+ (int)objecttype_STAFFID;
+ (int)objecttype_RECRUITMENTID;
+ (int)objecttype_TRAVELID;
+ (int)objecttype_CAPEXID;
+ (int)objecttype_CAPEXLINEITEMQOUTATIONID;

@end
