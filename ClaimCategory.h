//
//  ClaimCategory.h
//  Salt
//
//  Created by Rick Royd Aban on 2015/08/04.
//  Copyright (c) 2015年 Applus Velosi. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ClaimCategory : NSObject

- (ClaimCategory *)initWithJSONDict:(NSDictionary *)jsonCategoryDict;
- (ClaimCategory *)initWithDictionary:(NSDictionary *)categoryDict;
- (int)propAttendeeTypeID;
- (int)propCategoryTypeID;
- (int)propCategoryID;
- (float)propSpendLimit;
- (NSString *)propName;
- (int)propCurrencyID;
- (NSString *)propCurrencyName;

+ (int)TYPE_MILEAGE;
+ (int)TYPE_BUSINESSADVANCE;
+ (int)TYPE_ASSET;

@end
