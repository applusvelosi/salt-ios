//
//  AppDelegate.h
//  Salt iOS
//
//  Created by Rick Royd Aban on 5/18/15.
//  Copyright (c) 2015 Rick Royd Aban. All rights reserved.
//

#define ALL @"All"

//mainHR since server wont abstract with the mainHR related processes
#define MAINHRID 198
#define MAINHREMAIL @"juancarlos.urbano@applusvelosi.com"

#import <UIKit/UIKit.h>
#import "Staff.h"
#import "Office.h"
#import "PageNavigatorFactory.h"
#import "VCSlider.h"
#import "OfflineGateway.h"
#import "OnlineGateway.h"
#import "DSCurrencyList.h"
#import "Dashboard.h"
#import <MBProgressHUD.h>
#import <FirebaseDatabase/FirebaseDatabase.h>


@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic, readonly) VCSlider *propSlider;
@property (strong, nonatomic) PageNavigatorFactory *propPageNavigator;
@property (strong, nonatomic) OfflineGateway *propGatewayOffline;
@property (strong, nonatomic) OnlineGateway *propGatewayOnline;
@property (strong, nonatomic) Dashboard *dashboard;
@property (strong, nonatomic) NSDateFormatter *propFormatVelosiDate, *propDateFormatMonthyear, *propDateFormatLeaveDuration, *propDateFormatDateTime, *propDateFormatByProcessed;
@property (strong, nonatomic, readonly) NSArray *filterYears;
@property (assign, nonatomic, readonly) int currYear;
@property (strong,nonatomic) NSNumberFormatter *decimalFormat;
@property (nonatomic) FIRDatabaseReference *databaseRef;

//Datasources
@property (strong, nonatomic, readonly) DSCurrencyList *propCurrencies;

//required by IOS
@property (strong, nonatomic) UIWindow *window;
+ (MBProgressHUD*)showGlogalHUDWithView:(UIView *)view ;
+ (void)hideGlogalHUDWithView:(UIView*)view;

- (Staff *)getStaff;
- (Office *)office;
- (void)updateStaffDataWithStaff:(Staff *)staff office:(Office *)office key:(OnlineGateway *)onlineGateway;
//- (void)initMyLeaves:(NSArray *)myLeaves leavesForApproval:(NSArray *)leavesForApproval myClaims:(NSArray *)myClaims claimsForApproval:(NSArray *)claimsForApproval claimsForAccount:(NSArray *)claimsForAccount;
- (void)initMyLeaves:myLeaveResult leavesForApproval:leavesForApprovalResult;
- (void)updateMyLeaves:(NSMutableArray *)myLeaves;
- (void)updateLeavesForApproval:(NSMutableArray *)leavesForApproval;
- (void)updateMonthlyHolidays:(NSMutableArray *)monthlyHolidays;
- (void)updateLocalHolidays:(NSMutableArray *)localHolidays;
- (void)updateMyClaims:(NSMutableArray *)myClaims;
- (void)updateClaimsForApproval:(NSMutableArray *)claimsForApproval;
//- (void)updateClaimsForPayment:(NSMutableArray *)claimsForPayment;
- (void)updateRecruitmentsForApproval:(NSMutableArray *)recruitmentsForApproval;
- (void)updateCapexesForApproval:(NSMutableArray *)capexesForApproval;
- (void)updateGiftsForApproval:(NSMutableArray *)giftsForApproval;
- (void)updateContractsForApproval:(NSMutableArray *)contractsForApproval;
- (NSArray *)giftsForApproval;
- (NSArray *)myLeaves;
- (NSArray *)leavesForApproval;
- (NSArray *)monthlyHolidays;
- (NSArray *)localHolidays;
- (NSArray *)myClaims;
- (NSArray *)claimsForApproval;
- (NSArray *)claimsForPayment;
- (NSArray *)recruitmentsForApproval;
- (NSArray *)capexesForApproval;
- (NSArray *)contractsForApproval;
- (void)updateMyClaimItems:(NSMutableArray *)myClaimItems forClaimID:(int)claimID;
- (void)updateForApprovalClaimItems:(NSMutableArray *)forApprovalClaimItems forClaimID:(int)claimID;
- (void)updateForPaymentClaimItems:(NSMutableArray *)forPaymentClaimItems forClaimID:(int)claimID;
- (NSArray *)myClaimItemsForMyClaimID:(int)myClaimID;
- (NSArray *)forApprovalClaimItemsForClaimID:(int)claimID;
- (NSArray *)forPaymentClaimItemsForClaimID:(int)claimID;

- (void)setSlider:(VCSlider *)slider;
+ (NSString *)all;
@end

