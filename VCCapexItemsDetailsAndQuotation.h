//
//  VCCapexItemQoutation.h
//  Salt
//
//  Created by Rick Royd Aban on 1/12/16.
//  Copyright © 2016 Applus Velosi. All rights reserved.
//

#import "VCPage.h"
#import "VCCapexItemQoutationPageContent.h"
#import "CapexForApprovalLineItem.h"


@interface VCCapexItemsDetailsAndQuotation : VCPage<UITableViewDataSource,UITableViewDelegate>
@property (strong, nonatomic) CapexForApprovalLineItem *capexLineItem;
@end
