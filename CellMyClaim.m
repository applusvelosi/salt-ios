//
//  CellMyClaim.m
//  Salt
//
//  Created by Rick Royd Aban on 2015/07/27.
//  Copyright (c) 2015年 Applus Velosi. All rights reserved.
//

#import "CellMyClaim.h"
#import "VelosiColors.h"

@implementation CellMyClaim
-(void)awakeFromNib{
    [super awakeFromNib];
    self.propFieldStatus.textColor = [VelosiColors lightGrayDisabled];
}

@end
