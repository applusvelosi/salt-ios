//
//  VCContractsForApproval.m
//  Salt
//
//  Created by Rick Royd Aban on 13/06/2017.
//  Copyright © 2017 Applus Velosi. All rights reserved.
//

#import "VCContractsForApproval.h"
#import "LoaderDelegate.h"
#import "AppDelegate.h"
#import "Contract.h"
#import "CellContractsForApproval.h"
#import "VelosiColors.h"
#import "VCContractForApprovalDetail.h"

@interface VCContractsForApproval () <UISearchResultsUpdating, LoaderDelegate, UIScrollViewDelegate, UISearchBarDelegate> {
    UISearchController *_searchController;
    NSMutableArray *_contracts;
    NSString *_searchString;
    BOOL _searchWasCancelled;
    AppDelegate *_appDelegate;
}

@end

@implementation VCContractsForApproval

#pragma mark -
#pragma mark === View Life Cycle Management ===
#pragma mark -
- (void)viewDidLoad {
    [super viewDidLoad];
    
    _contracts = [NSMutableArray array];
    _appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    _searchString = @"";
    _searchWasCancelled = NO;
    self.parentVC.delegate = self;
//    No search results controller to display the search results in the current view
    _searchController = [[UISearchController alloc] initWithSearchResultsController:nil];
    _searchController.searchResultsUpdater = self;
    _searchController.dimsBackgroundDuringPresentation = NO;
    _searchController.hidesNavigationBarDuringPresentation = NO;
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(didChangePrefeeredContentSize:)
                                                 name:UIContentSizeCategoryDidChangeNotification object:nil];
    
//    self.searchController.searchBar.delegate = self;
    _searchController.searchBar.tintColor = [UIColor blueColor];
    _searchController.searchBar.placeholder = @"Requestor office or name";
    
    self.tableView.tableHeaderView = _searchController.searchBar;
    self.definesPresentationContext = YES;
    [_searchController.searchBar sizeToFit];
    _searchController.searchBar.delegate = self;
    
    self.tableView.rowHeight = UITableViewAutomaticDimension;
    self.tableView.estimatedRowHeight = 140;
}

-(void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    NSIndexPath *indexPath = [self.tableView indexPathForCell:sender];
    Contract *contract = (indexPath)?[_contracts objectAtIndex:indexPath.row]:nil;
    if ([segue.identifier isEqualToString:@"vcidcontractdetail"]) {
        VCContractForApprovalDetail *destVC = segue.destinationViewController;
        destVC.contactId = contract.propContractId;
        destVC.propAppDelegate = _appDelegate;
    }
}

#pragma mark -
#pragma mark === Helper methods ===
#pragma mark -
-(void)didChangePrefeeredContentSize:(NSNotification*)notification{
    [self.tableView reloadData];
}

-(void)relaodContractsForApprovalOffice:(NSString *) searchStr {
    [_contracts removeAllObjects];
    if (searchStr.length > 0) {
        for (Contract *contractForApproval in [_appDelegate contractsForApproval]) {
            if ([contractForApproval.propCompany localizedCaseInsensitiveContainsString:searchStr] || [contractForApproval.propRequestor localizedCaseInsensitiveContainsString:searchStr] || searchStr.length < 1)
                [_contracts addObject:contractForApproval];
        }
    } else {
        [_contracts addObjectsFromArray:[_appDelegate contractsForApproval]];
    }
    [self.tableView reloadData];
}

#pragma mark -
#pragma mark - === scroll view delegate ===
#pragma mark -
-(void)scrollViewWillBeginDragging:(UIScrollView *)scrollView {
    [_searchController.searchBar setShowsCancelButton:NO animated:YES];
    [_searchController.searchBar resignFirstResponder];
}

#pragma mark -
#pragma mark - search results delegate
#pragma mark -
-(void)updateSearchResultsForSearchController:(UISearchController *)searchController {
    if (searchController.active)
        [searchController.searchBar setShowsCancelButton:YES animated:YES];
}


#pragma mark -
#pragma mark === Search bar delegate ===
#pragma mark -
- (void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar {
    _searchWasCancelled = NO;
}

-(void)searchBarCancelButtonClicked:(UISearchBar *)searchBar {
    _searchWasCancelled = YES;
}

-(void)searchBarTextDidEndEditing:(UISearchBar *)searchBar {
    if (_searchWasCancelled)
        searchBar.text = _searchString;
    else
        _searchString = searchBar.text;
}

-(void)searchBarSearchButtonClicked:(UISearchBar *)searchBar {
    [searchBar setShowsCancelButton:NO animated:YES];
}

-(void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText {
    _searchString = searchText;
    [self relaodContractsForApprovalOffice:_searchString];
}


#pragma mark -
#pragma mark === Loader delegate ===
#pragma mark -
-(void)loadFinished {
    [self relaodContractsForApprovalOffice:_searchString];
}

-(void)loadFailedWithError:(NSString *)error {
    [[[UIAlertView alloc] initWithTitle:@"" message:error delegate:nil cancelButtonTitle:@"Dismiss" otherButtonTitles:nil, nil] show];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _contracts.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellId = @"contractcell";
    CellContractsForApproval *cell = [tableView dequeueReusableCellWithIdentifier:cellId];
    if (cell == nil)
        cell = [[CellContractsForApproval alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellId];
    Contract *contract = [_contracts objectAtIndex:indexPath.row];
    switch (contract.propRequestStatus) {
        case ContractStatusIdApprovedByCm:
            cell.propFieldStatus.text = @"Approved by CM";
            cell.propFieldStatus.textColor = [VelosiColors greenAcceptance];
            break;
        case ContractStatusIdApprovedByRfm:
            cell.propFieldStatus.text = @"Approved by RFM";
            cell.propFieldStatus.textColor = [VelosiColors greenAcceptance];
            break;
        case ContractStatusIdApprovedByRm:
            cell.propFieldStatus.text = @"Approved by RM";
            cell.propFieldStatus.textColor = [VelosiColors greenAcceptance];
            break;
        case ContractStatusIdApprovedByLm:
            cell.propFieldStatus.text = @"Approved by LM";
            cell.propFieldStatus.textColor = [VelosiColors greenAcceptance];
            break;
        case ContractStatusIdApprovedByCfo:
            cell.propFieldStatus.text = @"Approved by CFO";
            cell.propFieldStatus.textColor = [VelosiColors greenAcceptance];
            break;
        case ContractStatusIdApprovedByEvp:
            cell.propFieldStatus.text = @"Approved by EVP";
            cell.propFieldStatus.textColor = [VelosiColors greenAcceptance];
            break;
        case ContractStatusIdRejectedByCm:
            cell.propFieldStatus.text = @"Rejected by CM";
            cell.propFieldStatus.textColor = [VelosiColors redRejection];
            break;
        case ContractStatusIdRejectedByRfm:
            cell.propFieldStatus.text = @"Rejected by RFM";
            cell.propFieldStatus.textColor = [VelosiColors redRejection];
            break;
        case ContractStatusIdRejectedByRm:
            cell.propFieldStatus.text = @"Rejected by RM";
            cell.propFieldStatus.textColor = [VelosiColors redRejection];
            break;
        case ContractStatusIdRejectedByLm:
            cell.propFieldStatus.text = @"Rejected by LM";
            cell.propFieldStatus.textColor = [VelosiColors redRejection];
            break;
        case ContractStatusIdRejectedByCfo:
            cell.propFieldStatus.text = @"Rejected by CFO";
            cell.propFieldStatus.textColor = [VelosiColors redRejection];
            break;
        case ContractStatusIdRejectedByEvp:
            cell.propFieldStatus.text = @"Rejected by EVP";
            cell.propFieldStatus.textColor = [VelosiColors redRejection];
            break;
        default:
            cell.propFieldStatus.text = contract.propRequestStatusName;
            cell.propFieldStatus.textColor = [UIColor lightGrayColor];
            break;
    }
    
    cell.propFieldOffice.text = contract.propCompany;
    cell.propFieldRequestorName.text = contract.propRequestor;
    cell.propFieldLocalCurrency.text = contract.propReferenceNumber;
    
    cell.bounds = CGRectMake(0, 0, CGRectGetWidth(tableView.bounds), 99999);
    cell.contentView.bounds = cell.bounds;
    [cell layoutIfNeeded];
    
    cell.propFieldOffice.preferredMaxLayoutWidth = CGRectGetWidth(cell.propFieldOffice.frame);
    cell.propFieldRequestorName.preferredMaxLayoutWidth = CGRectGetWidth(cell.propFieldRequestorName.frame);
    
    return cell;
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
