//
//  CellCapexForApproval.m
//  Salt
//
//  Created by Rick Royd Aban on 10/5/15.
//  Copyright © 2015 Applus Velosi. All rights reserved.
//

#import "CellCapexForApproval.h"
#import "VelosiColors.h"

@implementation CellCapexForApproval

-(void)awakeFromNib{
    [super awakeFromNib];
    self.propFieldStatus.textColor = [VelosiColors lightGrayDisabled];
}

//- (void)layoutSubviews {
//    [super layoutSubviews];
//    CGFloat nameLabelWidth = self.propFieldName.frame.size.width;
//    CGFloat officeLabelWidth = self.propFieldOffice.frame.size.width;
//    self.propFieldOffice.preferredMaxLayoutWidth = officeLabelWidth; //CGRectGetWidth(self.propFieldOffice.frame);
//    self.propFieldName.preferredMaxLayoutWidth = nameLabelWidth; //CGRectGetWidth(self.propFieldName.frame);
//    [super layoutSubviews];
//}

@end
