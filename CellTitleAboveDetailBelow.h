//
//  CellTitleAboveDetailBelow.h
//  Salt
//
//  Created by Rick Royd Aban on 10/5/15.
//  Copyright © 2015 Applus Velosi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CellTitleAboveDetailBelow : UITableViewCell

@property (strong, nonatomic) IBOutlet UILabel *propName;
@property (strong, nonatomic) IBOutlet UILabel *propAnnualCost;

@end
