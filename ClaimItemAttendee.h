//
//  ClaimItemAttendee.h
//  Salt
//
//  Created by Rick Royd Aban on 2015/09/07.
//  Copyright (c) 2015年 Applus Velosi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ClaimItemAttendee : NSObject

- (instancetype)initWithName:(NSString *)name jobTitle:(NSString *)jobTitle desc:(NSString *)desc;

- (NSString *)propName;
- (NSString *)propJobTitle;
- (NSString *)propDesc;

- (NSString *)jsonize;

@end
