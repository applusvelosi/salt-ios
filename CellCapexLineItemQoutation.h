//
//  CellCapexLineItemQoutation.h
//  Salt
//
//  Created by Rick Royd Aban on 10/7/15.
//  Copyright © 2015 Applus Velosi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CellCapexLineItemQoutation : UITableViewCell

@property (strong, nonatomic) IBOutlet UILabel *propFieldSupplier;
@property (strong, nonatomic) IBOutlet UILabel *propFieldAmount;
@property (strong, nonatomic) IBOutlet UILabel *propFieldUSD;
@property (strong, nonatomic) IBOutlet UILabel *propFieldScheme;
@property (strong, nonatomic) IBOutlet UILabel *propFieldPayment;
@property (strong, nonatomic) IBOutlet UILabel *propFieldDesc;

@end
