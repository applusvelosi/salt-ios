//
//  VCCapexesForApproval.m
//  Salt
//
//  Created by Rick Royd Aban on 10/5/15.
//  Copyright © 2015 Applus Velosi. All rights reserved.
//

#import "VCCapexesForApproval.h"
#import "VCCapexesForApprovalSearchResult.h"

@interface VCCapexesForApproval () {

}

@end

@implementation VCCapexesForApproval

- (void)viewDidLoad{
    [super viewDidLoad];
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    [self reloadCapexesForApproval];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"embedContainerVC"]) {
        VCCapexesForApprovalSearchResult *destVC = segue.destinationViewController;
        destVC.parentVC = self;
    }
}

- (IBAction)toggleList:(id)sender {
    [self.propAppDelegate.propSlider toggleSidebar];
}

- (void)reloadCapexesForApproval {
    [AppDelegate showGlogalHUDWithView:self.view];
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
        id result = [self.propAppDelegate.propGatewayOnline capexesForApproval];
        dispatch_async(dispatch_get_main_queue(), ^{
            [AppDelegate hideGlogalHUDWithView:self.view];
            if([result isKindOfClass:[NSString class]])
                [self.delegate loadFailedWithError:result];
            else
                [self.propAppDelegate updateCapexesForApproval:result];
            [self.delegate loadFinished];
        });
    });
}


@end
