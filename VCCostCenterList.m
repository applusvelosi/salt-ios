//
//  VCCostCenterList.m
//  Salt
//
//  Created by Rick Royd Aban on 2/3/16.
//  Copyright © 2016 Applus Velosi. All rights reserved.
//

#import "VCCostCenterList.h"
#import "MBProgressHUD.h"

@interface VCCostCenterList()<UIAlertViewDelegate>{
    
    IBOutlet UITableView *_propLV;
    NSMutableArray *_costCenters;
}
@end

@implementation VCCostCenterList

- (void)viewDidLoad{
    [super viewDidLoad];

    _costCenters = [NSMutableArray array];
    _propLV.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    _propLV.delegate = self;
    _propLV.dataSource = self;
    
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
        id result = [self.propAppDelegate.propGatewayOnline allCostCentersForThisOffice];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
            if([result isKindOfClass:[NSString class]])
                [[[UIAlertView alloc] initWithTitle:@"" message:result delegate:self cancelButtonTitle:@"Dismiss" otherButtonTitles:nil, nil] show];
            else{
                [_costCenters removeAllObjects];
                [_costCenters addObjectsFromArray:result];
                [_propLV reloadData];
            }
        });
    });
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    cell.textLabel.text = ((CostCenter *)[_costCenters objectAtIndex:indexPath.row]).propCostCenterName;
    
    return cell;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return _costCenters.count;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    NSLog(@"vccostcenterlist %d %@",[[_costCenters objectAtIndex:indexPath.row] propCostCenterID],[[_costCenters objectAtIndex:indexPath.row] propCostCenterName]);
    CostCenter *costCenter = [_costCenters objectAtIndex:indexPath.row];
    _vcClaimHeader.propCostCenter = costCenter;
    [_vcClaimHeader updateCostCenter:costCenter];
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    [self.navigationController popViewControllerAnimated:YES];
}
@end
