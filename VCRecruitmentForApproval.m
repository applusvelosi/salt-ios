//
//  VCRecruitmentForApproval.m
//  Salt
//
//  Created by Rick Royd Aban on 9/30/15.
//  Copyright © 2015 Applus Velosi. All rights reserved.
//

#import "VCRecruitmentForApproval.h"
#import "VelosiCustomPicker.h"
#import "CellRecruitmentForApproval.h"
#import "VCRecruitmentForApprovalDetail.h"
#import "Recruitment.h"
#import "VelosiColors.h"
#import "MBProgressHUD.h"

@interface VCRecruitmentForApproval(){
    
    IBOutlet UITextField *_propFieldPosition;
    IBOutlet UITextField  *_propFieldStatus;
    IBOutlet UITableView *_propLV;
    
    UIPickerView *_pickerDeploymentOffice, *_pickerStatus;
    
    NSMutableArray  *_recruitmentPositions, *_recruitmentStatuses,
                    *_filteredRecruitments;
}

@end

@implementation VCRecruitmentForApproval

#pragma mark -
#pragma mark === View Life Cycle Management ===
#pragma mark -

- (void)viewDidLoad{
    [super viewDidLoad];
    
    _recruitmentPositions = [@[[AppDelegate all]] mutableCopy];
    _recruitmentStatuses = [@[[AppDelegate all]] mutableCopy];
    _filteredRecruitments = [NSMutableArray array];
    
    _pickerDeploymentOffice = [[VelosiCustomPicker alloc] initWithArray:_recruitmentPositions rowSelectionDelegate:self selectedItem:nil];
    _pickerStatus = [[VelosiCustomPicker alloc] initWithArray:_recruitmentStatuses rowSelectionDelegate:self selectedItem:nil];
    
    _propLV.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    _propLV.estimatedRowHeight = 140;
    _propLV.rowHeight = UITableViewAutomaticDimension;
    _propLV.dataSource = self;
    _propLV.delegate = self;
    
    _propFieldPosition.delegate = self;
    _propFieldStatus.delegate = self;
    
    _propFieldPosition.text = [AppDelegate all];
    _propFieldStatus.text = [AppDelegate all];
}

- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    [self refresh];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    VCRecruitmentForApprovalDetail *vcRecruitmentForApprovalDetail = (VCRecruitmentForApprovalDetail *)segue.destinationViewController;
    Recruitment *selectedRecruitment = [_filteredRecruitments objectAtIndex:((CellRecruitmentForApproval *)sender).tag];
    vcRecruitmentForApprovalDetail.propRecruitmentID = [selectedRecruitment propRecruitmentRequestID];
}

#pragma mark -
#pragma mark - === scroll view delegate ===
#pragma mark -
-(void)scrollViewWillBeginDragging:(UIScrollView *)scrollView {
    if ([_propFieldPosition isFirstResponder])
        [_propFieldPosition resignFirstResponder];
    if ([_propFieldStatus isFirstResponder])
        [_propFieldStatus resignFirstResponder];
}


#pragma mark -
#pragma mark === Private Method ===
#pragma mark -

- (void)refresh{
    UIWindow *window = [[[UIApplication sharedApplication] delegate] window];
    [AppDelegate showGlogalHUDWithView:window];
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
        id result = [self.propAppDelegate.propGatewayOnline recruitmentsForApproval];
        
        dispatch_async(dispatch_get_main_queue(),^{
            [AppDelegate hideGlogalHUDWithView:window];
            [_recruitmentPositions removeAllObjects];
            [_recruitmentStatuses removeAllObjects];
            [_recruitmentPositions addObject:[AppDelegate all]];
            [_recruitmentStatuses addObject:[AppDelegate all]];
            
            if([result isKindOfClass:[NSString class]])
                [[[UIAlertView alloc] initWithTitle:@"" message:result delegate:nil cancelButtonTitle:@"Dismiss" otherButtonTitles:nil, nil] show];
            else{
                [self.propAppDelegate updateRecruitmentsForApproval:result];
                
                for(Recruitment *recruitment in result){
                    NSString *officeName = [recruitment propOfficeOfDeploymentName];
                    NSString *statusName = [recruitment propStatusName];
                    
                    if(![_recruitmentPositions containsObject:officeName])
                        [_recruitmentPositions addObject:officeName];
                    if(![_recruitmentStatuses containsObject:statusName])
                        [_recruitmentStatuses addObject:statusName];
                }
            }
            
            _pickerDeploymentOffice = [[VelosiCustomPicker alloc] initWithArray:_recruitmentPositions rowSelectionDelegate:self selectedItem:[AppDelegate all]];
            _pickerStatus = [[VelosiCustomPicker alloc] initWithArray:_recruitmentStatuses rowSelectionDelegate:self selectedItem:[AppDelegate all]];
            
            [self filterRecruitments];
        });
    });
}

- (void)filterRecruitments{
    [_filteredRecruitments removeAllObjects];
    for(Recruitment *recruitment in [self.propAppDelegate recruitmentsForApproval]){
        if([_propFieldPosition.text isEqualToString:[recruitment propRequesterOfficeName]] || [_propFieldPosition.text isEqualToString:[AppDelegate all]]){
            if([_propFieldStatus.text isEqualToString:[recruitment propStatusName]] || [_propFieldStatus.text isEqualToString:[AppDelegate all]]){
                [_filteredRecruitments addObject:recruitment];
            }
        }
    }
    
    [_propLV reloadData];
}


#pragma mark -
#pragma mark - Table view data source delegate
#pragma mark -

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    CellRecruitmentForApproval *cell = (CellRecruitmentForApproval *)[tableView dequeueReusableCellWithIdentifier:@"cell"];
    Recruitment *recruitment = [_filteredRecruitments objectAtIndex:indexPath.row];
    NSString *tempStatusName;
    if([recruitment propStatusID] == RECRUITMENT_STATUSID_APPROVEDBYCM){
        cell.propFieldStatus.textColor = [VelosiColors greenAcceptance];
        tempStatusName = @"Approved by CM";
    }else if([recruitment propStatusID] == RECRUITMENT_STATUSID_REJECTEDBYCM){
        cell.propFieldStatus.textColor = [VelosiColors redRejection];
        tempStatusName = @" Rejected by CM";
    }else if([recruitment propStatusID] == RECRUITMENT_STATUSID_APPROVEDBYRM){
        cell.propFieldStatus.textColor = [VelosiColors greenAcceptance];
        tempStatusName = @"Approved by RM";
    }else if([recruitment propStatusID] == RECRUITMENT_STATUSID_REJECTEDBYRM){
        cell.propFieldStatus.textColor = [VelosiColors redRejection];
        tempStatusName = @"Rejected by RM";
    }else if([recruitment propStatusID] == RECRUITMENT_STATUSID_APPROVEDBYMHR){
        cell.propFieldStatus.textColor = [VelosiColors greenAcceptance];
        tempStatusName = @"Approved by MHR";
    }else if([recruitment propStatusID] == RECRUITMENT_STATUSID_REJECTEDBYMHR){
        cell.propFieldStatus.textColor = [VelosiColors redRejection];
        tempStatusName = @"Rejected by MHR";
    }else{
        cell.propFieldStatus.textColor = [UIColor lightGrayColor];
        tempStatusName = [recruitment propStatusName];
    }
    
    cell.propFieldPositionType.text = [recruitment propRecruitmentNumber];
    cell.propFieldApplicantName.text = [recruitment propRequesterName];
    cell.propFieldOfficeName.text = [recruitment propRequesterOfficeName];
    cell.propFieldStatus.text = tempStatusName;
    cell.tag = indexPath.row;
    
    cell.bounds = CGRectMake(0, 0, CGRectGetWidth(tableView.bounds), 99999);
    cell.contentView.bounds = cell.bounds;
    [cell layoutIfNeeded];
    
    cell.propFieldOfficeName.preferredMaxLayoutWidth = CGRectGetWidth(cell.propFieldOfficeName.frame);
    cell.propFieldApplicantName.preferredMaxLayoutWidth = CGRectGetWidth(cell.propFieldApplicantName.frame);
    
    return cell;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return _filteredRecruitments.count;
}

#pragma mark -
#pragma mark - Text Field Delegate
#pragma mark -


- (void)textFieldDidBeginEditing:(UITextField *)textField{
    if(textField == _propFieldPosition)
        textField.inputView = _pickerDeploymentOffice;
    else if(textField == _propFieldStatus)
        textField.inputView = _pickerStatus;
}

#pragma mark -
#pragma mark - VelosiPickerRowSelection Delegate
#pragma mark -

- (void)pickerSelection:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component{
    if(pickerView == _pickerDeploymentOffice){
        _propFieldPosition.text = [_recruitmentPositions objectAtIndex:row];
    }else if(pickerView == _pickerStatus){
        _propFieldStatus.text = [_recruitmentStatuses objectAtIndex:row];
    }
    
    [self filterRecruitments];
}

#pragma mark -
#pragma mark - UI Event Handler
#pragma mark -

- (IBAction)toggleList:(id)sender {
    [self.propAppDelegate.propSlider toggleSidebar];
}
- (IBAction)refresh:(id)sender {
    [self refresh];
}


@end
