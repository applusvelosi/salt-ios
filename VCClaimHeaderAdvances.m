//
//  VCClaimHeaderAdvances.m
//  Salt
//
//  Created by Rick Royd Aban on 2015/08/12.
//  Copyright (c) 2015年 Applus Velosi. All rights reserved.
//

#import "VCClaimHeaderAdvances.h"
#import "MBProgressHUD.h"
#import "VCCostCenterList.h"
#import "AppDelegate.h"

@interface VCClaimHeaderAdvances (){
    
    IBOutlet UITextField *_propFieldStaff;
    IBOutlet UITextField *_propFieldOffice;
    IBOutlet UITableViewCell *_propCellCostCenter;
    IBOutlet UITextField *_propFieldApprover;

    NSString *_olBusinessAdvanceJSON;
    BusinessAdvance *_newBusinessAdvance;
    AppDelegate *_propAppDelegate;
}

@end

@implementation VCClaimHeaderAdvances

- (void)viewDidLoad {
    [super viewDidLoad];

    _propAppDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    for(BusinessAdvance *ba in _propAppDelegate.myClaims){
        if(ba.propTypeID==CLAIMTYPEID_ADVANCES && !(ba.propStatusID==CLAIMSTATUSID_LIQUIDATED || ba.propStatusID==CLAIMSTATUSID_CANCELLED)){
            [[[UIAlertView alloc] initWithTitle:@"" message:@"Cannot create another unliquidated Business Advance" delegate:self cancelButtonTitle:@"Dismiss" otherButtonTitles:nil, nil] show];
            break;
        }
    }
    
    if(_propBusinessAdvance != nil){ //use the data in the claim object
        _propFieldStaff.text = [_propBusinessAdvance propStaffName];
        _propFieldOffice.text = [_propBusinessAdvance propOfficeName];
        _propCellCostCenter.detailTextLabel.text = [_propBusinessAdvance propCostCenterName];
        _propFieldApprover.text = [_propBusinessAdvance propApproverName];
        [self updateCostCenter:[[CostCenter alloc] initWithID:_propBusinessAdvance.propCostCenterID name:_propBusinessAdvance.propCostCenterName]];
    }else{ //use the data in the user object from appdelegate
        _propFieldStaff.text = [[self.propAppDelegate getStaff] propFullName];
        _propFieldOffice.text = [[self.propAppDelegate office] officeName];
        _propFieldApprover.text = [[self.propAppDelegate getStaff] propExpenseApproverName];
        CostCenter *costCenter = [[CostCenter alloc] initWithID:self.propAppDelegate.getStaff.propCostCenterID name:self.propAppDelegate.getStaff.propCostCenterName];
        self.propCostCenter = costCenter;
        [self updateCostCenter:costCenter];
    }
}

- (IBAction)save:(id)sender {
    _olBusinessAdvanceJSON = (_propBusinessAdvance != nil)?[_propBusinessAdvance jsonForWebServices]:[ClaimHeader jsonFromNewEmptyClaimHeader];
    _newBusinessAdvance = [[BusinessAdvance alloc] initWithCostCenterID:[[super propCostCenter] propCostCenterID] costCenterName:[[super propCostCenter] propCostCenterName] claimTypeID:CLAIMTYPEID_ADVANCES isPaidByCC:NO baIDCharged:0 bacNumber:@"" appDelegate:self.propAppDelegate];
    [AppDelegate showGlogalHUDWithView:self.view];
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
        NSString *result = nil; //[self.propAppDelegate.propGatewayOnline saveClaimWithNewClaimHeaderJSON:[_newBusinessAdvance jsonForWebServices] oldClaimHeaderJSON:_olBusinessAdvanceJSON];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [AppDelegate hideGlogalHUDWithView:self.view];
            [[[UIAlertView alloc] initWithTitle:@"" message:([result isEqualToString:@"OK"])?@"Business Advance Saved Successfully":result delegate:self cancelButtonTitle:@"Dismiss" otherButtonTitles:nil, nil] show];
        });
    });
}

- (void)updateCostCenter:(CostCenter *)costCenter{
    _propCellCostCenter.detailTextLabel.text = [costCenter propCostCenterName];
    [_propBusinessAdvance updateCostCenter:costCenter];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    [self.navigationController dismissViewControllerAnimated:YES completion:nil];
}

@end
