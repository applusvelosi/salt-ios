//
//  CapexForApprovalLineItem.m
//  Salt
//
//  Created by Rick Royd Aban on 10/6/15.
//  Copyright © 2015 Applus Velosi. All rights reserved.
//

#import "CapexForApprovalLineItem.h"

@interface CapexForApprovalLineItem(){
    NSMutableDictionary *_capexForApprovalLineItem;
}
@end

@implementation CapexForApprovalLineItem

- (CapexForApprovalLineItem *)initWithDictionary:(NSDictionary *)jsonCapexForApprovalLineItem onlineGateway:(id)onlineGateway{
    if([super init]){
        _capexForApprovalLineItem = [NSMutableDictionary dictionaryWithDictionary:jsonCapexForApprovalLineItem];
        [_capexForApprovalLineItem setObject:@([[_capexForApprovalLineItem objectForKey:@"BaseCurrencyID"] intValue]) forKey:@"BaseCurrencyID"];
        [_capexForApprovalLineItem setObject:@([[_capexForApprovalLineItem objectForKey:@"CapexID"] intValue]) forKey:@"CapexID"];
        [_capexForApprovalLineItem setObject:@([[_capexForApprovalLineItem objectForKey:@"CapexLineItemID"] intValue]) forKey:@"CapexLineItemID"];
        [_capexForApprovalLineItem setObject:@([[_capexForApprovalLineItem objectForKey:@"CategoryID"] intValue]) forKey:@"CategoryID"];
        [_capexForApprovalLineItem setObject:@([[_capexForApprovalLineItem objectForKey:@"CurrencyID"] intValue]) forKey:@"CurrencyID"];
        [_capexForApprovalLineItem setObject:@([[_capexForApprovalLineItem objectForKey:@"RequesterID"] intValue]) forKey:@""];
//        [_capexForApprovalLineItem setObject:@([[_capexForApprovalLineItem objectForKey:@"baseCurrencySymbol"] integerValue]) forKey:@"BaseCurrencySymbol"];
        //dates
        [_capexForApprovalLineItem setObject:[onlineGateway deserializeJsonDateString:[_capexForApprovalLineItem objectForKey:@"DateCreated"]] forKey:@"DateCreated"];
    }
    
    return self;
}

-(NSDictionary *)savableDictionary{
    return _capexForApprovalLineItem;
}

- (float)propAmount{
    return [[_capexForApprovalLineItem objectForKey:@"Amount"] floatValue];
}

- (float)propAmountInUSD{
    return [[_capexForApprovalLineItem objectForKey:@"AmountInUSD"] floatValue];
}

- (int)propBaseCurrencyID{
    return [[_capexForApprovalLineItem objectForKey:@"BaseCurrencyID"] intValue];
}

- (NSString *)propBaseCurrencyName{
    return [_capexForApprovalLineItem objectForKey:@"BaseCurrencyName"];
}

- (NSString *)propBaseCurrencySymbol{
    return [_capexForApprovalLineItem objectForKey:@"BaseCurrencySymbol"];
}

- (float)propBaseExchangeRate{
    return [[_capexForApprovalLineItem objectForKey:@"BaseExchangeRate"] floatValue];
}

- (int)propCapexID{
    return [[_capexForApprovalLineItem objectForKey:@"CapexID"] intValue];
}

- (int)propCapexLineItemID{
    return [[_capexForApprovalLineItem objectForKey:@"CapexLineItemID"] intValue];
}

- (NSString *)propCapexLineItemNumber{
    return [_capexForApprovalLineItem objectForKey:@"CapexLineItemNumber"];
}

- (NSString *)propCapexNumber{
    return [_capexForApprovalLineItem objectForKey:@"CapexNumber"];
}

- (int)propCategoryID{
    return [[_capexForApprovalLineItem objectForKey:@"CategoryID"] intValue];
}

- (NSString *)propCategoryName{
    return [_capexForApprovalLineItem objectForKey:@"CategoryName"];
}

- (int)propCurrencyID{
    return [[_capexForApprovalLineItem objectForKey:@"CurrencyID"] intValue];
}

- (NSString *)propCurrencyName{
    return [_capexForApprovalLineItem objectForKey:@"CurrencyName"];
}

- (NSString *)propCurrencySymbol{
    return [_capexForApprovalLineItem objectForKey:@"CurrencySymbol"];
}

- (NSString *)propDateCreated{
    return [_capexForApprovalLineItem objectForKey:@"DateCreated"];
}

- (NSString *)propDescription{
    return [_capexForApprovalLineItem objectForKey:@"Description"];
}

- (int)propQuantity{
    return [[_capexForApprovalLineItem objectForKey:@"Quantity"] intValue];
}

- (int)propRequesterID{
    return [[_capexForApprovalLineItem objectForKey:@"RequesterID"] intValue];
}

- (NSString *)propRequesterName{
    return [_capexForApprovalLineItem objectForKey:@"RequesterName"];
}

- (float)propUnitCost{
    return [[_capexForApprovalLineItem objectForKey:@"UnitCost"] floatValue];
}

- (float)propUSDExchangeRate{
    return [[_capexForApprovalLineItem objectForKey:@"USDExchangeRate"] floatValue];
}

- (float)propUsefulLife{
    return [[_capexForApprovalLineItem objectForKey:@"UsefulLife"] floatValue];
}
@end
