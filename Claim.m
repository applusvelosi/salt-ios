//
//  Claim.m
//  Salt
//
//  Created by Rick Royd Aban on 2015/07/29.
//  Copyright (c) 2015年 Applus Velosi. All rights reserved.
//

#import "Claim.h"

@implementation Claim

-(instancetype)initClaimWithJSONDict:(NSDictionary *)dictionary withAppDelegate:(AppDelegate *)appDelegate {
    return [super initWithJSONDictionary:dictionary withAppDelegate:appDelegate];
}

- (BOOL)isPaidByCompanyCard{
    return [[[super claimHeader] objectForKey:@"IsPaidByCompanyCC"] boolValue];
}

- (void)updateIsClaimPaidByCC:(BOOL)isPaidByCC {
    [[super claimHeader] setObject:[NSNumber numberWithBool:isPaidByCC] forKey:@"IsPaidByCompanyCC"];
}



@end
