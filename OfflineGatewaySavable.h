//
//  OfflineGatewaySavable.h
//  Salt
//
//  Created by Rick Royd Aban on 03/01/2017.
//  Copyright © 2017 Applus Velosi. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol OfflineGatewaySavable <NSObject>

- (NSDictionary *)savableDictionary;

@end
