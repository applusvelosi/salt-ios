//
//  VCSlider.m
//  Salt iOS
//
//  Created by Rick Royd Aban on 5/18/15.
//  Copyright (c) 2015 Rick Royd Aban. All rights reserved.
//

#define MAXPANNING 250
#define MINPANTOSHOWMAINPAGE 100
#define ANIMATIONDURATION 0.3f

#import "VCSlider.h"
#import "DAKeyboardControl.h"
#import "AppDelegate.h"
#import "VelosiColors.h"
#import "CellSidebar.h"
#import "PageNavigatorFactory.h"
//#import <Parse/PFQuery.h>

@interface VCSlider (){
    IBOutlet UITableView *_propLvSidebar;
    IBOutlet UIView *_propMainPage;
    
    CGFloat _mainPageX;
    UIViewController *_currMainController;
    NSIndexPath *_currIndexPath;
    BOOL _isSidebarShowing;
    
    
    NSArray *_sidebarMyAccountLabels, *_sidebarMyAccountImages, *_sidebarMyAccountHighlitedImages;
    NSMutableArray *_sidebarForApprovalLabels, *_sidebarForApprovalImages, *_sidebarForApprovalHighlightedImages;
    NSArray *_sidebarHolidayLabels, *_sidebarHolidayImages, *_sidebarHolidayHighlightedImages;
    
    AppDelegate *appDelegate;
}
@end

@implementation VCSlider


#pragma mark -
#pragma mark === View Life Cycle Management ===
#pragma mark -
- (void)viewDidLoad {
    [super viewDidLoad];
    
    [_propMainPage  addKeyboardPanningWithActionHandler:nil];
    appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    [appDelegate setSlider:self];
    _mainPageX = 0;
    _isSidebarShowing = NO;
    
    _propMainPage.layer.shadowColor = [UIColor blackColor].CGColor;
    _propMainPage.layer.shadowOpacity = 1;
    _propMainPage.layer.shadowOffset = CGSizeMake(0, 0);
    
    [self updateSidebarItemsShouldReload:NO];
    _propLvSidebar.delegate = self;
    _propLvSidebar.dataSource = self;
    
    NSIndexPath *initIndexPath = ([appDelegate.propGatewayOffline isLoggedIn])?[NSIndexPath indexPathForRow:0 inSection:0]:[NSIndexPath indexPathForRow:4 inSection:0];
    
    [_propLvSidebar.delegate tableView:_propLvSidebar didSelectRowAtIndexPath:initIndexPath];
}


#pragma mark -
#pragma mark === Public Methods ===
#pragma mark -
- (void)toggleSidebar{
    if(!_isSidebarShowing)
        [self.view endEditing:YES];
    [self updateSidebarWillShow:!_isSidebarShowing];
}

- (void)login {
    if([appDelegate.propGatewayOffline isLoggedIn]){
        NSIndexPath *reloadedIndexPath = [NSIndexPath indexPathForRow:0 inSection:0];
        [_propLvSidebar selectRowAtIndexPath:reloadedIndexPath animated:NO scrollPosition:UITableViewScrollPositionBottom];
        [_propLvSidebar.delegate tableView:_propLvSidebar didSelectRowAtIndexPath:reloadedIndexPath];
        [self updateSidebarItemsShouldReload:YES];
    }else
        [[[UIAlertView alloc] initWithTitle:@"" message:@"Error! Not logged in" delegate:nil cancelButtonTitle:@"Dismiss" otherButtonTitles:nil, nil] show];
}

- (void)switchToLeavesForApprovalPage {
    [_propLvSidebar.delegate tableView:_propLvSidebar didSelectRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:1]];
}

- (void)switchToClaimsForApprovalPage {
    [_propLvSidebar.delegate tableView:_propLvSidebar didSelectRowAtIndexPath:[NSIndexPath indexPathForRow:1 inSection:1]];
}

- (void)switchToCapexForApprovalPage {
    [_propLvSidebar.delegate tableView:_propLvSidebar didSelectRowAtIndexPath:[NSIndexPath indexPathForRow:2 inSection:1]];
}

- (void)switchToRecruitmentsForApprovalPage {
    [_propLvSidebar.delegate tableView:_propLvSidebar didSelectRowAtIndexPath:[NSIndexPath indexPathForRow:3 inSection:1]];
}

-(void)switchToTravelForApprovalPage {
}


#pragma mark -
#pragma mark === Helper methods ===
#pragma mark -
- (void)changePage:(UIViewController *)controller{
    if([controller isKindOfClass:[UINavigationController class]])
        [(UINavigationController *)controller setDelegate:self];
    
    if (_currMainController == nil) {
        controller.view.frame = _propMainPage.bounds;
        _currMainController = controller;
        [self addChildViewController:_currMainController];
        [_propMainPage addSubview:_currMainController.view];
        [_currMainController didMoveToParentViewController:self];
    } else if (_currMainController != controller && controller !=nil) {
        controller.view.frame = _propMainPage.bounds;
        [_currMainController willMoveToParentViewController:nil];
        [self addChildViewController:controller];
        self.view.userInteractionEnabled = NO;
        [self transitionFromViewController:_currMainController
                          toViewController:controller
                                  duration:0
                                   options:UIViewAnimationOptionTransitionNone
                                animations:^{}
                                completion:^(BOOL finished){
                                    self.view.userInteractionEnabled = YES;
                                    [_currMainController removeFromParentViewController];
                                    [controller didMoveToParentViewController:self];
                                    _currMainController = controller;
                                    [self updateSidebarWillShow:NO];
                                }
         ];
    }
}

- (void)updateSidebarWillShow:(BOOL)willShow{
    _propMainPage.userInteractionEnabled = NO;
    _propLvSidebar.userInteractionEnabled = NO;
    
    [UIView animateWithDuration:ANIMATIONDURATION animations:^{
        _propMainPage.transform = CGAffineTransformMakeTranslation((willShow)?MAXPANNING:0, 0);
    } completion:^(BOOL finished){
        _propMainPage.userInteractionEnabled = YES;
        _propLvSidebar.userInteractionEnabled = YES;
        _isSidebarShowing = willShow;
        _mainPageX = 0;
    }];
}

- (void)updateSidebarItemsShouldReload:(BOOL)shouldReload{
    //LABELS
    _sidebarMyAccountLabels = @[@"Home", @"My Leaves", @"My Claims", @"My Calendar", @"Logout"];
    _sidebarForApprovalLabels = [NSMutableArray arrayWithObjects:@"Leaves", @"Claims", @"Capexes", @"HR", nil];
    //@[@"Leaves", @"Claims", @"Capexes", @"HR"];
    
    _sidebarHolidayLabels = @[@"This Week", @"This Month", @"Local Holidays"];
    
    //IMAGES
    _sidebarMyAccountImages = @[@"icon_home", @"icon_myleaves", @"icon_myclaims", @"icon_mycalendar", @"icon_logout"];
    _sidebarForApprovalImages = [NSMutableArray arrayWithObjects:@"icon_leavesforapproval", @"icon_claims_forapproval", @"icon_capexforapprovals", @"icon_rfa", nil];
    _sidebarHolidayImages = @[@"icon_weeklyholidays", @"icon_monthlyholidays", @"icon_localholidays"];
    
    //HIGHLIGHTED IMAGES
    _sidebarMyAccountHighlitedImages = @[@"icon_home_sel", @"icon_myleaves_sel", @"icon_myclaims_sel", @"icon_mycalendar_sel", @"icon_logout_sel"];
    _sidebarForApprovalHighlightedImages = [NSMutableArray arrayWithObjects:@"icon_leavesforapproval_sel", @"icon_claims_forapproval_sel", @"icon_capexforapprovals_sel", @"icon_rfa_sel", nil];
    _sidebarHolidayHighlightedImages = [NSMutableArray arrayWithObjects:@"icon_weeklyholidays_sel", @"icon_monthlyholidays_sel", @"icon_localholidays_sel", nil];
    
    if([[appDelegate getStaff] isAdmin] || [[appDelegate getStaff] isCM] || [[appDelegate getStaff] isManager]) {
        if (![[appDelegate getStaff] isManager]) {
            [_sidebarForApprovalLabels addObject:@"Gifts & Hospitality"];
            [_sidebarForApprovalImages addObject:@"icon_giftsforapproval"];
            [_sidebarForApprovalHighlightedImages addObject:@"icon_giftsforapproval_sel"];
        }
        if (LMid == [[appDelegate getStaff] propStaffID] || [[appDelegate getStaff] isAdmin] || [[appDelegate getStaff] isCM]) {
            [_sidebarForApprovalLabels addObject:@"Contracts & Tenders"];
            [_sidebarForApprovalImages addObject:@"icon_contractsforapproval"];
            [_sidebarForApprovalHighlightedImages addObject:@"icon_contractforapproval_sel"];
        }
    }
    [_propLvSidebar reloadData];
}


#pragma mark -
#pragma mark === Table view data source ===
#pragma mark -
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return ([[appDelegate getStaff] isAdmin] || [[appDelegate getStaff] isCM] || [[appDelegate getStaff] isManager])?3:2;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    switch (section) {
        case 0:
            return @"My Account";
        case 1:
            return ([[appDelegate getStaff] isAdmin] || [[appDelegate getStaff] isCM] || [[appDelegate getStaff] isManager] || [[appDelegate getStaff] isAM]) ? @"For Approval" : @"Holidays";
        case 2:
            return @"Holidays";
        default:
            return @"";
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    switch (section) {
        case 0:
            return 5; //HOME, MY LEAVES, MY CLAIMS, MY CALENDAR, LOGOUT
        case 1:
            //APPROVAL SECTION (LEAVES, CLAIMS, RECRUIMENT, CAPEX, GIFTS, CONTRACT)
            if ([[appDelegate getStaff] isAdmin] || [[appDelegate getStaff] isCM] || [[appDelegate getStaff] isManager]) {
                if ([[appDelegate getStaff] isAdmin] || [[appDelegate getStaff] isCM])
                    return 6;
                else if (LMid == [[appDelegate getStaff] propStaffID])
                    return 5;
                if (![[appDelegate getStaff] isManager])
                    return 5;
                return 4;
            } else 
                return 3;
        case 2:
            return 3; //HOLIDAY SECTION (WEEKLY, MONTHLY, LOCAL)
        default:
            return 0;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    CellSidebar *cell = [_propLvSidebar dequeueReusableCellWithIdentifier:@"cell"];
    
    switch (indexPath.section) {
        case 0:
            cell.cellTitle.text = [_sidebarMyAccountLabels objectAtIndex:indexPath.row];
            if(_currIndexPath.section == 0 && _currIndexPath.row == indexPath.row) {
                cell.cellIImage.image = [UIImage imageNamed:[_sidebarMyAccountHighlitedImages objectAtIndex:indexPath.row]];
                cell.cellTitle.textColor = [VelosiColors orangeVelosi];
            } else {
                cell.cellIImage.image = [UIImage imageNamed:[_sidebarMyAccountImages objectAtIndex:indexPath.row]];
                cell.cellTitle.textColor = [VelosiColors blackSidebarFont];
            }
            break;
            
        case 1:
            cell.cellTitle.text = [([[appDelegate getStaff] isAdmin] || [[appDelegate getStaff] isCM] || [[appDelegate getStaff] isManager])?_sidebarForApprovalLabels:_sidebarHolidayLabels objectAtIndex:indexPath.row];
            if(_currIndexPath.section == 1 && _currIndexPath.row == indexPath.row) {
                cell.cellIImage.image = [UIImage imageNamed:[_sidebarForApprovalHighlightedImages objectAtIndex:indexPath.row]];
                cell.cellTitle.textColor = [VelosiColors orangeVelosi];
            } else {
                cell.cellIImage.image = [UIImage imageNamed:[([[appDelegate getStaff] isAdmin] || [[appDelegate getStaff] isCM] || [[appDelegate getStaff] isManager] || [[appDelegate getStaff] isAM])?_sidebarForApprovalImages:_sidebarHolidayImages objectAtIndex:indexPath.row]];
                cell.cellTitle.textColor = [VelosiColors blackSidebarFont];
            }
            break;
        case 2:
            cell.cellTitle.text = [_sidebarHolidayLabels objectAtIndex:indexPath.row];
            
            if(_currIndexPath.section == 2 && _currIndexPath.row == indexPath.row){
                cell.cellIImage.image = [UIImage imageNamed:[_sidebarHolidayHighlightedImages objectAtIndex:indexPath.row]];
                cell.cellTitle.textColor = [VelosiColors orangeVelosi];
            }else{
                cell.cellIImage.image = [UIImage imageNamed:[_sidebarHolidayImages objectAtIndex:indexPath.row]];
                cell.cellTitle.textColor = [VelosiColors blackSidebarFont];
            }
            
            break;
            
        default:
            break;
    }
    
    cell.selected = (_currIndexPath.section == indexPath.section)?YES:NO;
    
    return cell;
}


#pragma mark -
#pragma mark === Table view delegate ===
#pragma mark -
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    //Have to manually set label and image colors before and after selection since ios will remove borders when relying to default highlighting functionalities
    NSIndexPath *temp = indexPath;
    if(_currIndexPath!=nil && _currIndexPath.section == indexPath.section && _currIndexPath.row == indexPath.row)
        [self updateSidebarWillShow:NO];
    else {
        CellSidebar *currSelectedCell = (CellSidebar *)[_propLvSidebar cellForRowAtIndexPath:indexPath];
        CellSidebar *prevSelectedCell = (CellSidebar *)[_propLvSidebar cellForRowAtIndexPath:_currIndexPath];
        
        //manage icon and label color for previously selected cells
        [prevSelectedCell.cellTitle setTextColor:[VelosiColors blackSidebarFont]];
        
        switch (_currIndexPath.section) {
            case 0: [prevSelectedCell.cellIImage setImage:[UIImage imageNamed:[_sidebarMyAccountImages objectAtIndex:_currIndexPath.row]]]; break;
            case 1: [prevSelectedCell.cellIImage setImage:[UIImage imageNamed:[([[appDelegate getStaff] isAdmin] || [[appDelegate getStaff] isManager] || [[appDelegate getStaff] isCM])?_sidebarForApprovalImages:_sidebarHolidayImages objectAtIndex:_currIndexPath.row]]];
                break;
            case 2: [prevSelectedCell.cellIImage setImage:[UIImage imageNamed:[_sidebarHolidayImages objectAtIndex:_currIndexPath.row]]]; break;
            default: break;
        }
        
        [currSelectedCell.cellTitle setTextColor:[VelosiColors orangeVelosi]];
        switch (indexPath.section) {
            case 0: //HOME ITEMS
                [currSelectedCell.cellIImage setImage:[UIImage imageNamed:[_sidebarMyAccountHighlitedImages objectAtIndex:indexPath.row]]];
                switch (indexPath.row) {
                    case 0: [self changePage:[appDelegate.propPageNavigator vcHome]]; break;
                    case 1: [self changePage:[appDelegate.propPageNavigator vcMyLeaves]]; break;
                    case 2: [self changePage:[appDelegate.propPageNavigator vcMyClaims]]; break;
                    case 3: [self changePage:[appDelegate.propPageNavigator vcMyCalendar]]; break;
                    case 4: {   [currSelectedCell.cellIImage setImage:[UIImage imageNamed:@"icon_logout_sel"]]; //LOGOUT
                        [appDelegate.propGatewayOffline logout];
                        [appDelegate.propPageNavigator logout];
                        [self changePage:[appDelegate.propPageNavigator vcLogin]];
                    }
                    default: break;
                }
                break;
            case 1:
                if([[appDelegate getStaff] isAdmin] || [[appDelegate getStaff] isManager] || [[appDelegate getStaff] isCM]) {
                    [currSelectedCell.cellIImage setImage:[UIImage imageNamed:[_sidebarForApprovalHighlightedImages objectAtIndex:indexPath.row]]];
                    if (indexPath.row == 0)
                        [self changePage:[appDelegate.propPageNavigator vcLeavesForApproval]];
                    else if (indexPath.row == 1)
                        [self changePage:[appDelegate.propPageNavigator vcClaimsForApproval]];
                    else if (indexPath.row == 2)
                        [self changePage:[appDelegate.propPageNavigator vcCapexForApproval]];
                    else if (indexPath.row == 3)
                        [self changePage:[appDelegate.propPageNavigator vcRecruitmentForApproval]];
                    else if (indexPath.row == 4) {
                        if (LMid == [[appDelegate getStaff] propStaffID])
                            [self changePage:[appDelegate.propPageNavigator vcContractForApproval]];
                        else
                            [self changePage:[appDelegate.propPageNavigator vcGiftsForApproval]];
                    } else if (indexPath.row == 5)
                        [self changePage:[appDelegate.propPageNavigator vcContractForApproval]];
                    
//                    switch(indexPath.row) {
//                        case 0: [self changePage:[appDelegate.propPageNavigator vcLeavesForApproval]]; break;
//                        case 1: [self changePage:[appDelegate.propPageNavigator vcClaimsForApproval]]; break;
//                        case 2: [self changePage:[appDelegate.propPageNavigator vcCapexForApproval]]; break;
//                        case 3: [self changePage:[appDelegate.propPageNavigator vcRecruitmentForApproval]]; break;
//                        case 4: [self changePage:[appDelegate.propPageNavigator vcGiftsForApproval]]; break;
//                        case 5: [self changePage:[appDelegate.propPageNavigator vcContractForApproval]]; break;
//                    }
                    
                } else { //HOLIDAYS
                    [currSelectedCell.cellIImage setImage:[UIImage imageNamed:[_sidebarHolidayHighlightedImages objectAtIndex:indexPath.row]]];
                    switch(indexPath.row) {
                        case 0: [self changePage:[appDelegate.propPageNavigator vcWeeklyHolidays ]];
                            break;
                        case 1: [self changePage:[appDelegate.propPageNavigator vcMonthlyHolidays]];
                            break;
                        case 2: [self changePage:[appDelegate.propPageNavigator vcLocalHolidays]];
                            break;
                    }
                }
                break;
            case 2: //HOLIDAYS
                [currSelectedCell.cellIImage setImage:[UIImage imageNamed:[_sidebarForApprovalHighlightedImages objectAtIndex:indexPath.row]]];
                switch (indexPath.row) {
                    case 0: [self changePage:[appDelegate.propPageNavigator vcWeeklyHolidays]]; break;
                    case 1: [self changePage:[appDelegate.propPageNavigator vcMonthlyHolidays]]; break;
                    case 2: [self changePage:[appDelegate.propPageNavigator vcLocalHolidays]]; break;
                }
                break;
            default:
                break;
        }
    }
    _currIndexPath = temp;
}

@end
