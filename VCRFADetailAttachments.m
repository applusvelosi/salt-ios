//
//  VCRFADetailAttachments.m
//  Salt
//
//  Created by Rick Royd Aban on 10/5/15.
//  Copyright © 2015 Applus Velosi. All rights reserved.
//

#import "VCRFADetailAttachments.h"
#import "MBProgressHUD.h"

@interface VCRFADetailAttachments(){
    
    IBOutlet UITableView *_propLV;
    NSMutableArray *_docNames;
    UIDocumentInteractionController *_dic;
}
@end

@implementation VCRFADetailAttachments

#pragma mark -
#pragma mark === View Life Cycle Management ===
#pragma mark -

- (void)viewDidLoad{
    [super viewDidLoad];
    
    _docNames = [NSMutableArray array];
    for(NSDictionary *doc in [_propRecruitment propDocuments])
        [_docNames addObject:[doc objectForKey:@"OrigDocName"]];
    
    _propLV.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    _propLV.dataSource = self;
    _propLV.delegate = self;
    
    _dic = [[UIDocumentInteractionController alloc] init];
    _dic.delegate = self;

}

#pragma mark -
#pragma mark === Table view data source delegate ===
#pragma mark -

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    cell.textLabel.text = [_docNames objectAtIndex:indexPath.row];
    
    return cell;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [_propRecruitment propDocuments].count;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [AppDelegate showGlogalHUDWithView:self.view];
    NSDictionary *selectedDoc = [[_propRecruitment propDocuments] objectAtIndex:indexPath.row];
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
        NSString *urlString = [NSString stringWithFormat:@"%@?dID=%d&refID=%d&obTypeID=%d", NSLocalizedString(@"ImageViewerLink", @""), [[selectedDoc objectForKey:@"DocID"] intValue], [[selectedDoc objectForKey:@"RefID"] intValue], [[selectedDoc objectForKey:@"ObjectType"] intValue]];
        id urlData = [self.propAppDelegate.propGatewayOnline makeWebServiceCall:urlString requestMethod:GET postBody:nil];
        
        if (![urlData isKindOfClass:[NSString class]]) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [AppDelegate hideGlogalHUDWithView:self.view];
                NSString *filePath = [NSTemporaryDirectory() stringByAppendingPathComponent:[selectedDoc objectForKey:@"DocName"]];
                NSLog(@"file writed to %@",filePath);
                [urlData writeToFile:filePath atomically:YES];
                NSURL *filePathURL = [NSURL fileURLWithPath:filePath];
                _dic.URL = filePathURL;
                [_dic presentPreviewAnimated:YES];
            });
        } else {
            dispatch_async(dispatch_get_main_queue(), ^{
                [AppDelegate hideGlogalHUDWithView:self.view];
                UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"" message:urlData preferredStyle:UIAlertControllerStyleAlert];
                UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"Dismiss" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action){
                    
                }];
                [alertController addAction:cancelAction];
                [self presentViewController:alertController animated:YES completion:nil];
            });
        }
    });
}

#pragma mark -
#pragma mark === Document Interaction Controller delegate ===
#pragma mark -

- (UIViewController *)documentInteractionControllerViewControllerForPreview:(UIDocumentInteractionController *)controller{
    return self;
}

@end
