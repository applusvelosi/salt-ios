//
//  Staff.h
//  Salt iOS
//
//  Created by Rick Royd Aban on 5/18/15.
//  Copyright (c) 2015 Rick Royd Aban. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "OnlineGateway.h"

#define LMid 139

@class OfflineGateway;

typedef NS_ENUM(NSInteger, USERPOSITION) {
    USERPOSITION_DEFAULT,
    USERPOSITION_ADMIN,
    USERPOSITION_CM,
    USERPOSITION_RM,
    USERPOSITION_LM,
    USERPOSITION_CFO,
    USERPOSITION_CEO
};

@interface Staff : NSObject

- (Staff *)initWithOnlineGateway:(OnlineGateway *)onlineGateway staffDictionary:(NSDictionary *)staffDictionary;
- (Staff *)initWithOfflineGateway:(OfflineGateway *)key staffDictionary:(NSDictionary *)staffDictionary;

- (NSMutableDictionary *)staffDictionary;

- (NSString *)propAccountName;
- (int)propAccountID;
- (BOOL)propIsActive;
- (NSString *)propActiveDirectoryName;
- (float)propApproverLimit;
- (float)propCarryOverLeave;
- (int)propCostCenterID;
- (NSString *)propCostCenterName;
- (NSString *)propDateCreated;
- (NSString *)propDateModified;
- (NSString *)propBirthdate;
- (int)propDepartmentID;
- (NSString *)propDepartmentName;
- (NSString *)propEmail;
- (NSString *)propEmploymentEndDate;
- (NSString *)propEmploymentStartDate;
- (int)propExpenseApproverID;
- (NSString *)propExpenseApproverName;
- (NSString *)propFirstName;
- (BOOL)propHasFriday;
- (NSString *)propGender;
- (BOOL)propISADAuth;
- (BOOL)propISAllowProxySubmission;
- (BOOL)propIsApprover;
- (BOOL)propIsCorporateApprover;
- (BOOL)propIsRegional;
- (NSString *)propJobTitle;
- (NSString *)propLastName;
- (int)propLeaveApprover1ID;
- (NSString *)propLeaveApprover1Email;
- (NSString *)propLeaveApprover1Name;
- (int)propLeaveApprover2ID;
- (NSString *)propLeaveApprover2Email;
- (NSString *)propLeaveApprover2Name;
- (int)propLeaveApprover3ID;
- (NSString *)propLeaveApprover3Email;
- (NSString *)propLeaveApprover3Name;
- (NSString *)propLocation;
- (NSString *)propLockDate;
- (int)propMaxConsecutiveDays;
- (NSString *)propMobile;
- (BOOL)propHasMonday;
- (NSString *)propNotes;
- (NSString *)propOfficeAddress;
- (int)propOfficeID;
- (NSString *)propOfficeName;
- (NSString *)propPassword;
- (NSString *)propPayrollID;
- (NSString *)propPhone;
- (NSString *)propExt;
- (int)propPrefCurrencyID;
- (NSString *)propPrefCurrencyName;
- (int)propProxyStaffID;
- (NSString *)propProxyStaffName;
- (BOOL)propHasSaturday;
- (int)propSecurityLevel;
- (NSString *)propSecurityLevelName;
- (float)propSickLeaveAllowance;
- (int)propStaffID;
- (NSString *)propStaffNumber;
- (int)propStatusID;
- (NSString *)propStatusName;
- (BOOL)propHasSunday;
- (BOOL)propHasThursday;
- (NSString *)propTitle;
-  (float)propTotalVacationLeave;
- (BOOL)propHasTuesday;
- (float)propVacationLeaveAllowance;
- (BOOL)propHasWednesday;
- (int)propWorkingDays;

- (NSString *)propFullName;
- (void)setUserPosition:(USERPOSITION)userPosition;
- (USERPOSITION)getUserPosition;
- (BOOL)isUser;
- (BOOL)isManager;
- (BOOL)isAccount;
- (BOOL)isAdmin;
- (BOOL)isCM;
- (BOOL)isAM;


@end
