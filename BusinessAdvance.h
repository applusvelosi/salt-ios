//
//  BusinessAdvance.h
//  Salt
//
//  Created by Rick Royd Aban on 2015/07/29.
//  Copyright (c) 2015年 Applus Velosi. All rights reserved.
//

#import "ClaimHeader.h"

@interface BusinessAdvance : ClaimHeader

- (ClaimHeader *)initWithCostCenterID:(int)costCenterID costCenterName:(NSString *)costCenterName claimTypeID:(int)claimTypeID isPaidByCC:(BOOL)isPaidByCC baIDCharged:(int)baIDCharged bacNumber:(NSString *)bacNumber appDelegate:(AppDelegate *)appDelegate;

@end
