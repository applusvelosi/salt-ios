//
//  VCLiquidationDetail.m
//  Salt
//
//  Created by Rick Royd Aban on 2015/07/31.
//  Copyright (c) 2015年 Applus Velosi. All rights reserved.
//

#import "MBProgressHUD.h"
#import "VCLiquidation.h"
#import "CellClaimDetailOverview.h"
#import "CellClaimDetailAmounts.h"
#import "CellClaimDetailProcess.h"
#import "VCClaimItems.h"

@interface VCLiquidation ()<UIAlertViewDelegate>{
    IBOutlet UITableView *_propLV;
    IBOutlet UILabel *_propLabelClaimItem;
}

@end

@implementation VCLiquidation

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _propLV.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    _propLV.estimatedRowHeight = 30;
    _propLV.rowHeight = UITableViewAutomaticDimension;
    _propLV.dataSource = self;
    _propLV.delegate = self;
}

- (void)viewDidAppear:(BOOL)animated{
    [_propLV reloadData];
    
    NSMutableArray *rightbarButtonItems = [NSMutableArray array];
    if(_liquidation.propStatusID == CLAIMSTATUSID_OPEN){
//        [rightbarButtonItems addObject:[[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemEdit target:self action:@selector(editClaim)]];
        [rightbarButtonItems addObject:[[UIBarButtonItem alloc] initWithTitle:@"Cancel" style:UIBarButtonItemStylePlain target:self action:@selector(cancelClaim)]];
        
        NSLog(@"claimitems %lu",[self.propAppDelegate myClaimItemsForMyClaimID:_liquidation.propClaimID].count);
        if([self.propAppDelegate myClaimItemsForMyClaimID:_liquidation.propClaimID].count > 0){ //at least 1 item to submit the claim header
            [rightbarButtonItems addObject:[[UIBarButtonItem alloc] initWithTitle:@"Submit" style:UIBarButtonItemStylePlain target:self action:@selector(submitClaim)]];
        }
        
        self.navigationItem.rightBarButtonItems = rightbarButtonItems;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    switch (indexPath.row) {
        case 0:{
            CellClaimDetailOverview *cellClaimDetail = (CellClaimDetailOverview *)[tableView dequeueReusableCellWithIdentifier:@"cell_overview"];
            cellClaimDetail.propClaimNumber.text = [_liquidation propClaimNumber];
            cellClaimDetail.propFieldPaidByCC.text = @"";
            cellClaimDetail.propFieldStatus.text = [_liquidation propStatusName];
            cellClaimDetail.propFieldClaimant.text = [_liquidation propStaffName];
            cellClaimDetail.propFieldDateExpensed.text = [_liquidation propDateSubmitted:self.propAppDelegate];
            
            return cellClaimDetail;
        }
            
        case 1:{
            
            UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell_claimitems"];
            cell.detailTextLabel.text = [NSString stringWithFormat:@"Claim Items (%lu)", [self.propAppDelegate myClaimItemsForMyClaimID:_liquidation.propClaimID].count];
            return cell;
        }
            
        case 2:{
            CellClaimDetailAmounts *cellClaimAmounts = (CellClaimDetailAmounts *)[tableView dequeueReusableCellWithIdentifier:@"cell_otherdetails"];
            cellClaimAmounts.propFieldApproverName.text = [_liquidation propApproverName];
            cellClaimAmounts.propFieldChargeTo.text = [_liquidation propCostCenterName];
            cellClaimAmounts.propFieldAmountApproved.text = [NSString stringWithFormat:@"%d",[_liquidation propApprovedAmount]];
            cellClaimAmounts.propFieldAmountRejected.text = [NSString stringWithFormat:@"%d",[_liquidation propRejectedAmount]];
            cellClaimAmounts.propFieldAmountForDeduction.text = [NSString stringWithFormat:@"%.2f",[_liquidation propForDeductionAmount]];
            cellClaimAmounts.propFieldAmountForPayment.text = [NSString stringWithFormat:@"%.2f", [_liquidation propForPaymentAmount]];
            cellClaimAmounts.propFieldAmountTotal.text = [NSString stringWithFormat:@"%d", [_liquidation propTotalClaim]];
            
            return cellClaimAmounts;
        }
            
        case 3:{
            CellClaimDetailProcess *cellClaimDetailProcess = (CellClaimDetailProcess *)[tableView dequeueReusableCellWithIdentifier:@"cell_processdetail"];
            cellClaimDetailProcess.propFieldProcessedByApprover.text = [_liquidation propDateApprovedByApprover:self.propAppDelegate];
            cellClaimDetailProcess.propFieldProcessedByAccount.text = [_liquidation propDateApprovedByAccount:self.propAppDelegate];
            cellClaimDetailProcess.propFieldDatePaid.text = [_liquidation propDatePaid:self.propAppDelegate];
            
            return cellClaimDetailProcess;
        }
            
        default:
            return nil;
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 4;
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    ((VCClaimItems *)segue.destinationViewController).propClaimHeader = _liquidation;
}

- (void)editClaim{
    
}

- (void)cancelClaim{
    [[[UIAlertView alloc] initWithTitle:@"" message:@"Are you sure you want to cancel this claim header?" delegate:self cancelButtonTitle:@"Dismiss" otherButtonTitles:@"Cancel", nil] show];
}

- (void)submitClaim{
    [self updateClaimHeaderWithStatusID:CLAIMSTATUSID_SUBMITTED updatableDate:@"DateSubmitted" successMessage:@"Claim Submitted Successfully"];
}

- (void)updateClaimHeaderWithStatusID:(int)statusID updatableDate:(NSString *)updatableDate successMessage:(NSString *)successMessage{
    [AppDelegate showGlogalHUDWithView:self.view];
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
//        ClaimHeader *newClaimHeader = [[ClaimHeader alloc] initWithDictionary:[[_liquidation savableDictionary] mutableCopy]];
//        [newClaimHeader updateStatusWithAppdelegate:self.propAppDelegate statusID:statusID updatableDate:updatableDate]; //TEMP
        id result = nil; //[self.propAppDelegate.propGatewayOnline saveClaimWithNewClaimHeaderJSON:[newClaimHeader  oldClaimHeaderJSON:[_liquidation jsonForWebServices]];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [AppDelegate hideGlogalHUDWithView:self.view];
            [[[UIAlertView alloc] initWithTitle:@"" message:([result isEqualToString:@"OK"])?successMessage:result delegate:self cancelButtonTitle:@"Dismiss" otherButtonTitles:nil, nil] show];
        });
    });
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if(buttonIndex == 0)
        [self.navigationController popViewControllerAnimated:YES];
    else
        [self updateClaimHeaderWithStatusID:CLAIMSTATUSID_SUBMITTED updatableDate:@"DateSubmitted" successMessage:@"Claim Cancelled Successfully"];
}

@end
