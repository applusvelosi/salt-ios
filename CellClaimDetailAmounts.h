//
//  CellClaimPaidByCCOtherDetails.h
//  Salt
//
//  Created by Rick Royd Aban on 2015/07/27.
//  Copyright (c) 2015年 Applus Velosi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CellClaimDetailAmounts : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *propFieldApproverName;
@property (weak, nonatomic) IBOutlet UILabel *propFieldChargeTo;
@property (weak, nonatomic) IBOutlet UILabel *propFieldAmountApproved;
@property (weak, nonatomic) IBOutlet UILabel *propFieldAmountRejected;
@property (weak, nonatomic) IBOutlet UILabel *propFieldAmountForDeduction;
@property (weak, nonatomic) IBOutlet UILabel *propFieldAmountForPayment;
@property (weak, nonatomic) IBOutlet UILabel *propFieldAmountTotal;

@end
