//
//  CellMyLeave.h
//  Salt iOS
//
//  Created by Rick Royd Aban on 5/21/15.
//  Copyright (c) 2015 Rick Royd Aban. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CellMyLeave : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *propLabelType;
@property (weak, nonatomic) IBOutlet UILabel *propLabelDuration;
@property (weak, nonatomic) IBOutlet UILabel *propLabelStatus;

@end
