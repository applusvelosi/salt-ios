//
//  VCGiftForApprovalDetail.h
//  Salt
//
//  Created by Rick Royd Aban on 11/05/2017.
//  Copyright © 2017 Applus Velosi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

@interface VCGiftForApprovalDetail : UITableViewController

@property (assign, nonatomic) int propGiftHeaderId;
@property (strong, nonatomic) AppDelegate *propAppDelegate;

@end
