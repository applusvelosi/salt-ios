//
//  VCClaimItemAddAttendee.h
//  Salt
//
//  Created by Rick Royd Aban on 2/29/16.
//  Copyright © 2016 Applus Velosi. All rights reserved.
//

#import "VCDetail.h"

@interface VCClaimItemAddAttendee : VCDetail

@property (strong, nonatomic) NSMutableArray *propClaimItemAttendees;

@end
