//
//  CellRFADetailPositionInfo.h
//  Salt
//
//  Created by Rick Royd Aban on 10/2/15.
//  Copyright © 2015 Applus Velosi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CellRFADetailPositionInfo : UITableViewCell

@property (strong, nonatomic) IBOutlet UILabel *propFieldOffice;
@property (strong, nonatomic) IBOutlet UILabel *propFieldDepartment;
@property (strong, nonatomic) IBOutlet UILabel *propFieldStartDate;
@property (strong, nonatomic) IBOutlet UILabel *propFieldJobTitle;
@property (strong, nonatomic) IBOutlet UILabel *propFieldEmpCategory;
@property (strong, nonatomic) IBOutlet UILabel *propFieldAnnualRevenue;
@property (strong, nonatomic) IBOutlet UILabel *propFieldGrossBaseBunos;

@property (strong, nonatomic) IBOutlet UILabel *propFieldSalaryRange;
@property (strong, nonatomic) IBOutlet UIImageView *propCboxIsBudgettedCost;
@property (strong, nonatomic) IBOutlet UIImageView *propCboxIsSpecificPerson;
@property (strong, nonatomic) IBOutlet UILabel *propServeranceLabel;
@property (strong,nonatomic) IBOutlet UILabel *propFieldSeveranceDetails;

@end
