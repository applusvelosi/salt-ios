//
//  Contract.h
//  Salt
//
//  Created by Rick Royd Aban on 07/06/2017.
//  Copyright © 2017 Applus Velosi. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "OnlineGateway.h"

typedef NS_ENUM(int, ContractStatusId) {
    ContractStatusIdOpen = 70,
    ContractStatusIdSubmitted,
    ContractStatusIdApprovedByCm,
    ContractStatusIdRejectedByCm,
    ContractStatusIdApprovedByRm,
    ContractStatusIdRejectedByRm,
    ContractStatusIdApprovedByLm,
    ContractStatusIdRejectedByLm,
    ContractStatusIdApprovedByCfo,
    ContractStatusIdRejectedByCfo,
    ContractStatusIdApprovedByEvp,
    ContractStatusIdRejectedByEvp,
    ContractStatusIdApprovedByRfm = 146,
    ContractStatusIdRejectedByRfm = 147
};

@interface Contract : NSObject

@property (nonatomic, strong) NSString *propAdditionalMembership, *propAgentCommission, *propAgentName, *propApproverNote, *propBankPerformanceGuarantee, *propBillingCurrency, *propBusinessOrigDocName, *propCmEmail, *propCmName, *propCertificatesValid, *propCfoName, *propCommercialTermsOther, *propCompany, *propCompanyCurrency, *propCompetingGrounds, *propCompetingAdvantage, *propCompetitorOffers, *propCompetitors, *propConsequentialLoss, *propContractingStrategy, *propDateProcessedByCfo, *propDateProcessedByCm, *propDateProcessedByEvp, *propDateProcessedByLm, *propDateProcessedByRm, *propDateStartDate, *propDateTenderSubmission, *propDateRequest, *propDealBreakers, *propEndClient, *propEquipmentRequired, *propEvpEmail, *propEvpName, *propExistingOrNew, *propHsqe, *propImportExport, *propInsurance, *propIntellectualPropertyRights, *propInvoicing, *propJobStaff, *propJustification, *propLmEmail, *propLmName,
    *propLegalOther2, *propLimitationLiability, *propLiquidatedDamage, *propMainOrigDocName, *propNegotiations, *propNotes,
    *propOperational, *propOtherLocalTaxes, *propPaymentTerms, *propPermanentEstablishment, *propPricing, *propPricingAdditional,
    *propPrimaryResponsibility, *propProjectExecutionLocation, *propProjectManagerCriteria, *propTeamBuildup, *propRmEmail, *propRmName,
    *propReferenceNumber, *propRequestStatusName, *propRequestor, *propRfmEmail, *propRfmName, *propRiskAssessmentOthers,
    *propSpecificTraining, *propStaffAware, *propStatusName, *propTechnicalAnalysisOthers, *propVisa;

@property (nonatomic, assign) int propAgentApproved, propCmId, propCfoId, propCompetentPersonnel, propContractId, propContractType, propCapexRequired, propCapexRequiredEquiv, propProjectValue, propProjectValueEquiv,
    propCreditRisk, propEquipment, propEquipmentAvailable, propEquipmentCost, propEvpId, propInfrastructure, propLmId, propLegalOthers,
    propOfficeId, propPaymentBehavior, propPersonnel, propProbabilityOfAward, propProjectDuration, propProjectDurationRange, propRfmId,propRmId,
    propRequestStatus, propServiceLines, propServices, propStaffId, propStatus;
@property (nonatomic, assign) BOOL propIsAgentRequired;
@property (nonatomic, assign) float propGrossMargin, propRateToEuro;

- (instancetype)initWithJSONDictionary:(NSDictionary *)dictionary withOnlineGateway:(OnlineGateway *)onlineGateway;
- (instancetype)initWithJSONDictionary:(NSMutableDictionary *)contractDict;

- (NSString *)processedDateSubmitted:(AppDelegate *)appdelegate;
- (NSString *)processedDateByCM:(AppDelegate *)appdelegate;
- (NSString *)processedDateByRM:(AppDelegate *)appdelegate;
- (NSString *)processedDateByLM:(AppDelegate *)appdelegate;
- (NSString *)processedDateByCFO:(AppDelegate *)appdelegate;
- (NSString *)processedDateByEVP:(AppDelegate *)appdelegate;
- (NSString *)getDateStart:(AppDelegate *)appDelegate;
- (NSString *)getDateTenderSubmission:(AppDelegate *)appDelegate;
- (NSString *) updateJSONGiftWithStatusId:(ContractStatusId) statusId keyForUpdatableDate:(NSString *)keyUpdatableDate withApproverNote:(NSString *)note appDelegate:(AppDelegate *)appdelegate;
- (NSMutableArray *)propBusinessCaseDoc;
- (NSString *)getPaymentBehavior:(int) key;
- (NSString *)getCreditRisk:(int) key;
- (NSString *)getDurationRange:(int) key;
- (NSString *)getStatus:(int) key;
- (NSString *)getServiceLine:(int) key;
- (NSString *)getContractType:(int) key;

@end
