//
//  CellMinifiedDetailAmount.h
//  Salt
//
//  Created by Rick Royd Aban on 2015/08/03.
//  Copyright (c) 2015年 Applus Velosi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CellMinifiedDetailAmount : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *propFieldApproverName;
@property (weak, nonatomic) IBOutlet UILabel *propFieldCostCenter;
@property (weak, nonatomic) IBOutlet UILabel *propFieldAmountApproved;
@property (weak, nonatomic) IBOutlet UILabel *propFieldAmountRejected;
@property (weak, nonatomic) IBOutlet UILabel *propFieldAmountTotal;

@end
