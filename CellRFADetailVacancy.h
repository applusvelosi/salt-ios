//
//  CellRFADetailVacancy.h
//  Salt
//
//  Created by Rick Royd Aban on 10/2/15.
//  Copyright © 2015 Applus Velosi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CellRFADetailVacancy : UITableViewCell

@property (strong, nonatomic) IBOutlet UILabel *propFieldRequestType;
@property (strong, nonatomic) IBOutlet UIImageView *propFieldYes;
@property (strong, nonatomic) IBOutlet UIImageView *propFieldNo;
@property (strong, nonatomic) IBOutlet UILabel *propFieldReasonForPosition;
@property (strong, nonatomic) IBOutlet UILabel *propReasonLabel;

@end
