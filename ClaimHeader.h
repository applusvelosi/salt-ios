//
//  ClaimHeader.h
//  Salt
//
//  Created by Rick Royd Aban on 2015/07/27.
//  Copyright (c) 2015年 Applus Velosi. All rights reserved.
//
/*
    IMPORTANT!
    Please take note that claim header should just have a read only access to claim items.
    The getClaim does not include the list of its claim item since it is not going to be efficient with regards to memory and access time, 
    it will just returns an empty list for claim items.
    There is a separate interface for retrieving line items for this claimheader
 
    So for that reason, we let the view to manage the claim item updates through the app delegate
 */

//I did not make this as an enum neither a map so as to achieve a manual but reversible key-value and value-key pair access
//which is implemented on the static methods of this class

#define CLAIMTYPEID_CLAIMS 1
#define CLAIMTYPEID_ADVANCES 2
#define CLAIMTYPEID_LIQUIDATION 3

#define CLAIMTYPEDESC_CLAIMS @"Claim"
#define CLAIMTYPEDESC_ADVANCES @"Business Advance"
#define CLAIMTYPEDESC_LIQUIDATION @"Liquidation"

#define CLAIMSTATUSID_OPEN 1
#define CLAIMSTATUSID_SUBMITTED 2
#define CLAIMSTATUSID_APPROVEDBYAPPROVER 3
#define CLAIMSTATUSID_REJECTEDBYAPPROVER 4
#define CLAIMSTATUSID_CANCELLED 5
#define CLAIMSTATUSID_PAID 6
#define CLAIMSTATUSID_APPROVEDBYCM 7
#define CLAIMSTATUSID_REJECTEDBYCM 9
#define CLAIMSTATUSID_REJECTEDBYACCOUNTS 11
#define CLAIMSTATUSID_APPROVEDBYACCOUNTS 16
#define CLAIMSTATUSID_RETURN 17
#define CLAIMSTATUSID_LIQUIDATED 23
#define CLAIMSTATUSID_PAIDUNDERCOMPANYCARD 24
#define CLAIMSTATUSID_REJECTEDFORSALARYDEDUCTION 25
#define CLAIMSTATUSID_SENTTOSAP 69

#define CLAIMSTATUSDESC_OPEN @"Open"
#define CLAIMSTATUSDESC_SUBMITTED @"Submitted"
#define CLAIMSTATUSDESC_APPROVEDBYAPPROVER @"Approved by Approver"
#define CLAIMSTATUSDESC_REJECTEDBYAPPROVER @"Rejected by Approver"
#define CLAIMSTATUSDESC_CANCELLED @"Cancelled"
#define CLAIMSTATUSDESC_PAID @"Paid"
#define CLAIMSTATUSDESC_APPROVEDBYCM @"Approved by CM"
#define CLAIMSTATUSDESC_REJECTEDBYCM @"Rejected by CM"
#define CLAIMSTATUSDESC_APPROVEDBYACCOUNTS @"Approved by Accounts"
#define CLAIMSTATUSDESC_REJECTEDBYACCOUNTS @"Rejected by Accounts"
#define CLAIMSTATUSDESC_RETURN @"Return"
#define CLAIMSTATAUSDESC_LIQUIDATED @"Liquidated"
#define CLAIMSTATUSDESC_PAIDUNDERCOMPANYCARD @"Paid Under Company Card"
#define CLAIMSTATUSDESC_REJECTEDFORSALARYDEDUCTION @"Rejected For Salary Deduction"
#define CLAIMSTATUSDESC_SENTTOSAP @"Sent To Sap"

#import <Foundation/Foundation.h>
#import "OfflineGatewaySavable.h"
#import "CostCenter.h"
@class AppDelegate;
@class OnlineGateway;
@class Staff;
@class Office;

typedef void (^LCSumValuesProvider)(float *lcTotal, float *approvedTotal, float *rejectedTotal, float *deductionTotal, float *paymentTotal);

@interface ClaimHeader : NSObject<OfflineGatewaySavable>

@property (strong, nonatomic) NSMutableDictionary *claimHeader;

@property (nonatomic) NSString *approversNote;
@property (nonatomic, assign) int claimStatus;
@property (nonatomic) NSString *statusName;
@property (nonatomic, assign) int officeID;
@property (nonatomic) NSArray *claimLineItems;
@property (nonatomic, assign) int claimCMId;

+ (NSArray *)claimTypeDescs;
+ (NSString *)typeDescForTypeKey:(int)key;
+ (NSString *)statusDescForStatusKey:(int)statusID;
+ (int)typeKeyForTypeDesc:(NSString *)desc;
+ (int)statusKeyForStatusDesc:(NSString *)desc;
- (instancetype)initWithDictionary:(NSMutableDictionary *)claimHeaderDictionary;
- (instancetype)initWithJSONDictionary:(NSDictionary *)dictionary withAppDelegate:(AppDelegate *)appDelegate;
- (ClaimHeader *)initWithCostCenterID:(int)costCenterID costCenterName:(NSString *)costCenterName claimTypeID:(int)claimTypeID isPaidByCC:(BOOL)isPaidByCC baIDCharged:(int)baIDCharged bacNumber:(NSString *)bacNumber appDelegate:(AppDelegate *)appDelegate;
- (void)updateCostCenter:(CostCenter *)costCenter;
- (int)propClaimID;
- (NSString *)propClaimNumber;
- (int)propStaffID;
- (NSString *)propStaffName;
//- (int)propOfficeID;
- (NSString *)propApproversNote;
- (NSString *)propOfficeName;
- (int)propCostCenterID;
- (NSString *)propCostCenterName;
- (int)propApproverID;
- (NSString *)propApproverName;
- (NSString *)propApproverEmail;
- (int)propAccountID;
- (NSString *)propAccountEmail;
- (int)propCMId;
- (BOOL)propIsPaidByCC;
- (int)propTypeID;
- (NSString *)propTypeName;
- (int)propStatusID;
- (NSString *)propStatusName;
- (NSString *)propCurrencyThree;
- (int)propTotalClaim;
- (int)propApprovedAmount;
- (int)propRejectedAmount;
- (NSString*)propRequesterName;
- (int)propParentClaimID;
- (NSString*)propParentClaimNumber;
- (int)propHasYesNoDoc;
- (NSString *)propAttachedCert;
- (NSMutableArray*)propDocuments;
- (NSString *)propDateCreated:(AppDelegate *)appdelegate;
- (NSString *)propDateSubmitted:(AppDelegate *)appdelegate;
- (NSString *)propDateApprovedByApprover:(AppDelegate *)appdelegate;
- (NSString *)propDateApprovedByAccount:(AppDelegate *)appdelegate;
- (NSString *)propDateApprovedByDirector:(AppDelegate *)appdelegate;
- (NSString *)propDateModified:(AppDelegate *)appdelegate;
- (NSString *)propDateCancelled:(AppDelegate *)appdelegate;
- (NSString *)propDateRejected:(AppDelegate *)appdelegate;
- (NSString *)propDatePaid:(AppDelegate *)appdelegate;
- (float)propTotalAmountInLC;
//- (void)updateStatusWithAppdelegate:(AppDelegate *)appdelegate ID:(int)statusID;

- (NSArray *)dictForWebServices; //the claim item must be converted into its array form instead of our default string form
- (NSString *)jsonForWebServices;
+ (NSString *)jsonFromNewEmptyClaimHeader;

- (NSString *)jsonClaimForApprovalWithStatusID:(int)statusID withKeyForUpdatableDate:(NSString *)keyForUpdatableDate approversNote:(NSString*)note withAppDelegate:(AppDelegate*)appDelegate;
- (NSString *)jsonClaimChildForApprovalWebServicesWithStatusID:(int)statusID claimId:(int)claimId claimNumber:(NSString*)claimNum parentClaimID:(int)parentID parentClaimNumber:(NSString *)parentNumber claimCMId:(int)nextApproverIdtfa statusDesc:(NSString*)statusDesc keyForUpdatableDate:(NSString *)keyForUpdatableDate approversNote:(NSString *)approverNote withAppDelegate:(AppDelegate *)appDelegate;
- (NSString *)jsonClaimParentForApprovalWebServicesWithStatusID:(int)statusID statusDesc:(NSString*)description keyForUpdatableDate:(NSString *)keyForUpdatableDate approversNote:(NSString *)approverNote withAppDelegate:(AppDelegate*)appDelegate;

@end

