//
//  VCGeneralClaimItemsDetail.h
//  Salt
//
//  Created by Rick Royd Aban on 17/11/2016.
//  Copyright © 2016 Applus Velosi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ClaimHeader.h"
#import "VCPage.h"

@interface VCGeneralClaimItemsDetail : VCPage
@property (strong, nonatomic) UIPageViewController *pageVC;
@property (strong, nonatomic) ClaimHeader *propClaimHeader;
@property (assign, nonatomic) NSUInteger pageIndex;

@end
