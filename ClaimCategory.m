//
//  ClaimCategory.m
//  Salt
//
//  Created by Rick Royd Aban on 2015/08/04.
//  Copyright (c) 2015年 Applus Velosi. All rights reserved.
//

#import "ClaimCategory.h"

@interface ClaimCategory(){
    NSMutableDictionary *_dict;
}
@end

@implementation ClaimCategory

- (ClaimCategory *)initWithJSONDict:(NSDictionary *)jsonCategoryDict{
    if([self init]){
        _dict = [NSMutableDictionary dictionary];
        [_dict setObject:[jsonCategoryDict objectForKey:@"Attendee"] forKey:@"Attendee"];
        [_dict setObject:[jsonCategoryDict objectForKey:@"CategoryTypeID"] forKey:@"CategoryTypeID"];
        [_dict setObject:[jsonCategoryDict objectForKey:@"CategoryID"] forKey:@"CategoryID"];
        [_dict setObject:[jsonCategoryDict objectForKey:@"SpendLimit"] forKey:@"SpendLimit"];
        [_dict setObject:[jsonCategoryDict objectForKey:@"Description"] forKey:@"Description"];
        [_dict setObject:[jsonCategoryDict objectForKey:@"Currency"] forKey:@"Currency"];
        [_dict setObject:[jsonCategoryDict objectForKey:@"CurrencyAbb"] forKey:@"CurrencyAbb"];
    }
        
    return self;
}

- (ClaimCategory *)initWithDictionary:(NSDictionary *)categoryDict{
    if([super init])
        _dict = [NSMutableDictionary dictionaryWithDictionary:categoryDict];
    
    return self;
}

- (int)propAttendeeTypeID{
    return [[_dict objectForKey:@"Attendee"] intValue];
}

- (int)propCategoryTypeID{
    return [[_dict objectForKey:@"CategoryTypeID"] intValue];
}

- (int)propCategoryID{
    return [[_dict objectForKey:@"CategoryID"] intValue];
}

- (float)propSpendLimit{
    return [[_dict objectForKey:@"SpendLimit"] floatValue];
}

- (NSString *)propName{
    return [_dict objectForKey:@"Description"];
}

- (int)propCurrencyID{
    return [[_dict objectForKey:@"Currency"] intValue];
}

- (NSString *)propCurrencyName{
    return [_dict objectForKey:@"CurrencyAbb"];
}

+ (int)TYPE_MILEAGE{
    return 1;
}

+ (int)TYPE_BUSINESSADVANCE{
    return 4;
}

+ (int)TYPE_ASSET{
    return 7;
}

@end
