//
//  VCCapexesForApprovalSearchResult.m
//  Salt
//
//  Created by Rick Royd Aban on 30/08/2016.
//  Copyright © 2016 Applus Velosi. All rights reserved.
//

#import "VCCapexesForApprovalSearchResult.h"
#import "CellCapexForApproval.h"
#import "VCCapexForApprovalDetail.h"
#import "MBProgressHUD.h"
#import "AppDelegate.h"
#import "VelosiColors.h"
#import "LoaderDelegate.h"

@interface VCCapexesForApprovalSearchResult ()<LoaderDelegate, UISearchResultsUpdating, UIScrollViewDelegate, UISearchBarDelegate> {
    NSMutableArray *_capexes;
    AppDelegate *_appDelegate;
    UISearchController *_searchController;
    NSString *_stringFilter;
    BOOL _searchWasCancelled;
}

@end

@implementation VCCapexesForApprovalSearchResult

#pragma mark -
#pragma mark === View Life Cycle Management ===
#pragma mark -
- (void)viewDidLoad {
    [super viewDidLoad];
    _capexes = [NSMutableArray array];
    _appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    _stringFilter = @"";
    self.parentVC.delegate = self;
    _searchWasCancelled = NO;
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(didChangePrefeeredContentSize:)
                                                 name:UIContentSizeCategoryDidChangeNotification object:nil];
    
    // No search results controller to display the search results in the current view
    _searchController = [[UISearchController alloc] initWithSearchResultsController:nil];
    _searchController.searchResultsUpdater = self;
    _searchController.dimsBackgroundDuringPresentation = NO;
    _searchController.hidesNavigationBarDuringPresentation = NO;
    
    _searchController.searchBar.tintColor = [UIColor blueColor];
    _searchController.searchBar.placeholder = @"Staff name or Office";
    
    self.tableView.tableHeaderView = _searchController.searchBar;
    self.definesPresentationContext = YES;
    [_searchController.searchBar sizeToFit];
    _searchController.searchBar.delegate = self;
    
    self.tableView.rowHeight = UITableViewAutomaticDimension;
    self.tableView.estimatedRowHeight = 140;
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    NSIndexPath *indexPath = [self.tableView indexPathForCell:sender];
    CapexHeader *capex = [self capexForIndexPath:indexPath];
    ((VCCapexForApprovalDetail *)segue.destinationViewController).capexHeaderID = [capex propCapexID];
}


#pragma mark -
#pragma mark === Loader delegate ===
#pragma mark -
-(void)loadFinished {
    [self reloadCapexesForApprovalWithOfficeOrRequestor:_stringFilter];
}

-(void)loadFailedWithError:(NSString *)error {
    [[[UIAlertView alloc] initWithTitle:@"" message:error delegate:nil cancelButtonTitle:@"Dismiss" otherButtonTitles:nil, nil] show];
}


#pragma mark -
#pragma mark - === scroll view delegate ===
#pragma mark -
-(void)scrollViewWillBeginDragging:(UIScrollView *)scrollView {
    [_searchController.searchBar setShowsCancelButton:NO animated:YES];
    [_searchController.searchBar resignFirstResponder];
}

#pragma mark -
#pragma mark === Search bar delegate ===
#pragma mark -
- (void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar {
    _searchWasCancelled = NO;
}

- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar {
//    [self reloadCapexesForApprovalWithOfficeOrRequestor:_stringFilter];
    _searchWasCancelled = YES;
}

-(void)searchBarTextDidEndEditing:(UISearchBar *)searchBar {
    if (_searchWasCancelled) {
        searchBar.text = _stringFilter;
    } else {
        _stringFilter = searchBar.text;
    }
}

-(void)searchBarSearchButtonClicked:(UISearchBar *)searchBar {
    [searchBar setShowsCancelButton:NO animated:YES];
}

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText {
    _stringFilter = searchText;
    [self reloadCapexesForApprovalWithOfficeOrRequestor:_stringFilter];
}

#pragma mark -
#pragma mark === Helper methods ===
#pragma mark -
-(void)didChangePrefeeredContentSize:(NSNotification*)notification{
    [self.tableView reloadData];
}

-(CapexHeader *)capexForIndexPath:(NSIndexPath *)indexPath{
    CapexHeader *capex = nil;
    if (indexPath)
        capex = [_capexes objectAtIndex:indexPath.row];
    return capex;
}

#pragma mark -
#pragma mark - Table view data source delegate
#pragma mark -
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [_capexes count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellReuseId = @"SearchResultCell";
    CellCapexForApproval *cell = [tableView dequeueReusableCellWithIdentifier:cellReuseId];
    if (cell == nil)
        cell = [[CellCapexForApproval alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellReuseId];

    CapexHeader *capexHeader = [_capexes objectAtIndex:indexPath.row];
    
    NSString *tempStatusName;
    if([capexHeader propStatusID] == CapexStatusIdApprovedByCm){
        cell.propFieldStatus.textColor = [VelosiColors greenAcceptance];
        tempStatusName = @"Approved by CM";
    }else if([capexHeader propStatusID] == CapexStatusIdRejectedByCm){
        cell.propFieldStatus.textColor = [VelosiColors redRejection];
        tempStatusName = @"Rejected by CM";
    }else if([capexHeader propStatusID] == CapexStatusIdApprovedByRm){
        cell.propFieldStatus.textColor = [VelosiColors greenAcceptance];
        tempStatusName = @"Approved by RM";
    }else if([capexHeader propStatusID] == CapexStatusIdRejectedByRm){
        cell.propFieldStatus.textColor = [VelosiColors redRejection];
        tempStatusName = @"Rejected by RM";
    }else if([capexHeader propStatusID] == CapexStatusIdApprovedByCeo){
        cell.propFieldStatus.textColor = [VelosiColors greenAcceptance];
        tempStatusName = @"Approved by CEO";
    }else if([capexHeader propStatusID] == CapexStatusIdRejectedByCeo){
        cell.propFieldStatus.textColor = [VelosiColors redRejection];
        tempStatusName = @"Rejected by CEO";
    }else if([capexHeader propStatusID] == CapexStatusIdApprovedByCfo){
        cell.propFieldStatus.textColor = [VelosiColors greenAcceptance];
        tempStatusName = @"Approved by CFO";
    }else if([capexHeader propStatusID] == CapexStatusIdRejectedByCfo){
        cell.propFieldStatus.textColor = [VelosiColors redRejection];
        tempStatusName = @"Rejected by CFO";
    }else{
        cell.propFieldStatus.textColor = [UIColor lightGrayColor];
        tempStatusName = [capexHeader propStatusName];
    }
    
    cell.propFieldOffice.text = [capexHeader propOfficeName];
    cell.propFieldCostCenter.text = [capexHeader propCapexNumber];
    cell.propFieldStatus.text = tempStatusName;
    cell.propFieldName.text = [capexHeader propRequesterName];
    
    cell.bounds = CGRectMake(0, 0, CGRectGetWidth(tableView.bounds), 99999);
    cell.contentView.bounds = cell.bounds;
    [cell layoutIfNeeded];
    
    cell.propFieldOffice.preferredMaxLayoutWidth = CGRectGetWidth(cell.propFieldOffice.frame);
    cell.propFieldName.preferredMaxLayoutWidth = CGRectGetWidth(cell.propFieldName.frame);
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    CapexHeader *capexHeader = [_capexes objectAtIndex:indexPath.row];
    NSLog(@"User selected %@", capexHeader.propRequesterName);
}

//#pragma mark - UISearchResultsUpdating
//- (void)updateSearchResultsForSearchController:(UISearchController *)searchController {
//    NSString *searchString = [self.searchController.searchBar text];
//    if([searchString length] > 0){
//        NSArray *tempCapexes = [[NSArray alloc] init];
//        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"(propRequesterName CONTAINS[c] %@) OR (propOfficeName CONTAINS[c] %@)", searchString, searchString];
//        tempCapexes = [_filterdCapexes filteredArrayUsingPredicate:predicate];
//        _filterdCapexes = [tempCapexes mutableCopy];
//    }else
//        [self fetchFilteredCapexes];
//    [self.tableView reloadData];
//}

#pragma mark -
#pragma mark - search results delegate
#pragma mark -
- (void)updateSearchResultsForSearchController:(UISearchController *)searchController {
    if (searchController.active)
        [searchController.searchBar setShowsCancelButton:YES animated:YES];
}

- (void)reloadCapexesForApprovalWithOfficeOrRequestor:(NSString *)searchString {
    [_capexes removeAllObjects];
    if (searchString.length > 0) {
        for (CapexHeader *capexForApproval in [_appDelegate capexesForApproval]) {
            if ( [[capexForApproval propRequesterName] localizedCaseInsensitiveContainsString:searchString] || [[capexForApproval propOfficeName] localizedCaseInsensitiveContainsString:searchString] || searchString.length < 1 )
                [_capexes addObject:capexForApproval];
        }
    } else {
        [_capexes addObjectsFromArray:[_appDelegate capexesForApproval]];
    }
    [self.tableView reloadData];
}

@end
