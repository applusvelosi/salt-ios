//
//  UserPageNavigators.m
//  Jobs
//
//  Created by Rick Royd Aban on 10/2/14.
//  Copyright (c) 2014 applusvelosi. All rights reserved.
//

#import "PageNavigatorFactory.h"


@interface PageNavigatorFactory() {
    UIStoryboard *_sbMain, *_sbLogin, *_sbHome, *_sbMyLeaves, *_sbLeavesForApproval, *_sbWeeklyHolidays, *_sbMonthlyHolidays, *_sbLocalHolidays, *_sbMyCalendar, *_sbMyClaim, *_sbClaimsForApproval, *_sbCapexForApproval, *_sbRecruitmentForApproval, *_sbGiftForApproval, *_sbContractForApproval;
    UINavigationController *_navigatorLogin, *_navigatorHome, *_navigatorMyLeaves, *_navigatorLeaveInput, *_navigatorLeavesForApproval, *_navigatorWeeklyHolidays, *_navigatorMonthlyHolidays, *_navigatorLocalHolidays, *_navigatorMyCalendar, *_navigatorMyClaims, *_navigatorClaimsForApproval, *_navigatorCapexForApproval, *_navigatorRecruitmentForApproval, *_navigatorGiftForApproval, *_navigatorContractForApproval;
    VCHomeLeaves *_homeLeaveOverview;
}
@end


@implementation PageNavigatorFactory
//+ (PageNavigatorFactory *)sharedNavigators{
//    if(sharedNavigators == nil)
//        sharedNavigators = [[PageNavigatorFactory alloc] init];
//    
//    return sharedNavigators;
//}

+(PageNavigatorFactory *)sharedNavigators {
    static PageNavigatorFactory *sharedNavigator = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedNavigator = [[PageNavigatorFactory alloc] init];
    });
    return sharedNavigator;
}


- (instancetype)init{
    self = [super init];
    
    if(self) {
        //initialize storyboards
        _sbMain = [UIStoryboard storyboardWithName:@"SBMain" bundle:nil];
        _sbLogin = [UIStoryboard storyboardWithName:@"SBLogin" bundle:nil];
        _sbHome = [UIStoryboard storyboardWithName:@"SBHome" bundle:nil];
        _sbMyLeaves = [UIStoryboard storyboardWithName:@"SBMyLeaves" bundle:nil];
        _sbLeavesForApproval = [UIStoryboard storyboardWithName:@"SBLeavesForApproval" bundle:nil];
        _sbWeeklyHolidays = [UIStoryboard storyboardWithName:@"SBWeeklyHolidays" bundle:nil];
        _sbMonthlyHolidays = [UIStoryboard storyboardWithName:@"SBMonthlyHolidays" bundle:nil];
        _sbLocalHolidays = [UIStoryboard storyboardWithName:@"SBLocalHolidays" bundle:nil];
        _sbMyCalendar = [UIStoryboard storyboardWithName:@"SBMyCalendar" bundle:nil];
        _sbMyClaim = [UIStoryboard storyboardWithName:@"SBMyClaims" bundle:nil];
        _sbClaimsForApproval = [UIStoryboard storyboardWithName:@"SBClaimsForApproval" bundle:nil];
        _sbCapexForApproval = [UIStoryboard storyboardWithName:@"SBCapexForApproval" bundle:nil];
        _sbRecruitmentForApproval = [UIStoryboard storyboardWithName:@"SBRecruitmentForApproval" bundle:nil];
        _sbGiftForApproval = [UIStoryboard storyboardWithName:@"SBGiftsHospitalitiesForApproval" bundle:nil];
        _sbContractForApproval = [UIStoryboard storyboardWithName:@"SBContractsForApproval" bundle:nil];
    }
    return self;
}

- (void)logout {
    _navigatorHome = nil;
    _navigatorMyLeaves = nil;
    _navigatorLeavesForApproval = nil;
    _navigatorWeeklyHolidays = nil;
    _navigatorMonthlyHolidays = nil;
    _navigatorLocalHolidays = nil;
    _navigatorMyCalendar = nil;
}

- (UINavigationController *)vcLogin{
    if(_navigatorLogin == nil)
        _navigatorLogin = [_sbLogin instantiateViewControllerWithIdentifier:@"loginPage"];
    return _navigatorLogin;
}

- (UINavigationController *)vcHome{
    if(_navigatorHome == nil)
        _navigatorHome = [_sbHome instantiateViewControllerWithIdentifier:@"homePage"];
    return _navigatorHome;
}

- (UINavigationController *)vcMyLeaves{
    if(_navigatorMyLeaves == nil)
        _navigatorMyLeaves = [_sbMyLeaves instantiateViewControllerWithIdentifier:@"myleavePage"];
    return _navigatorMyLeaves;
}

- (UINavigationController *)vcLeaveInput{
    if(_navigatorLeaveInput == nil)
        _navigatorLeaveInput = [_sbMyLeaves instantiateViewControllerWithIdentifier:@"leaveinputPage"];
    return _navigatorLeaveInput;
}

- (UINavigationController *)vcLeavesForApproval{
    if(_navigatorLeavesForApproval == nil)
        _navigatorLeavesForApproval = [_sbLeavesForApproval instantiateViewControllerWithIdentifier:@"leavesforapprovalPage"];
    return _navigatorLeavesForApproval;
}

- (UINavigationController *)vcWeeklyHolidays{
    if(_navigatorWeeklyHolidays == nil)
        _navigatorWeeklyHolidays = [_sbWeeklyHolidays instantiateViewControllerWithIdentifier:@"weeklyholidaypage"];
    return _navigatorWeeklyHolidays;
}

- (UINavigationController *)vcMonthlyHolidays{
    if(_navigatorMonthlyHolidays == nil)
        _navigatorMonthlyHolidays = [_sbMonthlyHolidays instantiateViewControllerWithIdentifier:@"monthlyHolidaysPage"];
    return _navigatorMonthlyHolidays;
}

- (UINavigationController *)vcLocalHolidays{
    if(_navigatorLocalHolidays == nil)
        _navigatorLocalHolidays = [_sbLocalHolidays instantiateViewControllerWithIdentifier:@"localHolidaysPage"];
    return _navigatorLocalHolidays;
}

- (UINavigationController *)vcMyCalendar{
    if(_navigatorMyCalendar == nil)
        _navigatorMyCalendar = [_sbMyCalendar instantiateViewControllerWithIdentifier:@"myCalendarPage"];
    return _navigatorMyCalendar;
}

- (UINavigationController *)vcMyClaims{
    if(_navigatorMyClaims == nil)
        _navigatorMyClaims = [_sbMyClaim instantiateViewControllerWithIdentifier:@"myClaimsPage"];
    return _navigatorMyClaims;
}

- (UINavigationController *)vcClaimsForApproval{
    if(_navigatorClaimsForApproval == nil)
        _navigatorClaimsForApproval = [_sbClaimsForApproval instantiateViewControllerWithIdentifier:@"claimsForApprovalPage"];
    return _navigatorClaimsForApproval;
}

- (UINavigationController *)vcCapexForApproval{
    if(_navigatorCapexForApproval == nil)
        _navigatorCapexForApproval = [_sbCapexForApproval instantiateViewControllerWithIdentifier:@"capexForApprovalPage"];
    return _navigatorCapexForApproval;
}

- (UINavigationController *)vcRecruitmentForApproval{
    if(_navigatorRecruitmentForApproval == nil)
        _navigatorRecruitmentForApproval = [_sbRecruitmentForApproval instantiateViewControllerWithIdentifier:@"recruitmentForApprovalPage"];
    return _navigatorRecruitmentForApproval;
}

- (UINavigationController *)vcGiftsForApproval {
    if (_navigatorGiftForApproval == nil)
        _navigatorGiftForApproval = [_sbGiftForApproval instantiateViewControllerWithIdentifier:@"giftForApprovalPage"];
    return  _navigatorGiftForApproval;
}

- (UINavigationController *)vcContractForApproval {
    if (_navigatorContractForApproval == nil)
        _navigatorContractForApproval = [_sbContractForApproval instantiateViewControllerWithIdentifier:@"contractForApprovalPage"];
    return _navigatorContractForApproval;
}

@end
