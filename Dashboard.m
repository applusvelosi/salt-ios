//
//  Dashboard.m
//  Salt
//
//  Created by Rick Royd Aban on 1/19/16.
//  Copyright © 2016 Applus Velosi. All rights reserved.
//

#import "Dashboard.h"

@interface Dashboard(){
    int _advanceApproval, _advanceApprovedByAccount, _advanceApprovedByCM, _advanceApprovedByManager, _advanceLiquidated, _advanceOpen, _advancePaid, _advancePayment, _advanceSubmitted;
    int _capexApproval;
    int _claimApproval, _claimApprovedByAccounts, _claimApprovedByManager, _claimOpen, _claimPaid, _claimPayment, _claimSubmitted;
    int _leaveApproval;
    int _liquidationApproval, _liquidationApprovedByAccounts, _liquidationApprovedByManager, _liquidationLiquidated, _liquidationOpen, _liquidationSubmitted;
    int _pendingBT, _pendingHospitalization, _pendingSL, _pendingUnpaid, _pendingVL;
    int _recruitmentApproval;
    float _remSL, _remVL;
    int _usedBT, _usedHospitalization, _usedSL, _usedUnpaid, _usedVL;
}
@end

@implementation Dashboard

- (Dashboard *)initWithDictionary:(NSDictionary *)jsonDashboard{
    
    if([super init]){
        _advanceApproval = [[jsonDashboard objectForKey:@"advanceApproval"] intValue];
        _advanceApprovedByAccount = [[jsonDashboard objectForKey:@"advanceApprovedByAccounts"] intValue];
        _advanceApprovedByCM = [[jsonDashboard objectForKey:@"advanceApprovedByCM"] intValue];
        _advanceApprovedByManager = [[jsonDashboard objectForKey:@"advanceApprovedByManager"] intValue];
        _advanceLiquidated = [[jsonDashboard objectForKey:@"advanceLiquidated"] intValue];
        _advanceOpen = [[jsonDashboard objectForKey:@"advanceOpen"] intValue];
        _advancePaid = [[jsonDashboard objectForKey:@"advancePaid"] intValue];
        _advancePayment = [[jsonDashboard objectForKey:@"advancesPayment"] intValue];
        _advanceSubmitted = [[jsonDashboard objectForKey:@"advanceSubmitted"] intValue];
        _capexApproval = [[jsonDashboard objectForKey:@"capexApproval"] intValue];
        _claimApproval = [[jsonDashboard objectForKey:@"claimApproval"] intValue];
        _claimApprovedByAccounts = [[jsonDashboard objectForKey:@"claimApprovedByAccounts"] intValue];
        _claimApprovedByManager = [[jsonDashboard objectForKey:@"claimApprovedByManager"] intValue];
        _claimOpen = [[jsonDashboard objectForKey:@"claimOpen"] intValue];
        _claimPaid = [[jsonDashboard objectForKey:@"claimPaid"] intValue];
        _claimPayment = [[jsonDashboard objectForKey:@"claimPayment"] intValue];
        _claimSubmitted = [[jsonDashboard objectForKey:@"claimSubmitted"] intValue];
        _leaveApproval = [[jsonDashboard objectForKey:@"leaveApproval"] intValue];
        _liquidationApproval = [[jsonDashboard objectForKey:@"liquidationApproval"] intValue];
        _liquidationApprovedByAccounts = [[jsonDashboard objectForKey:@"liquidationApprovedByAccounts"] intValue];
        _liquidationApprovedByManager = [[jsonDashboard objectForKey:@"liquidationApprovedByManager"] intValue];
        _liquidationLiquidated = [[jsonDashboard objectForKey:@"liquidationLiquidated"] intValue];
        _liquidationOpen = [[jsonDashboard objectForKey:@"liquidationOpen"] intValue];
        _liquidationSubmitted = [[jsonDashboard objectForKey:@"liquidationSubmitted"] intValue];
        _pendingBT = [[jsonDashboard objectForKey:@"pendingBT"] intValue];
        _pendingHospitalization = [[jsonDashboard objectForKey:@"pendingHospitalization"] intValue];
        _pendingSL = [[jsonDashboard objectForKey:@"pendingSL"] intValue];
        _pendingUnpaid = [[jsonDashboard objectForKey:@"pendingUnpaid"] intValue];
        _pendingVL = [[jsonDashboard objectForKey:@"pendingVL"] intValue];
        _recruitmentApproval = [[jsonDashboard objectForKey:@"recruitmentApproval"] intValue];
        _remSL = [[jsonDashboard objectForKey:@"remainingSL"] floatValue];
        _remVL = [[jsonDashboard objectForKey:@"remainingVL"] floatValue];
        _usedBT = [[jsonDashboard objectForKey:@"usedBT"] intValue];
        _usedHospitalization = [[jsonDashboard objectForKey:@"usedHospitalization"] intValue];
        _usedSL = [[jsonDashboard objectForKey:@"usedSL"] intValue];
        _usedUnpaid = [[jsonDashboard objectForKey:@"usedUnpaid"] intValue];
        _usedVL = [[jsonDashboard objectForKey:@"usedVL"] intValue];
    }
    
    return self;
}

- (int)propAdvanceApproval{ return _advanceApproval; }
- (int)propAdvanceApprovedByAccounts{ return _advanceApprovedByAccount; }
- (int)propAdvanceApprovedByCM{ return _advanceApprovedByCM; }
- (int)propAdvanceApprovedByManager{ return _advanceApprovedByManager; }
- (int)propAdvanceLiquidated{ return _advanceLiquidated; }
- (int)propAdvanceOpen{ return _advanceOpen;}
- (int)propAdvancePaid{ return _advancePaid; }
- (int)propAdvancePayment{ return _advancePayment; }
- (int)propAdvanceSubmitted{ return _advanceSubmitted; }
- (int)propCapexApproval{ return _capexApproval; }
- (int)propClaimApproval{ return _claimApproval; }
- (int)propClaimApprovedByAccounts{ return _claimApprovedByAccounts; }
- (int)propClaimApprovedByManager{ return _claimApprovedByManager; }
- (int)propClaimOpen{ return _claimOpen; }
- (int)propClaimPaid{ return _claimPaid; }
- (int)propClaimPayment{ return _claimPayment; }
- (int)propClaimSubmitted{ return _claimSubmitted; }
- (int)propLeaveApproval{ return _leaveApproval; }
- (int)propLiquidationApproval{ return _liquidationApproval; }
- (int)propLiquidationApprovedByAccounts{ return _liquidationApprovedByAccounts; }
- (int)propLiquidationApprovedByManager{ return _liquidationApprovedByManager; }
- (int)propLiquidationLiquidated{ return _liquidationLiquidated; }
- (int)propLiquidationOpen{ return _liquidationOpen; }
- (int)propLiquidationSubmitted{ return _liquidationSubmitted; }
- (int)propPendingBT{ return _pendingBT; }
- (int)propPendingHospitalization{ return _pendingHospitalization; }
- (int)propPendingSL{ return _pendingSL; }
- (int)propPendingUnpaid{ return _pendingUnpaid; }
- (int)propPendingVL{ return _pendingVL; }
- (int)propRecruitmentApproval{ return _recruitmentApproval; }
- (float)propRemSL{ return _remSL; }
- (float)propRemVL{ return _remVL; }
- (int)propUsedBT{ return _usedBT; }
- (int)propUsedHospitalization{ return _usedHospitalization; }
- (int)propUsedSL{ return _usedSL; }
- (int)propUsedUnpaid{ return _usedUnpaid; }
- (int)propUsedVL{ return _usedVL; }

@end
