//
//  Currency.m
//  Salt
//
//  Created by Rick Royd Aban on 2/19/16.
//  Copyright © 2016 Applus Velosi. All rights reserved.
//

#import "Currency.h"

@interface Currency(){
    int _currID;
    NSString *_currName, *_currThree;
}
@end

@implementation Currency

- (Currency *)initWithID:(int)currencyID name:(NSString *)currencyName currThree:(NSString *)currThree{
    if([super init]){
        _currID = currencyID;
        _currName = currencyName;
        _currThree = currThree;
    }
    
    return self;
}

- (int)propCurrID{
    return _currID;
}

- (NSString *)propCurrencyName{
    return _currName;
}

- (NSString *)propCurrencyThree{
    return _currThree;
}

@end
