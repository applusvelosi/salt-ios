//
//  Recruitment.m
//  Salt
//
//  Created by Rick Royd Aban on 10/1/15.
//  Copyright © 2015 Applus Velosi. All rights reserved.
//

#import "Recruitment.h"
#import "AppDelegate.h"

@interface Recruitment(){
    NSMutableDictionary *_recruitment;
    
}
@end

@implementation Recruitment

- (instancetype)initWithDictionary:(NSDictionary *)jsonRecruitment onlineGateway:(OnlineGateway *)onlineGateway{
    
    if([super init]){
        _recruitment = [[NSDictionary dictionaryWithDictionary:jsonRecruitment] mutableCopy];
        [_recruitment setObject:@([[_recruitment objectForKey:@"CountryManager"] intValue]) forKey:@"CountryManager"];
        [_recruitment setObject:@([[_recruitment objectForKey:@"EmployeeCategoryID"] intValue]) forKey:@"EmployeeCategoryID"];
        [_recruitment setObject:@([[_recruitment objectForKey:@"EmploymentTypeID"] intValue]) forKey:@"EmploymentTypeID"];
        [_recruitment setObject:@([[_recruitment objectForKey:@"OfficeOfDeployment"] intValue]) forKey:@"OfficeOfDeployment"];
        [_recruitment setObject:@([[_recruitment objectForKey:@"PositionTypeID"] intValue]) forKey:@"PositionTypeID"];
        [_recruitment setObject:@([[_recruitment objectForKey:@"RecruitmentRequestID"] intValue]) forKey:@"RecruitmentRequestID"];
        [_recruitment setObject:@([[_recruitment objectForKey:@"RegionalManager"] intValue]) forKey:@"RegionalManager"];
        [_recruitment setObject:@([[_recruitment objectForKey:@"RequesterID"] intValue]) forKey:@"RequesterID"];
        [_recruitment setObject:@([[_recruitment objectForKey:@"RequesterOfficeID"] intValue]) forKey:@"RequesterOfficeID"];
        [_recruitment setObject:@([[_recruitment objectForKey:@"TimeBaseTypeID"] intValue]) forKey:@"TimeBaseTypeID"];
        [_recruitment setObject:@([[_recruitment objectForKey:@"StatusID"] intValue]) forKey:@"StatusID"];
        //dates
        [_recruitment setObject:[onlineGateway deserializeJsonDateString:[_recruitment objectForKey:@"DateProcessedByCountryManager"]] forKey:@"DateProcessedByCountryManager"];
        [_recruitment setObject:[onlineGateway deserializeJsonDateString:[_recruitment objectForKey:@"DateProcessedByRegionalManager"]] forKey:@"DateProcessedByRegionalManager"];
        [_recruitment setObject:[onlineGateway deserializeJsonDateString:[_recruitment objectForKey:@"DateRejected"]] forKey:@"DateRejected"];
        [_recruitment setObject:[onlineGateway deserializeJsonDateString:[_recruitment objectForKey:@"DateRequested"]] forKey:@"DateRequested"];
        [_recruitment setObject:[onlineGateway deserializeJsonDateString:[_recruitment objectForKey:@"TargettedStartDate"]] forKey:@"TargettedStartDate"];
        [_recruitment setObject:[onlineGateway deserializeJsonDateString:[_recruitment objectForKey:@"DateProcessedByCEO"]] forKey:@"DateProcessedByCEO"];
        [_recruitment setObject:[onlineGateway deserializeJsonDateString:[_recruitment objectForKey:@"DateProcessedByHR"]] forKey:@"DateProcessedByHR"];
        [_recruitment setObject:[_recruitment objectForKey:@"OtherBenefits"] forKey:@"OrigOtherBenefits"];
        [_recruitment setObject:[_recruitment objectForKey:@"Documents"] forKey:@"Documents"];
        
        [_recruitment setObject:[[NSString alloc] initWithData:[NSJSONSerialization dataWithJSONObject:[_recruitment objectForKey:@"OtherBenefits"] options:0 error:nil] encoding:NSUTF8StringEncoding] forKey:@"OtherBenefits"];
//        [_recruitment setObject:[[NSString alloc] initWithData:[NSJSONSerialization dataWithJSONObject:[_recruitment objectForKey:@"Documents"] options:0 error:nil] encoding:NSUTF8StringEncoding] forKey:@"Documents"];

    }
    
    return self;
}

- (instancetype)initWithDictionary:(NSMutableDictionary *)recruitment{
    if([super init]){
        _recruitment = recruitment;
    }
    
    return self;
}

- (NSDictionary *)savableDictionary{
    return _recruitment;
}

-(NSString *)formatDateToBeViewed:(NSString *)dateFromStr appDelegate:(AppDelegate *)app{
    NSInteger janOne2K = 946684800000/1000; //Jan 1, 2000 GMT in epoch time
    NSDate *jan1 = [NSDate dateWithTimeIntervalSince1970:janOne2K];
    NSDate *dateFromString = [[NSDate alloc] init];
    dateFromString = [app.propDateFormatByProcessed dateFromString:dateFromStr];
    if ([dateFromString compare:jan1] == NSOrderedDescending || [dateFromString compare:jan1] == NSOrderedSame) //Date Before Jan 1, 2000 must not be viewed
        return [app.propDateFormatByProcessed stringFromDate:dateFromString];
    return @"";
}

- (float)propAnnualRevenue{
    return [[_recruitment objectForKey:@"AnnualRevenue"] floatValue];
}

- (NSString *)propApproverNote{
    return ([_recruitment objectForKey:@"ApproverNote"] == (id)[NSNull null])?@"":[_recruitment objectForKey:@"ApproverNote"];
}

- (int)propCMID{
    return [[_recruitment objectForKey:@"CountryManager"] intValue];
}

- (NSString *)propCMEmail{
    return ([_recruitment objectForKey:@"CountryManagerEmail"] == (id)[NSNull null])?@"":[_recruitment objectForKey:@"CountryManagerEmail"];
}

- (NSString *)propCMName{
    return ([_recruitment objectForKey:@"CountryManagerName"] == (id)[NSNull null])?@"":[_recruitment objectForKey:@"CountryManagerName"];
}

- (NSString *)propDateRejected:(AppDelegate *)app{
    return [self formatDateToBeViewed:[_recruitment objectForKey:@"DateRejected"] appDelegate:app];
}

- (NSString *)propDateRequested:(AppDelegate *)app{
    return [self formatDateToBeViewed:[_recruitment objectForKey:@"DateRequested"] appDelegate:app];
}

- (NSString *)propDateProcessedByCM:(AppDelegate *)app{
    return [self formatDateToBeViewed:[_recruitment objectForKey:@"DateProcessedByCountryManager"] appDelegate:app];
}

- (NSString *)propDateProcessedByRHM:(AppDelegate *)app{
    return [self formatDateToBeViewed:[_recruitment objectForKey:@"dateProcessedByRegionalHR"] appDelegate:app];
}

- (NSString *)propDateProcessedByRM:(AppDelegate *)app{
    return [self formatDateToBeViewed:[_recruitment objectForKey:@"DateProcessedByRegionalManager"] appDelegate:app];
}

-(NSString *)propDateProcessedByHR:(AppDelegate *)app{
    return [self formatDateToBeViewed:[_recruitment objectForKey:@"DateProcessedByHR"] appDelegate:app];
}

-(NSString *)propDateProcessedByCEO:(AppDelegate *)app{
    return [self formatDateToBeViewed:[_recruitment objectForKey:@"DateProcessedByCEO"] appDelegate:app];
}

- (int)propDepartmentToBeAssignedID{
    return [[_recruitment objectForKey:@"DepartmentToBeAssigned"] intValue];
}

- (NSString *)propDepartmentToBeAssignedName{
    return ([_recruitment objectForKey:@"DepartmentToBeAssignedName"] == (id)[NSNull null])?@"":[_recruitment objectForKey:@"DepartmentToBeAssignedName"];
}

- (NSString *)propEmail{
    return ([_recruitment objectForKey:@"Email"] == (id)[NSNull null])?@"":[_recruitment objectForKey:@"Email"];
}

- (int)propEmployeeCategoryID{
    return [[_recruitment objectForKey:@"EmployeeCategoryID"] intValue];
}

- (NSString *)propEmployeeCategoryName{
    return ([_recruitment objectForKey:@"EmployeeCategoryName"] == (id)[NSNull null])?@"":[_recruitment objectForKey:@"EmployeeCategoryName"];
}

- (int)propEmploymentTypeID{
    return [[_recruitment objectForKey:@"EmploymentTypeID"] intValue];
}

- (NSString *)propEmploymentTypeName{
    return ([_recruitment objectForKey:@"EmploymentTypeName"] == (id)[NSNull null])?@"":[_recruitment objectForKey:@"EmploymentTypeName"];
}

- (float)propGrossBaseBonus{
    return [[_recruitment objectForKey:@"GrossBaseBonus"] floatValue];
}

- (float)propHoursPerWeek{
    return [[_recruitment objectForKey:@"HoursPerWeek"] floatValue];
}

-(BOOL)propIsRequiredReplacement{
    return [[_recruitment objectForKey:@"IsRequired"] boolValue];
}

- (BOOL)propIsBudgetedCost{
    return [[_recruitment objectForKey:@"IsBudgetedCost"] boolValue];
}

- (BOOL)propIsPositionMayBePermanent{
    return [[_recruitment objectForKey:@"IsPositionMayBePermanent"] boolValue];
}

- (BOOL)propIsSpecificPerson{
    return [[_recruitment objectForKey:@"IsSpecificPerson"] boolValue];
}

- (BOOL)propIsWithAttachment{
    return [[_recruitment objectForKey:@"IsWithAttachment"] boolValue];
}

- (NSString *)propJobTitle{
    return ([_recruitment objectForKey:@"JobTitle"] == (id)[NSNull null])?@"":[_recruitment objectForKey:@"JobTitle"];
}

- (int)propOfficeOfDeploymentID{
    return [[_recruitment objectForKey:@"OfficeOfDeployment"] intValue];
}

- (NSString *)propOfficeOfDeploymentName{
    return ([_recruitment objectForKey:@"OfficeOfDeploymentName"] == (id)[NSNull null])?@"":[_recruitment objectForKey:@"OfficeOfDeploymentName"];
}

- (NSMutableArray *)propOtherBenefits{
    NSMutableArray *otherBenefits = [NSMutableArray array];
    for(NSDictionary *otherBenefit in [NSJSONSerialization JSONObjectWithData:[[_recruitment objectForKey:@"OtherBenefits"] dataUsingEncoding:NSUTF8StringEncoding] options:NSJSONReadingMutableContainers error:nil])
        [otherBenefits addObject:[NSString stringWithFormat:@"%@%@%@",[otherBenefit objectForKey:@"BenefitName"], RECRUITMENT_OTHERBENEFIT_DELIMITER, [otherBenefit objectForKey:@"ActualCost"]]];
    
    
    return otherBenefits;
}

- (NSDictionary *)propDocuments{
//    return [NSJSONSerialization JSONObjectWithData:[[_recruitment objectForKey:@"Documents"]dataUsingEncoding:NSUTF8StringEncoding] options:NSJSONReadingMutableContainers error:0];
    return [_recruitment objectForKey:@"Documents"];
}

-(int)propPositionTypeID{
    return [[_recruitment objectForKey:@"PositionTypeID"] intValue];
}

- (NSString *)propPositionTypeName{
    return ([_recruitment objectForKey:@"PositionTypeName"] == (id)[NSNull null])?@"":[_recruitment objectForKey:@"PositionTypeName"];
}

- (NSString *)propReason{
    return ([_recruitment objectForKey:@"Reason"] == (id)[NSNull null])?@"":[_recruitment objectForKey:@"Reason"];
}

- (NSString *)propRecruitmentNumber{
//    return ([_recruitment objectForKey:@"RecruitmentNumber"] == (id)[NSNull null])?@"":[_recruitment objectForKey:@"RecruitmentNumber"];
    return [_recruitment objectForKey:@"RecruitmentNumber"];
}

- (int)propRecruitmentRequestID{
    return [[_recruitment objectForKey:@"RecruitmentRequestID"] intValue];
}

- (int)propRegionalHRManager {
    return [[_recruitment objectForKey:@"RegionalHRManagerId"] intValue];
}

- (int)propRMID{
    return [[_recruitment objectForKey:@"RegionalManager"] intValue];
}

- (NSString *)propRMEmail{
    return ([_recruitment objectForKey:@"RegionalManagerEmail"] == (id)[NSNull null])?@"":[_recruitment objectForKey:@"RegionalManagerEmail"];
}

- (NSString *)propRMName{
    return ([_recruitment objectForKey:@"RegionalManagerName"] == (id)[NSNull null])?@"":[_recruitment objectForKey:@"RegionalManagerName"];
}

- (NSString *)propReplacementFor{
    return ([_recruitment objectForKey:@"ReplacementFor"] == (id)[NSNull null])?@"":[_recruitment objectForKey:@"ReplacementFor"];
}

- (NSString *)propRequesterDepartmentName{
    return ([_recruitment objectForKey:@"RequesterDepartment"] == (id)[NSNull null])?@"":[_recruitment objectForKey:@"RequesterDepartment"];
}

- (int)propRequesterID{
    return [[_recruitment objectForKey:@"RequesterID"] intValue];
}

- (NSString *)propRequesterName{
    return ([_recruitment objectForKey:@"RequesterName"] == (id)[NSNull null])?@"":[_recruitment objectForKey:@"RequesterName"];
}

- (int)propRequesterOfficeID{
    return [[_recruitment objectForKey:@"RequesterOfficeID"] intValue];
}

- (NSString *)propRequesterOfficeName{
    return ([_recruitment objectForKey:@"RequesterOfficeName"] == (id)[NSNull null])?@"":[_recruitment objectForKey:@"RequesterOfficeName"];
}

- (NSString *)propRequesterPhoneNumber{
    return ([_recruitment objectForKey:@"RequesterPhoneNumber"] == (id)[NSNull null])?@"":[_recruitment objectForKey:@"RequesterPhoneNumber"];
}

- (float)propSalaryRangeFrom{
    return [[_recruitment objectForKey:@"SalaryRangeFrom"] floatValue];
}

- (float)propSalaryRangeTo{
    return [[_recruitment objectForKey:@"SalaryRangeTo"] floatValue];
}

- (NSString *)propSeverancePayment{
    return ([_recruitment objectForKey:@"SeverancePayment"] == (id)[NSNull null])?@"":[_recruitment objectForKey:@"SeverancePayment"];
}

- (int)propStatusID{
    return [[_recruitment objectForKey:@"StatusID"] intValue];
}

- (NSString *)propStatusName{
    return ([_recruitment objectForKey:@"StatusName"] == (id)[NSNull null])?@"":[_recruitment objectForKey:@"StatusName"];
}

- (NSString *)propTargettedStartDate{
    return ([_recruitment objectForKey:@"TargettedStartDate"] == (id)[NSNull null])?@"":[_recruitment objectForKey:@"TargettedStartDate"];
}

- (int)propTimeBaseTypeID{
    return [[_recruitment objectForKey:@"TimeBaseTypeID"] intValue];    
}

- (NSString *)propTimeBaseTypeName{
    return ([_recruitment objectForKey:@"TimeBaseTypeName"] == (id)[NSNull null])?@"":[_recruitment objectForKey:@"TimeBaseTypeName"];
}

- (NSString *)JSONForUpdatingRecruitmentWithStatusID:(int)statusID keyForUpdatableDate:(NSString *)keyForUpdatableDate approverNotes:(NSString *)approverNotes appDelegate:(AppDelegate *)appDelegate{
    NSMutableDictionary *tempDict = [_recruitment mutableCopy];
    [tempDict setObject:@(statusID) forKey:@"StatusID"];
    [tempDict setObject:[tempDict objectForKey:@"OrigOtherBenefits"] forKey:@"OtherBenefits"];
    [tempDict setObject:[tempDict objectForKey:@"Documents"] forKey:@"Documents"];
    [tempDict setObject:[appDelegate.propGatewayOnline convertToUnixDate:[appDelegate.propDateFormatByProcessed dateFromString:[tempDict objectForKey:@"DateProcessedByCountryManager"]]] forKey:@"DateProcessedByCountryManager"];
    [tempDict setObject:[appDelegate.propGatewayOnline convertToUnixDate:[appDelegate.propDateFormatByProcessed dateFromString:[tempDict objectForKey:@"DateProcessedByRegionalManager"]]] forKey:@"DateProcessedByRegionalManager"];
    [tempDict setObject:[appDelegate.propGatewayOnline convertToUnixDate:[appDelegate.propDateFormatByProcessed dateFromString:[tempDict objectForKey:@"DateProcessedByHR"]]] forKey:@"DateProcessedByHR"];
    [tempDict setObject:[appDelegate.propGatewayOnline convertToUnixDate:[appDelegate.propDateFormatByProcessed dateFromString:[tempDict objectForKey:@"DateProcessedByCEO"]]] forKey:@"DateProcessedByCEO"];
    [tempDict setObject:[appDelegate.propGatewayOnline convertToUnixDate:[appDelegate.propDateFormatByProcessed dateFromString:[tempDict objectForKey:@"DateRejected"]]] forKey:@"DateRejected"];
    [tempDict setObject:[appDelegate.propGatewayOnline convertToUnixDate:[appDelegate.propDateFormatByProcessed dateFromString:[tempDict objectForKey:@"DateRequested"]]] forKey:@"DateRequested"];
    [tempDict setObject:[appDelegate.propGatewayOnline convertToUnixDate:[appDelegate.propDateFormatByProcessed dateFromString:[tempDict objectForKey:@"TargettedStartDate"]]] forKey:@"TargettedStartDate"];
    [tempDict setObject:approverNotes forKey:@"ApproverNote"];
    if(![keyForUpdatableDate isEqualToString:@"NA"])
        [tempDict setObject:[appDelegate.propGatewayOnline epochizeDate:[NSDate date]] forKey:keyForUpdatableDate];
    return [[NSString alloc] initWithData:[NSJSONSerialization dataWithJSONObject:tempDict options:0 error:nil] encoding:NSUTF8StringEncoding];
}

- (NSString *)jsonizeWithAppDelegate:(AppDelegate *)appDelegate{
    NSMutableDictionary *tempDict = [_recruitment mutableCopy];
    [tempDict setObject:[tempDict objectForKey:@"OrigOtherBenefits"] forKey:@"OtherBenefits"];
    [tempDict setObject:[tempDict objectForKey:@"Documents"] forKey:@"Documents"];
    [tempDict setObject:[appDelegate.propGatewayOnline convertToUnixDate:[appDelegate.propDateFormatByProcessed dateFromString:[tempDict objectForKey:@"DateProcessedByCountryManager"]]] forKey:@"DateProcessedByCountryManager"];
    [tempDict setObject:[appDelegate.propGatewayOnline convertToUnixDate:[appDelegate.propDateFormatByProcessed dateFromString:[tempDict objectForKey:@"DateProcessedByRegionalManager"]]] forKey:@"DateProcessedByRegionalManager"];
    [tempDict setObject:[appDelegate.propGatewayOnline convertToUnixDate:[appDelegate.propDateFormatByProcessed dateFromString:[tempDict objectForKey:@"DateProcessedByHR"]]] forKey:@"DateProcessedByHR"];
    [tempDict setObject:[appDelegate.propGatewayOnline convertToUnixDate:[appDelegate.propDateFormatByProcessed dateFromString:[tempDict objectForKey:@"DateProcessedByCEO"]]] forKey:@"DateProcessedByCEO"];
    [tempDict setObject:[appDelegate.propGatewayOnline convertToUnixDate:[appDelegate.propDateFormatByProcessed dateFromString:[tempDict objectForKey:@"DateRejected"]]] forKey:@"DateRejected"];
    [tempDict setObject:[appDelegate.propGatewayOnline convertToUnixDate:[appDelegate.propDateFormatByProcessed dateFromString:[tempDict objectForKey:@"DateRequested"]]] forKey:@"DateRequested"];
    [tempDict setObject:[appDelegate.propGatewayOnline convertToUnixDate:[appDelegate.propDateFormatByProcessed dateFromString:[tempDict objectForKey:@"TargettedStartDate"]]] forKey:@"TargettedStartDate"];
    
    return [[NSString alloc] initWithData:[NSJSONSerialization dataWithJSONObject:tempDict options:0 error:nil] encoding:NSUTF8StringEncoding];
}

@end
