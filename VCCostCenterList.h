//
//  VCCostCenterList.h
//  Salt
//
//  Created by Rick Royd Aban on 2/3/16.
//  Copyright © 2016 Applus Velosi. All rights reserved.
//

#import "VCPage.h"
#import "VCClaimHeader.h"

@interface VCCostCenterList : VCPage<UITableViewDataSource, UITableViewDelegate>

@property (strong, nonatomic) VCClaimHeader *vcClaimHeader;

@end
