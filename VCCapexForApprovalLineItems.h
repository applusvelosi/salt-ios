//
//  VCCapexForApprovalLineItemNew.h
//  Salt
//
//  Created by Rick Royd Aban on 4/4/16.
//  Copyright © 2016 Applus Velosi. All rights reserved.
//

#import "VCPage.h"


@interface VCCapexForApprovalLineItems : UITableViewController
@property (assign, nonatomic) int fieldCapexHeaderID;

@end

