//
//  CellClaimItem.h
//  Salt
//
//  Created by Rick Royd Aban on 2015/08/03.
//  Copyright (c) 2015年 Applus Velosi. All rights reserved.
//

#import "VCPage.h"

@interface CellClaimItem : UITableViewCell

@property (strong, nonatomic) IBOutlet UILabel *propCategoryName;
@property (strong, nonatomic) IBOutlet UILabel *propDateSubmitted;
@property (strong, nonatomic) IBOutlet UILabel *propTotalClaim;
@property (strong, nonatomic) IBOutlet UILabel *propStatusName;
@property (strong, nonatomic) IBOutlet UILabel *propDescription;


@end
