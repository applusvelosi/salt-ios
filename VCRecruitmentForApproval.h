//
//  VCRecruitmentForApproval.h
//  Salt
//
//  Created by Rick Royd Aban on 9/30/15.
//  Copyright © 2015 Applus Velosi. All rights reserved.
//

#import "VCPage.h"
#import "VelosiPickerRowSelectionDelegate.h"

@interface VCRecruitmentForApproval : VCPage<UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate, VelosiPickerRowSelectionDelegate, UIScrollViewDelegate>


@end
