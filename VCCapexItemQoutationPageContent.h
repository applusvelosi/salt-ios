//
//  VCCapexItemQoutationPageContent.h
//  Salt
//
//  Created by Rick Royd Aban on 19/09/2016.
//  Copyright © 2016 Applus Velosi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CapexLineItemQoutation.h"

@interface VCCapexItemQoutationPageContent : UITableViewController

@property (nonatomic,strong) CapexLineItemQoutation *quotationItem;
@property (nonatomic, strong) IBOutlet UINavigationItem *navItem;


@end
