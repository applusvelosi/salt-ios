//
//  CellClaimItemDetailOtherDetails.h
//  Salt
//
//  Created by Rick Royd Aban on 2/10/16.
//  Copyright © 2016 Applus Velosi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CellClaimItemDetailOtherDetails : UITableViewCell

@property (strong, nonatomic) IBOutlet UILabel *propLabelBillTo;
@property (strong, nonatomic) IBOutlet UILabel *propLabelNotesToBill;
@property (strong, nonatomic) IBOutlet UILabel *propLabelDescription;

@end
