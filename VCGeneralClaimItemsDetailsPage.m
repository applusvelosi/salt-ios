//
//  VCGeneralClaimItemsDetailsPage.m
//  Salt
//
//  Created by Rick Royd Aban on 17/11/2016.
//  Copyright © 2016 Applus Velosi. All rights reserved.
//

#import "VCGeneralClaimItemsDetailsPage.h"
#import "VelosiColors.h"

@interface VCGeneralClaimItemsDetailsPage () <UIDocumentInteractionControllerDelegate>
@property (weak, nonatomic) IBOutlet UIButton *propBarbuttonReject;
@property (weak, nonatomic) IBOutlet UIButton *propBarbuttonReturn;
@property (weak, nonatomic) IBOutlet UIButton *propBarbuttonApprove;

@property (weak, nonatomic) IBOutlet UILabel *propItemID;
@property (weak, nonatomic) IBOutlet UILabel *propCategory;
@property (weak, nonatomic) IBOutlet UILabel *propReceipt;
@property (weak, nonatomic) IBOutlet UILabel *propHasAttach;
@property (weak, nonatomic) IBOutlet UILabel *propStatus;
@property (weak, nonatomic) IBOutlet UILabel *propProjectCode;

@property (weak, nonatomic) IBOutlet UILabel *propAmount;
@property (weak, nonatomic) IBOutlet UILabel *propAmountLC;
@property (weak, nonatomic) IBOutlet UILabel *propUsedExRate;
@property (weak, nonatomic) IBOutlet UILabel *propStdExRate;
@property (weak, nonatomic) IBOutlet UILabel *propTaxApplied;

@property (weak, nonatomic) IBOutlet UILabel *propServiceCode;
@property (weak, nonatomic) IBOutlet UILabel *propBillTo;
@property (weak, nonatomic) IBOutlet UILabel *propAttachmentNote;
@property (weak, nonatomic) IBOutlet UILabel *propDescription;
@property (weak, nonatomic) IBOutlet UILabel *propApproversNote;

@property (nonatomic) UITapGestureRecognizer *attachmentTapRecognizer;
@property (nonatomic) UIDocumentInteractionController *document;
@property (nonatomic) NSMutableString *approversNote;
@end

@implementation VCGeneralClaimItemsDetailsPage

- (void)viewDidLoad {
    [super viewDidLoad];
    self.propItemID.text = [self.propClaimItem claimLineItemNumber];
    self.propCategory.text = [self.propClaimItem categoryName];
    self.document = [[UIDocumentInteractionController alloc] init];
    self.document.delegate = self;
    
    if ([[self.propClaimItem propAttachments] isKindOfClass:[NSArray class]]){
        self.propReceipt.text = [[[self.propClaimItem propAttachments] objectAtIndex:0] propDocName];
        self.propReceipt.textColor = [VelosiColors orangeVelosi];
        self.propReceipt.font = [UIFont fontWithName:@"TrebuchetMS-Bold" size:11];
        self.attachmentTapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onAttachmentClicked)];
        [self.propReceipt addGestureRecognizer:self.attachmentTapRecognizer];
    }else{
        self.propReceipt.text = @"NO ATTACHMENT";
    }
    
    switch (self.propClaimItem.attachmentFlag) {
        case 1:
            self.propHasAttach.text = @"YES";
            break;
        case 2:
            self.propHasAttach.text = @"NO";
            break;
        default:
            self.propHasAttach.text = @"N/A";
            break;
    }
    
    self.propStatus.text = [self.propClaimItem propStatusName];
    self.propProjectCode.text = [self.propClaimItem wbsCode];
    self.propAmount.text = [NSString stringWithFormat:@"%@ %@",[self.propAppDelegate.decimalFormat stringFromNumber:[NSNumber numberWithFloat:[self.propClaimItem amountForeing]]], [self.propClaimItem foreignCurrency]];
    self.propAmountLC.text = [NSString stringWithFormat:@"%@ %@", [self.propAppDelegate.decimalFormat stringFromNumber:[NSNumber numberWithFloat:[self.propClaimItem amountLocal]]], [self.propClaimItem localCurrencyName]];
    self.propUsedExRate.text = [NSString stringWithFormat:@"%.1f",[self.propClaimItem exchangeRate]];
    self.propStdExRate.text = [NSString stringWithFormat:@"%.1f",[self.propClaimItem standardExRate]];
    if (self.propClaimItem.isTaxRate) {
        if ([[self.propAppDelegate office] hasUsesSAP] && [[self.propAppDelegate office] hasUsesSAPService]) {
            self.propTaxApplied.text = [NSString stringWithFormat:@"%@ %@",self.propClaimItem.sapTaxCode, [self.propAppDelegate.decimalFormat stringFromNumber:[NSNumber numberWithFloat:self.propClaimItem.taxAmount]]];
        } else {
            self.propTaxApplied.text = [self.propAppDelegate.decimalFormat stringFromNumber:[NSNumber numberWithFloat:self.propClaimItem.taxAmount]];
        }
    } else {
        self.propTaxApplied.text = @"No";
    }
    self.propServiceCode.text = [self.propClaimItem serviceOrderCode];
    self.propBillTo.text = [self.propClaimItem propCompanyChargeToName];
    self.propAttachmentNote.text = [self.propClaimItem attachmentNote];
    self.propDescription.text = [self.propClaimItem description];
    [self.tableView setContentInset:UIEdgeInsetsMake(65, 0, 0, 0)];
    
    if ([self.propClaimHeader propTypeID] == CLAIMTYPEID_LIQUIDATION) {
        if (self.pageIndex == 0) {
            [self.propBarbuttonApprove setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
            [self.propBarbuttonReject setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
            [self.propBarbuttonReturn setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
            self.propBarbuttonApprove.enabled = NO;
            self.propBarbuttonReturn.enabled = NO;
            self.propBarbuttonReject.enabled = NO;
        }
    }else{
        [self.propBarbuttonReturn setEnabled:(self.propClaimHeader.propIsPaidByCC || self.propClaimHeader.propTypeID == CLAIMTYPEID_ADVANCES)?NO:YES];
    }
    
    self.approversNote = [[NSMutableString alloc] init];
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

-(void)viewDidLayoutSubviews {
    [super viewDidLayoutSubviews];
}

#pragma mark -
#pragma mark === Helper Method ===
#pragma mark -
-(void)onAttachmentClicked {
    [AppDelegate showGlogalHUDWithView:self.view];
    Document *doc = [[self.propClaimItem propAttachments] objectAtIndex:0];
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
        NSString *urlString = [NSString stringWithFormat:@"%@?dID=%d&refID=%d&obTypeID=%d", NSLocalizedString(@"ImageViewerLink", @""),[doc propDocID], [doc propRefID], [doc propObjectTypeID]];
        id urlData = [self.propAppDelegate.propGatewayOnline makeWebServiceCall:urlString requestMethod:GET postBody:nil];
        if (![urlData isKindOfClass:[NSString class]]) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [AppDelegate hideGlogalHUDWithView:self.view];
                NSString *filePath = [NSTemporaryDirectory() stringByAppendingPathComponent:[doc propDocName]];
                [urlData writeToFile:filePath atomically:YES];
                NSURL *filePathURL = [NSURL fileURLWithPath:filePath];
                self.document.URL = filePathURL;
                [self.document presentPreviewAnimated:YES];
            });
        } else {
            dispatch_async(dispatch_get_main_queue(), ^{
                [AppDelegate hideGlogalHUDWithView:self.view];
                UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"" message:urlData preferredStyle:UIAlertControllerStyleAlert];
                UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"Dismiss" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action){
                    
                }];
                [alertController addAction:cancelAction];
                [self presentViewController:alertController animated:YES completion:nil];
            });
        }
    });
}

#pragma clang diagnostic ignored "-Wnonnull"
- (void)updateClaimItemWithStatusID:(int)statusID updatableDate:(ClaimItemUpdatableDate)updatableDate approverNote:(NSString *)approversNote {
    [AppDelegate showGlogalHUDWithView:self.view];
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
        [self.propClaimItem setClaimStatus:statusID];
        [self.propClaimItem updatableDate:updatableDate withAppDelegate:self.propAppDelegate];
        [self.propClaimItem setNotes:approversNote];
        [self.propClaimItem setModifiedBy:[[self.propAppDelegate getStaff] propStaffID]];
        NSString *statusName = [[NSString alloc] init];
        if ([self.propClaimItem statusID] == CLAIMSTATUSID_APPROVEDBYAPPROVER)
            statusName = @"Approved by Approver";
        else if([self.propClaimItem statusID] == CLAIMSTATUSID_APPROVEDBYCM)
            statusName = @"Approved by CM";
        [self.propClaimItem setStatusName:statusName];
        NSString *claimItemJSON = [self.propClaimItem jsonForWebServicesWithAppDelegate:self.propAppDelegate];
        NSString *attachment = ([self.propClaimItem  propAttachments] != nil) ? [[[self.propClaimItem propAttachments] objectAtIndex:0] jsonObject]: [self.propClaimItem propAttachments];
        id result = [self.propAppDelegate.propGatewayOnline saveNewClaimItemJSON:claimItemJSON oldClaimItemID:0 document:attachment base64:nil];
        dispatch_async(dispatch_get_main_queue(), ^{
            [AppDelegate hideGlogalHUDWithView:self.view];
            UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"" message:((![result isKindOfClass:[NSString class]]) ? @"Updated Successfully" : result) preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"Dismiss" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action){
                [self.propAppDelegate updateForApprovalClaimItems:nil forClaimID:0 ];
                [self.propAppDelegate updateForApprovalClaimItems:result forClaimID:[_propClaimHeader propClaimID]];
                [self.navigationController popViewControllerAnimated:YES];
            }];
            [alertController addAction:cancelAction];
            [self presentViewController:alertController animated:YES completion:nil];
        });
    });
}

- (void)alertTextFieldDidChange:(UITextField *)sender
{
    UIAlertController *alertController = (UIAlertController *)self.presentedViewController;
    if (alertController)
    {
        UITextField *noteField = alertController.textFields.firstObject;
        UIAlertAction *confirmAction = alertController.actions.lastObject;
        confirmAction.enabled = noteField.text.length > 0;
    }
}


#pragma mark -
#pragma mark === UIDocument Interaction Controller Delegate ===
#pragma mark -
- (UIViewController *)documentInteractionControllerViewControllerForPreview:(UIDocumentInteractionController *)controller{
    return self;
}

#pragma mark -
#pragma mark -UI Event handler Management ===
#pragma mark -
- (IBAction)onApproveButtonClicked:(id)sender {
    if (self.propAppDelegate.getStaff.getUserPosition == USERPOSITION_CM || self.propAppDelegate.getStaff.getUserPosition == USERPOSITION_CFO) {
        [self updateClaimItemWithStatusID:CLAIMSTATUSID_APPROVEDBYCM updatableDate:ClaimItemUpdatableDateApprovedByDirector approverNote:@"CM: Approved"];
    } else {
        [self updateClaimItemWithStatusID:CLAIMSTATUSID_APPROVEDBYAPPROVER updatableDate:ClaimItemUpdatableDateApprovedByApprover approverNote:@"Approver: Approved"];
    }
}

- (IBAction)onRejectButtonClicked:(id)sender {
    NSString *alertTitle = @"Reason for Rejection";
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:alertTitle message:nil preferredStyle:UIAlertControllerStyleAlert];
    [alertController addTextFieldWithConfigurationHandler:^(UITextField *textField)
     {
         textField.placeholder = @"Required";
         [textField addTarget:self
                       action:@selector(alertTextFieldDidChange:)
             forControlEvents:UIControlEventEditingChanged];
     }];
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"Cancel"
                                                           style:UIAlertActionStyleCancel
                                                         handler:^(UIAlertAction *action) {
                                                             NSLog(@"Canceled");
                                                         }];
    UIAlertAction *rejectAction = [UIAlertAction actionWithTitle:@"Reject"
                                                           style:UIAlertActionStyleDefault
                                                         handler:^(UIAlertAction *action){
                                                             UITextField *rejectNote = alertController.textFields.firstObject;
                                                             if (self.propClaimHeader.propIsPaidByCC) {
                                                                 [self.approversNote appendFormat:@"APPROVER: %@;", rejectNote.text];
                                                                 [self updateClaimItemWithStatusID:CLAIMSTATUSID_REJECTEDFORSALARYDEDUCTION updatableDate:ClaimItemUpdatableDateRejected approverNote:self.approversNote];
                                                             }else{
                                                                 if (self.propAppDelegate.getStaff.getUserPosition == USERPOSITION_CM || self.propAppDelegate.getStaff.getUserPosition == USERPOSITION_CFO) {
                                                                     [self.approversNote appendFormat:@"CM: %@;", rejectNote.text];
                                                                     [self updateClaimItemWithStatusID:CLAIMSTATUSID_REJECTEDBYCM updatableDate:ClaimItemUpdatableDateRejected approverNote:self.approversNote];
                                                                 }else{
                                                                     [self.approversNote appendFormat:@"APPROVER: %@;", rejectNote.text];
                                                                     [self updateClaimItemWithStatusID:CLAIMSTATUSID_REJECTEDBYAPPROVER updatableDate:ClaimItemUpdatableDateRejected approverNote:self.approversNote];
                                                                 }
                                                             }
                                                         }];
    rejectAction.enabled = NO;
    [alertController addAction:cancelAction];
    [alertController addAction:rejectAction];
    [self presentViewController:alertController animated:YES completion:nil];
}


- (IBAction)onReturnButtonClicked:(id)sender {
    NSString *alertTitle = @"Reason for Returning";
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:alertTitle message:nil preferredStyle:UIAlertControllerStyleAlert];
    [alertController addTextFieldWithConfigurationHandler:^(UITextField *textField)
     {
         textField.placeholder = @"Required";
         [textField addTarget:self
                       action:@selector(alertTextFieldDidChange:)
             forControlEvents:UIControlEventEditingChanged];
     }];
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"Cancel"
                                                           style:UIAlertActionStyleCancel
                                                         handler:^(UIAlertAction *action) {
                                                             NSLog(@"Canceled");
                                                         }];
    UIAlertAction *returnAction = [UIAlertAction actionWithTitle:@"Return"
                                                           style:UIAlertActionStyleDefault
                                                         handler:^(UIAlertAction *action){
                                                             UITextField *returnNote = alertController.textFields.firstObject;
                                                             [self.approversNote appendFormat:@"APPROVER: %@;", returnNote.text];
                                                             [self updateClaimItemWithStatusID:CLAIMSTATUSID_RETURN updatableDate:ClaimItemUpdatableDateApprovedByApprover approverNote:self.approversNote];
                                                         }];
    returnAction.enabled = NO;
    [alertController addAction:cancelAction];
    [alertController addAction:returnAction];
    [self presentViewController:alertController animated:YES completion:nil];
}

/*
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:<#@"reuseIdentifier"#> forIndexPath:indexPath];
    
    // Configure the cell...
    
    return cell;
}
*/

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
