//
//  CFAMaximized.h
//  Salt
//
//  Created by Rick Royd Aban on 2015/08/26.
//  Copyright (c) 2015年 Applus Velosi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CFAMaximized : UITableViewCell

@property (strong, nonatomic) IBOutlet UILabel *propFieldCategory;
@property (strong, nonatomic) IBOutlet UILabel *propFieldStatus;
@property (strong, nonatomic) IBOutlet UILabel *propFieldExpenseDate;
@property (strong, nonatomic) IBOutlet UILabel *propFieldAmountForeign;
@property (strong, nonatomic) IBOutlet UILabel *propFieldVat;
@property (strong, nonatomic) IBOutlet UILabel *propFieldTax;
@property (strong, nonatomic) IBOutlet UILabel *propFieldAmountLocal;

@property (strong, nonatomic) IBOutlet UILabel *propFieldDescription;
@property (strong, nonatomic) IBOutlet UIImageView *propImageReceipt;

@end
