//
//  VCMileageClaimItemDetail.h
//  Salt
//
//  Created by Rick Royd Aban on 2015/08/06.
//  Copyright (c) 2015年 Applus Velosi. All rights reserved.
//

#import "VCPage.h"
#import "MileageClaimItem.h"

@interface VCMileageClaimItemDetail : VCPage<UITableViewDataSource, UITableViewDelegate>

@property (strong, nonatomic) MileageClaimItem *propClaimItem;

@end
