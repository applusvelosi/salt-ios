//
//  CellMyCalendarDate.h
//  Salt
//
//  Created by Rick Royd Aban on 6/22/15.
//  Copyright (c) 2015 Applus Velosi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "VCMyCalendar.h"

@interface CellMyCalendarDate : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *propLabel1;
@property (weak, nonatomic) IBOutlet UILabel *propLabel2;
@property (weak, nonatomic) IBOutlet UILabel *propLabel3;
@property (weak, nonatomic) IBOutlet UILabel *propLabel4;
@property (weak, nonatomic) IBOutlet UILabel *propLabel5;
@property (weak, nonatomic) IBOutlet UILabel *propLabel6;
@property (weak, nonatomic) IBOutlet UILabel *propLabel7;

@property (weak, nonatomic) IBOutlet UIView *propContainer1;
@property (weak, nonatomic) IBOutlet UIView *propContainer2;
@property (weak, nonatomic) IBOutlet UIView *propContainer3;
@property (weak, nonatomic) IBOutlet UIView *propContainer4;
@property (weak, nonatomic) IBOutlet UIView *propContainer5;
@property (weak, nonatomic) IBOutlet UIView *propContainer6;
@property (weak, nonatomic) IBOutlet UIView *propContainer7;


- (void)assignVCMyCalendar:(VCMyCalendar *)myCalendar;

@end
