//
//  VCClaimItemAttendeesViewController.h
//  Salt
//
//  Created by Rick Royd Aban on 2/29/16.
//  Copyright © 2016 Applus Velosi. All rights reserved.
//

#import "VCPage.h"

@interface VCClaimItemAttendees : VCPage<UITableViewDataSource, UITableViewDelegate>

@property (strong, nonatomic) NSMutableArray *claimitemAttendees;

@end
