//
//  ClaimItemAttendee.m
//  Salt
//
//  Created by Rick Royd Aban on 2015/09/07.
//  Copyright (c) 2015年 Applus Velosi. All rights reserved.
//

#import "ClaimItemAttendee.h"

@interface ClaimItemAttendee(){
    NSString *_name, *_jobTitle, *_desc;
}
@end

@implementation ClaimItemAttendee

- (instancetype)initWithName:(NSString *)name jobTitle:(NSString *)jobTitle desc:(NSString *)desc{
    if(self = [super init]){
        _name = name;
        _jobTitle = jobTitle;
        _desc = desc;
    }
    return self;
}

- (NSString *)propName{
    return _name;
}

- (NSString *)propJobTitle{
    return _jobTitle;
}

- (NSString *)propDesc{
    return _desc;
}

- (NSString *)jsonize{
    NSMutableString *jsonAttendee = [NSMutableString string];
    [jsonAttendee appendString:@"{"];
    [jsonAttendee appendFormat:@"\"FullName\":\"%@\"", _name];
    [jsonAttendee appendFormat:@",\"JobTitle\":\"%@\"", _jobTitle];
    [jsonAttendee appendFormat:@",\"Notes\":\"%@\"", _desc];
    [jsonAttendee appendString:@"}"];
    
    return jsonAttendee;
}

@end
