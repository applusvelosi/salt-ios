//
//  VCClaimItems.h
//  Salt
//
//  Created by Rick Royd Aban on 2015/08/03.
//  Copyright (c) 2015年 Applus Velosi. All rights reserved.
//

#import "VCPage.h"
#import "ClaimHeader.h"

@interface VCClaimItems : VCPage<UITableViewDelegate, UITableViewDataSource>

@property (strong, nonatomic) ClaimHeader *propClaimHeader;
@property (strong, nonatomic) UITableViewController *propParentVC;

@end
