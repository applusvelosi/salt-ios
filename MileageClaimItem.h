//
//  MileageClaimItem.h
//  Salt
//
//  Created by Rick Royd Aban on 2015/08/05.
//  Copyright (c) 2015年 Applus Velosi. All rights reserved.
//

#import "ClaimItem.h"

@interface MileageClaimItem : ClaimItem

+ (NSString *)MILEAGETYPE_VAL_KILOMETER;
+ (NSString *)MILEAGETYPE_VAL_MILE;
+ (int) MILEAGETYPE_KEY_KILOMETER;
+ (int) MILEAGETYPE_KEY_MILE;

- (int)propMileageTypeID;
- (NSString *)propMileageTypeName;
- (float)propMileageRate;
- (float)propMileage;
- (NSString *)propMileageFrom;
- (NSString *)propMileageTo;
- (BOOL)isMileageReturn;

@end
