//
//  DSCurrencyList.m
//  Salt
//
//  Created by Rick Royd Aban on 2015/09/10.
//  Copyright (c) 2015年 Applus Velosi. All rights reserved.
//

#import "DSCurrencyList.h"
#import "Currency.h"

@interface DSCurrencyList(){
    NSArray *_currencyList;
    NSMutableArray *_currencies, *_currencyNames, *_currencyThrees;
}
@end

@implementation DSCurrencyList

- (DSCurrencyList *)initCurrencyList{
    if([super init]){
        _currencyList = @[
                          @"1-UAE Dirham-AED",
                          @"2-Albanian Lek-ALL",
                          @"3-Neth Antilles Guilder-ANG",
                          @"4-Argentine Peso-ARS",
                          @"5-Australian Dollar-AUD",
                          @"6-Aruba Florin-AWG",
                          @"7-Barbados Dollar-BBD",
                          @"8-Bangladesh Taka-BDT",
                          @"9-Bulgarian Lev-BGN",
                          @"10-Bahraini Dinar-BHD",
                          @"11-Burundi Franc-BIF",
                          @"12-Bermuda Dollar-BMD",
                          @"13-Brunei Dollar-BND",
                          @"14-Bolivian Boliviano-BOB",
                          @"15-Brazilian Real-BRL",
                          @"16-Bahamian Dollar-BSD",
                          @"17-Bhutan Ngultrum-BTN",
                          @"18-Botswana Pula-BWP",
                          @"19-Belarus Ruble-BYR",
                          @"20-Belize Dollar-BZD",
                          @"21-Canadian Dollar-CAD",
                          @"22-Swiss Franc-CHF",
                          @"23-Chilean Peso-CLP",
                          @"24-Chinese Yuan-CNY",
                          @"25-Colombian Peso-COP",
                          @"26-Costa Rica Colon-CRC",
                          @"27-Cuban Peso-CUP",
                          @"28-Cape Verde Escudo-CVE",
                          @"29-Czech Koruna-CZK",
                          @"30-Dijibouti Franc-DJF",
                          @"31-Danish Krone-DKK",
                          @"32-Dominican Peso-DOP",
                          @"33-Algerian Dinar-DZD",
                          @"34-Estonian Kroon-EEK",
                          @"35-Egyptian Pound-EGP",
                          @"36-Ethiopian Birr-ETB",
                          @"37-Euro-EUR",
                          @"38-Fiji Dollar-FJD",
                          @"39-Falkland Islands Pound-FKP",
                          @"40-British Pound-GBP",
                          @"41-Gibraltar Pound-GIP",
                          @"42-Gambian Dalasi-GMD",
                          @"43-Guinea Franc-GNF",
                          @"44-Guatemala Quetzal-GTQ",
                          @"45-Guyana Dollar-GYD",
                          @"46-Hong Kong Dollar-HKD",
                          @"47-Honduras Lempira-HNL",
                          @"48-Croatian Kuna-HRK",
                          @"49-Haiti Gourde-HTG",
                          @"50-Hungarian Forint-HUF",
                          @"51-Indonesian Rupiah-IDR",
                          @"52-Israeli Shekel-ILS",
                          @"53-Indian Rupee-INR",
                          @"54-Iraqi Dinar-IQD",
                          @"55-Iran Rial-IRR",
                          @"56-Iceland Krona-ISK",
                          @"57-Jamaican Dollar-JMD",
                          @"58-Jordanian Dinar-JOD",
                          @"59-Japanese Yen-JPY",
                          @"60-Kenyan Shilling-KES",
                          @"61-Cambodia Riel-KHR",
                          @"62-Comoros Franc-KMF",
                          @"63-North Korean Won-KPW",
                          @"64-Korean Won-KRW",
                          @"65-Kuwaiti Dinar-KWD",
                          @"66-Cayman Islands Dollar-KYD",
                          @"67-Kazakhstan Tenge-KZT",
                          @"68-Lao Kip-LAK",
                          @"69-Lebanese Pound-LBP",
                          @"70-Sri Lanka Rupee-LKR",
                          @"71-Liberian Dollar-LRD",
                          @"72-Lesotho Loti-LSL",
                          @"73-Lithuanian Lita-LTL",
                          @"74-Latvian Lat-LVL",
                          @"75-Libyan Dinar-LYD",
                          @"76-Moroccan Dirham-MAD",
                          @"77-Moldovan Leu-MDL",
                          @"78-Macedonian Denar-MKD",
                          @"79-Myanmar Kyat-MMK",
                          @"80-Mongolian Tugrik-MNT",
                          @"81-Macau Pataca-MOP",
                          @"82-Mauritania Ougulya-MRO",
                          @"83-Mauritius Rupee-MUR",
                          @"84-Maldives Rufiyaa-MVR",
                          @"85-Malawi Kwacha-MWK",
                          @"86-Mexican Peso-MXN",
                          @"87-Malaysian Ringgit-MYR",
                          @"88-Namibian Dollar-NAD",
                          @"89-Nigerian Naira-NGN",
                          @"90-Nicaragua Cordoba-NIO",
                          @"91-Norwegian Krone-NOK",
                          @"92-Nepalese Rupee-NPR",
                          @"93-New Zealand Dollar-NZD",
                          @"94-Omani Rial-OMR",
                          @"95-Panama Balboa-PAB",
                          @"96-Peruvian Nuevo Sol-PEN",
                          @"97-Papua New Guinea Kina-PGK",
                          @"98-Philippine Peso-PHP",
                          @"99-Pakistani Rupee-PKR",
                          @"100-Polish Zloty-PLN",
                          @"101-Paraguayan Guarani-PYG",
                          @"102-Qatar Rial-QAR",
                          @"103-Romanian New Leu-RON",
                          @"104-Russian Rouble-RUB",
                          @"105-Rwanda Franc-RWF",
                          @"106-Saudi Arabian Riyal-SAR",
                          @"107-Solomon Islands Dollar-SBD",
                          @"108-Seychelles Rupee-SCR",
                          @"109-Swedish Krona-SEK",
                          @"110-Singapore Dollar-SGD",
                          @"111-St Helena Pound-SHP",
                          @"112-Slovak Koruna-SKK",
                          @"113-Sierra Leone Leone-SLL",
                          @"114-Somali Shilling-SOS",
                          @"115-Sao Tome Dobra-STD",
                          @"116-El Salvador Colon-SVC",
                          @"117-Syrian Pound-SYP",
                          @"118-Swaziland Lilageni-SZL",
                          @"119-Thai Baht-THB",
                          @"120-Tunisian Dinar-TND",
                          @"121-Tonga Pa'anga-TOP",
                          @"122-Turkish Lira-TRY",
                          @"123-Trinidad & Tobago Dollar-TTD",
                          @"124-Taiwan Dollar-TWD",
                          @"125-Tanzanian Shilling-TZS",
                          @"126-Ukraine Hryvnia-UAH",
                          @"127-Ugandan Shilling-UGX",
                          @"128-U.S. Dollar-USD",
                          @"129-Uruguayan New Peso-UYU",
                          @"130-Vietnam Dong-VND",
                          @"131-Vanuatu Vatu-VUV",
                          @"132-Samoa Tala-WST",
                          @"133-CFA Franc-XAF",
                          @"134-Silver Ounces-XAG",
                          @"135-Gold Ounces-XAU",
                          @"136-East Caribbean Dollar-XCD",
                          @"137-Copper Pounds-XCP",
                          @"138-CFA Franc-XOF",
                          @"139-Palladium Ounces-XPD",
                          @"140-Pacific Franc-XPF",
                          @"141-Platinum Ounces-XPT",
                          @"142-Yemen Riyal-YER",
                          @"143-South African Rand-ZAR",
                          @"144-Zambian Kwacha-ZMK",
                          @"145-Zimbabwe Dollar-ZWD",
                          @"148-Angolan Kwanza-AOA",
                          @"151-Ghana Cedi-GHS",
                          @"152-Uzbekistani Som-UZS",
                          @"153-Azerbaijani Manat-AZN"
        ];
        
        _currencies = [NSMutableArray array];
        _currencyNames = [NSMutableArray array];
        _currencyThrees = [NSMutableArray array];
        
        for(NSString *currency in _currencyList){
            NSArray *currencyParts = [currency componentsSeparatedByString:@"-"];
            [_currencies addObject:[[Currency alloc] initWithID:[[currencyParts objectAtIndex:0] intValue] name:[currencyParts objectAtIndex:1] currThree:[currencyParts objectAtIndex:2]]];
            [_currencyNames addObject:[currencyParts objectAtIndex:1]];
            [_currencyThrees addObject:[currencyParts objectAtIndex:2]];
        }
    }
    
    return self;
}

- (NSArray *)propCurrencies{
    return _currencies;
}

- (NSArray *)propCurrencyNames{
    return _currencyNames;
}

- (NSArray *)propCurrencyThrees{
    return _currencyThrees;
}

@end
