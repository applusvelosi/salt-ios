//
//  VCGeneralClaimItemsDetailsPage.h
//  Salt
//
//  Created by Rick Royd Aban on 17/11/2016.
//  Copyright © 2016 Applus Velosi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ClaimItem.h"

@interface VCGeneralClaimItemsDetailsPage : UITableViewController

@property NSUInteger pageIndex;

@property ClaimHeader *propClaimHeader;
@property ClaimItem *propClaimItem;
@property AppDelegate *propAppDelegate;

@end
