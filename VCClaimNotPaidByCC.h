//
//  VCClaimNotPaidByCC.h
//  Salt
//
//  Created by Rick Royd Aban on 2015/07/29.
//  Copyright (c) 2015年 Applus Velosi. All rights reserved.
//

#import "VCPage.h"
#import "ClaimHeader.h"
#import "ClaimNotPaidByCC.h"
#import "VCClaimPaidByCC.h"

@interface VCClaimNotPaidByCC : UITableViewController

@property (strong, nonatomic) ClaimNotPaidByCC *claimNotPaidByCC;
@property (nonatomic, strong) AppDelegate *propAppDelegate;
@property (copy, nonatomic) LCSumValuesProvider lcSumValuesProvider;

@end

