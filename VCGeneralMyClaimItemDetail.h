//
//  VCGeneralMyClaimItemDetail.h
//  Salt
//
//  Created by Rick Royd Aban on 06/03/2017.
//  Copyright © 2017 Applus Velosi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ClaimItem.h"
#import "VCPage.h"

@interface VCGeneralMyClaimItemDetail : VCPage<UITableViewDelegate, UITableViewDataSource>

@property (strong, nonatomic) ClaimItem *propClaimItem;

@end
