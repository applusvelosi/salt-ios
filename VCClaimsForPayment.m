//
//  VCClaimsForPayment.m
//  Salt
//
//  Created by Rick Royd Aban on 2015/07/31.
//  Copyright (c) 2015年 Applus Velosi. All rights reserved.
//

#import "VCClaimsForPayment.h"

@interface VCClaimsForPayment (){
    
    IBOutlet UITableView *_propLV;
}

@end

@implementation VCClaimsForPayment

- (void)viewDidLoad {
    [super viewDidLoad];

    _propLV.dataSource = self;
    _propLV.delegate = self;
    _propLV.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    
}

- (IBAction)toggleList:(id)sender {
    [self.propAppDelegate.propSlider toggleSidebar];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    return nil;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 0;
}

- (IBAction)refresh:(id)sender {
}

@end
