//
//  CellClaimItemDetailAmount.h
//  Salt
//
//  Created by Rick Royd Aban on 2/10/16.
//  Copyright © 2016 Applus Velosi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CellClaimItemDetailAmount : UITableViewCell

@property (strong, nonatomic) IBOutlet UILabel *propLabelAmountForeign;
@property (strong, nonatomic) IBOutlet UILabel *propLabelAmountLocal;
@property (strong, nonatomic) IBOutlet UILabel *propLabelAmountForex;
@property (strong, nonatomic) IBOutlet UILabel *propLabelAmountTax;

@end
