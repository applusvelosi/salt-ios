//
//  VCClaimPaidByCC.h
//  Salt
//
//  Created by Rick Royd Aban on 2015/07/27.
//  Copyright (c) 2015年 Applus Velosi. All rights reserved.
//

#import "VCPage.h"
#import "ClaimPaidByCC.h"

@interface VCClaimPaidByCC : UITableViewController

@property (strong, nonatomic) ClaimPaidByCC *claimPaidByCC;
@property (copy, nonatomic) LCSumValuesProvider lcSumValuesProvider;
@property (nonatomic, strong) AppDelegate *propAppDelegate;

@end
