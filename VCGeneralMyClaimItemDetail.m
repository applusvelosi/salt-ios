//
//  VCGeneralMyClaimItemDetail.m
//  Salt
//
//  Created by Rick Royd Aban on 06/03/2017.
//  Copyright © 2017 Applus Velosi. All rights reserved.
//

#import "VCGeneralMyClaimItemDetail.h"
#import "CellClaimItemDetailOverview.h"
#import "CellClaimItemDetailAmount.h"

@interface VCGeneralMyClaimItemDetail (){
    IBOutlet UITableView *_propLV;
}

@end

@implementation VCGeneralMyClaimItemDetail

#pragma mark -
#pragma mark === View Life Cycle Management ===
#pragma mark -
- (void)viewDidLoad {
    [super viewDidLoad];
    
    _propLV.delegate = self;
    _propLV.dataSource = self;
    _propLV.rowHeight = UITableViewAutomaticDimension;
    _propLV.estimatedRowHeight = 140;
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/


#pragma mark -
#pragma mark - Table view data source delegate
#pragma mark -
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    switch (indexPath.row) {
        case 0: {
            NSLog(@"CLAIM ITEM CAT NAME: %@",self.propClaimItem.propCategoryName);
            CellClaimItemDetailOverview *cell = [tableView dequeueReusableCellWithIdentifier:@"cell_overview"];
            cell.propLabelCategoryName.text = [self.propClaimItem propCategoryName];
            cell.propLabelItemNumber.text = [self.propClaimItem propItemNumber];
            cell.propLabelProjectName.text = self.propClaimItem.wbsCode;
            cell.propLabelDate.text = [self.propClaimItem propDateCreatedWithAppDelegate:self.propAppDelegate];
            switch (self.propClaimItem.attachmentFlag) {
                case 1:
                    cell.propLabelAttachment.text = @"YES";
                    break;
                case 2:
                    cell.propLabelAttachment.text = @"NO";
                    break;
                default:
                    cell.propLabelAttachment.text = @"N/A";
                    break;
            }
            return cell;
        }
            break;
        case 1: {
            CellClaimItemDetailAmount *cell = [tableView dequeueReusableCellWithIdentifier:@"cell_amount"];
            cell.propLabelAmountForeign.text = [NSString stringWithFormat:@"%@ %@",[self.propAppDelegate.decimalFormat stringFromNumber:[NSNumber numberWithFloat:[self.propClaimItem amountForeing]]], [self.propClaimItem foreignCurrency]];
            cell.propLabelAmountLocal.text = [NSString stringWithFormat:@"%@ %@", [self.propAppDelegate.decimalFormat stringFromNumber:[NSNumber numberWithFloat:[self.propClaimItem amountLocal]]], [self.propClaimItem localCurrencyName]];
            cell.propLabelAmountForex.text = [NSString stringWithFormat:@"%.1f",[self.propClaimItem exchangeRate]];;
            if (self.propClaimItem.isTaxRate) {
                if ([[self.propAppDelegate office] hasUsesSAP] && [[self.propAppDelegate office] hasUsesSAPService]) {
                    cell.propLabelAmountTax.text = [NSString stringWithFormat:@"%@ %@",self.propClaimItem.sapTaxCode, [self.propAppDelegate.decimalFormat stringFromNumber:[NSNumber numberWithFloat:self.propClaimItem.taxAmount]]];
                } else {
                    cell.propLabelAmountTax.text = [self.propAppDelegate.decimalFormat stringFromNumber:[NSNumber numberWithFloat:self.propClaimItem.taxAmount]];
                }
            } else {
                cell.propLabelAmountTax.text = @"No";
            }
            
            return cell;
        }
            break;
        default:
            return nil;
            break;
    }
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 1;
}


@end
