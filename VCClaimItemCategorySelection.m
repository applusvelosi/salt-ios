//
//  VCClaimItemCategorySelection.m
//  Salt
//
//  Created by Rick Royd Aban on 2015/09/08.
//  Copyright (c) 2015年 Applus Velosi. All rights reserved.
//

#import "VCClaimItemCategorySelection.h"
#import "MBProgressHUD.h"
#import "ClaimCategory.h"
#import "CellClaimItemCategory.h"
#import "VCClaimItemInputGeneral.h"

@interface VCClaimItemCategorySelection(){
    
    IBOutlet UITableView *_propLV;
    NSMutableArray *_categories;
}
@end

@implementation VCClaimItemCategorySelection

- (void)viewDidLoad{
    [super  viewDidLoad];
    
    _categories = [NSMutableArray array];    
    _propLV.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    _propLV.rowHeight = UITableViewAutomaticDimension;
    _propLV.estimatedRowHeight = 48.0;
    _propLV.delegate = self;
    _propLV.dataSource = self;

    [self reloadData];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    CellClaimItemCategory *cell = [tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
    [cell.propCategoryName setPreferredMaxLayoutWidth:200.0];
    cell.propCategoryName.text = [(ClaimCategory *)[_categories objectAtIndex:indexPath.row] propName];
//    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"basic_cell"];
//    cell.textLabel.text = [(ClaimCategory *)[_categories objectAtIndex:indexPath.row] propName];
    
    return cell;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return _categories.count;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [self performSegueWithIdentifier:@"selectclaimitemcategorytogeneraliteminput" sender:nil];
}

- (IBAction)refresh:(id)sender {
    [self reloadData];
}

- (void)reloadData{
    [AppDelegate showGlogalHUDWithView:self.view];
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
        id result = [self.propAppDelegate.propGatewayOnline claimItemCategoriesByOffice];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [AppDelegate hideGlogalHUDWithView:self.view];
            if([result isKindOfClass:[NSString class]])
                [[[UIAlertView alloc] initWithTitle:@"" message:result delegate:nil cancelButtonTitle:@"Dismiss" otherButtonTitles:nil, nil] show];
            else{
                [_categories removeAllObjects];
                [_categories addObjectsFromArray:result];
                [_propLV reloadData];
            }
        });
    });
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    ((VCClaimItemInputGeneral *)segue.destinationViewController).propClaimHeader = _propClaimHeader;
    ((VCClaimItemInputGeneral *)segue.destinationViewController).propClaimCategory = [_categories objectAtIndex:((UITableViewCell *)sender).tag];
    
}

- (IBAction)cancel:(id)sender {
    [self.navigationController dismissViewControllerAnimated:YES completion:nil];
}


@end
