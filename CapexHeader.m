//
//  CapexHeader.m
//  Salt
//
//  Created by Rick Royd Aban on 10/1/15.   
//  Copyright © 2015 Applus Velosi. All rights reserved.
//

#import "CapexHeader.h"
#import "AppDelegate.h"
#import "Document.h"

@interface CapexHeader(){
    NSMutableDictionary *_capexHeader;
}
@end

@implementation CapexHeader

- (CapexHeader *)initWithDictionary:(NSDictionary *)jsonCapexHeader onlineGateway:(OnlineGateway *)onlineGateway{
    if([super init]) {
        _capexHeader = [[NSDictionary dictionaryWithDictionary:jsonCapexHeader] mutableCopy];
        [_capexHeader setObject:[onlineGateway deserializeJsonDateString:[_capexHeader objectForKey:@"DateCreated"]] forKey:@"DateCreated"];
        [_capexHeader setObject:[onlineGateway deserializeJsonDateString:[_capexHeader objectForKey:@"DateSubmitted"]] forKey:@"DateSubmitted"];
        [_capexHeader setObject:[onlineGateway deserializeJsonDateString:[_capexHeader objectForKey:@"DateProcessedByCountryManager"]] forKey:@"DateProcessedByCountryManager"];
        [_capexHeader setObject:[onlineGateway deserializeJsonDateString:[_capexHeader objectForKey:@"DateProcessedByRFM"]] forKey:@"DateProcessedByRFM"];
        [_capexHeader setObject:[onlineGateway deserializeJsonDateString:[_capexHeader objectForKey:@"DateProcessedByRegionalManager"]] forKey:@"DateProcessedByRegionalManager"];
        [_capexHeader setObject:[onlineGateway deserializeJsonDateString:[_capexHeader objectForKey:@"DateProcessedByCFO"]] forKey:@"DateProcessedByCFO"];
         [_capexHeader setObject:[onlineGateway deserializeJsonDateString:[_capexHeader objectForKey:@"DateProcessedByCEO"]] forKey:@"DateProcessedByCEO"];
        [_capexHeader setObject:[_capexHeader objectForKey:@"Documents"] forKey:@"Documents"];
//        [_capexHeader setObject:[[NSString alloc] initWithData:[NSJSONSerialization dataWithJSONObject:[_capexHeader objectForKey:@"Documents"] options:0 error:nil] encoding:NSUTF8StringEncoding] forKey:@"Documents"];
    }
    return self;
}

-(NSString *)getCapexJson{
    return [[NSString alloc] initWithData:[NSJSONSerialization dataWithJSONObject:_capexHeader options:0 error:nil] encoding:NSUTF8StringEncoding];
}

- (CapexHeader *)initWithDictionary:(NSMutableDictionary *)jsonCapexHeader{
    if([super init])
        _capexHeader = jsonCapexHeader;
    
    return self;
}

- (NSDictionary *)savableDictionary{
    return _capexHeader;
}

-(NSString *)propApproverNote{
    return [_capexHeader objectForKey:@"ApproverNote"];
}

- (NSString *)propAttachedCer{
    NSString *attachedCer = [NSString stringWithFormat:@"%@",[_capexHeader objectForKey:@"AttachedCER"]];
    return (attachedCer.length<1/* || [attachedCer isEqualToString:@""]*/)?NO_ATTACHMENT:attachedCer;
    
}


#pragma mark -
#pragma mark === Helper Method ===
#pragma mark -
-(NSString *)formatDateToBeViewed:(NSString *)dateFromStr appDelegate:(AppDelegate *)app{
    NSInteger janOneTwoThousand = 946684800000/1000; //Jan 1, 2000 GMT in epoch time
    NSDate *jan1 = [NSDate dateWithTimeIntervalSince1970:janOneTwoThousand];
    NSDate *dateFromString = [app.propDateFormatByProcessed dateFromString:dateFromStr];
    if ([dateFromString compare:jan1] == NSOrderedDescending || [dateFromString compare:jan1] == NSOrderedSame) //Date Before Jan 1, 2000 must not be viewed
        return [app.propDateFormatByProcessed stringFromDate:dateFromString];
    return @"";
}

- (int)propCapexID{
    return [[_capexHeader objectForKey:@"CapexID"] intValue];
}

- (NSArray *)propCapexLineItems{
    return [NSArray array];
}

- (NSString *)propCapexNumber{
    return [_capexHeader objectForKey:@"CapexNumber"];
}

- (int)propCostCenterID{
    return [[_capexHeader objectForKey:@"CostCenterID"] intValue];
}

- (NSString *)propCostCenterName{
    return [_capexHeader objectForKey:@"CostCenterName"];
}

- (int)propCMID{
    return [[_capexHeader objectForKey:@"CountryManager"] intValue];
}

- (NSString *)propCMEmail{
    return [_capexHeader objectForKey:@"CountryManagerEmail"];
}

- (NSString *)propCMName{
    return [_capexHeader objectForKey:@"CountryManagerName"];
}

- (NSString *)propDateCreated:(AppDelegate *)app{
    return [self formatDateToBeViewed:[_capexHeader objectForKey:@"DateCreated"] appDelegate:app];
}

- (NSString *)propDateSubmitted:(AppDelegate *)app{
    return [self formatDateToBeViewed:[_capexHeader objectForKey:@"DateSubmitted"] appDelegate:app];
}

- (NSString *)propDateProcessedByCM:(AppDelegate *)app{
    return [self formatDateToBeViewed:[_capexHeader objectForKey:@"DateProcessedByCountryManager"] appDelegate:app];
}

-(NSString *)propDateProcessedByCFO:(AppDelegate *)app{
    return [self formatDateToBeViewed:[_capexHeader objectForKey:@"DateProcessedByCFO"] appDelegate:app];
}

- (NSString *)propDateProcessedByRM:(AppDelegate *)app{
    return [self formatDateToBeViewed:[_capexHeader objectForKey:@"DateProcessedByRegionalManager"] appDelegate:app];
}

-(NSString *)propDateProcessedByRFM:(AppDelegate *)app{
    return [self formatDateToBeViewed:[_capexHeader objectForKey:@"DateProcessedByRFM"] appDelegate:app];
}

-(NSString *)propDateProcessedByCEO:(AppDelegate *)app{
    return [self formatDateToBeViewed:[_capexHeader objectForKey:@"DateProcessedByCEO"] appDelegate:app];
}

- (int)propDepartmentID{
    return [[_capexHeader objectForKey:@"DepartmentID"] intValue];
}

- (NSString *)propDepartmentName{
    return ([[_capexHeader objectForKey:@"DepartmentName"] isKindOfClass:[NSNull class]])?@"":[_capexHeader objectForKey:@"DepartmentName"];
}

- (int)propInvestmentTypeID{
    return [[_capexHeader objectForKey:@"InvestmentTypeID"] intValue];
}

- (NSString *)propInvestmentTypeName{
    return ([[_capexHeader objectForKey:@"InvestmentTypeName"] isKindOfClass:[NSNull class]])?@"":[_capexHeader objectForKey:@"InvestmentTypeName"];
}

- (NSString *)propNote{
    return [_capexHeader objectForKey:@"Note"];
}

- (int)propOfficeID{
    return [[_capexHeader objectForKey:@"OfficeID"] intValue];
}

- (NSString *)propOfficeName{
    return [_capexHeader objectForKey:@"OfficeName"];
}

-(int)propRFMID {
    return [[_capexHeader objectForKey:@"RfmId"] intValue];
}

- (int)propRMID{
    return [[_capexHeader objectForKey:@"RegionalManager"] intValue];
}

- (NSString *)propRMEmail{
    return [_capexHeader objectForKey:@"RegionalManagerEmail"];
}

- (NSString *)propRMName{
    return [_capexHeader objectForKey:@"RegionalManagerName"];
}

- (NSString *)propRequesterEmail{
    return [_capexHeader objectForKey:@"RequesterEmail"];
}

- (int)propRequesterID{
    return [[_capexHeader objectForKey:@"RequesterID"] intValue];
}

- (NSString *)propRequesterName{
    return [_capexHeader objectForKey:@"RequesterName"];
}

- (int)propStatusID{
    return [[_capexHeader objectForKey:@"StatusID"] intValue];
}

- (NSString *)propStatusName{
    return [_capexHeader objectForKey:@"StatusName"];
}

- (float)propTotal{
    return [[_capexHeader objectForKey:@"Total"] floatValue];
}

- (float)propTotalAmountInUSD{
    return [[_capexHeader objectForKey:@"TotalAmountInUSD"] floatValue];
}

- (NSMutableArray *)propDocuments{
    NSMutableArray *docArray = [NSMutableArray array];
//    NSArray *jsonAttachments = [NSJSONSerialization JSONObjectWithData:[[_capexHeader objectForKey:@"Documents"]dataUsingEncoding:NSUTF8StringEncoding] options:0 error:0];
    NSArray *jsonAttachments = [_capexHeader objectForKey:@"Documents"];
    for (NSDictionary *jsonDoc in jsonAttachments) {
        [docArray addObject:[[Document alloc] initWithDictionary:jsonDoc]];
    }
    return docArray;
}

//- (NSMutableArray *)propAttachments{
//    NSMutableArray *ret = [NSMutableArray array];
//    if([[_claimItem objectForKey:@"IsWithReceipt"] boolValue]){
//        NSArray *jsonAttachments = [NSJSONSerialization JSONObjectWithData:[[_claimItem objectForKey:@"Attachment"] dataUsingEncoding:NSUTF8StringEncoding] options:NSJSONReadingMutableContainers error:nil];
//        for(NSDictionary *jsonAttachment in jsonAttachments)
//            [ret addObject:[[Document alloc] initWithDictionary:jsonAttachment]];
//    }else
//        NSLog(@"NO CRECRIPT");
//    
//    return ret;
//}

- (NSString *)JSONForUpdatingCapexHeaderWithStatusID:(CapexStatusId)statusID keyForUpdatableDate:(NSString *)keyForUpdatableDate withApproverNote:(NSString *)note appDelegate:(AppDelegate *)appDelegate {
    
    NSMutableDictionary *tempDict = [NSMutableDictionary dictionaryWithDictionary:_capexHeader];
    [tempDict setObject:@(statusID) forKey:@"StatusID"];
    [tempDict setObject:note forKey:@"ApproverNote"];
    NSString *deptName = ([[_capexHeader objectForKey:@"DepartmentName"] isKindOfClass:[NSNull class]])?@"":[_capexHeader objectForKey:@"DepartmentName"];
    [tempDict setObject:[tempDict objectForKey:@"Documents"] forKey:@"Documents"];
    [tempDict setObject:deptName forKey:@"DepartmentName"];
    
    [tempDict setObject:[appDelegate.propGatewayOnline convertToUnixDate:[appDelegate.propDateFormatByProcessed dateFromString:[tempDict objectForKey:@"DateCreated"]]] forKey:@"DateCreated"];
    [tempDict setObject:[appDelegate.propGatewayOnline convertToUnixDate:[appDelegate.propDateFormatByProcessed dateFromString:[tempDict objectForKey:@"DateProcessedByRegionalManager"]]] forKey:@"DateProcessedByRegionalManager"];
    [tempDict setObject:[appDelegate.propGatewayOnline convertToUnixDate:[appDelegate.propDateFormatByProcessed dateFromString:[tempDict objectForKey:@"DateSubmitted"]]] forKey:@"DateSubmitted"];
    [tempDict setObject:[appDelegate.propGatewayOnline convertToUnixDate:[appDelegate.propDateFormatByProcessed dateFromString:[tempDict objectForKey:@"DateProcessedByCountryManager"]]] forKey:@"DateProcessedByCountryManager"];
    [tempDict setObject:[appDelegate.propGatewayOnline convertToUnixDate:[appDelegate.propDateFormatByProcessed dateFromString:[tempDict objectForKey:@"DateProcessedByRFM"]]] forKey:@"DateProcessedByRFM"];
    [tempDict setObject:[appDelegate.propGatewayOnline convertToUnixDate:[appDelegate.propDateFormatByProcessed dateFromString:[tempDict objectForKey:@"DateProcessedByCFO"]]] forKey:@"DateProcessedByCFO"];
    [tempDict setObject:[appDelegate.propGatewayOnline convertToUnixDate:[appDelegate.propDateFormatByProcessed dateFromString:[tempDict objectForKey:@"DateProcessedByCEO"]]] forKey:@"DateProcessedByCEO"];
    
    [tempDict setObject:@[] forKey:@"CapexLineItems"];
    switch (statusID) {
        case CapexStatusIdApprovedByCm:
            [tempDict setObject:@"Approved by CM" forKey:@"StatusName"];
            break;
        case CapexStatusIdApprovedByRfm:
            [tempDict setObject:@"Approved by RFM" forKey:@"StatusName"];
            break;
        case CapexStatusIdApprovedByRm:
            [tempDict setObject:@"Approved by RM" forKey:@"StatusName"];
            break;
        case CapexStatusIdApprovedByCfo:
            [tempDict setObject:@"Approved by CFO" forKey:@"StatusName"];
            break;
        case CapexStatusIdApprovedByCeo:
            [tempDict setObject:@"Approved by CEO" forKey:@"StatusName"];
            break;
        case CapexStatusIdRejectedByCm:
            [tempDict setObject:@"Rejected by CM" forKey:@"StatusName"];
            break;
        case CapexStatusIdRejectedByRfm:
            [tempDict setObject:@"Rejected by RFM" forKey:@"StatusName"];
            break;
        case CapexStatusIdRejectedByRm:
            [tempDict setObject:@"Rejected by RM" forKey:@"StatusName"];
            break;
        case CapexStatusIdRejectedByCfo:
            [tempDict setObject:@"Rejected by CFO" forKey:@"StatusName"];
            break;
        case CapexStatusIdRejectedByCeo:
            [tempDict setObject:@"Rejected by CEO" forKey:@"StatusName"];
            break;
        case CapexStatusIdReturned:
            [tempDict setObject:@"Open" forKey:@"StatusName"];
        default:
            break;
    }
    if(![keyForUpdatableDate isEqualToString:@"NA"])
        [tempDict setObject:[appDelegate.propGatewayOnline epochizeDate:[NSDate date]] forKey:keyForUpdatableDate];
    return [[NSString alloc] initWithData:[NSJSONSerialization dataWithJSONObject:tempDict options:0 error:nil] encoding:NSUTF8StringEncoding];
}

- (NSString *)jsonizeWithappDelegate:(AppDelegate *)appDelegate {
    
    NSMutableDictionary *tempDict = [NSMutableDictionary dictionaryWithDictionary:_capexHeader];
    [tempDict setObject:[tempDict objectForKey:@"StatusID"] forKey:@"StatusID"];
    [tempDict setObject:[tempDict objectForKey:@"ApproverNote"] forKey:@"ApproverNote"];
    [tempDict setObject:[tempDict objectForKey:@"Documents"] forKey:@"Documents"];
    [tempDict setObject:[appDelegate.propGatewayOnline convertToUnixDate:[appDelegate.propDateFormatByProcessed dateFromString:[tempDict objectForKey:@"DateCreated"]]] forKey:@"DateCreated"];
    [tempDict setObject:[appDelegate.propGatewayOnline convertToUnixDate:[appDelegate.propDateFormatByProcessed dateFromString:[tempDict objectForKey:@"DateProcessedByRegionalManager"]]] forKey:@"DateProcessedByRegionalManager"];
    [tempDict setObject:[appDelegate.propGatewayOnline convertToUnixDate:[appDelegate.propDateFormatByProcessed dateFromString:[tempDict objectForKey:@"DateSubmitted"]]] forKey:@"DateSubmitted"];
    [tempDict setObject:[appDelegate.propGatewayOnline convertToUnixDate:[appDelegate.propDateFormatByProcessed dateFromString:[tempDict objectForKey:@"DateProcessedByCountryManager"]]] forKey:@"DateProcessedByCountryManager"];
    [tempDict setObject:[appDelegate.propGatewayOnline convertToUnixDate:[appDelegate.propDateFormatByProcessed dateFromString:[tempDict objectForKey:@"DateProcessedByRFM"]]] forKey:@"DateProcessedByRFM"];
    [tempDict setObject:[appDelegate.propGatewayOnline convertToUnixDate:[appDelegate.propDateFormatByProcessed dateFromString:[tempDict objectForKey:@"DateProcessedByCFO"]]] forKey:@"DateProcessedByCFO"];
    [tempDict setObject:[appDelegate.propGatewayOnline convertToUnixDate:[appDelegate.propDateFormatByProcessed dateFromString:[tempDict objectForKey:@"DateProcessedByCEO"]]] forKey:@"DateProcessedByCEO"];
    return [[NSString alloc] initWithData:[NSJSONSerialization dataWithJSONObject:tempDict options:0 error:nil] encoding:NSUTF8StringEncoding];
}

@end
