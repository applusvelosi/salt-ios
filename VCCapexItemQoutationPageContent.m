//
//  VCCapexItemQoutationPageContent.m
//  Salt
//
//  Created by Rick Royd Aban on 19/09/2016.
//  Copyright © 2016 Applus Velosi. All rights reserved.
//

#import "VCCapexItemQoutationPageContent.h"
#import "AppDelegate.h"
#import "VelosiColors.h"
#import "MBProgressHUD.h"

@interface VCCapexItemQoutationPageContent () <UIDocumentInteractionControllerDelegate>{
    IBOutlet UILabel *_fieldIsPrimary;
    IBOutlet UILabel *_fieldSupplier;
    IBOutlet UILabel *_fieldLC;
    IBOutlet UILabel *_fieldAmountInUSD;
    IBOutlet UILabel *_fieldFinancialScheme;
    IBOutlet UILabel *_fieldNote;
    IBOutlet UILabel *_fieldLCAmount;
    IBOutlet UILabel *_fieldAttachment;
    AppDelegate *_appDelegate;
    UITapGestureRecognizer *_attachmentTapRecognizer;
    UIDocumentInteractionController *_docInteractionController;
}

@end

@implementation VCCapexItemQoutationPageContent

#pragma mark -
#pragma mark === View Life Cycle Management ===
#pragma mark -

- (void)viewDidLoad {
    [super viewDidLoad];
    _docInteractionController = [[UIDocumentInteractionController alloc] init];
    _docInteractionController.delegate = self;
    _appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    _fieldIsPrimary.text = ([self.quotationItem propIsPrimary])?@"YES":@"NO";
    _fieldSupplier.text = [self.quotationItem propSupplierName];
    _fieldLC.text = [self.quotationItem propCurrencyThree];
    _fieldAmountInUSD.text = [_appDelegate.decimalFormat stringFromNumber:[NSNumber numberWithFloat:[self.quotationItem propAmountInUSD]]];
    _fieldFinancialScheme.text = [self.quotationItem propFinancingSchemenName];
    _fieldNote.text = [self.quotationItem propNote];
    _fieldLCAmount.text = [_appDelegate.decimalFormat stringFromNumber:[NSNumber numberWithFloat:[self.quotationItem propAmount]]];
    if ([[self.quotationItem propDocuments] count] > 0) {
        _fieldAttachment.text = [[[self.quotationItem propDocuments] objectAtIndex:0] objectForKey:@"DocName"];
        _fieldAttachment.textColor = [VelosiColors orangeVelosi];
        _fieldAttachment.font = [UIFont fontWithName:@"TrebuchetMS-Bold" size:13];
        _attachmentTapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onAttachmentClicked)];
        [_fieldAttachment addGestureRecognizer:_attachmentTapRecognizer];
    }else{
        _fieldAttachment.text = @"No Attachment";
        _fieldAttachment.textColor = [VelosiColors blackFont];
    }
}

#pragma mark -
#pragma mark === Private Method ===
#pragma mark -

- (void)onAttachmentClicked {
    [AppDelegate showGlogalHUDWithView:self.view];
    NSDictionary *doc = [[self.quotationItem propDocuments] objectAtIndex:0];
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
        NSString *urlString = [NSString stringWithFormat:@"%@?dID=%d&refID=%d&obTypeID=%d", NSLocalizedString(@"ImageViewerLink", @""), [[doc objectForKey:@"DocID"] intValue], [[doc objectForKey:@"RefID"] intValue], [[doc objectForKey:@"ObjectType"] intValue]];
        NSURL *url = [NSURL URLWithString:urlString];
        NSData *urlData = [NSData dataWithContentsOfURL:url];
        if(urlData){
            dispatch_async(dispatch_get_main_queue(), ^{
                [AppDelegate hideGlogalHUDWithView:self.view];
                NSString *filePath = [NSTemporaryDirectory() stringByAppendingPathComponent:[doc objectForKey:@"DocName"]];
                
                [urlData writeToFile:filePath atomically:YES];
                NSURL *filePathURL = [NSURL fileURLWithPath:filePath];
                _docInteractionController.URL = filePathURL;
                [_docInteractionController presentPreviewAnimated:YES];
            });
        }
    });
}

#pragma mark -
#pragma mark === UIDocument Interaction Controller Delegate ===
#pragma mark -


- (UIViewController *)documentInteractionControllerViewControllerForPreview:(UIDocumentInteractionController *)controller{
    return self;
}



@end
