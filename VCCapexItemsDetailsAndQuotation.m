//
//  VCCapexItemQoutation.m
//  Salt
//
//  Created by Rick Royd Aban on 1/12/16.
//  Copyright © 2016 Applus Velosi. All rights reserved.
//

#import "VCCapexItemsDetailsAndQuotation.h"
#import "CellCapexLineItemDetails.h"
#import "MBProgressHUD.h"
#import "CapexForApprovalLineItem.h"
#import "AppDelegate.h"
//#import "PageVCCapexItemQuotation.h"
#import "CellCapexForApprovalSalesQuotList.h"
#import "CapexLineItemQoutation.h"

@interface VCCapexItemsDetailsAndQuotation(){
    
    IBOutlet UITableView *_propLV;
//    PageVCCapexItemQuotation *_pageVC;
    NSMutableArray *_quotations;
}
@end

@implementation VCCapexItemsDetailsAndQuotation

#pragma mark -
#pragma mark === View Life Cycle Management ===
#pragma mark -

- (void)viewDidLoad{
    [super viewDidLoad];
    [self initSetup];
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    NSIndexPath *indexPath = [_propLV indexPathForCell:sender];
    CapexLineItemQoutation *quotItem = [self capexLineItemIndexPath:indexPath];
    ((VCCapexItemQoutationPageContent *)segue.destinationViewController).quotationItem = quotItem;
    ((VCCapexItemQoutationPageContent *)segue.destinationViewController).navItem.title = [NSString stringWithFormat:@"SALES QUOTE - 00%ld",indexPath.row + 1];
}

#pragma mark -
#pragma mark ==Private Method ===
#pragma mark -

-(CapexLineItemQoutation *) capexLineItemIndexPath:(NSIndexPath *)indexPath{
    CapexLineItemQoutation *quotItem = nil;
    if (indexPath)
        quotItem = [_quotations objectAtIndex:indexPath.row];
    return quotItem;
}


-(void)initSetup{
    _propLV.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    _propLV.estimatedRowHeight = 140;
    _propLV.rowHeight = UITableViewAutomaticDimension;
    _propLV.dataSource = self;
    _propLV.delegate = self;
    _quotations = [NSMutableArray array];
    [self fetchLineItemQuotation];
}

-(void)fetchLineItemQuotation {
    [AppDelegate showGlogalHUDWithView:self.view];
//    _propLV.scrollEnabled = NO;
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
        id result = [self.propAppDelegate.propGatewayOnline capexLineItemQoutationForLineItemID:self.capexLineItem.propCapexLineItemID];
        dispatch_async(dispatch_get_main_queue(), ^{
            [AppDelegate hideGlogalHUDWithView:self.view];
//            _propLV.scrollEnabled = YES;
            if([result isKindOfClass:[NSString class]]) //failure in retrieving
                [[[UIAlertView alloc] initWithTitle:@"" message:result delegate:nil cancelButtonTitle:@"Dismiss" otherButtonTitles:nil, nil] show];
            else{
                [_quotations removeAllObjects];
                [_quotations addObjectsFromArray:result];
            }
            [_propLV reloadData];
        });
    });
}

#pragma mark -
#pragma mark === UITableViewDataSource Delegate Methods ===
#pragma mark -

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    switch (section) {
        case 0:
            return 1;
            break;
        case 1:
            return _quotations.count;
            break;
        default:
            return 0;
            break;
    }
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 2;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *cellLineItemDetailsReuseID = @"cellItemDetail", *quotationCell = @"quotCell";
    if (indexPath.section == 0) {
        CellCapexLineItemDetails *cell = [tableView dequeueReusableCellWithIdentifier:cellLineItemDetailsReuseID];
        
        cell.propDesc.text =  [self.capexLineItem propDescription];
        cell.propCategory.text = [self.capexLineItem propCategoryName];
        cell.propQuantity.text = [NSString stringWithFormat:@"%d",[self.capexLineItem propQuantity]];
        cell.propCurrency.text = [self.capexLineItem propBaseCurrencySymbol];
        cell.propUnitCost.text =  [self.propAppDelegate.decimalFormat stringFromNumber:[NSNumber numberWithFloat:[self.capexLineItem propUnitCost]]];
        cell.propLC.text = [self.capexLineItem propCurrencySymbol];
        cell.propLocalCurrencyAmount.text = [self.propAppDelegate.decimalFormat stringFromNumber:[NSNumber numberWithFloat:[self.capexLineItem propAmount]]];
        cell.propUSDRate.text =  [self.propAppDelegate.decimalFormat stringFromNumber:[NSNumber numberWithFloat:[self.capexLineItem propUSDExchangeRate]]];
        cell.propAmountInUSD.text = [self.propAppDelegate.decimalFormat stringFromNumber:[NSNumber numberWithFloat:[self.capexLineItem propAmountInUSD]]];
        cell.propUsefulLife.text = [self.propAppDelegate.decimalFormat stringFromNumber:[NSNumber numberWithFloat:[self.capexLineItem propUsefulLife]]];
        
        cell.bounds = CGRectMake(0, 0, CGRectGetWidth(tableView.bounds), 99999);
        cell.contentView.bounds = cell.bounds;
        [cell layoutIfNeeded];
        cell.propDesc.preferredMaxLayoutWidth = CGRectGetWidth(cell.propDesc.frame);
//
        return cell;
    }else if(indexPath.section == 1){
        CellCapexForApprovalSalesQuotList *cell = [tableView dequeueReusableCellWithIdentifier:quotationCell];
        cell.propSalesQuot.text = [NSString stringWithFormat:@"SALES QUOTE - 00%ld",indexPath.row + 1];
//        //add page view controller to parent child hierarchy
//        [self fetchLineItemQuotation:^(NSMutableArray *quotations){
//            _pageVC.propQuotations = [NSMutableArray arrayWithArray:quotations];
//            NSLog(@"QUOTATION SIZE: %ld",quotations.count);
//            [self addChildViewController:_pageVC];
//            _pageVC.view.frame = cellQuot.contentView.bounds;
//            [_pageVC didMoveToParentViewController:self];
//            [cellQuot.contentView addSubview:_pageVC.view];
//        }];
        return cell;
    }
    return nil;
}

//-(void)tableView:(UITableView *)tableView didEndDisplayingCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
//    if (indexPath.section == 1 && indexPath.row == 0) {
//        //remove page view controller to parent child hierarchy
//        [_pageVC removeFromParentViewController];
//        [_pageVC.view removeFromSuperview];
//    }
//}


-(NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger) section {
    switch (section) {
        case 0:
            return @"ASSET DETAILS";
            break;
        case 1:
            return @"QUOTATIONS";
            break;
        default:
            return nil;
            break;
    }
}

@end
