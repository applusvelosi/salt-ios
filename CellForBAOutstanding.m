//
//  CellForBAOutstanding.m
//  Salt
//
//  Created by Rick Royd Aban on 10/03/2017.
//  Copyright © 2017 Applus Velosi. All rights reserved.
//

#import "CellForBAOutstanding.h"
#import "VelosiColors.h"

@implementation CellForBAOutstanding

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.propClaimNum = [[UILabel alloc] init];
        [self.propClaimNum setLineBreakMode:NSLineBreakByWordWrapping];
        [self.propClaimNum setNumberOfLines:0];
        [self.propClaimNum setTranslatesAutoresizingMaskIntoConstraints:NO];
        [self.contentView addSubview:self.propClaimNum];
        self.propAmount = [[UILabel alloc] init];
        
        [self.propAmount setLineBreakMode:NSLineBreakByWordWrapping];
        [self.propAmount setNumberOfLines:0];
        [self.propAmount setTranslatesAutoresizingMaskIntoConstraints:NO];
        [self.contentView addSubview:self.propAmount];
        self.propStatus = [[UILabel alloc] init];
        
        [self.propStatus setLineBreakMode:NSLineBreakByWordWrapping];
        [self.propStatus setNumberOfLines:0];
        [self.propStatus setTranslatesAutoresizingMaskIntoConstraints:NO];
        [self.contentView addSubview:self.propStatus];
        self.propStaff = [[UILabel alloc] init];
        
        [self.propStaff setLineBreakMode:NSLineBreakByWordWrapping];
        [self.propStaff setNumberOfLines:0];
        [self.propStaff setTranslatesAutoresizingMaskIntoConstraints:NO];
        [self.contentView addSubview:self.propStaff];
        
        NSDictionary *views = NSDictionaryOfVariableBindings(_propClaimNum, _propAmount, _propStatus, _propStaff);
        if ([reuseIdentifier isEqual:@"outstandingheadercell"]) {
            [self.contentView setBackgroundColor:[VelosiColors greyListViewHeaderBackground]];
            [self.propClaimNum setTextColor:[UIColor whiteColor]];
            [self.propClaimNum setFont:[UIFont boldSystemFontOfSize:12.0f]];
            [self.propClaimNum setText:@"Claim #"];
            [self.propAmount setTextColor:[UIColor whiteColor]];
            [self.propAmount setFont:[UIFont boldSystemFontOfSize:12.0f]];
            [self.propAmount setText:@"Amount"];
            [self.propStatus setTextColor:[UIColor whiteColor]];
            [self.propStatus setFont:[UIFont boldSystemFontOfSize:12.0f]];
            [self.propStatus setText:@"Status"];
            [self.propStaff setTextColor:[UIColor whiteColor]];
            [self.propStaff setFont:[UIFont boldSystemFontOfSize:12.0f]];
            [self.propStaff setText:@"Staff"];
            NSLayoutConstraint *claimNumCenterVertical = [NSLayoutConstraint constraintWithItem:self.propClaimNum
                                                                                      attribute:NSLayoutAttributeCenterY
                                                                                      relatedBy:NSLayoutRelationEqual
                                                                                         toItem:self.contentView
                                                                                      attribute:NSLayoutAttributeCenterY
                                                                                     multiplier:1
                                                                                       constant:0];
            [self.contentView addConstraint:claimNumCenterVertical];
            
            NSArray *constraints = [NSLayoutConstraint constraintsWithVisualFormat:@"V:|[_propAmount]|" options:NSLayoutFormatAlignAllCenterX metrics:nil views:views];
            [self.contentView addConstraints:constraints];

            constraints = [NSLayoutConstraint constraintsWithVisualFormat:@"V:|[_propStatus]|" options:NSLayoutFormatAlignAllCenterX metrics:nil views:views];
            [self.contentView addConstraints:constraints];
            
            constraints = [NSLayoutConstraint constraintsWithVisualFormat:@"V:|[_propStaff]|" options:NSLayoutFormatAlignAllCenterX metrics:nil views:views];
            [self.contentView addConstraints:constraints];

            constraints = [NSLayoutConstraint constraintsWithVisualFormat:@"H:|-10-[_propClaimNum(45)]-(30)-[_propAmount(>=_propClaimNum)]-(30)-[_propStatus(==_propClaimNum)]-(20)-[_propStaff(==_propClaimNum)]|" options:0 metrics:nil views:views];
            [self.contentView addConstraints:constraints];

        }
        
        if ([reuseIdentifier isEqual:@"outstandingcell"]) {
            [self.propClaimNum setTextColor:[UIColor blackColor]];
            [self.propClaimNum setFont:[UIFont boldSystemFontOfSize:10.0f]];
            [self.propAmount setTextColor:[UIColor blackColor]];
            [self.propAmount setFont:[UIFont boldSystemFontOfSize:10.0f]];
            [self.propStatus setTextColor:[UIColor blackColor]];
            [self.propStatus setFont:[UIFont boldSystemFontOfSize:10.0f]];
            [self.propStaff setTextColor:[UIColor blackColor]];
            [self.propStaff setFont:[UIFont boldSystemFontOfSize:10.0f]];
            NSLayoutConstraint *claimNumCenterVertical = [NSLayoutConstraint constraintWithItem:self.propClaimNum
                                                                          attribute:NSLayoutAttributeCenterY
                                                                          relatedBy:NSLayoutRelationEqual
                                                                             toItem:self.contentView
                                                                          attribute:NSLayoutAttributeCenterY
                                                                         multiplier:1.0
                                                                           constant:0.0];
            [self.contentView addConstraint:claimNumCenterVertical];
            NSArray *constraints = [NSLayoutConstraint constraintsWithVisualFormat:@"H:|-8-[_propClaimNum(67)]-[_propAmount(==_propClaimNum)]-[_propStatus(<=50)]-[_propStaff]|" options:0 metrics:nil views:views];
            [self.contentView addConstraints:constraints];
            
            constraints = [NSLayoutConstraint constraintsWithVisualFormat:@"V:|[_propStatus]|" options:NSLayoutFormatAlignAllCenterY metrics:nil views:views];
            [self.contentView addConstraints:constraints];
            
            constraints = [NSLayoutConstraint constraintsWithVisualFormat:@"V:|[_propStaff]|" options:NSLayoutFormatAlignAllCenterY metrics:nil views:views];
            [self.contentView addConstraints:constraints];
        }
    }
    return self;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    // Configure the view for the selected state
}

@end
