//
//  GiftHospitality.m
//  Salt
//
//  Created by Rick Royd Aban on 26/04/2017.
//  Copyright © 2017 Applus Velosi. All rights reserved.
//

#import "GiftHospitality.h"
#import "AppDelegate.h"

@interface GiftHospitality () {
    NSMutableDictionary *_giftHospitality;
}

@end

@implementation GiftHospitality

//Constructor for fetching claims from web service
- (instancetype)initWithJSONDictionary:(NSDictionary *)dictionary withOnlineGateway:(OnlineGateway *)onlineGateway {
    self = [super init];
    if (self) {
        _giftHospitality = [[NSMutableDictionary dictionaryWithDictionary:dictionary] mutableCopy];
        _propStatusId = [[_giftHospitality objectForKey:@"StatusID"] intValue];
        _propStaffName = [_giftHospitality objectForKey:@"RequestorName"];
        _propOfficeName = [_giftHospitality objectForKey:@"RequestorOfficeName"];
        _localCurrencySymbol = [_giftHospitality objectForKey:@"CurrencySymbol"];
        _propAmountInLc = [[_giftHospitality objectForKey:@"TotalAmount"] floatValue];
        _propStatusName = [_giftHospitality objectForKey:@"StatusName"];
        _propGiftHospitalityId = [[_giftHospitality objectForKey:@"GiftsHospitalityID"] intValue];
        _propCurrencyThree = [_giftHospitality objectForKey:@"CurrencySymbol"];
        _propAmountInEuro = [[_giftHospitality objectForKey:@"AmountInEUR"] floatValue];
        _propEuroRate = [[_giftHospitality objectForKey:@"ExchangeRate"] floatValue];
        _propIsReceivedGiven = [[_giftHospitality objectForKey:@"IsReceivedGiven"] boolValue];
        _propIsDonation = [[_giftHospitality objectForKey:@"IsDonation"] boolValue];
        _propIsPublicOfficial = [[_giftHospitality objectForKey:@"IsPublicOfficial"] boolValue];
        _propGiftType = [_giftHospitality objectForKey:@"GiftType"];
        _propDescription = [_giftHospitality objectForKey:@"Description"];
        _propReason = [_giftHospitality objectForKey:@"Reason"];
        _propApproverNote = [_giftHospitality objectForKey:@"ApproverNote"];
        _propReferenceNum = [_giftHospitality objectForKey:@"ReferenceNo"];
        _propCountryManagerName = [_giftHospitality objectForKey:@"ProcessedByCMName"];
        _propRegionalManagerName = [_giftHospitality objectForKey:@"ProcessedByRMName"];
        _propRequestorOfficeId = [[_giftHospitality objectForKey:@"RequestorOfficeID"] intValue];
        _propCeoName = [_giftHospitality objectForKey:@"ProcessedByCeoName"];
        _propCsrName = [_giftHospitality objectForKey:@"ApplusCsrName"];
        _propCountry = [_giftHospitality objectForKey:@"CountryName"];
        _propCompanyName = [_giftHospitality objectForKey:@"CompanyName"];
        _propCity = [_giftHospitality objectForKey:@"RecipientCity"];
        _propProcessedByCmId = [[_giftHospitality objectForKey:@"ProcessedByCMId"] intValue];
        _propProcessedByRmId = [[_giftHospitality objectForKey:@"ProcessedByRMId"] intValue];
        _propProcessedByCeoId = [[_giftHospitality objectForKey:@"ProcessedByCeoID"] intValue];
        _propApplusCsrId = [[_giftHospitality objectForKey:@"ApplusCsrId"] intValue];
        
        [_giftHospitality setObject:[onlineGateway deserializeJsonDateString:[_giftHospitality objectForKey:@"DateApprovedApplus"]] forKey:@"DateApprovedApplus"];
        [_giftHospitality setObject:[onlineGateway deserializeJsonDateString:[_giftHospitality objectForKey:@"DateCreated"]] forKey:@"DateCreated"];
        [_giftHospitality setObject:[onlineGateway deserializeJsonDateString:[_giftHospitality objectForKey:@"DateProcessedByCEO"]] forKey:@"DateProcessedByCEO"];
        [_giftHospitality setObject:[onlineGateway deserializeJsonDateString:[_giftHospitality objectForKey:@"DateProcessedByCountryManager"]] forKey:@"DateProcessedByCountryManager"];
        [_giftHospitality setObject:[onlineGateway deserializeJsonDateString:[_giftHospitality objectForKey:@"DateProcessedByRegionalManager"]] forKey:@"DateProcessedByRegionalManager"];
        [_giftHospitality setObject:[onlineGateway deserializeJsonDateString:[_giftHospitality objectForKey:@"DateReceivedGiven"]] forKey:@"DateReceivedGiven"];
        [_giftHospitality setObject:[onlineGateway deserializeJsonDateString:[_giftHospitality objectForKey:@"DateRejected"]] forKey:@"DateRejected"];
        [_giftHospitality setObject:[onlineGateway deserializeJsonDateString:[_giftHospitality objectForKey:@"DateSubmitted"]] forKey:@"DateSubmitted"];
    }
    return self;
}

//Constructor for copying from another gift or deserializing from offline gateway
- (instancetype)initWithDictionary:(NSMutableDictionary *)giftHeaderDictionary {
    if(self = [super init])
        _giftHospitality = [NSMutableDictionary dictionaryWithDictionary:giftHeaderDictionary];
    return self;
}

#pragma mark -
#pragma mark === Helper methods ===
#pragma mark -
-(NSString *)formatDateToBeViewed:(NSString *)dateFromStr appDelegate:(AppDelegate *)app{
    NSInteger janOneTwoThousand = 946684800000/1000; //Jan 1, 2000 GMT in epoch time
    NSDate *jan1 = [NSDate dateWithTimeIntervalSince1970:janOneTwoThousand];
    NSDate *dateFromString = [app.propDateFormatByProcessed dateFromString:dateFromStr];
    if ([dateFromString compare:jan1] == NSOrderedDescending || [dateFromString compare:jan1] == NSOrderedSame) //Date Before Jan 1, 2000 must not be viewed
        return [app.propDateFormatByProcessed stringFromDate:dateFromString];
    return @"";
}

- (NSString *)processedDateSubmitted:(AppDelegate *)appdelegate {
    return [self formatDateToBeViewed:[_giftHospitality objectForKey:@"DateSubmitted"] appDelegate:appdelegate];
}

- (NSString *)processedDateByCM:(AppDelegate *)appdelegate {
    return [self formatDateToBeViewed:[_giftHospitality objectForKey:@"DateProcessedByCountryManager"] appDelegate:appdelegate];
}

- (NSString *)processedDateByRM:(AppDelegate *)appdelegate {
    return [self formatDateToBeViewed:[_giftHospitality objectForKey:@"DateProcessedByRegionalManager"] appDelegate:appdelegate];
}

- (NSString *)processedDateByCEO:(AppDelegate *)appdelegate {
    return [self formatDateToBeViewed:[_giftHospitality objectForKey:@"DateProcessedByCEO"] appDelegate:appdelegate];
}

- (NSString *)processedDateByCSR:(AppDelegate *)appdelegate {
    return [self formatDateToBeViewed:[_giftHospitality objectForKey:@"DateApprovedApplus"] appDelegate:appdelegate];
}

- (NSString *)processedDateIsReceived:(AppDelegate *)appdelegate {
    return [self formatDateToBeViewed:[_giftHospitality objectForKey:@"DateReceivedGiven"] appDelegate:appdelegate];
}

- (NSString *)jSONForUpdatingGiftWithStatusId:(GiftStatusId)statusId keyForUpdatableDate:(NSString *)keyForUpdatableDate withApproverNote:(NSString *)note appDelegate:(AppDelegate *)appDelegate {
    
    NSMutableDictionary *tempDict = [NSMutableDictionary dictionaryWithDictionary:_giftHospitality];
    [tempDict setObject:@(statusId) forKey:@"StatusID"];
    [tempDict setObject:note forKey:@"ApproverNote"];
    
    [tempDict setObject:[appDelegate.propGatewayOnline convertToUnixDate:[appDelegate.propDateFormatByProcessed dateFromString:[tempDict objectForKey:@"DateApprovedApplus"]]] forKey:@"DateApprovedApplus"];
    [tempDict setObject:[appDelegate.propGatewayOnline convertToUnixDate:[appDelegate.propDateFormatByProcessed dateFromString:[tempDict objectForKey:@"DateCreated"]]] forKey:@"DateCreated"];
    [tempDict setObject:[appDelegate.propGatewayOnline convertToUnixDate:[appDelegate.propDateFormatByProcessed dateFromString:[tempDict objectForKey:@"DateProcessedByCEO"]]] forKey:@"DateProcessedByCEO"];
    [tempDict setObject:[appDelegate.propGatewayOnline convertToUnixDate:[appDelegate.propDateFormatByProcessed dateFromString:[tempDict objectForKey:@"DateProcessedByCountryManager"]]] forKey:@"DateProcessedByCountryManager"];
    [tempDict setObject:[appDelegate.propGatewayOnline convertToUnixDate:[appDelegate.propDateFormatByProcessed dateFromString:[tempDict objectForKey:@"DateProcessedByRegionalManager"]]] forKey:@"DateProcessedByRegionalManager"];
    [tempDict setObject:[appDelegate.propGatewayOnline convertToUnixDate:[appDelegate.propDateFormatByProcessed dateFromString:[tempDict objectForKey:@"DateReceivedGiven"]]] forKey:@"DateReceivedGiven"];
    [tempDict setObject:[appDelegate.propGatewayOnline convertToUnixDate:[appDelegate.propDateFormatByProcessed dateFromString:[tempDict objectForKey:@"DateRejected"]]] forKey:@"DateRejected"];
    [tempDict setObject:[appDelegate.propGatewayOnline convertToUnixDate:[appDelegate.propDateFormatByProcessed dateFromString:[tempDict objectForKey:@"DateSubmitted"]]] forKey:@"DateSubmitted"];
    switch (statusId) {
        case GiftStatusIdApprovedByCm:
            [tempDict setObject:@"Approved by CM" forKey:@"StatusName"];
            break;
        case GiftStatusIdApprovedByRm:
            [tempDict setObject:@"Approved by RM" forKey:@"StatusName"];
            break;
        case GiftStatusIdApprovedByCeo:
            [tempDict setObject:@"Approved by EVP" forKey:@"StatusName"];
            break;
        case GiftStatusIdRejectedByCm:
            [tempDict setObject:@"Rejected by CM" forKey:@"StatusName"];
            break;
        case GiftStatusIdRejectedByRm:
            [tempDict setObject:@"Rejected by RM" forKey:@"StatusName"];
            break;
        case GiftStatusIdRejectedByCeo:
            [tempDict setObject:@"Rejected by EVP" forKey:@"StatusName"];
            break;
        default:
            break;
    }
    if(![keyForUpdatableDate isEqualToString:@"NA"]) {
        if([keyForUpdatableDate isEqualToString:@"DateProcessedByRMAndCEO"]) {
            [tempDict setObject:[appDelegate.propGatewayOnline epochizeDate:[NSDate date]] forKey:@"DateProcessedByCEO"];
            [tempDict setObject:[appDelegate.propGatewayOnline epochizeDate:[NSDate date]] forKey:@"DateProcessedByRegionalManager"];
        }else {
            [tempDict setObject:[appDelegate.propGatewayOnline epochizeDate:[NSDate date]] forKey:keyForUpdatableDate];
        }
    }
    return [[NSString alloc] initWithData:[NSJSONSerialization dataWithJSONObject:tempDict options:0 error:nil] encoding:NSUTF8StringEncoding];
}

- (NSString *)jsonizeWithappDelegate:(AppDelegate *)appDelegate {
    NSMutableDictionary *tempDict = [NSMutableDictionary dictionaryWithDictionary:_giftHospitality];
    [tempDict setObject:[tempDict objectForKey:@"StatusID"] forKey:@"StatusID"];
    [tempDict setObject:[tempDict objectForKey:@"ApproverNote"] forKey:@"ApproverNote"];
    [tempDict setObject:[appDelegate.propGatewayOnline convertToUnixDate:[appDelegate.propDateFormatByProcessed dateFromString:[tempDict objectForKey:@"DateApprovedApplus"]]] forKey:@"DateApprovedApplus"];
    [tempDict setObject:[appDelegate.propGatewayOnline convertToUnixDate:[appDelegate.propDateFormatByProcessed dateFromString:[tempDict objectForKey:@"DateCreated"]]] forKey:@"DateCreated"];
    [tempDict setObject:[appDelegate.propGatewayOnline convertToUnixDate:[appDelegate.propDateFormatByProcessed dateFromString:[tempDict objectForKey:@"DateProcessedByCEO"]]] forKey:@"DateProcessedByCEO"];
    [tempDict setObject:[appDelegate.propGatewayOnline convertToUnixDate:[appDelegate.propDateFormatByProcessed dateFromString:[tempDict objectForKey:@"DateProcessedByCountryManager"]]] forKey:@"DateProcessedByCountryManager"];
    [tempDict setObject:[appDelegate.propGatewayOnline convertToUnixDate:[appDelegate.propDateFormatByProcessed dateFromString:[tempDict objectForKey:@"DateProcessedByRegionalManager"]]] forKey:@"DateProcessedByRegionalManager"];
    [tempDict setObject:[appDelegate.propGatewayOnline convertToUnixDate:[appDelegate.propDateFormatByProcessed dateFromString:[tempDict objectForKey:@"DateReceivedGiven"]]] forKey:@"DateReceivedGiven"];
    [tempDict setObject:[appDelegate.propGatewayOnline convertToUnixDate:[appDelegate.propDateFormatByProcessed dateFromString:[tempDict objectForKey:@"DateRejected"]]] forKey:@"DateRejected"];
    [tempDict setObject:[appDelegate.propGatewayOnline convertToUnixDate:[appDelegate.propDateFormatByProcessed dateFromString:[tempDict objectForKey:@"DateSubmitted"]]] forKey:@"DateSubmitted"];
    return [[NSString alloc] initWithData:[NSJSONSerialization dataWithJSONObject:tempDict options:0 error:nil] encoding:NSUTF8StringEncoding];
}

@end
