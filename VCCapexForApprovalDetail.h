//
//  CapexForApprovalDetail.h
//  Salt
//
//  Created by Rick Royd Aban on 10/6/15.
//  Copyright © 2015 Applus Velosi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CapexHeader.h"

@interface VCCapexForApprovalDetail : UITableViewController<UIAlertViewDelegate, UIDocumentInteractionControllerDelegate>

@property (assign, nonatomic) int capexHeaderID;

@end
