//
//  CellForBAOutstanding.h
//  Salt
//
//  Created by Rick Royd Aban on 10/03/2017.
//  Copyright © 2017 Applus Velosi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CellForBAOutstanding : UITableViewCell

@property (nonatomic, strong) UILabel *propClaimNum;
@property (nonatomic, strong) UILabel *propAmount;
@property (nonatomic, strong) UILabel *propStatus;
@property (nonatomic, strong) UILabel *propStaff;

@end
