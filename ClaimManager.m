//
//  ClaimHeaderSplitter.m
//  Salt
//
//  Created by Rick Royd Aban on 16/02/2017.
//  Copyright © 2017 Applus Velosi. All rights reserved.
//

#import "ClaimManager.h"
#import "ClaimHeader.h"
#import "ClaimItem.h"
#import <Objection/Objection.h>

@interface ClaimManager ()
@end

@implementation ClaimManager

-(void)claimViewController:(UIViewController*)claimVC
     willProcessClaimSplit:(ClaimHeader*)claimHeader
                  statusID:(int)statusID
   withKeyForUpdatableDate:(NSString *)keyForUpdatableDate
                statusDesc:(NSString *)description
             approversNote:(NSString *)approversNote
            processMessage:(NSString *)msg
               appDelegate:(AppDelegate*)appDelegate {
    
    int nextApproverId = [claimHeader propCMId];
    NSArray *claimLineItems = [appDelegate forApprovalClaimItemsForClaimID:[claimHeader propClaimID]];
    NSMutableArray *noReceiptApproveItems = [[NSMutableArray alloc] init];
    __block int splitClaimId, newClaimId, rejectedStatusCtr, returnStatusCtr, approveNoReceiptCtr, approveYesReceiptCtr, statusId;
    splitClaimId = rejectedStatusCtr = returnStatusCtr = approveNoReceiptCtr = approveYesReceiptCtr = 0;
    dispatch_group_t groupQueue = dispatch_group_create();
    
    [AppDelegate showGlogalHUDWithView:claimVC.view];
    if (claimHeader.propTypeID == CLAIMTYPEID_CLAIMS) {
        if ( [[appDelegate getStaff] getUserPosition] == USERPOSITION_CM || [[appDelegate getStaff] getUserPosition] == USERPOSITION_CFO ) {
            dispatch_group_enter(groupQueue);
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                for (ClaimItem *lineItem in claimLineItems) {
                    if (lineItem.statusID != CLAIMSTATUSID_RETURN) {
                        if (lineItem.statusID == CLAIMSTATUSID_APPROVEDBYCM) {
                            id result = [appDelegate.propGatewayOnline processClaimForApprovalWithClaimId:[claimHeader propClaimID] claimStatusId:statusID approversId:nextApproverId approversNote:approversNote];
                            dispatch_group_async(groupQueue, dispatch_get_main_queue(), ^{
                                if (result != nil) {
                                    [self viewAlertDialogWithTitle:@"Error" message:result viewController:claimVC];
                                }
                            });
                        } else {
                            rejectedStatusCtr++;
                        }
                    }else{
                        if (splitClaimId != 0) {
                            [lineItem setClaimID:splitClaimId];
                            NSString *jsonSplitItem = [[NSString alloc] initWithData:[NSJSONSerialization dataWithJSONObject:[lineItem savableDictionaryWithAppDelegate:appDelegate] options:0 error:nil] encoding:NSUTF8StringEncoding];
                            id resultSplitItem = [appDelegate.propGatewayOnline saveNewClaimItemJSON:jsonSplitItem oldClaimItemID:0 document:nil base64:nil];
                            dispatch_group_async(groupQueue, dispatch_get_main_queue(), ^{
                                if ([resultSplitItem isKindOfClass:[NSString class]])
                                    [self viewAlertDialogWithTitle:@"Error" message:resultSplitItem viewController:claimVC];
                            });
                        }else{
                            NSString *splittedJSONClaim = [claimHeader jsonClaimChildForApprovalWebServicesWithStatusID:statusID claimId:0 claimNumber:@"" parentClaimID:[claimHeader propClaimID] parentClaimNumber:[claimHeader propClaimNumber] claimCMId:nextApproverId statusDesc:description keyForUpdatableDate:keyForUpdatableDate approversNote:approversNote withAppDelegate:appDelegate];
                            id resultSplitClaim = [appDelegate.propGatewayOnline saveClaimWithNewClaimHeaderJSON:splittedJSONClaim oldClaimHeaderID:[claimHeader propClaimID] document:nil base64File:nil];
                            dispatch_group_async(groupQueue, dispatch_get_main_queue(), ^{
                                if ([resultSplitClaim isKindOfClass:[NSString class]]) {
                                    [self viewAlertDialogWithTitle:@"Error" message:resultSplitClaim viewController:claimVC];
                                }else{
                                    splitClaimId = [(NSNumber*)resultSplitClaim intValue];
                                    [lineItem setClaimID:splitClaimId];
                                    dispatch_group_async(groupQueue, dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                                        NSString *jsonResultItem = [[NSString alloc] initWithData:[NSJSONSerialization dataWithJSONObject:[lineItem savableDictionaryWithAppDelegate:appDelegate] options:0 error:nil] encoding:NSUTF8StringEncoding];
                                        id resultReturnItem = [appDelegate.propGatewayOnline saveNewClaimItemJSON:jsonResultItem oldClaimItemID:0 document:nil base64:nil];
                                        dispatch_group_async(groupQueue, dispatch_get_main_queue(), ^{
                                            if ([resultReturnItem isKindOfClass:[NSString class]])
                                                [self viewAlertDialogWithTitle:@"Error" message:resultReturnItem viewController:claimVC];
                                        });
                                    });
                                    
                                }
                            });
                        }
                    }
                }
                statusId = (rejectedStatusCtr == claimLineItems.count)?CLAIMSTATUSID_REJECTEDBYCM:statusID;
                dispatch_group_async(groupQueue, dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                    id result = [appDelegate.propGatewayOnline processClaimForApprovalWithClaimId:[claimHeader propClaimID] claimStatusId:statusId approversId:0 approversNote:approversNote];
                    dispatch_group_async(groupQueue, dispatch_get_main_queue(), ^{
                        if (result != nil)
                            [self viewAlertDialogWithTitle:@"Error" message:result viewController:claimVC];
                    });
                });
                dispatch_group_leave(groupQueue);
            });
        } else { //immediate manager claim processing
            dispatch_group_enter(groupQueue);
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                for (ClaimItem *lineItem in claimLineItems) {
                    id resultForex = [appDelegate.propGatewayOnline forexRateFrom:[lineItem foreignCurrency] to:@"USD"];
                    dispatch_group_async(groupQueue, dispatch_get_main_queue(), ^{
                        if (![resultForex isKindOfClass:[NSString class]]) {
                            float usdAmount = [resultForex floatValue] * [lineItem amountForeing];
                            NSString *amountInUSD = [appDelegate.decimalFormat stringFromNumber:[NSNumber numberWithFloat:usdAmount]];
                            if ([lineItem statusID] != CLAIMSTATUSID_RETURN) {
                                if ([amountInUSD compare:@"10" options:NSNumericSearch] > NSOrderedAscending || [amountInUSD compare:@"10" options:NSNumericSearch] == NSOrderedSame) {
                                    if ([lineItem attachmentFlag] == 1) { //with attachment
                                        if ([lineItem statusID] == CLAIMSTATUSID_APPROVEDBYAPPROVER) {
                                            approveYesReceiptCtr++;
                                        } else {
                                            rejectedStatusCtr++;
                                        }
                                    }else{
                                        if ([lineItem statusID] == CLAIMSTATUSID_APPROVEDBYAPPROVER) {
                                            approveNoReceiptCtr++;
                                            [noReceiptApproveItems addObject:lineItem];
                                        }else{
                                            rejectedStatusCtr++;
                                        }
                                    }
                                }else {
                                    if ([lineItem statusID] == CLAIMSTATUSID_REJECTEDBYAPPROVER || [lineItem statusID] == CLAIMSTATUSID_REJECTEDFORSALARYDEDUCTION)
                                        rejectedStatusCtr++;
                                }
                            }else{
                                returnStatusCtr++;
                            }
                        } else {
                            [self viewAlertDialogWithTitle:@"Error" message:resultForex viewController:claimVC];
                        }
                    });
                }
                
                if (approveNoReceiptCtr == claimLineItems.count ) {
                    id result = [appDelegate.propGatewayOnline processClaimForApprovalWithClaimId:[claimHeader propClaimID] claimStatusId:statusID approversId:nextApproverId approversNote:approversNote];
                    dispatch_group_async(groupQueue, dispatch_get_main_queue(), ^{
                        if (result != nil)
                            [self viewAlertDialogWithTitle:@"Error" message:result viewController:claimVC];
                    });
                    
                } else if (returnStatusCtr == claimLineItems.count) {
                    id result = [appDelegate.propGatewayOnline processClaimForApprovalWithClaimId:[claimHeader propClaimID] claimStatusId:CLAIMSTATUSID_OPEN approversId:0 approversNote:approversNote];
                    dispatch_group_async(groupQueue, dispatch_get_main_queue(), ^{
                        if (result != nil) {
                            [self viewAlertDialogWithTitle:@"Error" message:result viewController:claimVC];
                        } else {
                            for (ClaimItem *returnItem in claimLineItems) {
                                dispatch_group_async(groupQueue, dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                                    [returnItem setClaimStatus:CLAIMSTATUSID_OPEN];
                                    NSString *jsonStringItem = [[NSString alloc] initWithData:[NSJSONSerialization dataWithJSONObject:[returnItem savableDictionaryWithAppDelegate:appDelegate] options:0 error:nil] encoding:NSUTF8StringEncoding];
                                    id resultReturnItem = [appDelegate.propGatewayOnline saveNewClaimItemJSON:jsonStringItem oldClaimItemID:0 document:nil base64:nil];
                                    dispatch_group_async(groupQueue, dispatch_get_main_queue(), ^{
                                        if ([resultReturnItem isKindOfClass:[NSString class]]) {
                                            [self viewAlertDialogWithTitle:@"Error" message:resultReturnItem viewController:claimVC];
                                        }
                                    });
                                });
                            }
                        }
                    });
                } else {
                    if (returnStatusCtr != 0) {
                        dispatch_group_async(groupQueue, dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                            NSString *newJSONClaim = [claimHeader jsonClaimChildForApprovalWebServicesWithStatusID:CLAIMSTATUSID_OPEN claimId:0 claimNumber:@"" parentClaimID:[claimHeader propClaimID] parentClaimNumber:[claimHeader propClaimNumber] claimCMId:nextApproverId statusDesc:description keyForUpdatableDate:keyForUpdatableDate approversNote:approversNote withAppDelegate:appDelegate];
                            id result = [appDelegate.propGatewayOnline saveClaimWithNewClaimHeaderJSON:newJSONClaim oldClaimHeaderID:[claimHeader propClaimID] document:nil base64File:nil];
                            dispatch_group_async(groupQueue, dispatch_get_main_queue(), ^{
                                if (![result isKindOfClass:[NSString class]]) {
                                    newClaimId = [(NSNumber*)result intValue];
                                    for (ClaimItem *returnItem in claimLineItems) {
                                        if ([returnItem statusID] == CLAIMSTATUSID_RETURN) {
                                            [returnItem setClaimID:newClaimId];
                                            [returnItem setClaimStatus:CLAIMSTATUSID_OPEN];
                                            NSString *jsonResultItem = [[NSString alloc] initWithData:[NSJSONSerialization dataWithJSONObject:[returnItem savableDictionaryWithAppDelegate:appDelegate] options:0 error:nil] encoding:NSUTF8StringEncoding];
                                            dispatch_group_async(groupQueue, dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                                                id resultReturnItem = [appDelegate.propGatewayOnline saveNewClaimItemJSON:jsonResultItem oldClaimItemID:0 document:nil base64:nil];
                                                dispatch_group_async(groupQueue, dispatch_get_main_queue(), ^{
                                                    if ([resultReturnItem isKindOfClass:[NSString class]]) {
                                                        [self viewAlertDialogWithTitle:@"Error" message:resultReturnItem viewController:claimVC];
                                                    }
                                                });
                                            });
                                        }
                                    }
                                    dispatch_group_async(groupQueue, dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                                        id resultItems = [appDelegate.propGatewayOnline claimItemsForClaimID:[claimHeader propClaimID]];
                                        dispatch_group_async(groupQueue, dispatch_get_main_queue(), ^{
                                            if ([resultItems isKindOfClass:[NSString class]]) {
                                                [self viewAlertDialogWithTitle:@"Error" message:resultItems viewController:claimVC];
                                            }else{
                                                rejectedStatusCtr = 0;
                                                NSArray *parentLineItems = (NSMutableArray*)resultItems;
                                                for (ClaimItem *noReturnItem in parentLineItems) {
                                                    if (noReturnItem.statusID == CLAIMSTATUSID_REJECTEDBYAPPROVER || noReturnItem.statusID == CLAIMSTATUSID_REJECTEDFORSALARYDEDUCTION) {
                                                        rejectedStatusCtr++;
                                                    }
                                                }
                                                if (rejectedStatusCtr == parentLineItems.count) {
                                                    dispatch_group_async(groupQueue, dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                                                        id result = [appDelegate.propGatewayOnline processClaimForApprovalWithClaimId:[claimHeader propClaimID] claimStatusId:CLAIMSTATUSID_REJECTEDBYAPPROVER approversId:0 approversNote:approversNote];
                                                        dispatch_group_async(groupQueue, dispatch_get_main_queue(), ^{
                                                            if (result != nil) {
                                                                [self viewAlertDialogWithTitle:@"Error" message:result viewController:claimVC];
                                                            }
                                                        });
                                                    });
                                                } else if (approveNoReceiptCtr == parentLineItems.count || approveYesReceiptCtr == parentLineItems.count) {
                                                    dispatch_group_async(groupQueue, dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                                                        id result = [appDelegate.propGatewayOnline processClaimForApprovalWithClaimId:[claimHeader propClaimID] claimStatusId:statusID approversId:nextApproverId approversNote:approversNote];
                                                        dispatch_group_async(groupQueue, dispatch_get_main_queue(), ^{
                                                            if (result != nil) {
                                                                [self viewAlertDialogWithTitle:@"Error" message:result viewController:claimVC];
                                                            }
                                                        });
                                                    });
                                                } else {
                                                    if (approveNoReceiptCtr != 0) {
                                                        dispatch_group_async(groupQueue, dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                                                            NSString *splittedJSONClaim = [claimHeader jsonClaimChildForApprovalWebServicesWithStatusID:statusID claimId:0 claimNumber:@"" parentClaimID:[claimHeader propClaimID] parentClaimNumber:[claimHeader propClaimNumber] claimCMId:nextApproverId statusDesc:description keyForUpdatableDate:keyForUpdatableDate approversNote:approversNote withAppDelegate:appDelegate];
                                                            id resultSplitClaim = [appDelegate.propGatewayOnline saveClaimWithNewClaimHeaderJSON:splittedJSONClaim oldClaimHeaderID:[claimHeader propClaimID] document:nil base64File:nil];
                                                            dispatch_group_async(groupQueue, dispatch_get_main_queue(), ^{
                                                                if ([resultSplitClaim isKindOfClass:[NSString class]]) {
                                                                    [self viewAlertDialogWithTitle:@"Error" message:resultSplitClaim viewController:claimVC];
                                                                } else {
                                                                    splitClaimId = [(NSNumber*)resultSplitClaim intValue];
                                                                    for (ClaimItem *spitItem in noReceiptApproveItems) {
                                                                        [spitItem setClaimID:splitClaimId];
                                                                        
                                                                        dispatch_group_async(groupQueue, dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                                                                            NSString *jsonResultItem = [[NSString alloc] initWithData:[NSJSONSerialization dataWithJSONObject:[spitItem savableDictionaryWithAppDelegate:appDelegate] options:0 error:nil] encoding:NSUTF8StringEncoding];
                                                                            id resultReturnItem = [appDelegate.propGatewayOnline saveNewClaimItemJSON:jsonResultItem oldClaimItemID:0 document:nil base64:nil];
                                                                            dispatch_group_async(groupQueue, dispatch_get_main_queue(), ^{
                                                                                if ([resultReturnItem isKindOfClass:[NSString class]]) {
                                                                                    [self viewAlertDialogWithTitle:@"Error" message:resultReturnItem viewController:claimVC];
                                                                                }
                                                                            });
                                                                        });
                                                                    }
                                                                    statusId = (rejectedStatusCtr == parentLineItems.count)?CLAIMSTATUSID_REJECTEDBYAPPROVER:statusID;
                                                                    dispatch_group_async(groupQueue, dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                                                                        id result = [appDelegate.propGatewayOnline processClaimForApprovalWithClaimId:[claimHeader propClaimID] claimStatusId:statusId approversId:0 approversNote:approversNote];
                                                                        dispatch_group_async(groupQueue, dispatch_get_main_queue(), ^{
                                                                            if (result != nil) {
                                                                                [self viewAlertDialogWithTitle:@"Error" message:result viewController:claimVC];
                                                                            }
                                                                        });
                                                                    });
                                                                }
                                                            });
                                                        });
                                                    }else{
                                                        dispatch_group_async(groupQueue, dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                                                            id result = [appDelegate.propGatewayOnline processClaimForApprovalWithClaimId:[claimHeader propClaimID] claimStatusId:statusID approversId:0 approversNote:approversNote];
                                                            dispatch_group_async(groupQueue, dispatch_get_main_queue(), ^{
                                                                if (result != nil) {
                                                                    [self viewAlertDialogWithTitle:@"Error" message:result viewController:claimVC];
                                                                }
                                                            });
                                                        });
                                                    }
                                                }
                                            }
                                        });
                                    });
                                } else {
                                    [self viewAlertDialogWithTitle:@"" message:result viewController:claimVC];
                                }
                            });
                        });
                    } else { //
                        if (approveNoReceiptCtr != 0) {
                            dispatch_group_async(groupQueue, dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                                NSString *splittedJSONClaim = [claimHeader jsonClaimChildForApprovalWebServicesWithStatusID:statusID claimId:0 claimNumber:@"" parentClaimID:[claimHeader propClaimID] parentClaimNumber:[claimHeader propClaimNumber] claimCMId:nextApproverId statusDesc:description keyForUpdatableDate:keyForUpdatableDate approversNote:approversNote withAppDelegate:appDelegate];
                                id resultSplitClaim = [appDelegate.propGatewayOnline saveClaimWithNewClaimHeaderJSON:splittedJSONClaim oldClaimHeaderID:[claimHeader propClaimID] document:nil base64File:nil];
                                dispatch_group_async(groupQueue, dispatch_get_main_queue(), ^{
                                    if ([resultSplitClaim isKindOfClass:[NSString class]]) {
                                        [self viewAlertDialogWithTitle:@"Error" message:resultSplitClaim viewController:claimVC];
                                    } else {
                                        splitClaimId = [(NSNumber*)resultSplitClaim intValue];
                                        for (ClaimItem *spitItem in noReceiptApproveItems) {
                                            [spitItem setClaimID:splitClaimId];
                                            dispatch_group_async(groupQueue, dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                                                NSString *jsonResultItem = [[NSString alloc] initWithData:[NSJSONSerialization dataWithJSONObject:[spitItem savableDictionaryWithAppDelegate:appDelegate] options:0 error:nil] encoding:NSUTF8StringEncoding];
                                                id resultReturnItem = [appDelegate.propGatewayOnline saveNewClaimItemJSON:jsonResultItem oldClaimItemID:0 document:nil base64:nil];
                                                dispatch_group_async(groupQueue, dispatch_get_main_queue(), ^{
                                                    if ([resultReturnItem isKindOfClass:[NSString class]])
                                                        [self viewAlertDialogWithTitle:@"Error" message:resultReturnItem viewController:claimVC];
                                                });
                                            });
                                        }
                                        dispatch_group_async(groupQueue, dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                                            id resultItems = [appDelegate.propGatewayOnline claimItemsForClaimID:[claimHeader propClaimID]];
                                            dispatch_group_async(groupQueue, dispatch_get_main_queue(), ^{
                                                if ([resultItems isKindOfClass:[NSString class]]) {
                                                    [self viewAlertDialogWithTitle:@"Error" message:resultItems viewController:claimVC];
                                                }else{
                                                    NSArray *parentLineItems = (NSMutableArray*)resultItems;
                                                    statusId = (rejectedStatusCtr == parentLineItems.count)?CLAIMSTATUSID_REJECTEDBYAPPROVER:statusID;
                                                    dispatch_group_async(groupQueue, dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                                                        id result = [appDelegate.propGatewayOnline processClaimForApprovalWithClaimId:[claimHeader propClaimID] claimStatusId:statusId approversId:0 approversNote:approversNote];
                                                        dispatch_group_async(groupQueue, dispatch_get_main_queue(), ^{
                                                            if (result != nil)
                                                                [self viewAlertDialogWithTitle:@"Error" message:result viewController:claimVC];
                                                        });
                                                    });
                                                }
                                            });
                                        });
                                    }
                                });
                            });
                        } else {
                            statusId = (rejectedStatusCtr == claimLineItems.count)?CLAIMSTATUSID_REJECTEDBYAPPROVER:statusID;
                            dispatch_group_async(groupQueue, dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                                id result = [appDelegate.propGatewayOnline processClaimForApprovalWithClaimId:[claimHeader propClaimID] claimStatusId:statusId approversId:0 approversNote:approversNote];
                                dispatch_group_async(groupQueue, dispatch_get_main_queue(), ^{
                                    if (result != nil)
                                        [self viewAlertDialogWithTitle:@"Error" message:result viewController:claimVC];
                                });
                            });
                        }
                    }
                }
                dispatch_group_leave(groupQueue);
            });
        }
    } else if (claimHeader.propTypeID == CLAIMTYPEID_ADVANCES) {
        dispatch_group_enter(groupQueue);
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            for (ClaimItem *baItem in claimLineItems) {
                if (baItem.statusID == CLAIMSTATUSID_REJECTEDBYAPPROVER || baItem.statusID == CLAIMSTATUSID_REJECTEDBYCM) {
                    statusId = ( [[appDelegate getStaff] getUserPosition] != USERPOSITION_CM || [[appDelegate getStaff] getUserPosition] != USERPOSITION_CFO )?CLAIMSTATUSID_REJECTEDBYAPPROVER:CLAIMSTATUSID_APPROVEDBYCM;
                    id result = [appDelegate.propGatewayOnline processClaimForApprovalWithClaimId:[claimHeader propClaimID] claimStatusId:statusId approversId:0 approversNote:approversNote];
                    dispatch_group_async(groupQueue, dispatch_get_main_queue(), ^{
                        if (result != nil)
                            [self viewAlertDialogWithTitle:@"Error" message:result viewController:claimVC];
                    });
                } else {
                    int nextApprover = ( [[appDelegate getStaff] getUserPosition] == USERPOSITION_CM || [[appDelegate getStaff] getUserPosition] == USERPOSITION_CFO )?0:nextApproverId;
                    id result = [appDelegate.propGatewayOnline processClaimForApprovalWithClaimId:[claimHeader propClaimID] claimStatusId:statusID approversId:nextApprover approversNote:approversNote];
                    dispatch_group_async(groupQueue, dispatch_get_main_queue(), ^{
                        if (result != nil)
                            [self viewAlertDialogWithTitle:@"Error" message:result viewController:claimVC];
                    });

                }
            }
            dispatch_group_leave(groupQueue);
        });
    } else {  // FOR LIQUIDATION
        if ( [[appDelegate getStaff] getUserPosition] == USERPOSITION_CM || [[appDelegate getStaff] getUserPosition] == USERPOSITION_CFO ) {
            dispatch_group_enter(groupQueue);
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                int i, itemSize;
                i = ([[claimLineItems objectAtIndex:0] propCategoryTypeID] == [ClaimCategory TYPE_BUSINESSADVANCE])?1:0;
                for (; i < claimLineItems.count; i++) {
                    if ([[claimLineItems objectAtIndex:i] statusID] != CLAIMSTATUSID_RETURN) {
                        if ([[claimLineItems objectAtIndex:i] statusID] == CLAIMSTATUSID_REJECTEDBYCM || [[claimLineItems objectAtIndex:i] statusID] == CLAIMSTATUSID_REJECTEDFORSALARYDEDUCTION)
                            rejectedStatusCtr++;
                    } else {
                        dispatch_group_async(groupQueue, dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                            [[claimLineItems objectAtIndex:i] setClaimID:[claimHeader propClaimID]];
                            NSString *jsonStringItem = [[NSString alloc] initWithData:[NSJSONSerialization dataWithJSONObject:[[claimLineItems objectAtIndex:i] savableDictionaryWithAppDelegate:appDelegate] options:0 error:nil] encoding:NSUTF8StringEncoding];
                            id resultReturnItem = [appDelegate.propGatewayOnline saveNewClaimItemJSON:jsonStringItem oldClaimItemID:0 document:nil base64:nil];
                            dispatch_group_async(groupQueue, dispatch_get_main_queue(), ^{
                                if ([resultReturnItem isKindOfClass:[NSString class]]) {
                                    [self viewAlertDialogWithTitle:@"Error" message:resultReturnItem viewController:claimVC];
                                }
                            });
                        });
                    }
                }
                itemSize = ([[claimLineItems objectAtIndex:0] propCategoryTypeID] == [ClaimCategory TYPE_BUSINESSADVANCE]) ? (int)claimLineItems.count-1 : (int)claimLineItems.count;
                statusId = (rejectedStatusCtr == itemSize) ? CLAIMSTATUSID_REJECTEDBYCM : statusID;
                id result = [appDelegate.propGatewayOnline processClaimForApprovalWithClaimId:[claimHeader propClaimID] claimStatusId:statusID approversId:0 approversNote:approversNote];
                dispatch_group_async(groupQueue, dispatch_get_main_queue(), ^{
                    if (result != nil)
                        [self viewAlertDialogWithTitle:@"Error" message:result viewController:claimVC];
                });
                dispatch_group_leave(groupQueue);
            });
        }else { //immediate manager liquidation processing
            dispatch_group_enter(groupQueue);
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                for (int i = 1; i < claimLineItems.count; i++) {
                    id resultForex = [appDelegate.propGatewayOnline forexRateFrom:[[claimLineItems objectAtIndex:i] foreignCurrency] to:@"USD"];
                    dispatch_group_async(groupQueue, dispatch_get_main_queue(), ^{
                        if (![resultForex isKindOfClass:[NSString class]]) {
                            float usdAmount = [(NSNumber*)resultForex floatValue] * [[claimLineItems objectAtIndex:i] amountForeing];
                            NSString *amountInUSD = [appDelegate.decimalFormat stringFromNumber:[NSNumber numberWithFloat:usdAmount]];
                            if ([[claimLineItems objectAtIndex:i] statusID] != CLAIMSTATUSID_RETURN) {
                                if ([amountInUSD compare:@"10" options:NSNumericSearch] > NSOrderedAscending || [amountInUSD compare:@"10" options:NSNumericSearch] == NSOrderedSame) {
                                    if ([[claimLineItems objectAtIndex:i] attachmentFlag] == 1) { //with attachment
                                        if ([[claimLineItems objectAtIndex:i] statusID] == CLAIMSTATUSID_REJECTEDBYAPPROVER || [[claimLineItems objectAtIndex:i] statusID] == CLAIMSTATUSID_REJECTEDFORSALARYDEDUCTION)
                                            rejectedStatusCtr++;
                                    }else{
                                        if ([[claimLineItems objectAtIndex:i] statusID] == CLAIMSTATUSID_APPROVEDBYAPPROVER) {
                                            approveNoReceiptCtr++;
                                            [noReceiptApproveItems addObject:[claimLineItems objectAtIndex:i]];
                                        }else{
                                            rejectedStatusCtr++;
                                        }
                                    }
                                }else {
                                    if ([[claimLineItems objectAtIndex:i] statusID] == CLAIMSTATUSID_REJECTEDBYAPPROVER || [[claimLineItems objectAtIndex:i] statusID] == CLAIMSTATUSID_REJECTEDFORSALARYDEDUCTION)
                                        rejectedStatusCtr++;
                                }
                            }else{
                                returnStatusCtr++;
                            }
                        } else {
                            [self viewAlertDialogWithTitle:@"Error" message:resultForex viewController:claimVC];
                        }
                    });
                }
                
                if (approveNoReceiptCtr == claimLineItems.count-1 ) {
                    id result = [appDelegate.propGatewayOnline processClaimForApprovalWithClaimId:[claimHeader propClaimID] claimStatusId:statusID approversId:nextApproverId approversNote:approversNote];
                    dispatch_group_async(groupQueue, dispatch_get_main_queue(), ^{
                        if (result != nil)
                            [self viewAlertDialogWithTitle:@"Error" message:result viewController:claimVC];
                    });
                    
                } else if (returnStatusCtr == claimLineItems.count-1) {
                    id result = [appDelegate.propGatewayOnline processClaimForApprovalWithClaimId:[claimHeader propClaimID] claimStatusId:CLAIMSTATUSID_OPEN approversId:0 approversNote:approversNote];
                    dispatch_group_async(groupQueue, dispatch_get_main_queue(), ^{
                        if (result != nil) {
                            [self viewAlertDialogWithTitle:@"Error" message:result viewController:claimVC];
                        } else {
                            for (int i = 1; i < claimLineItems.count; i++) {
                                dispatch_group_async(groupQueue, dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                                    [[claimLineItems objectAtIndex:i] setClaimStatus:CLAIMSTATUSID_OPEN];
                                    NSString *jsonStringItem = [[NSString alloc] initWithData:[NSJSONSerialization dataWithJSONObject:[[claimLineItems objectAtIndex:i] savableDictionaryWithAppDelegate:appDelegate] options:0 error:nil] encoding:NSUTF8StringEncoding];
                                    id resultReturnItem = [appDelegate.propGatewayOnline saveNewClaimItemJSON:jsonStringItem oldClaimItemID:0 document:nil base64:nil];
                                    dispatch_group_async(groupQueue, dispatch_get_main_queue(), ^{
                                        if ([resultReturnItem isKindOfClass:[NSString class]]) {
                                            [self viewAlertDialogWithTitle:@"Error" message:resultReturnItem viewController:claimVC];
                                        }
                                    });
                                });
                            }
                        }
                    });
                } else { //with approved/rejected/return and greater or equal than 10$ line items
                    if (returnStatusCtr != 0) {
                        id result = [appDelegate.propGatewayOnline processClaimForApprovalWithClaimId:[claimHeader propClaimID] claimStatusId:CLAIMSTATUSID_OPEN approversId:0 approversNote:approversNote];
                        dispatch_group_async(groupQueue, dispatch_get_main_queue(), ^{
                            if (result != nil) {
                                [self viewAlertDialogWithTitle:@"Error" message:result viewController:claimVC];
                            } else {
                                for (int i = 1; i < claimLineItems.count; i++) {
                                    if ([[claimLineItems objectAtIndex:i] statusID] == CLAIMSTATUSID_RETURN) {
                                        dispatch_group_async(groupQueue, dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                                            [[claimLineItems objectAtIndex:i] setClaimStatus:CLAIMSTATUSID_OPEN];
                                            NSString *jsonStringItem = [[NSString alloc] initWithData:[NSJSONSerialization dataWithJSONObject:[[claimLineItems objectAtIndex:i] savableDictionaryWithAppDelegate:appDelegate] options:0 error:nil] encoding:NSUTF8StringEncoding];
                                            id resultReturnItem = [appDelegate.propGatewayOnline saveNewClaimItemJSON:jsonStringItem oldClaimItemID:0 document:nil base64:nil];
                                            dispatch_group_async(groupQueue, dispatch_get_main_queue(), ^{
                                                if ([resultReturnItem isKindOfClass:[NSString class]]) {
                                                    [self viewAlertDialogWithTitle:@"Error" message:resultReturnItem viewController:claimVC];
                                                }
                                            });
                                        });
                                    }
                                }
                            }
                        });
                    }
                    if (approveNoReceiptCtr != 0) {
                        dispatch_group_async(groupQueue, dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                            NSString *splittedJSONClaim = [claimHeader jsonClaimChildForApprovalWebServicesWithStatusID:statusID claimId:0 claimNumber:@"" parentClaimID:[claimHeader propClaimID] parentClaimNumber:[claimHeader propClaimNumber] claimCMId:nextApproverId statusDesc:description keyForUpdatableDate:keyForUpdatableDate approversNote:approversNote withAppDelegate:appDelegate];
                            id resultSplitClaim = [appDelegate.propGatewayOnline saveClaimWithNewClaimHeaderJSON:splittedJSONClaim oldClaimHeaderID:[claimHeader propClaimID] document:nil base64File:nil];
                            dispatch_group_async(groupQueue, dispatch_get_main_queue(), ^{
                                if ([resultSplitClaim isKindOfClass:[NSString class]]) {
                                    [self viewAlertDialogWithTitle:@"Error" message:resultSplitClaim viewController:claimVC];
                                } else {
                                    splitClaimId = [(NSNumber*)resultSplitClaim intValue];
                                    for (ClaimItem *spitItem in noReceiptApproveItems) {
                                        [spitItem setClaimID:splitClaimId];
                                        dispatch_group_async(groupQueue, dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                                            NSString *jsonResultItem = [[NSString alloc] initWithData:[NSJSONSerialization dataWithJSONObject:[spitItem savableDictionaryWithAppDelegate:appDelegate] options:0 error:nil] encoding:NSUTF8StringEncoding];
                                            id resultReturnItem = [appDelegate.propGatewayOnline saveNewClaimItemJSON:jsonResultItem oldClaimItemID:0 document:nil base64:nil];
                                            dispatch_group_async(groupQueue, dispatch_get_main_queue(), ^{
                                                if ([resultReturnItem isKindOfClass:[NSString class]])
                                                    [self viewAlertDialogWithTitle:@"Error" message:resultReturnItem viewController:claimVC];
                                            });
                                        });
                                    }
                                    dispatch_group_async(groupQueue, dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                                        id resultItems = [appDelegate.propGatewayOnline claimItemsForClaimID:[claimHeader propClaimID]];
                                        dispatch_group_async(groupQueue, dispatch_get_main_queue(), ^{
                                            if ([resultItems isKindOfClass:[NSString class]]) {
                                                [self viewAlertDialogWithTitle:@"Error" message:resultItems viewController:claimVC];
                                            }else{
                                                NSArray *parentLineItems = (NSMutableArray*)resultItems;
                                                statusId = (rejectedStatusCtr == parentLineItems.count)?CLAIMSTATUSID_REJECTEDBYAPPROVER:statusID;
                                                dispatch_group_async(groupQueue, dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                                                    id result = [appDelegate.propGatewayOnline processClaimForApprovalWithClaimId:[claimHeader propClaimID] claimStatusId:statusId approversId:0 approversNote:approversNote];
                                                    dispatch_group_async(groupQueue, dispatch_get_main_queue(), ^{
                                                        if (result != nil)
                                                            [self viewAlertDialogWithTitle:@"Error" message:result viewController:claimVC];
                                                    });
                                                });
                                            }
                                        });
                                    });
                                }
                            });
                        });
                        if (returnStatusCtr == 0) {
                            dispatch_group_async(groupQueue, dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                                id resultItems = [appDelegate.propGatewayOnline claimItemsForClaimID:[claimHeader propClaimID]];
                                dispatch_group_async(groupQueue, dispatch_get_main_queue(), ^{
                                    if ([resultItems isKindOfClass:[NSString class]]) {
                                        [self viewAlertDialogWithTitle:@"Error" message:resultItems viewController:claimVC];
                                    }else{
                                        NSArray *parentLineItems = (NSMutableArray*)resultItems;
                                        statusId = (rejectedStatusCtr == parentLineItems.count)?CLAIMSTATUSID_REJECTEDBYAPPROVER:statusID;
                                    }
                                });
                            });
                        }else{
                            statusId = CLAIMSTATUSID_OPEN;
                        }
                        dispatch_group_async(groupQueue, dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                            id result = [appDelegate.propGatewayOnline processClaimForApprovalWithClaimId:[claimHeader propClaimID] claimStatusId:statusId approversId:0 approversNote:approversNote];
                            dispatch_group_async(groupQueue, dispatch_get_main_queue(), ^{
                                if (result != nil)
                                    [self viewAlertDialogWithTitle:@"Error" message:result viewController:claimVC];
                            });
                        });
                    } else {
                        if (returnStatusCtr == 0) {
                            dispatch_group_async(groupQueue, dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                                id resultItems = [appDelegate.propGatewayOnline claimItemsForClaimID:[claimHeader propClaimID]];
                                dispatch_group_async(groupQueue, dispatch_get_main_queue(), ^{
                                    if ([resultItems isKindOfClass:[NSString class]]) {
                                        [self viewAlertDialogWithTitle:@"Error" message:resultItems viewController:claimVC];
                                    }else{
                                        NSArray *parentLineItems = (NSMutableArray*)resultItems;
                                        statusId = (rejectedStatusCtr == parentLineItems.count)?CLAIMSTATUSID_REJECTEDBYAPPROVER:statusID;
                                    }
                                });
                            });
                        }else{
                            statusId = CLAIMSTATUSID_OPEN;
                        }
                        dispatch_group_async(groupQueue, dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                            id result = [appDelegate.propGatewayOnline processClaimForApprovalWithClaimId:[claimHeader propClaimID] claimStatusId:statusId approversId:0 approversNote:approversNote];
                            dispatch_group_async(groupQueue, dispatch_get_main_queue(), ^{
                                if (result != nil)
                                    [self viewAlertDialogWithTitle:@"Error" message:result viewController:claimVC];
                            });
                        });
                    }
                }
                dispatch_group_leave(groupQueue);
            }); //end of async
        }
    }
    dispatch_group_notify(groupQueue, dispatch_get_main_queue(), ^{
        [self viewAlertDialogWithTitle:@"" message:msg viewController:claimVC];
    });
}

- (void)viewAlertDialogWithTitle:(NSString*)title message:(NSString*)msg viewController:(UIViewController*)claimVC {
    [AppDelegate hideGlogalHUDWithView:claimVC.view];
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:title message:msg preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"Dismiss" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
        [claimVC.navigationController popViewControllerAnimated:YES];
    }];
    [alertController addAction:cancelAction];
    [claimVC presentViewController:alertController animated:YES completion:nil];
}

@end
