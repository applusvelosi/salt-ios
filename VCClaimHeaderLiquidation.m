//
//  VCClaimHeaderLiquidation.m
//  Salt
//
//  Created by Rick Royd Aban on 2015/08/12.
//  Copyright (c) 2015年 Applus Velosi. All rights reserved.
//

#import "VCClaimHeaderLiquidation.h"
#import "MBProgressHUD.h"
#import "VCCostCenterList.h"
#import "VCLiquidationBARefs.h"

@interface VCClaimHeaderLiquidation (){
    
    IBOutlet UITextField *_propFieldStaff;
    IBOutlet UITextField *_propFieldOffice;
    IBOutlet UITableViewCell *_propCellCostCenter;
    IBOutlet UITextField *_propFieldApprover;
    IBOutlet UITableViewCell *_propCellRefBA;

    BusinessAdvance *_refBA;
    NSString *_oldLiquidationJSON;
    Liquidation *_newLiquidation;
}

@end

@implementation VCClaimHeaderLiquidation

- (void)viewDidLoad {
    [super viewDidLoad];

    if(_propLiquidation != nil){ //use the data in the claim object
        _propFieldStaff.text = [_propLiquidation propStaffName];
        _propFieldOffice.text = [_propLiquidation propOfficeName];
        _propCellCostCenter.detailTextLabel.text = [_propLiquidation propCostCenterName];
        _propFieldApprover.text = [_propLiquidation propApproverName];
        [self updateCostCenter:[[CostCenter alloc] initWithID:_propLiquidation.propCostCenterID name:_propLiquidation.propCostCenterName]];
    }else{ //use the data in the user object from appdelegate
        _propFieldStaff.text = [[self.propAppDelegate getStaff] propFullName];
        _propFieldOffice.text = [[self.propAppDelegate office] officeName];
        _propFieldApprover.text = [[self.propAppDelegate getStaff] propExpenseApproverName];
        CostCenter *costCenter = [[CostCenter alloc] initWithID:self.propAppDelegate.getStaff.propCostCenterID name:self.propAppDelegate.getStaff.propCostCenterName];
        self.propCostCenter = costCenter;
        [self updateCostCenter:costCenter];
    }
    
    _propRefBAs = [NSMutableArray array];
    for(ClaimHeader *claimHeader in [self.propAppDelegate myClaims]){
        if(claimHeader.propTypeID==CLAIMTYPEID_ADVANCES && claimHeader.propStatusID==CLAIMSTATUSID_PAID)
            [_propRefBAs addObject:claimHeader];
    }
}

- (IBAction)save:(id)sender {
    if(_refBA != nil){
        _oldLiquidationJSON = (_propLiquidation != nil)?[_propLiquidation jsonForWebServices]:[ClaimHeader jsonFromNewEmptyClaimHeader];
        _newLiquidation = [[Liquidation alloc] initWithCostCenterID:[[super propCostCenter] propCostCenterID] costCenterName:[[super propCostCenter] propCostCenterName] claimTypeID:CLAIMTYPEID_LIQUIDATION isPaidByCC:NO baIDCharged:[_refBA propClaimID] bacNumber:[_refBA propClaimNumber] appDelegate:self.propAppDelegate
                           ];
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
            NSString *result = nil; //[self.propAppDelegate.propGatewayOnline saveClaimWithNewClaimHeaderJSON:[_newLiquidation jsonForWebServices] oldClaimHeaderJSON:_oldLiquidationJSON];
            
            dispatch_async(dispatch_get_main_queue(), ^{
                [MBProgressHUD hideAllHUDsForView:self.view animated:YES];

                
                [[[UIAlertView alloc] initWithTitle:@"" message:([result isEqualToString:@"OK"])?@"Liquidation Saved Successfully":result delegate:self cancelButtonTitle:@"Dismiss" otherButtonTitles:nil, nil] show];
            });
        });
    }else
        [[[UIAlertView alloc] initWithTitle:@"" message:@"Please choose a Business Advance for reference" delegate:nil cancelButtonTitle:@"Cancel" otherButtonTitles:nil, nil] show];
}

- (void)updateCostCenter:(CostCenter *)costCenter{
    _propCellCostCenter.detailTextLabel.text = [costCenter propCostCenterName];
    [_propLiquidation updateCostCenter:costCenter];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    if([[segue destinationViewController] isKindOfClass:[VCCostCenterList class]])
        ((VCCostCenterList *)segue.destinationViewController).vcClaimHeader = self;
    else
        ((VCLiquidationBARefs *)segue.destinationViewController).vcClaimHeaderLiq = self;
}

- (void)updateRefBA:(BusinessAdvance *)ba{
    _refBA = ba;
    _propCellRefBA.textLabel.text = ba.propClaimNumber;
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    [self.navigationController dismissViewControllerAnimated:YES completion:nil];
}


@end
