//
//  VCClaimTypeSelection.m
//  Salt
//
//  Created by Rick Royd Aban on 2/15/16.
//  Copyright © 2016 Applus Velosi. All rights reserved.
//

#import "VCClaimTypeSelection.h"

@interface VCClaimTypeSelection ()

@end

@implementation VCClaimTypeSelection

- (void)viewDidLoad {
    [super viewDidLoad];
}

- (IBAction)cancel:(id)sender {
    [self.navigationController dismissViewControllerAnimated:YES completion:nil];
}


@end
