//
//  CellClaimItemAttendee.h
//  Salt
//
//  Created by Rick Royd Aban on 2/10/16.
//  Copyright © 2016 Applus Velosi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CellClaimItemDetailAttendee : UITableViewCell

@property (strong, nonatomic) IBOutlet UILabel *propLabelName;
@property (strong, nonatomic) IBOutlet UILabel *propLabelJobTitle;
@property (strong, nonatomic) IBOutlet UILabel *propLabelNotes;
@end
