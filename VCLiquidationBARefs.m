//
//  VCLiquidationBARefs.m
//  Salt
//
//  Created by Rick Royd Aban on 2/3/16.
//  Copyright © 2016 Applus Velosi. All rights reserved.
//

#import "VCLiquidationBARefs.h"
#import "BusinessAdvance.h"

@interface VCLiquidationBARefs(){
    
    IBOutlet UITableView *_propLV;
}
@end

@implementation VCLiquidationBARefs

- (void)viewDidLoad{
    [super viewDidLoad];
    
    _propLV.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    _propLV.delegate = self;
    _propLV.dataSource = self;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    cell.textLabel.text = ((BusinessAdvance *)[_vcClaimHeaderLiq.propRefBAs objectAtIndex:indexPath.row]).propClaimNumber;
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [_vcClaimHeaderLiq updateRefBA:[_vcClaimHeaderLiq.propRefBAs objectAtIndex:indexPath.row]];
    [self.navigationController popViewControllerAnimated:YES];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return _vcClaimHeaderLiq.propRefBAs.count;
}

@end
