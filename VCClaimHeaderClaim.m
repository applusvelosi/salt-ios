//
//  VCClaimHeaderClaim.m
//  Salt
//
//  Created by Rick Royd Aban on 2015/08/12.
//  Copyright (c) 2015年 Applus Velosi. All rights reserved.
//

#import "VCClaimHeaderClaim.h"
#import "VCCostCenterList.h"
#import "MBProgressHUD.h"

@interface VCClaimHeaderClaim (){
    
    IBOutlet UITextField *_propFieldStaff;
    IBOutlet UITextField *_propFieldOffice;
    IBOutlet UITableViewCell *_propCellCostCenter;
    IBOutlet UITextField *_propFieldApprover;
    IBOutlet UISwitch *_propSwitchIsPaidByCC;
    
    NSString *_oldClaimJSON;
    Claim *_newClaim;
}

@end

@implementation VCClaimHeaderClaim

- (void)viewDidLoad {
    [super viewDidLoad];
    if(_propClaim != nil){ //use the data in the claim object
        _propFieldStaff.text = [_propClaim propStaffName];
        _propFieldOffice.text = [_propClaim propOfficeName];
        _propCellCostCenter.detailTextLabel.text = [_propClaim propCostCenterName];
        _propFieldApprover.text = [_propClaim propApproverName];
        _propSwitchIsPaidByCC.on = [_propClaim isPaidByCompanyCard];
        [self updateCostCenter:[[CostCenter alloc] initWithID:_propClaim.propCostCenterID name:_propClaim.propCostCenterName]];
    }else{ //use the data in the user object from appdelegate`
        _propFieldStaff.text = [[self.propAppDelegate getStaff] propFullName];
        _propFieldOffice.text = [[self.propAppDelegate office] officeName];
        _propFieldApprover.text = [[self.propAppDelegate getStaff] propExpenseApproverName];
        CostCenter *costCenter = [[CostCenter alloc] initWithID:self.propAppDelegate.getStaff.propCostCenterID name:self.propAppDelegate.getStaff.propCostCenterName];
        self.propCostCenter = costCenter;
        [self updateCostCenter:costCenter];
    }
}

- (IBAction)save:(id)sender {
    _oldClaimJSON = (_propClaim != nil)?[_propClaim jsonForWebServices]:[ClaimHeader jsonFromNewEmptyClaimHeader];
    _newClaim = [[ClaimPaidByCC alloc] initWithCostCenterID:[[super propCostCenter] propCostCenterID] costCenterName:[[super propCostCenter] propCostCenterName] claimTypeID:CLAIMTYPEID_CLAIMS isPaidByCC:(_propSwitchIsPaidByCC.isOn) baIDCharged:0 bacNumber:@"" appDelegate:self.propAppDelegate];
    [AppDelegate showGlogalHUDWithView:self.view];
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
        NSString *result = nil; //[self.propAppDelegate.propGatewayOnline saveClaimWithNewClaimHeaderJSON:[_newClaim jsonForWebServices] oldClaimHeaderJSON:_oldClaimJSON];
        dispatch_async(dispatch_get_main_queue(), ^{
            [AppDelegate hideGlogalHUDWithView:self.view];
            [[[UIAlertView alloc] initWithTitle:@"" message:([result isEqualToString:@"OK"])?@"Claim Saved Successfully":result delegate:self cancelButtonTitle:@"Dismiss" otherButtonTitles:nil, nil] show];   
        });
    });
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    if([segue.destinationViewController isKindOfClass:[VCCostCenterList class]])
        ((VCCostCenterList *)segue.destinationViewController).vcClaimHeader = self;
}

- (void)updateCostCenter:(CostCenter *)costCenter{
    _propCellCostCenter.detailTextLabel.text = [costCenter propCostCenterName];
    [_propClaim updateCostCenter:costCenter];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    [self.navigationController dismissViewControllerAnimated:YES completion:nil];
}

@end
