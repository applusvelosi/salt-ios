//
//  VCClaimsForApprovalList.h
//  Salt
//
//  Created by Rick Royd Aban on 02/05/2017.
//  Copyright © 2017 Applus Velosi. All rights reserved.
//

#import "VCPage.h"
#import "LoaderDelegate.h"

@interface VCClaimsForApprovalList : VCPage

@property (nonatomic, weak) id<LoaderDelegate> delegate;

-(void) reloadClaimsForApprovalList;

@end
