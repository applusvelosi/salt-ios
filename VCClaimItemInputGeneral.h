//
//  VCClaimItemInputGeneral.h
//  Salt
//
//  Created by Rick Royd Aban on 2015/09/10.
//  Copyright (c) 2015年 Applus Velosi. All rights reserved.
//

#import "VCDetail.h"
#import "ClaimItem.h"
#import "VelosiPickerRowSelectionDelegate.h"

@interface VCClaimItemInputGeneral : VCDetail<VelosiPickerRowSelectionDelegate, UIAlertViewDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate, UIDocumentInteractionControllerDelegate>

@property (strong, nonatomic) ClaimHeader *propClaimHeader;
@property (strong, nonatomic) ClaimItem *propClaimItem;
@property (strong, nonatomic) ClaimCategory *propClaimCategory;

@end
