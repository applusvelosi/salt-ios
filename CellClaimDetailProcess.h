//
//  CellClaimPaidByCCProcessDetail.h
//  Salt
//
//  Created by Rick Royd Aban on 2015/07/27.
//  Copyright (c) 2015年 Applus Velosi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CellClaimDetailProcess : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *propFieldProcessedByApprover;
@property (weak, nonatomic) IBOutlet UILabel *propFieldProcessedByAccount;
@property (weak, nonatomic) IBOutlet UILabel *propFieldDatePaid;

@end
