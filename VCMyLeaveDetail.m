//
//  VCMyLeaveDetail.m
//  Salt iOS
//
//  Created by Rick Royd Aban on 5/28/15.
//  Copyright (c) 2015 Rick Royd Aban. All rights reserved.
//

#define HEADERSPINNERTYPE @"Select Leave Type"
#define HEADERSPINNERNUMDDAYS @"Select Days"
#define HALFDAY_AM @"0.5 AM"
#define HALFDAY_PM @"0.5 PM"
#define ONEDAYLEAVE @"1.0 Day"

#import "VCMyLeaveDetail.h"
#import "VelosiDatePicker.h"
#import "MBProgressHUD.h"
#import "VelosiCustomPicker.h"
#import "VCLeaveInput.h"
#import "MBProgressHUD.h"

typedef NS_ENUM(NSUInteger, WeekDay) {
    SUN = 1,
    MON,
    TUE,
    WED,
    THU,
    FRI,
    SAT,
};

@interface VCMyLeaveDetail (){
    IBOutlet UITextField *_propFieldLeaveType;
    IBOutlet UITextField *_propFieldLeaveStatus;
    IBOutlet UITextField *_propFieldStaff;
    IBOutlet UITextField *_propFieldDateFrom;
    IBOutlet UITextField *_propFieldDateTo;
    IBOutlet UITextField *_propFieldDays;
    IBOutlet UITextField *_propFieldWorkingDays;
    IBOutlet UITextField *_propRemainingLeaveCredits;
    IBOutlet UITextView *_propTextViewNotes;
    IBOutlet UIButton *_propButtonFollowup;

    NSMutableDictionary *_daysMap;
    NSMutableArray *_days;
}
    
@end

@implementation VCMyLeaveDetail

- (void)viewDidLoad {
    [super viewDidLoad];
    [AppDelegate showGlogalHUDWithView:self.view];
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
        id holidayResult = [self.propAppDelegate.propGatewayOnline localHolidays];
        dispatch_async(dispatch_get_main_queue(), ^{
            _days = [NSMutableArray array];
            _daysMap = [NSMutableDictionary dictionary];
            [_days addObject:HALFDAY_AM];
            [_daysMap setObject:@"0.1" forKey:HALFDAY_AM];
            [_days addObject:HALFDAY_PM];
            [_daysMap setObject:@"0.2" forKey:HALFDAY_PM];
            [_days addObject:ONEDAYLEAVE];
            [_daysMap setObject:@"1.0" forKey:ONEDAYLEAVE];
            for(float i=2; i<30; i++) {
                NSString *dayValue = [NSString stringWithFormat:@"%.1f",i];
                NSString *dayString = [NSString stringWithFormat:@"%@ Days",dayValue];
                [_days addObject:dayString];
                [_daysMap setObject:dayValue forKey:dayString];
            }
            
            
            if([holidayResult isKindOfClass:[NSString class]])
                [[[UIAlertView alloc] initWithTitle:@"" message:holidayResult delegate:nil cancelButtonTitle:@"Dismiss" otherButtonTitles:nil, nil] show];
            else{
                if(_propLeave != nil) {
                    self.navigationItem.title = @"Leave Details";
                    _propFieldLeaveType.text = [_propLeave propTypeDescription];
                    _propFieldLeaveStatus.text = [_propLeave propStatusDescription];
                    _propFieldStaff.text = [_propLeave propStaffName];
                    _propFieldDateFrom.text = [_propLeave propStartDate];
                    _propFieldDateTo.text = [_propLeave propEndDate];
                    _propFieldDays.text = [_daysMap allKeysForObject:[NSString stringWithFormat:@"%1.1f",[_propLeave propDays]]][0];
                    
                    NSDate *startDate = [self.propAppDelegate.propFormatVelosiDate dateFromString:[_propLeave propStartDate]];
                    NSDate *endDate = [self.propAppDelegate.propFormatVelosiDate dateFromString:[_propLeave propEndDate]];
                    NSCalendar *cal = [NSCalendar currentCalendar];
                    cal.timeZone = [NSTimeZone systemTimeZone];
                    NSDateComponents *timeZoneComponents = [[NSDateComponents alloc] init];
                    [timeZoneComponents setHour:8];
                    [timeZoneComponents setMinute:0];
                    [timeZoneComponents setSecond:0];
                    NSDate *startLeave = [cal dateByAddingComponents:timeZoneComponents toDate:startDate options:NSCalendarMatchStrictly];
                    NSDate *endLeave = [cal dateByAddingComponents:timeZoneComponents toDate:endDate options:NSCalendarMatchStrictly];
                    
                    NSCalendar *currentCal = [NSCalendar currentCalendar];
                    NSCalendarUnit calUnits = (NSCalendarUnitDay | NSCalendarUnitWeekday | NSCalendarUnitMonth | NSCalendarUnitYear);
                    NSDateComponents *comparCal = [currentCal components:calUnits fromDate:startLeave];
                    comparCal.calendar = currentCal;
                    NSMutableArray *holidaysIncluded = [[NSMutableArray alloc] init];
                    float incrementaDays = ([[_propFieldDays.text componentsSeparatedByString:@" "][0] isEqualToString:@"0.5"]) ? 0.5f : 1.0f;
                    int holidayCount = 0;
                    for(LocalHoliday *holiday in holidayResult){
                        NSDate *holidayDate = [self.propAppDelegate.propFormatVelosiDate dateFromString:[holiday propDate]];
                        if([holidayDate compare:startDate]==NSOrderedSame || [holidayDate compare:endDate]==NSOrderedSame || ([holidayDate compare:startDate]==NSOrderedDescending && [holidayDate compare:endLeave]==NSOrderedAscending))
                            [holidaysIncluded addObject:[self.propAppDelegate.propFormatVelosiDate stringForObjectValue:holidayDate]];
                    }
                    NSCalendar *currCalendar = [NSCalendar currentCalendar];
                    currCalendar.timeZone = [NSTimeZone systemTimeZone];
                    NSCalendarUnit calendarUnits = (NSCalendarUnitDay | NSCalendarUnitWeekday | NSCalendarUnitMonth | NSCalendarUnitYear);
                    NSDateComponents *comparableWorkingCalendar = [currCalendar components:calendarUnits fromDate:startDate];
                    comparableWorkingCalendar.calendar = currCalendar;
                    float workingDays = 0.0f;
                    
                    while ([[comparableWorkingCalendar date] compare:endDate] <= NSOrderedSame) {
                        NSDateComponents *tempCalendarComponents = [currCalendar components:calendarUnits fromDate:[comparableWorkingCalendar date]];
                        if ([tempCalendarComponents weekday] == MON && [self.propAppDelegate.getStaff propHasMonday]){
                            if ([holidaysIncluded containsObject:[self.propAppDelegate.propFormatVelosiDate stringFromDate:[comparableWorkingCalendar date]]])
                                holidayCount++;
                            else workingDays+=incrementaDays;
                        }
                        if ([tempCalendarComponents weekday] == TUE && [self.propAppDelegate.getStaff propHasTuesday]){
                            if ([holidaysIncluded containsObject:[self.propAppDelegate.propFormatVelosiDate stringFromDate:[comparableWorkingCalendar date]]])
                                holidayCount++;
                            else workingDays+=incrementaDays;
                        }
                        if ([tempCalendarComponents weekday] == WED && [self.propAppDelegate.getStaff propHasWednesday]){
                            if ([holidaysIncluded containsObject:[self.propAppDelegate.propFormatVelosiDate stringFromDate:[comparableWorkingCalendar date]]])
                                holidayCount++;
                            else workingDays+=incrementaDays;
                        }
                        if ([tempCalendarComponents weekday] == THU && [self.propAppDelegate.getStaff propHasThursday]) {
                            if ([holidaysIncluded containsObject:[self.propAppDelegate.propFormatVelosiDate stringFromDate:[comparableWorkingCalendar date]]])
                                holidayCount++;
                            else workingDays+=incrementaDays;
                        }
                        if ([tempCalendarComponents weekday] == FRI && [self.propAppDelegate.getStaff propHasFriday]){
                            if ([holidaysIncluded containsObject:[self.propAppDelegate.propFormatVelosiDate stringFromDate:[comparableWorkingCalendar date]]])
                                holidayCount++;
                            else workingDays+=incrementaDays;
                        }
                        if ([tempCalendarComponents weekday] == SAT && [self.propAppDelegate.getStaff propHasSaturday]) {
                            if ([holidaysIncluded containsObject:[self.propAppDelegate.propFormatVelosiDate stringFromDate:[comparableWorkingCalendar date]]])
                                holidayCount++;
                            else workingDays+=incrementaDays;
                        }
                        if ([tempCalendarComponents weekday] == SUN && [self.propAppDelegate.getStaff propHasSunday]) {
                            if ([holidaysIncluded containsObject:[self.propAppDelegate.propFormatVelosiDate stringFromDate:[comparableWorkingCalendar date]]])
                                holidayCount++;
                            else workingDays+=incrementaDays;
                        }
                        comparableWorkingCalendar.day++;
                    }
                    _propFieldWorkingDays.text = [NSString stringWithFormat:@"%.1f", workingDays];
                    if ([_propLeave propTypeID] == LEAVETYPEID_VACATION || [_propLeave propTypeID] == LEAVETYPEID_UNPAID || [_propLeave propTypeID] == LEAVETYPEID_BUSINESSTRIP)
                        _propRemainingLeaveCredits.text = [NSString stringWithFormat:@"%.1f",[self.propAppDelegate.dashboard propRemVL]];
                    else
                        _propRemainingLeaveCredits.text = [NSString stringWithFormat:@"%.1f",[self.propAppDelegate.dashboard propRemSL]];
                    
                    _propTextViewNotes.text = [_propLeave propNotes];
                    _propButtonFollowup.hidden = ([_propLeave propStatusID] == LEAVESTATUSID_PENDING)?NO:YES;

                    [self hideCancelButton];
//                    NSComparisonResult result = [filedDate compare:currentDate];
//                    [barbButtonItems addObject:];
//                    [barbButtonItems addObject:[[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemEdit target:self action:@selector(editLeave)]];
                    
                }else{
                    _propButtonFollowup.hidden = YES;
//                    self.navigationItem.rightBarButtonItems = @[[[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemEdit target:self action:@selector(editLeave)]];
                }
                [AppDelegate hideGlogalHUDWithView:self.view];
            }
        });
    });
    _propFieldLeaveType.delegate = self;
    _propFieldDateFrom.delegate = self;
    _propFieldDateTo.delegate = self;
    _propFieldDays.delegate = self;
    _propTextViewNotes.delegate = self;
}

- (void)hideCancelButton {
    //can cancel leave for pending leaves and can cancel while the start date is in the future
    NSDate *filedDate = [[self.propAppDelegate.propDateFormatLeaveDuration dateFromString:[_propLeave propStartDate]] dateByAddingTimeInterval:[[NSTimeZone defaultTimeZone] secondsFromGMT]];
    
    NSString *currentStringDate = [self.propAppDelegate.propDateFormatLeaveDuration stringFromDate:[NSDate date]];
    NSDate *currentDate = [[self.propAppDelegate.propDateFormatLeaveDuration dateFromString:currentStringDate] dateByAddingTimeInterval:[[NSTimeZone defaultTimeZone] secondsFromGMT]];
    
    NSMutableArray *barbButtonItems = [[NSMutableArray alloc] init];
    UIBarButtonItem *cancelBtn = [[UIBarButtonItem alloc] initWithTitle:@"Cancel" style:UIBarButtonItemStylePlain target:self action:@selector(cancelLeave)];
    if ([_propLeave propStatusID] == LEAVESTATUSID_PENDING || ([_propLeave propStatusID] == LEAVESTATUSID_APPROVED && [filedDate compare:currentDate] == NSOrderedDescending)) {
        [barbButtonItems addObject:cancelBtn];
    } else {
        [barbButtonItems removeLastObject];
    }
    self.navigationItem.rightBarButtonItems = [NSArray arrayWithArray:barbButtonItems];
}

- (IBAction)followUp:(id)sender {
    [AppDelegate showGlogalHUDWithView:self.view];
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
        NSString *result = [self.propAppDelegate.propGatewayOnline followupLeave:[_propLeave propLeaveID]];
        dispatch_async(dispatch_get_main_queue(), ^{
            [AppDelegate hideGlogalHUDWithView:self.view];
            [self viewAlertDialogWithTitle:@"" message:(result==nil)?@"Follow Up Email Sent Successfully":result];
        });
    });
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    ((VCLeaveInput *)segue.destinationViewController).propLeave = _propLeave;
}

- (void)cancelLeave{
    
    NSString *alertMsg = @"You are about to delete a leave entry from your record. And your approver will be notified to cancel request. \nIf you would like to continue with this cancellation. Click on the OK button below, otherwise click Cancel.";
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"" message:alertMsg preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *closeAction = [UIAlertAction actionWithTitle:@"Cancel"
                                                          style:UIAlertActionStyleCancel
                                                        handler:^(UIAlertAction *action){
                                                            NSLog(@"Closed");
                                                        }];
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"OK"
                                                           style:UIAlertActionStyleDefault
                                                         handler:^(UIAlertAction *action) {
                                                             [AppDelegate showGlogalHUDWithView:self.view];
                                                             dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
                                                                 id result = [self.propAppDelegate.propGatewayOnline processLeaveJSON:[_propLeave propLeaveID] forStatusID:LEAVESTATUSID_CANCELLED];
                                                                 dispatch_async(dispatch_get_main_queue(), ^{
                                                                     [AppDelegate hideGlogalHUDWithView:self.view];
                                                                     if([result isEqualToString:@"OK"]){
                                                                         self.navigationItem.rightBarButtonItems = @[];
                                                                         _propButtonFollowup.titleLabel.textColor = [UIColor lightGrayColor];
                                                                         _propButtonFollowup.userInteractionEnabled = NO;
                                                                         _propFieldLeaveStatus.text = LEAVESTATUSDESC_CANCELLED;
                                                                         [[[UIAlertView alloc] initWithTitle:@"" message:@"Leave was cancelled successfully" delegate:self cancelButtonTitle:@"Dismiss" otherButtonTitles:nil, nil] show];
                                                                     }else{
                                                                         [self viewAlertDialogWithTitle:@"" message:result];
                                                                     }
                                                                 });
                                                             });
                                                         }];
    [alertController addAction:closeAction];
    [alertController addAction:cancelAction];
    
    [self presentViewController:alertController animated:YES completion:nil];
}


#pragma mark -
#pragma mark === Helper Method ===
#pragma mark -
- (void)viewAlertDialogWithTitle:(NSString*)title message:(NSString*)msg{
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:title message:msg preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"Dismiss" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
        [self.navigationController popViewControllerAnimated:YES];
    }];
    [alertController addAction:cancelAction];
    [self presentViewController:alertController animated:YES completion:nil];
}

//- (void)editLeave{
//    [self performSegueWithIdentifier:@"leavedetailtoeditleave" sender:nil];
//}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    [self.navigationController popViewControllerAnimated:YES];
}

@end
