//
//  VCClaimItems.m
//  Salt
//
//  Created by Rick Royd Aban on 2015/08/03.
//  Copyright (c) 2015年 Applus Velosi. All rights reserved.
//

#import "VCClaimItems.h"
#import "MBProgressHUD.h"
#import "ClaimItem.h"
#import "CellClaimItem.h"
#import "ClaimCategory.h"
#import "VCGeneralMyClaimItemDetail.h"
#import "VCMileageClaimItemDetail.h"
#import "VelosiColors.h"

@interface VCClaimItems (){
    
    IBOutlet UITableView *_propLV;
    ClaimItem *_selectedClaimItem;
    UIBarButtonItem *_buttonRefresh;
}

@end

@implementation VCClaimItems


#pragma mark -
#pragma mark === View Life Cycle Management ===
#pragma mark -
- (void)viewDidLoad {
    [super viewDidLoad];
    
    _propLV.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    _propLV.delegate  = self;
    _propLV.dataSource = self;
    
    _buttonRefresh = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemRefresh target:self action:@selector(refresh)];
    self.navigationItem.rightBarButtonItems = @[_buttonRefresh];
}

- (void)viewDidAppear:(BOOL)animated{
    [self refresh];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    
    NSLog(@"SEGUE ID: %@",segue.identifier);
    if ([_selectedClaimItem propCategoryTypeID]== [ClaimCategory TYPE_MILEAGE]) {
        ((VCMileageClaimItemDetail *)segue.destinationViewController).propClaimItem = [[MileageClaimItem alloc] initWithDictionary:[_selectedClaimItem savableDictionary]];
    } else {
        ((VCGeneralMyClaimItemDetail *)segue.destinationViewController).propClaimItem = _selectedClaimItem;
    }
}


#pragma mark -
#pragma mark - Table view data source delegate
#pragma mark -
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    CellClaimItem *cellClaimItem = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    ClaimItem *myclaimItem = [[self.propAppDelegate myClaimItemsForMyClaimID:[_propClaimHeader propClaimID]] objectAtIndex:indexPath.row];
    
    cellClaimItem.propCategoryName.text = [myclaimItem propCategoryName];
    cellClaimItem.propDateSubmitted.text = [myclaimItem propDateCreatedWithAppDelegate:self.propAppDelegate];

    NSString *statusName;
    int statusID = [myclaimItem statusID];
    statusName = [ClaimHeader statusDescForStatusKey:statusID];
    
    cellClaimItem.propStatusName.text = statusName;
    if(statusID==CLAIMSTATUSID_APPROVEDBYACCOUNTS || statusID==CLAIMSTATUSID_APPROVEDBYAPPROVER || statusID==CLAIMSTATUSID_APPROVEDBYCM || statusID==CLAIMSTATUSID_LIQUIDATED || statusID==CLAIMSTATUSID_PAID || statusID==CLAIMSTATUSID_PAIDUNDERCOMPANYCARD)
        cellClaimItem.propStatusName.textColor = [VelosiColors greenAcceptance];
    else if(statusID==CLAIMSTATUSID_REJECTEDBYACCOUNTS || statusID==CLAIMSTATUSID_REJECTEDBYAPPROVER || statusID==CLAIMSTATUSID_REJECTEDBYCM || statusID==CLAIMSTATUSID_REJECTEDFORSALARYDEDUCTION || statusID==CLAIMSTATUSID_RETURN || statusID == CLAIMSTATUSID_CANCELLED)
        cellClaimItem.propStatusName.textColor = [UIColor redColor];
    else
        cellClaimItem.propStatusName.textColor = [UIColor grayColor];
    
    cellClaimItem.propTotalClaim.text = [NSString stringWithFormat:@"%@ %.2f",[myclaimItem propLocalCurrencyName], [myclaimItem propLocalAmount]];
    cellClaimItem.propDescription.text = [myclaimItem description];
    
    return cellClaimItem;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    _selectedClaimItem = [[self.propAppDelegate myClaimItemsForMyClaimID:[_propClaimHeader propClaimID]] objectAtIndex:indexPath.row];
    [self performSegueWithIdentifier:([_selectedClaimItem propCategoryTypeID]== [ClaimCategory TYPE_MILEAGE])?@"myclaimitemstomileageclaimitemdetail":@"myclaimitemstogeneralclaimitemdetail" sender:[tableView cellForRowAtIndexPath:indexPath]];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [self.propAppDelegate myClaimItemsForMyClaimID:[_propClaimHeader propClaimID]].count;
}

- (IBAction)reload:(id)sender {
    [self refresh];
}

- (void)refresh{
    [AppDelegate showGlogalHUDWithView:self.view];
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
        id result = [self.propAppDelegate.propGatewayOnline claimItemsForClaimID:[_propClaimHeader propClaimID]];
                     
        dispatch_async(dispatch_get_main_queue(), ^{
            [AppDelegate hideGlogalHUDWithView:self.view];
            if([result isKindOfClass:[NSString class]])
                [[[UIAlertView alloc] initWithTitle:@"" message:result delegate:nil cancelButtonTitle:@"Dismiss" otherButtonTitles:nil, nil] show];
            else
                [self.propAppDelegate updateMyClaimItems:result forClaimID:[_propClaimHeader propClaimID]];
            
            [_propLV reloadData];
        });
    });
}

@end
