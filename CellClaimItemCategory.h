//
//  CellClaimItemCategory.h
//  Salt
//
//  Created by Rick Royd Aban on 2015/09/08.
//  Copyright (c) 2015年 Applus Velosi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CellClaimItemCategory : UITableViewCell

@property (strong, nonatomic) IBOutlet UILabel *propCategoryName;

@end
