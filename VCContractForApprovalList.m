//
//  VCContractForApprovalList.m
//  Salt
//
//  Created by Rick Royd Aban on 09/06/2017.
//  Copyright © 2017 Applus Velosi. All rights reserved.
//

#import "VCContractForApprovalList.h"
#import "VCContractsForApproval.h"

@interface VCContractForApprovalList ()

@end

@implementation VCContractForApprovalList

#pragma mark -
#pragma mark === View Life Cycle Management ===
#pragma mark -
- (void)viewDidLoad {
    [super viewDidLoad];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [self reloadContractsForApprovalList];
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"embedVCContracts"]) {
        VCContractsForApproval *destVC = segue.destinationViewController;
        destVC.parentVC = self;
    }
}

- (void)reloadContractsForApprovalList {
    [AppDelegate showGlogalHUDWithView:self.view];
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        id result = [self.propAppDelegate.propGatewayOnline contractsTenderForApproval];
        dispatch_async(dispatch_get_main_queue(), ^{
            [AppDelegate hideGlogalHUDWithView:self.view];
            if ([result isKindOfClass:[NSString class]])
                [self.delegate loadFailedWithError:result];
            else
                [self.propAppDelegate updateContractsForApproval:result];
            [self.delegate loadFinished];
        });
    });
}

- (IBAction) toogleList:(id)sender {
    [self.propAppDelegate.propSlider toggleSidebar];
}

@end
