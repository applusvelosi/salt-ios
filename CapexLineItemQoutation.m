//
//  CapexLineItemQoutation.m
//  Salt
//
//  Created by Rick Royd Aban on 10/7/15.
//  Copyright © 2015 Applus Velosi. All rights reserved.
//

#import "CapexLineItemQoutation.h"

@interface CapexLineItemQoutation(){
    NSMutableDictionary *_capexLineItemQoutation;
}
@end

@implementation CapexLineItemQoutation

- (CapexLineItemQoutation *)initWithDictionary:(NSDictionary *)jsonCapexLineItemQoutation onlineGateway:(OnlineGateway *)onlineGateway{
    if([super init]){
        _capexLineItemQoutation = [NSMutableDictionary dictionaryWithDictionary:jsonCapexLineItemQoutation];
        [_capexLineItemQoutation setObject:@([[_capexLineItemQoutation objectForKey:@"CapexLineItemID"] intValue]) forKey:@"CapexLineItemID"];
        [_capexLineItemQoutation setObject:@([[_capexLineItemQoutation objectForKey:@"CapexLineItemQuotationID"] intValue]) forKey:@"CapexLineItemQuotationID"];
        [_capexLineItemQoutation setObject:@([[_capexLineItemQoutation objectForKey:@"CurrencyID"] intValue]) forKey:@"CurrencyID"];
        [_capexLineItemQoutation setObject:@([[_capexLineItemQoutation objectForKey:@"FinancingSchemeID"] intValue]) forKey:@"FinancingSchemeID"];
        
        [_capexLineItemQoutation setObject:[[NSString alloc] initWithData:[NSJSONSerialization dataWithJSONObject:[_capexLineItemQoutation objectForKey:@"Documents"] options:0 error:nil] encoding:NSUTF8StringEncoding] forKey:@"Documents"];
    }
    return self;
}

-(CapexLineItemQoutation *)initWithData{
    if ([super init]) {
        _capexLineItemQoutation = [NSMutableDictionary new];
        [_capexLineItemQoutation setObject:@YES forKey:@"IsPrimary"];
        [_capexLineItemQoutation setObject:@50.0f forKey:@"Amount"];
        [_capexLineItemQoutation setObject:@"My Note" forKey:@"Note"];
        [_capexLineItemQoutation setObject:@"financial name" forKey:@"FinancingSchemeName"];
        [_capexLineItemQoutation setObject:@20.4f forKey:@"AmountInUSD"];
        [_capexLineItemQoutation setObject:@"USD" forKey:@"CurrencyName"];
        [_capexLineItemQoutation setObject:@"supplier test" forKey:@"SupplierName"];
    }
    return self;
}

- (float)propAmount{
    return [[_capexLineItemQoutation objectForKey:@"Amount"] floatValue];
}

- (float)propAmountInUSD{
    return [[_capexLineItemQoutation objectForKey:@"AmountInUSD"] floatValue];
}

- (int)propCapexLineItemID{
    return [[_capexLineItemQoutation objectForKey:@"CapexLineItemID"] intValue];
}

- (int)propCapexLineItemQoutationID{
    return [[_capexLineItemQoutation objectForKey:@"CapexLineItemQoutationID"] intValue];
}

- (int)propCurrencyID{
    return [[_capexLineItemQoutation objectForKey:@"CurrencyID"] intValue];
}

- (NSString *)propCurrencyThree{
    return [_capexLineItemQoutation objectForKey:@"CurrencyName"];
}

- (NSMutableArray *)propDocuments{
    return [NSJSONSerialization JSONObjectWithData:[[_capexLineItemQoutation objectForKey:@"Documents"]dataUsingEncoding:NSUTF8StringEncoding] options:NSJSONReadingMutableContainers error:0];
}

- (float)propExchangeRate{
    return [[_capexLineItemQoutation objectForKey:@"ExchangeRate"] floatValue];
}

- (int)propFinancingSchemeID{
    return [[_capexLineItemQoutation objectForKey:@"FinancingSchemeID"] intValue];
}

- (NSString *)propFinancingSchemenName{
    return [_capexLineItemQoutation objectForKey:@"FinancingSchemeName"];
}

- (BOOL)propIsPrimary{
    return [[_capexLineItemQoutation objectForKey:@"IsPrimary"] boolValue];
}

- (NSString *)propNote{
    return [_capexLineItemQoutation objectForKey:@"Note"];
}

- (NSString *)propPaymentTerm{
    return [_capexLineItemQoutation objectForKey:@"PaymentTerm"];
}

- (NSString *)propSupplierName{
    return [_capexLineItemQoutation objectForKey:@"SupplierName"];
}

@end
