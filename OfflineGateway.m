//
//  OfflineGateway.m
//  Salt iOS
//
//  Created by Rick Royd Aban on 5/22/15.
//  Copyright (c) 2015 Rick Royd Aban. All rights reserved.
//

#define PREVUSERNAME @"prevusername"
#define KEY_MYLEAVES @"myleaveskey"
#define KEY_LEAVESFORAPPROVAL @"leavesforapprovalkey"
#define KEY_STAFF @"staffkey"
#define kEY_OFFICE @"officekey"
//holidays
#define KEY_LOCALHOLIDAYS @"localholidayskey"
#define KEY_MONTHLYHOLIDAYS @"monthkyholidayskey"
#define KEY_MYCLAIMS @"myclaimskey"
#define KEY_CLAIMSFORAPPROVAL @"claimsforapprovalkey"
#define KEY_CLAIMSFORPAYMENT @"claimsforpaymentkey"
//recruitments
#define KEY_RECRUITMENTSFORAPPROVAL @"recruitmentsforapprovalkey"
//capexes
#define KEY_CAPEXESFORAPPROVAL @"capexesforapprovalkey"
//gifts
#define KEY_GIFTSFORAPPROVAL @"giftsforapprovalkey"
//contracts
#define KEY_CONTRACTSFORAPPROVAL @"contractsforapprovalkey"

#import "OfflineGateway.h"

@interface OfflineGateway(){
    AppDelegate *_appDelegate;
    NSUserDefaults *_prefs;
}
@end

@implementation OfflineGateway

- (OfflineGateway *) initWithAppDelegate:(AppDelegate *)appDelegate{
    if(self = [super init]){
        _prefs = [NSUserDefaults standardUserDefaults];
        _appDelegate = appDelegate;
    }
    return self;
}

- (void)updatePreviouslyUsedUsername:(NSString *)username{
    [_prefs setObject:username forKey:PREVUSERNAME];
}

- (BOOL)isLoggedIn{
    return ([_prefs objectForKey:KEY_STAFF] != nil)?YES:NO;
}

- (void)logout{
//    [[NSUserDefaults standardUserDefaults] removePersistentDomainForName:[[NSBundle mainBundle] bundleIdentifier]];
    [_prefs removeObjectForKey:KEY_STAFF];
    [_prefs removeObjectForKey:kEY_OFFICE];
    [_prefs removeObjectForKey:KEY_MYLEAVES];
    [_prefs removeObjectForKey:KEY_LEAVESFORAPPROVAL];
}

- (NSString *)getPrevUsername{
    return ([_prefs objectForKey:PREVUSERNAME] != nil)?[_prefs objectForKey:PREVUSERNAME]:@"";
}


#pragma mark -
#pragma mark - Staff/Office Shared Preference
#pragma mark -
- (void)serializeStaff:(Staff *)staff office:(Office *)office{
    [_prefs setObject:[[NSString alloc] initWithData:[NSJSONSerialization dataWithJSONObject:[staff staffDictionary] options:0 error:nil] encoding:NSUTF8StringEncoding] forKey:KEY_STAFF];
    [_prefs setObject:[[NSString alloc] initWithData:[NSJSONSerialization dataWithJSONObject:[office savableDictionary] options:0 error:nil] encoding:NSUTF8StringEncoding] forKey:kEY_OFFICE];
}
- (Staff *)deserializeStaff{
    return [[Staff alloc] initWithOfflineGateway:_appDelegate.propGatewayOffline staffDictionary:[NSJSONSerialization JSONObjectWithData:[[_prefs objectForKey:KEY_STAFF] dataUsingEncoding:NSUTF8StringEncoding] options:0 error:nil]];
}

- (Office *)deserializeStaffOffice{
    return [NSJSONSerialization JSONObjectWithData:[[_prefs objectForKey:kEY_OFFICE] dataUsingEncoding:NSUTF8StringEncoding] options:0 error:nil];
}


#pragma mark -
#pragma mark - Claims Shared Preference
#pragma mark -
- (void)serializeMyClaims:(NSMutableArray *)myClaims{
    NSMutableArray *claimMaps = [NSMutableArray array];
//    for(id claim in myClaims)
//        [claimMaps addObject:[claim savableDictionary]];
    [_prefs setObject:[[NSString alloc] initWithData:[NSJSONSerialization dataWithJSONObject:claimMaps options:0 error:nil] encoding:NSUTF8StringEncoding] forKey:KEY_MYCLAIMS];
}

- (NSMutableArray *)deserializeMyClaims{
    NSArray *savedMyClaimsDict = [NSJSONSerialization JSONObjectWithData:[[_prefs objectForKey:KEY_MYCLAIMS] dataUsingEncoding:NSUTF8StringEncoding] options:0 error:nil];
    NSMutableArray *returnableMyClaims = [NSMutableArray array];
    for(NSDictionary *savedMyClaimDict in savedMyClaimsDict)
        [returnableMyClaims addObject:[[Claim alloc] initWithDictionary:[savedMyClaimDict mutableCopy]]];
    return returnableMyClaims;
}


#pragma mark -
#pragma mark - Leaves Shared Preference
#pragma mark -
- (void)serializeMyLeaves:(NSMutableArray *)myLeaves{
    NSMutableArray *leaveMaps = [NSMutableArray array];
//    for(id leave in myLeaves)
//        [leaveMaps addObject:[leave savableDictionary]];
    [_prefs setObject:[[NSString alloc] initWithData:[NSJSONSerialization dataWithJSONObject:leaveMaps options:0 error:nil] encoding:NSUTF8StringEncoding] forKey:KEY_MYLEAVES];
}

- (NSMutableArray *)deserializeMyLeaves{
    NSArray *savedMyLeavesDict = [NSJSONSerialization JSONObjectWithData:[[_prefs objectForKey:KEY_MYLEAVES] dataUsingEncoding:NSUTF8StringEncoding] options:0 error:nil];
    NSMutableArray *returnableMyLeaves = [NSMutableArray array];
    for(NSDictionary *savedMyLeaveDict in savedMyLeavesDict)
        [returnableMyLeaves addObject:[[Leave alloc] initWithDictionary:[savedMyLeaveDict mutableCopy]]];
    return returnableMyLeaves;
}

#pragma mark -
#pragma mark - LocalHolidays Shared Preference
#pragma mark -
- (void)serializeLocalHolidays:(NSMutableArray *)localHolidays{
    NSMutableArray *localHolidaysMap = [NSMutableArray array];
//    for(LocalHoliday *localHoliday in localHolidays)
//        [localHolidaysMap addObject:[localHoliday savableDictionary]];
    [_prefs setObject:[[NSString alloc] initWithData:[NSJSONSerialization dataWithJSONObject:localHolidaysMap options:0 error:nil] encoding:NSUTF8StringEncoding] forKey:KEY_LOCALHOLIDAYS];
}

- (NSMutableArray *)deserializeLocalHolidays{
    NSMutableArray *localHolidays = [NSMutableArray array];
    if([_prefs objectForKey:KEY_LOCALHOLIDAYS]){
        NSArray *savedLocalHolidays = [NSMutableArray arrayWithArray:[NSJSONSerialization JSONObjectWithData:[[_prefs objectForKey:KEY_LOCALHOLIDAYS] dataUsingEncoding:NSUTF8StringEncoding] options:0 error:nil]];
        for(NSDictionary *savedLocalHolidayDict in savedLocalHolidays)
            [localHolidays addObject:[[LocalHoliday alloc] initWithDictionary:savedLocalHolidayDict]];
    }
    
    return localHolidays;
}

#pragma mark -
#pragma mark - MonthlyHolidays Shared Preference
#pragma mark -
- (void)serializeMonthlyHolidays:(NSMutableArray *)monthlyHolidays{
    NSMutableArray *monthlyHolidaysMap = [NSMutableArray array];
//    for(Holiday *monthlyHoliday in monthlyHolidays)
//        [monthlyHolidaysMap addObject:[monthlyHoliday savableDictionary]];
    [_prefs setObject:[[NSString alloc] initWithData:[NSJSONSerialization dataWithJSONObject:monthlyHolidaysMap options:0 error:nil] encoding:NSUTF8StringEncoding] forKey:KEY_MONTHLYHOLIDAYS];
}
- (NSMutableArray *)deserializeMonthlyHolidays{
    NSMutableArray *monthlyHolidays = [NSMutableArray array];
    if([_prefs objectForKey:KEY_MONTHLYHOLIDAYS]){
        NSArray *savedMonthlyHolidays = [NSMutableArray arrayWithArray:[NSJSONSerialization JSONObjectWithData:[[_prefs objectForKey:KEY_MONTHLYHOLIDAYS] dataUsingEncoding:NSUTF8StringEncoding] options:0 error:nil]];
        for(NSDictionary *savedMonthlyHolidayDict in savedMonthlyHolidays)
            [monthlyHolidays addObject:[[Holiday alloc] initWithDictionary:savedMonthlyHolidayDict]];
    }
    
    return monthlyHolidays;
}

//- (void)serializeClaimsForPayment:(NSMutableArray *)claimsForPayment{
//    NSMutableArray *claimMaps = [NSMutableArray array];
//    for(id claim in claimsForPayment)
//        [claimMaps addObject:[claim savableDictionary]];
//    
//    [_prefs setObject:[[NSString alloc] initWithData:[NSJSONSerialization dataWithJSONObject:claimMaps options:0 error:nil] encoding:NSUTF8StringEncoding] forKey:KEY_CLAIMSFORPAYMENT];
//}

#pragma mark -
#pragma mark - Claims for Approval Shared Preference
#pragma mark -
- (void)serializeClaimsForApproval:(NSMutableArray *)claimsForApproval{
    NSMutableArray *claimMaps = [NSMutableArray array];
//    for(id claim in claimsForApproval)
//        [claimMaps addObject:[claim savableDictionary]];
    [_prefs setObject:[[NSString alloc] initWithData:[NSJSONSerialization dataWithJSONObject:claimMaps options:0 error:nil] encoding:NSUTF8StringEncoding] forKey:KEY_CLAIMSFORAPPROVAL];
}

- (NSMutableArray *)deserializeClaimsForApproval{
    NSArray *savedClaimsForApprovalDict = [NSJSONSerialization JSONObjectWithData:[[_prefs objectForKey:KEY_CLAIMSFORAPPROVAL] dataUsingEncoding:NSUTF8StringEncoding] options:0 error:nil];
    NSMutableArray *returnableClaimsForApproval = [NSMutableArray array];
    for(NSDictionary *savedClaimForApprovalDict in savedClaimsForApprovalDict)
        [returnableClaimsForApproval addObject:[[Claim alloc] initWithDictionary:[savedClaimForApprovalDict mutableCopy]]];
    
    return returnableClaimsForApproval;
}

#pragma mark -
#pragma mark - Leaves for Approval Shared Preference
#pragma mark -
- (void)serializeLeavesForApproval:(NSMutableArray *)leavesForApproval{
    NSMutableArray *leaveForApprovalMaps = [NSMutableArray array];
//    for(Leave *leaveForApproval in leavesForApproval)
//        [leaveForApprovalMaps addObject:[leaveForApproval savableDictionary]];
    NSLog(@"serialize lfa with %ld items",leavesForApproval.count);
    [_prefs setObject:[[NSString alloc] initWithData:[NSJSONSerialization dataWithJSONObject:leaveForApprovalMaps options:0 error:nil] encoding:NSUTF8StringEncoding] forKey:KEY_LEAVESFORAPPROVAL];
}

- (NSMutableArray *)deserializeLeavesForApproval{
    NSArray *savedLeavesForApprovalDict = [NSJSONSerialization JSONObjectWithData:[[_prefs objectForKey:KEY_LEAVESFORAPPROVAL] dataUsingEncoding:NSUTF8StringEncoding] options:0 error:nil];
    NSMutableArray *returnableLeavesForApproval = [NSMutableArray array];
    for(NSDictionary *savedLeaveForApprovalDict in savedLeavesForApprovalDict)
        [returnableLeavesForApproval addObject:[[Leave alloc] initWithDictionary:[savedLeaveForApprovalDict mutableCopy]]];
    
    return returnableLeavesForApproval;
}

//- (NSMutableArray *)deserializeClaimsForPayment{
//    NSArray *savedClaimsForPaymentDict = [NSJSONSerialization JSONObjectWithData:[[_prefs objectForKey:KEY_CLAIMSFORPAYMENT] dataUsingEncoding:NSUTF8StringEncoding] options:0 error:nil];
//    NSMutableArray *returnableClaimsForPayment = [NSMutableArray array];
//    for(NSDictionary *savedClaimForPaymentDict in savedClaimsForPaymentDict)
//        [returnableClaimsForPayment addObject:[[Claim alloc] initWithDictionary:[savedClaimForPaymentDict mutableCopy]]];
//    
//    return returnableClaimsForPayment;
//}

#pragma mark -
#pragma mark - HR for Approval Shared Preference
#pragma mark -
- (void)serializeRecruitmentsForApproval:(NSMutableArray *)recruitmentsForApproval{
    NSMutableArray *recruitmentsForApprovalMaps = [NSMutableArray array];
//    for(id recruitmentForApproval in recruitmentsForApproval)
//        [recruitmentsForApprovalMaps addObject:[recruitmentForApproval savableDictionary]];
    [_prefs setObject:[[NSString alloc] initWithData:[NSJSONSerialization dataWithJSONObject:recruitmentsForApprovalMaps options:0 error:nil] encoding:NSUTF8StringEncoding] forKey:KEY_RECRUITMENTSFORAPPROVAL];
}

- (NSMutableArray *)deserializeRecruitmentForApproval{
    NSArray *savedRecruitmentsForApproval = [NSJSONSerialization JSONObjectWithData:[[_prefs objectForKey:KEY_RECRUITMENTSFORAPPROVAL] dataUsingEncoding:NSUTF8StringEncoding] options:0 error:nil];
    NSMutableArray *returnableRecruitmentsForApproval = [NSMutableArray array];
    for(NSDictionary *savedRecruitmentForApproval in savedRecruitmentsForApproval)
        [returnableRecruitmentsForApproval addObject:[[Recruitment alloc] initWithDictionary:[savedRecruitmentForApproval mutableCopy]]];
    return returnableRecruitmentsForApproval;
}

#pragma mark -
#pragma mark - Capexes for Approval Shared Preference
#pragma mark -
- (NSMutableArray *)deserializeCapexesForApproval{
    NSArray *savedCapexesForApproval = [NSJSONSerialization JSONObjectWithData:[[_prefs objectForKey:KEY_CAPEXESFORAPPROVAL] dataUsingEncoding:NSUTF8StringEncoding] options:0 error:nil];
    NSMutableArray *returnableCapexesForApproval = [NSMutableArray array];
    for(NSDictionary *savedCapexForApproval in savedCapexesForApproval)
        [returnableCapexesForApproval addObject:[[CapexHeader alloc] initWithDictionary:[savedCapexForApproval mutableCopy]]];
    return returnableCapexesForApproval;
}

- (void)serializeCapexesForApproval:(NSMutableArray *)capexesForApproval{
    NSMutableArray *capexesForApprovalMaps = [NSMutableArray array];
//    for(id capexForApproval in capexesForApproval)
//        [capexesForApprovalMaps addObject:[capexForApproval savableDictionary]];
    [_prefs setObject:[[NSString alloc] initWithData:[NSJSONSerialization dataWithJSONObject:capexesForApprovalMaps options:0 error:nil] encoding:NSUTF8StringEncoding] forKey:KEY_CAPEXESFORAPPROVAL];
}

#pragma mark -
#pragma mark - Gifts for Approval Shared Preference
#pragma mark -
- (void)serializeGiftsForApproval {
    NSMutableArray *gifts = [NSMutableArray array];
    [_prefs setObject:[[NSString alloc] initWithData:[NSJSONSerialization dataWithJSONObject:gifts options:0 error:nil] encoding:NSUTF8StringEncoding] forKey:KEY_GIFTSFORAPPROVAL];
}

- (NSMutableArray *)deserializeGiftsForApproval {
    NSArray *savedGiftsForApprovalDict = [NSJSONSerialization JSONObjectWithData:[[_prefs objectForKey:KEY_GIFTSFORAPPROVAL] dataUsingEncoding:NSUTF8StringEncoding] options:0 error:nil];
    NSMutableArray *returnableGiftsForApproval = [NSMutableArray array];
    for(NSDictionary *savedGift in savedGiftsForApprovalDict)
        [returnableGiftsForApproval addObject:[[GiftHospitality alloc] initWithDictionary:[savedGift mutableCopy]]];
    return returnableGiftsForApproval;
}

#pragma mark -
#pragma mark - Contracts for Approval Shared Preference
#pragma mark -
- (void)serializeContractsForApproval {
    NSMutableArray *contracts = [NSMutableArray array];
    [_prefs setObject:[[NSString alloc] initWithData:[NSJSONSerialization dataWithJSONObject:contracts options:0 error:nil] encoding:NSUTF8StringEncoding] forKey:KEY_CONTRACTSFORAPPROVAL];
}

-(NSMutableArray *)deserializeContractsForApproval {
    NSArray *savedContractsForApprovalDict = [NSJSONSerialization JSONObjectWithData:[[_prefs objectForKey:KEY_CONTRACTSFORAPPROVAL] dataUsingEncoding:NSUTF8StringEncoding] options:0 error:nil];
    NSMutableArray *returnableContractsForApproval = [NSMutableArray array];
    for (NSDictionary *savedContract in savedContractsForApprovalDict)
        [returnableContractsForApproval addObject:[[Contract alloc] initWithJSONDictionary:[savedContract mutableCopy]]];
    return returnableContractsForApproval;
}

@end
