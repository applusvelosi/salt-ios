//
//  CostCenter.m
//  Salt
//
//  Created by Rick Royd Aban on 2/2/16.
//  Copyright © 2016 Applus Velosi. All rights reserved.
//

#import "CostCenter.h"

@interface CostCenter(){
    int _costCenterID;
    NSString *_costCenterName;
}
@end

@implementation CostCenter

- (CostCenter *)initWithID:(int)costCenterID name:(NSString *)costCenterName{
    if([super init]){
        _costCenterID = costCenterID;
        _costCenterName = costCenterName;
    }
    
    return self;
}

- (int)propCostCenterID{ return _costCenterID; }
- (NSString *)propCostCenterName{ return _costCenterName; }

@end
