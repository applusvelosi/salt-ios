//
//  VCHomeLeaves.h
//  Salt
//
//  Created by Rick Royd Aban on 8/19/14.
//  Copyright (c) 2014 applusvelosi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "VCHomeScrollPage.h"
#import "Dashboard.h"

@interface VCHomeLeaves : VCHomeScrollPage

- (void)refresh:(Dashboard *)dashboard;

@end
