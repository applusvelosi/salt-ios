//
//  Project.h
//  Salt
//
//  Created by Rick Royd Aban on 2015/09/11.
//  Copyright (c) 2015年 Applus Velosi. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Project : NSObject

- (id)initWithDictionary:(NSDictionary *)dict;

//GETTER METHODS
- (int)propID;
- (NSString *)propName;
- (NSString *)propCode;

@end
