//
//  VCClaimsForApprovalItems.h
//  Salt
//
//  Created by Rick Royd Aban on 2015/08/25.
//  Copyright (c) 2015年 Applus Velosi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "VCPage.h"

@interface VCClaimForApprovalLineItems : VCPage<UITableViewDataSource, UITableViewDelegate>

@property (strong, nonatomic) ClaimHeader *propClaimHeader;
@property (strong, nonatomic) UITableViewController *propParentVC;

@end
