//
//  VCClaimsForApproval.h
//  Salt
//
//  Created by Rick Royd Aban on 2015/07/31.
//  Copyright (c) 2015年 Applus Velosi. All rights reserved.
//

#import "VCPage.h"
@class VCClaimsForApprovalList;

@protocol ClaimHeaderDelegate <NSObject>
-(void)claimViewController:(UIViewController*)claimVC willProcessClaimSplit:(ClaimHeader*)claimHeader statusID:(int)statusID withKeyForUpdatableDate:(NSString *)keyForUpdatableDate statusDesc:(NSString *)description approversNote:(NSString *)approversNote processMessage:(NSString*)msg appDelegate:(AppDelegate*)appDelegate;
@end

@interface VCClaimsForApproval : UITableViewController
@property (nonatomic, weak) VCClaimsForApprovalList *parentVC;
@end
