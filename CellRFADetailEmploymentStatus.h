//
//  CellRFADetailEmploymentStatus.h
//  Salt
//
//  Created by Rick Royd Aban on 10/2/15.
//  Copyright © 2015 Applus Velosi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CellRFADetailEmploymentStatus : UITableViewCell

@property (strong, nonatomic) IBOutlet UILabel *propFIeldTimeBase;
@property (strong, nonatomic) IBOutlet UILabel *propFieldEmploymentType;
@property (strong, nonatomic) IBOutlet UILabel *propFieldHrsPrWk;
@property (strong, nonatomic) IBOutlet UIImageView *propCboxIsPositionMaybecomePermanent;
@end
