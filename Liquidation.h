//
//  Liquidation.h
//  Salt
//
//  Created by Rick Royd Aban on 2015/07/29.
//  Copyright (c) 2015年 Applus Velosi. All rights reserved.
//

#import "ClaimHeader.h"
#import "BusinessAdvance.h"

@interface Liquidation : ClaimHeader

- (ClaimHeader *)initWithCostCenterID:(int)costCenterID costCenterName:(NSString *)costCenterName claimTypeID:(int)claimTypeID isPaidByCC:(BOOL)isPaidByCC baIDCharged:(int)baIDCharged bacNumber:(NSString *)bacNumber appDelegate:(AppDelegate *)appDelegate;

- (int)propBusinessAdvanceID;
- (NSString *)propBACNumber;
- (float)propForDeductionAmount;
- (float)propForPaymentAmount;

@end
