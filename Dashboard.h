//
//  Dashboard.h
//  Salt
//
//  Created by Rick Royd Aban on 1/19/16.
//  Copyright © 2016 Applus Velosi. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Dashboard : NSObject

- (Dashboard *)initWithDictionary:(NSDictionary *)jsonDashboard;

- (int)propAdvanceApproval;
- (int)propAdvanceApprovedByAccounts;
- (int)propAdvanceApprovedByCM;
- (int)propAdvanceApprovedByManager;
- (int)propAdvanceLiquidated;
- (int)propAdvanceOpen;
- (int)propAdvancePaid;
- (int)propAdvancePayment;
- (int)propAdvanceSubmitted;
- (int)propCapexApproval;
- (int)propClaimApproval;
- (int)propClaimApprovedByAccounts;
- (int)propClaimApprovedByManager;
- (int)propClaimOpen;
- (int)propClaimPaid;
- (int)propClaimPayment;
- (int)propClaimSubmitted;
- (int)propLeaveApproval;
- (int)propLiquidationApproval;
- (int)propLiquidationApprovedByAccounts;
- (int)propLiquidationApprovedByManager;
- (int)propLiquidationLiquidated;
- (int)propLiquidationOpen;
- (int)propLiquidationSubmitted;
- (int)propPendingBT;
- (int)propPendingHospitalization;
- (int)propPendingSL;
- (int)propPendingUnpaid;
- (int)propPendingVL;
- (int)propRecruitmentApproval;
- (float)propRemSL;
- (float)propRemVL;
- (int)propUsedBT;
- (int)propUsedHospitalization;
- (int)propUsedSL;
- (int)propUsedUnpaid;
- (int)propUsedVL;

@end
