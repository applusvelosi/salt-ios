//
//  ClaimItem.m
//  Salt
//
//  Created by Rick Royd Aban on 2015/07/29.
//  Copyright (c) 2015年 Applus Velosi. All rights reserved.
//

#import "ClaimItem.h"
#import "ClaimHeader.h"
#import "OnlineGateway.h"
#import "ClaimItemAttendee.h"

@implementation ClaimItem

+ (NSString *)KEY_CATEGORYID{
    return @"CategoryID";
}

+ (NSString *)KEY_CATEGORYTYPEID{
    return @"CategoryTypeID";
}

+ (NSString *)KEY_ATTENDEE_REQUIREMENT{
    return @"Attendee";
}

+ (NSString *)KEY_SPENDLIMIT{
    return @"SpendLimit";
}

//Constructor for fetching claim Items from web service
- (instancetype)initWithJSONClaimItem:(NSDictionary *)claimItemDict JSONCategory:(NSDictionary *)categoryDict appDelegate:(AppDelegate *)appDelegate{
    if(self = [super init]){
        //item specific data
        _claimItem = [NSMutableDictionary dictionaryWithDictionary:claimItemDict];
        
        [self setAmountLocal:[[_claimItem objectForKey:@"AmountInLC"] floatValue]];
        [self setAmountForeing:[[_claimItem objectForKey:@"Amount"] floatValue]];
        [self setAttachment:[_claimItem objectForKey:@"Attachment"]];
        [self setAttachmentNote:[_claimItem objectForKey:@"AttachmentNote"]];
        [self setAttendees:[_claimItem objectForKey:@"Attendees"]];
        [self setCategoryID:[[_claimItem objectForKey:@"CategoryID"] intValue]];
        [self setCategoryName:[_claimItem objectForKey:@"CategoryName"]];
        [self setClaimID:[[_claimItem objectForKey:@"ClaimID"] intValue]];
        [self setClaimLineItemID:[[_claimItem objectForKey:@"ClaimLineItemID"] intValue]];
        [self setClaimLineItemNumber:[_claimItem objectForKey:@"ClaimLineItemNumber"]];
        [self setClaimStatus:[[_claimItem objectForKey:@"ClaimStatus"] intValue]];
        [self setCompanyToChargeID:[[_claimItem objectForKey:@"CompanyToChargeID"] intValue]];
        [self setCompanyToChargeName:([_claimItem objectForKey:@"CompanyToChargeName"] == [NSNull null])?@"":[_claimItem objectForKey:@"CompanyToChargeName"]];
        [self setCostCenterName:[_claimItem objectForKey:@"CostCenterName"]];
        [self setCurrency:[[_claimItem objectForKey:@"Currency"] intValue]];
        [self setForeignCurrency:[_claimItem objectForKey:@"CurrencyName"]];
        [self setDateApprovedByApprover:[appDelegate.propGatewayOnline deserializeJsonDateString:[_claimItem objectForKey:@"DateApprovedByApprover"]]];
        [self setDateApprovedByCM:[appDelegate.propGatewayOnline deserializeJsonDateString:[_claimItem objectForKey:@"DateApprovedByDirector"]]];
        [self setDateCancelled:[appDelegate.propGatewayOnline deserializeJsonDateString:[_claimItem objectForKey:@"DateCancelled"]]];
        [self setDateRejected:[appDelegate.propGatewayOnline deserializeJsonDateString:[_claimItem objectForKey:@"DateRejected"]]];
        [self setDesc:[_claimItem objectForKey:@"Description"]];
        [self setExchangeRate:[[_claimItem objectForKey:@"ExchangeRate"] floatValue]];
        [self setAttachmentFlag:[[_claimItem objectForKey:@"HasAttachment"] intValue]];
        [self setRechargable:[[_claimItem objectForKey:@"IsRechargable"] boolValue]];
        [self setTaxRate:[[_claimItem objectForKey:@"IsTaxRate"] boolValue]];
        [self setWithReceipt:[[_claimItem objectForKey:@"IsWithReceipt"] boolValue]];
        [self setLocalCurrency:[[_claimItem objectForKey:@"LocalCurrency"] intValue]];
        [self setLocalCurrencyName:[_claimItem objectForKey:@"LocalCurrencyName"]];
        [self setMileage:[[_claimItem objectForKey:@"Mileage"] floatValue]];
        [self setMileageFrom:[_claimItem objectForKey:@"MileageFrom"]];
        [self setMileageRate:[[_claimItem objectForKey:@"MileageRate"] floatValue]];
        [self setMileageReturn:[[_claimItem objectForKey:@"MileageReturn"] boolValue]];
        [self setMileageTo:[_claimItem objectForKey:@"MileageTo"]];
        [self setMileageType:[[_claimItem objectForKey:@"MileageType"] intValue]];
        [self setModifiedBy:[[_claimItem objectForKey:@"ModifiedBy"] intValue]];
        [self setNotes:[_claimItem objectForKey:@"Notes"]];
        [self setProjectCodeID:[[_claimItem objectForKey:@"ProjectCodeID"] intValue]];
        [self setProjectName:([_claimItem objectForKey:@"ProjectName"] == [NSNull null])?@"":[_claimItem objectForKey:@"ProjectName"]];
        [self setRejectedBy:[[_claimItem objectForKey:@"RejectedBy"] intValue]];
        [self setRejectedByName:[_claimItem objectForKey:@"RejectedByName"]];
        [self setSapTaxCode:[_claimItem objectForKey:@"SapTaxCode"]];
        [self setServiceOrderCode:[_claimItem objectForKey:@"ServiceOrderCode"]];
        [self setStaffID:[[_claimItem objectForKey:@"StaffID"] intValue]];
        [self setStaffName:[_claimItem objectForKey:@"StaffName"]];
        [self setStatusName:[_claimItem objectForKey:@"StatusName"]];
        [self setStandardExRate:[[_claimItem objectForKey:@"StdExchangeRate"] floatValue]];
        [self setTaxAmount:[[_claimItem objectForKey:@"TaxAmount"] floatValue]];
        [self setWbsCode:[_claimItem objectForKey:@"WBSCode"]];
        
        [_claimItem setObject:[categoryDict objectForKey:[ClaimItem KEY_ATTENDEE_REQUIREMENT]] forKey:@"Attendee"];
        [_claimItem setObject:[categoryDict objectForKey:[ClaimItem KEY_CATEGORYTYPEID]] forKey:@"CategoryTypeID"];
        [_claimItem setObject:[categoryDict objectForKey:[ClaimItem KEY_SPENDLIMIT]] forKey:@"SpendLimit"];
        [_claimItem setObject:[_claimItem objectForKey:@"Attendees"] forKey:@"Attendees"];
        if([[_claimItem objectForKey:@"IsWithReceipt"] boolValue])
            [_claimItem setObject:[_claimItem objectForKey:@"Attachment"] forKey:@"Attachment"];
        [_claimItem setObject:[appDelegate.propGatewayOnline deserializeJsonDateString:[_claimItem objectForKey:@"DateCreated"]] forKey:@"DateCreated"];
        [_claimItem setObject:[appDelegate.propGatewayOnline deserializeJsonDateString:[_claimItem objectForKey:@"DateApprovedByApprover"]] forKey:@"DateApprovedByApprover"];
        [_claimItem setObject:[appDelegate.propGatewayOnline deserializeJsonDateString:[_claimItem objectForKey:@"DateApprovedByDirector"]] forKey:@"DateApprovedByDirector"];
        [_claimItem setObject:[appDelegate.propGatewayOnline deserializeJsonDateString:[_claimItem objectForKey:@"DateModified"]] forKey:@"DateModified"];
        [_claimItem setObject:[appDelegate.propGatewayOnline deserializeJsonDateString:[_claimItem objectForKey:@"DateCancelled"]] forKey:@"DateCancelled"];
        [_claimItem setObject:[appDelegate.propGatewayOnline deserializeJsonDateString:[_claimItem objectForKey:@"ExpenseDate"]] forKey:@"ExpenseDate"];
        [_claimItem setObject:[appDelegate.propGatewayOnline deserializeJsonDateString:[_claimItem objectForKey:@"DateRejected"]] forKey:@"DateRejected"];
        [_claimItem setObject:[appDelegate.propGatewayOnline deserializeJsonDateString:[_claimItem objectForKey:@"DatePaid"]] forKey:@"DatePaid"];
    }
    return self;
}

- (instancetype)initWithDictionary:(NSDictionary *)dict{
    if(self = [super init]){
        _claimItem = [[NSMutableDictionary alloc] initWithDictionary:dict];
    }
    
    return self;
}

- (instancetype)initWithClaimHeader:(ClaimHeader *)claimHeader{
    if(self = [super init]){
        _claimItem = [NSMutableDictionary dictionary];
        [_claimItem setObject:[NSNumber numberWithBool:true] forKey:@"Active"];
        [_claimItem setObject:[NSNumber numberWithInt:0] forKey:@"Amount"];
        [_claimItem setObject:[NSNumber numberWithInt:0] forKey:@"AmountInLC"];
        [_claimItem setObject:[NSNumber numberWithInt:0] forKey:@"CategoryID"];
        [_claimItem setObject:@"" forKey:@"CategoryName"];
        [_claimItem setObject:[NSNumber numberWithInt:[claimHeader propClaimID]] forKey:@"ClaimID"];
        [_claimItem setObject:[NSNumber numberWithInt:0] forKey:@"ClaimLineItemID"];
        [_claimItem setObject:@"" forKey:@"ClaimLineItemNumber"];
        [_claimItem setObject:[NSNumber numberWithInt:CLAIMSTATUSID_OPEN] forKey:@"ClaimStatus"] ;
        [_claimItem setObject:[NSNumber numberWithInt:0] forKey:@"CompanyToChargeID"];
        [_claimItem setObject:@"" forKey:@"CompanyToChargeName"];
        [_claimItem setObject:[claimHeader propCostCenterName] forKey:@"CostCenterName"];
        [_claimItem setObject:[NSNumber numberWithInt:0] forKey:@"Currency"];
        [_claimItem setObject:@"" forKey:@"CurrencyName"];
        [_claimItem setObject:@"/Date(-2208988800000+0000)/" forKey:@"DateApprovedByApprover"];
        [_claimItem setObject:@"/Date(-2208988800000+0000)/" forKey:@"DateApprovedByDirector"];
        [_claimItem setObject:@"/Date(-2208988800000+0000)/" forKey:@"DateCancelled"];
        [_claimItem setObject:@"/Date(-2208988800000+0000)/" forKey:@"DateCreated"];
        [_claimItem setObject:@"/Date(-2208988800000+0000)/" forKey:@"DateModified"];
        [_claimItem setObject:@"/Date(-2208988800000+0000)/" forKey:@"DatePaid"];
        [_claimItem setObject:@"/Date(-2208988800000+0000)/" forKey:@"DateRejected"];
        [_claimItem setObject:@"" forKey:@"Description"];
        [_claimItem setObject:[NSNumber numberWithFloat:0] forKey:@"ExchangeRate"];
        [_claimItem setObject:@"\"/Date(-2208988800000+0000)/\"" forKey:@"ExpenseDate"];
        [_claimItem setObject:[NSNumber numberWithBool:false] forKey:@"IsRechargable"];
        [_claimItem setObject:[NSNumber numberWithBool:false] forKey:@"IsTaxRate"];
        [_claimItem setObject:[NSNumber numberWithBool:false] forKey:@"IsWithReceipt"];
        [_claimItem setObject:[NSNumber numberWithInt:0] forKey:@"LocalCurrency"];
        [_claimItem setObject:@"" forKey:@"LocalCurrencyName"];
        [_claimItem setObject:[NSNumber numberWithFloat:0] forKey:@"Mileage"];
        [_claimItem setObject:@"" forKey:@"MileageFrom"];
        [_claimItem setObject:[NSNumber numberWithFloat:0] forKey:@"MileageRate"];
        [_claimItem setObject:[NSNumber numberWithBool:NO] forKey:@"MileageReturn"];
        [_claimItem setObject:@"" forKey:@"MileageTo"];
        [_claimItem setObject:[NSNumber numberWithInt:0] forKey:@"MileageType"];
        [_claimItem setObject:[NSNumber numberWithInt:0] forKey:@"ModifiedBy"];
        [_claimItem setObject:@"" forKey:@"Notes"];
        [_claimItem setObject:[NSNumber numberWithInt:0] forKey:@"ProjectCodeID"];
        [_claimItem setObject:@"" forKey:@"ProjectName"];
        [_claimItem setObject:[NSNumber numberWithInt:0] forKey:@"RejectedBy"];
        [_claimItem setObject:@"" forKey:@"RejectedByName"];
        [_claimItem setObject:[NSNumber numberWithInt:claimHeader.propStaffID] forKey:@"StaffID"];
        [_claimItem setObject:claimHeader.propStaffName forKey:@"StaffName"];
        [_claimItem setObject:CLAIMSTATUSDESC_OPEN forKey:@"StatusName"];
        [_claimItem setObject:[NSNumber numberWithFloat:0] forKey:@"StdExchangeRate"];
        [_claimItem setObject:[NSNumber numberWithFloat:0] forKey:@"TaxAmount"];
        [_claimItem setObject:@"[]" forKey:@"Attachment"];
        [_claimItem setObject:@"[]" forKey:@"Attendees"];
    }
    return self;
}

- (NSDictionary *)savableDictionary{
    return _claimItem;
}

- (NSDictionary *)savableDictionaryWithAppDelegate:(AppDelegate *)appDelegate {
    NSMutableDictionary *tempDic = [NSMutableDictionary dictionaryWithDictionary:_claimItem];
    [tempDic setObject:@([self claimID]) forKey:@"ClaimID"];
    [tempDic setObject:@([self statusID]) forKey:@"ClaimStatus"];
    [tempDic setObject:[appDelegate.propGatewayOnline convertToUnixDate:[appDelegate.propDateFormatByProcessed dateFromString:[tempDic objectForKey:@"DateApprovedByApprover"]]] forKey:@"DateApprovedByApprover"];
    [tempDic setObject:[appDelegate.propGatewayOnline convertToUnixDate:[appDelegate.propDateFormatByProcessed dateFromString:[tempDic objectForKey:@"DateApprovedByDirector"]]] forKey:@"DateApprovedByDirector"];
    [tempDic setObject:[appDelegate.propGatewayOnline convertToUnixDate:[appDelegate.propDateFormatByProcessed dateFromString:[tempDic objectForKey:@"DateCancelled"]]] forKey:@"DateCancelled"];
    [tempDic setObject:[appDelegate.propGatewayOnline convertToUnixDate:[appDelegate.propDateFormatByProcessed dateFromString:[tempDic objectForKey:@"DateCreated"]]] forKey:@"DateCreated"];
    [tempDic setObject:[appDelegate.propGatewayOnline convertToUnixDate:[appDelegate.propDateFormatByProcessed dateFromString:[tempDic objectForKey:@"DateModified"]]] forKey:@"DateModified"];
    [tempDic setObject:[appDelegate.propGatewayOnline convertToUnixDate:[appDelegate.propDateFormatByProcessed dateFromString:[tempDic objectForKey:@"DatePaid"]]] forKey:@"DatePaid"];
    [tempDic setObject:[appDelegate.propGatewayOnline convertToUnixDate:[appDelegate.propDateFormatByProcessed dateFromString:[tempDic objectForKey:@"DateRejected"]]] forKey:@"DateRejected"];
    [tempDic setObject:[appDelegate.propGatewayOnline convertToUnixDate:[appDelegate.propDateFormatByProcessed dateFromString:[tempDic objectForKey:@"ExpenseDate"]]] forKey:@"ExpenseDate"];
    return tempDic;
}

- (NSString *)jsonForWebServices:(AppDelegate*)appDelegate {
    NSMutableDictionary *tempDict = [NSMutableDictionary dictionaryWithDictionary:self.claimItem];
    [tempDict setObject:[appDelegate.propGatewayOnline convertToUnixDate:[appDelegate.propDateFormatByProcessed dateFromString:[tempDict objectForKey:@"DateCreated"]]] forKey:@"DateCreated"];
    [tempDict setObject:[appDelegate.propGatewayOnline convertToUnixDate:[appDelegate.propDateFormatByProcessed dateFromString:[tempDict objectForKey:@"DateApprovedByApprover"]]] forKey:@"DateApprovedByApprover"];
    [tempDict setObject:[appDelegate.propGatewayOnline convertToUnixDate:[appDelegate.propDateFormatByProcessed dateFromString:[tempDict objectForKey:@"DateApprovedByDirector"]]] forKey:@"DateApprovedByDirector"];
    [tempDict setObject:[appDelegate.propGatewayOnline convertToUnixDate:[appDelegate.propDateFormatByProcessed dateFromString:[tempDict objectForKey:@"DateModified"]]] forKey:@"DateModified"];
    [tempDict setObject:[appDelegate.propGatewayOnline convertToUnixDate:[appDelegate.propDateFormatByProcessed dateFromString:[tempDict objectForKey:@"DateCancelled"]]] forKey:@"DateCancelled"];
    [tempDict setObject:[appDelegate.propGatewayOnline convertToUnixDate:[appDelegate.propDateFormatByProcessed dateFromString:[tempDict objectForKey:@"ExpenseDate"]]] forKey:@"ExpenseDate"];
    [tempDict setObject:[appDelegate.propGatewayOnline convertToUnixDate:[appDelegate.propDateFormatByProcessed dateFromString:[tempDict objectForKey:@"DateRejected"]]] forKey:@"DateRejected"];
    [tempDict setObject:[appDelegate.propGatewayOnline convertToUnixDate:[appDelegate.propDateFormatByProcessed dateFromString:[tempDict objectForKey:@"DatePaid"]]] forKey:@"DatePaid"];
    
    return [[NSString alloc] initWithData:[NSJSONSerialization dataWithJSONObject:tempDict options:0 error:nil] encoding:NSUTF8StringEncoding];
}

- (NSString *)propAttachmentNote{
    return ([_claimItem objectForKey:@"AttachmentNote"] == [NSNull null])?@"":[_claimItem objectForKey:@"AttachmentNote"];
}

- (int)propStatusID{
    return [[_claimItem objectForKey:@"ClaimStatus"] intValue];
}

- (NSString *)propStatusName {
    return [ClaimHeader statusDescForStatusKey:[self statusID]];
}

- (int)propCategoryTypeID{
    return [[_claimItem  objectForKey:@"CategoryTypeID"] intValue];
}

- (float)propCategorySpendLimit{
    return [[_claimItem objectForKey:@"SpendLimit"] floatValue];
}

- (int)propItemID{
    return [[_claimItem objectForKey:@"ClaimLineItemID"] intValue];
}

- (NSString *)propItemNumber{
    return [_claimItem objectForKey:@"ClaimLineItemNumber"];
}

- (int)propProjectCodeID{
    return [[_claimItem objectForKey:@"ProjectCodeID"] intValue];
}

- (NSString *)propProjectName{
    return ([_claimItem objectForKey:@"ProjectName"] == [NSNull null]) ? @"" : [_claimItem objectForKey:@"ProjectName"];
}

- (NSString *)propProjectCode {
    return ([_claimItem objectForKey:@"WBSCode"] == [NSNull null])?@"":[_claimItem objectForKey:@"WBSCode"];
}

- (int)propCategoryID{
    return [[_claimItem objectForKey:@"CategoryID"] intValue];
}

- (NSString *)propCategoryName{
    return ([_claimItem objectForKey:@"CategoryName"] == [NSNull null]) ? @"" : [_claimItem objectForKey:@"CategoryName"];
}

- (float)propForeignAmount{
    return [[_claimItem objectForKey:@"Amount"] floatValue];
}

- (float)propLocalAmount{
    return [[_claimItem objectForKey:@"AmountInLC"] floatValue];
}

- (float)propTaxedAmount{
    return [[_claimItem objectForKey:@"TaxAmount"] floatValue];
}

- (float)propOnFileExchangeRate{ //standard exchange rate
    return [[_claimItem objectForKey:@"StdExchangeRate"] floatValue];
}

- (float)propOnActualConversionExchangeRate{ //actual exchange rate
    return [[_claimItem objectForKey:@"ExchangeRate"] floatValue];
}

#pragma mark -
#pragma mark === Helper Method ===
#pragma mark -
-(NSString *)formatDateToBeViewed:(NSString *)dateFromStr appDelegate:(AppDelegate *)app{
    NSInteger janOneTwoThousand = 946684800000/1000; //Jan 1, 2000 GMT in epoch time
    NSDate *jan1 = [NSDate dateWithTimeIntervalSince1970:janOneTwoThousand];
    NSDate *dateFromString = [app.propDateFormatByProcessed dateFromString:dateFromStr];
    if ([dateFromString compare:jan1] == NSOrderedDescending || [dateFromString compare:jan1] == NSOrderedSame) //Date Before Jan 1, 2000 must not be viewed
        return [app.propDateFormatByProcessed stringFromDate:dateFromString];
    return @"";
}

- (NSString *)propDateExpensedWithAppDelegate:(AppDelegate *)appdelegate{
    return [self formatDateToBeViewed:[_claimItem objectForKey:@"ExpenseDate"] appDelegate:appdelegate];
}
- (NSString *)propDateApprovedbyApproverWithAppDelegate:(AppDelegate *)appdelegate {
    return [self formatDateToBeViewed:[_claimItem objectForKey:@"DateApprovedByApprover"] appDelegate:appdelegate];
}
- (NSString *)propDateApprovedbyDirectorWithAppDelegate:(AppDelegate *)appdelegate {
    return [self formatDateToBeViewed:[_claimItem objectForKey:@"DateApprovedByDirector"] appDelegate:appdelegate];
}
- (NSString *)propDateCancelledWithAppDelegate:(AppDelegate *)appdelegate {
    return [self formatDateToBeViewed:[_claimItem objectForKey:@"DateCancelled"] appDelegate:appdelegate];
}
- (NSString *)propDateCreatedWithAppDelegate:(AppDelegate *)appdelegate {
    return [self formatDateToBeViewed:[_claimItem objectForKey:@"DateCreated"] appDelegate:appdelegate];
}
- (NSString *)propDateModifiedWithAppDelegate:(AppDelegate *)appdelegate {
    return [self formatDateToBeViewed:[_claimItem objectForKey:@"DateModified"] appDelegate:appdelegate];
}
- (NSString *)propDatePaidWithAppDelegate:(AppDelegate *)appdelegate {
    return [self formatDateToBeViewed:[_claimItem objectForKey:@"DatePaid"] appDelegate:appdelegate];
}
- (NSString *)propDateRejecteddWithAppDelegate:(AppDelegate *)appdelegate {
    return [self formatDateToBeViewed:[_claimItem objectForKey:@"DateRejected"] appDelegate:appdelegate];
}

- (void)updatableDate:(ClaimItemUpdatableDate)date withAppDelegate:(AppDelegate*) appDelegate {
    [self setDateApprovedByApprover:[appDelegate.propGatewayOnline convertToUnixDate:[appDelegate.propDateFormatByProcessed dateFromString:[self dateApprovedByApprover]]]];
    [self setDateApprovedByCM:[appDelegate.propGatewayOnline convertToUnixDate:[appDelegate.propDateFormatByProcessed dateFromString:[self dateApprovedByCM]]]];
    [self setDateCancelled:[appDelegate.propGatewayOnline convertToUnixDate:[appDelegate.propDateFormatByProcessed dateFromString:[self dateCancelled]]]];
    [self setDateRejected:[appDelegate.propGatewayOnline convertToUnixDate:[appDelegate.propDateFormatByProcessed dateFromString:[self dateRejected]]]];
    switch (date) {
        case ClaimItemUpdatableDateApprovedByApprover:
            [self setDateApprovedByApprover:[appDelegate.propGatewayOnline epochizeDate:[NSDate date]]];
            break;
        case ClaimItemUpdatableDateApprovedByCm:
            [self setDateApprovedByCM:[appDelegate.propGatewayOnline epochizeDate:[NSDate date]]];
            break;
        case ClaimItemUpdatableDateCancelled:
            [self setDateCancelled:[appDelegate.propGatewayOnline epochizeDate:[NSDate date]]];
            break;
        case ClaimItemUpdatableDateRejected:
            [self setDateRejected:[appDelegate.propGatewayOnline epochizeDate:[NSDate date]]];
            [self setRejectedBy:[[appDelegate getStaff] propStaffID]];
            [self setRejectedByName:[[appDelegate getStaff] propFullName]];
            break;
        default:
            break;
    }
}

- (int)propForeignCurrencyID{
    return [[_claimItem objectForKey:@"Currency"] intValue];
}

- (NSString *)propForeignCurrencyName{
    return [_claimItem objectForKey:@"CurrencyName"];
}

- (int)propLocalCurrencyID{
    return [[_claimItem objectForKey:@"LocalCurrency"] intValue];
}

- (NSString *)propLocalCurrencyName{
    return [_claimItem objectForKey:@"LocalCurrencyName"];
}

- (int)propCompanyChargeToID{
    return [[_claimItem objectForKey:@"CompanyToChargeID"] intValue];
}

- (NSString *)propCompanyChargeToName{
    return ([_claimItem objectForKey:@"CompanyToChargeName"] == [NSNull null]) ? @"" : [_claimItem objectForKey:@"CompanyToChargeName"];
}

- (BOOL)propHasReceipt{
    return [[_claimItem objectForKey:@"IsWithReceipt"] boolValue];
}

- (NSMutableArray *)propAttendees{
    NSMutableArray *ret = [NSMutableArray array];
    for(NSDictionary *attendee in [_claimItem objectForKey:@"Attendees"]){
        [ret addObject:[[ClaimItemAttendee alloc] initWithName:[attendee objectForKey:@"FullName"] jobTitle:[attendee objectForKey:@"JobTitle"] desc:[attendee objectForKey:@"Notes"]]];
    }
    return ret;
}

- (id)propAttachments{
    NSMutableArray *attachment = [NSMutableArray array];
    NSArray *jsonAttachments  = [_claimItem objectForKey:@"Attachment"];
    if ([self hasReceipt]) {
        if([jsonAttachments count] > 0) {
            for(NSDictionary *jsonAttachment in jsonAttachments)
                [attachment addObject:[[Document alloc] initWithDictionary:jsonAttachment]];
            return attachment;
        }else
            return nil;
    }else
        return nil;
}

- (void)propAmount:(float)amount {
    [_claimItem setObject:[NSNumber numberWithFloat:amount] forKey:@"Amount"];
}

- (void)propAmounLC:(float)amountLC {
    [_claimItem setObject:[NSNumber numberWithFloat:amountLC] forKey:@"AmountInLC"];
}

- (void)propCategory:(ClaimCategory *)category {
    [_claimItem setObject:[NSNumber numberWithInt:[category propCategoryID]] forKey:@"CategoryID"];
    [_claimItem setObject:[NSNumber numberWithInt:[category propAttendeeTypeID]] forKey:@"Attendee"];
    [_claimItem setObject:[NSNumber numberWithInt:[category propCategoryTypeID]] forKey:@"CategoryTypeID"];
    [_claimItem setObject:[NSNumber numberWithFloat:[category propSpendLimit]] forKey:@"SpendLimit"];
    [_claimItem setObject:[category propName] forKey:@"Description"];
}

- (void)propCompanyChargeTo:(Office *)companyToCharge {
    if(companyToCharge == nil){
        [_claimItem setObject:[NSNumber numberWithInt:0] forKey:@"CompanyToChargeID"];
        [_claimItem setObject:@"" forKey:@"CompanyToChargeName"];
    }else{
        [_claimItem setObject:[NSNumber numberWithInt:[companyToCharge officeID]] forKey:@"CompanyToChargeID"];
        [_claimItem setObject:[companyToCharge officeName] forKey:@"CompanyToChargeName"];
    }
}

- (void)propCurrency:(Currency *)currency {
    [_claimItem setObject:[NSNumber numberWithInt:[currency propCurrID]] forKey:@"Currency"];
    [_claimItem setObject:[currency propCurrencyThree] forKey:@"CurrencyName"];
}
//- (NSString*)propDescription {
//    return [_claimItem objectForKey:@"Description"];
//}
- (void)propForex:(float)forex{ [_claimItem setObject:[NSNumber numberWithFloat:forex] forKey:@"ExchangeRate"]; }

- (void)propExpenseDate:(NSString *)expenseDate {
    [_claimItem setObject:expenseDate forKey:@"ExpenseDate"];
}

- (void)propIsRechargeable:(BOOL)isRechargeable {
    [_claimItem setObject:[NSNumber numberWithBool:isRechargeable] forKey:@"IsRechargable"];
}

- (void)propIsTaxRated:(BOOL)isTaxRated {
    [_claimItem setObject:[NSNumber numberWithBool:isTaxRated] forKey:@"IsTaxRate"];
};

- (void)propHasReceipt:(BOOL)hasRecreipt {
    [_claimItem setObject:[NSNumber numberWithBool:hasRecreipt] forKey:@"IsWithReceipt"];
}

- (void)propLocalCurrency:(Currency *)localCurrency{
    [_claimItem setObject:[NSNumber numberWithInt:[localCurrency propCurrID]] forKey:@"LocalCurrency"];
    [_claimItem setObject:[localCurrency propCurrencyThree] forKey:@"LocalCurrencyName"];
}

- (void)propNotes:(NSString *)notes{ [_claimItem setObject:notes forKey:@"Notes"]; }
- (void)propProject:(Project *)project{
    [_claimItem setObject:[NSNumber numberWithInt:[project propID]] forKey:@"ProjectCodeID"];
    [_claimItem setObject:[project propName] forKey:@"ProjectName"];
}
- (void)propTaxAmount:(float)amount{ [_claimItem setObject:[NSNumber numberWithFloat:amount] forKey:@"TaxAmount"]; }

- (void)propDateCreated:(NSString *)dateCreated{
    [_claimItem setObject:dateCreated forKey:@"DateCreated"];
}

- (void)addAttachment:(Document *)document{
    [_claimItem setObject:[NSNumber numberWithBool:YES] forKey:@"IsWithReceipt"];
    [_claimItem setObject:[NSString stringWithFormat:@"[%@]", [document jsonObject]] forKey:@"Attachment"];
}

- (void)clearAndUpdateAttendees:(NSMutableArray *)attendees{
    NSMutableString *jsonAttendees = [NSMutableString string];
    [jsonAttendees appendString:@"["];
    for(int i=0; i<attendees.count; i++){
        [jsonAttendees appendFormat:@"%@",[[attendees objectAtIndex:i] jsonize]];
         if(i < attendees.count-1)
         [jsonAttendees appendString:@","];
    }
    [jsonAttendees appendString:@"]"];
         [_claimItem setObject:jsonAttendees forKey:@"Attendees"];
}

- (NSString *)jsonizeWithAppDelegate:(AppDelegate *)appDelegate {
    NSMutableDictionary *tempDict = [NSMutableDictionary dictionaryWithDictionary:self.claimItem];
    [tempDict setObject:[appDelegate.propGatewayOnline convertToUnixDate:[appDelegate.propDateFormatByProcessed dateFromString:[tempDict objectForKey:@"DateCreated"]]] forKey:@"DateCreated"];
    [tempDict setObject:[appDelegate.propGatewayOnline convertToUnixDate:[appDelegate.propDateFormatByProcessed dateFromString:[tempDict objectForKey:@"DateApprovedByApprover"]]] forKey:@"DateApprovedByApprover"];
    [tempDict setObject:[appDelegate.propGatewayOnline convertToUnixDate:[appDelegate.propDateFormatByProcessed dateFromString:[tempDict objectForKey:@"DateApprovedByDirector"]]] forKey:@"DateApprovedByDirector"];
    [tempDict setObject:[appDelegate.propGatewayOnline convertToUnixDate:[appDelegate.propDateFormatByProcessed dateFromString:[tempDict objectForKey:@"DateModified"]]] forKey:@"DateModified"];
    [tempDict setObject:[appDelegate.propGatewayOnline convertToUnixDate:[appDelegate.propDateFormatByProcessed dateFromString:[tempDict objectForKey:@"DateCancelled"]]] forKey:@"DateCancelled"];
    [tempDict setObject:[appDelegate.propGatewayOnline convertToUnixDate:[appDelegate.propDateFormatByProcessed dateFromString:[tempDict objectForKey:@"ExpenseDate"]]] forKey:@"ExpenseDate"];
    [tempDict setObject:[appDelegate.propGatewayOnline convertToUnixDate:[appDelegate.propDateFormatByProcessed dateFromString:[tempDict objectForKey:@"DateRejected"]]] forKey:@"DateRejected"];
    [tempDict setObject:[appDelegate.propGatewayOnline convertToUnixDate:[appDelegate.propDateFormatByProcessed dateFromString:[tempDict objectForKey:@"DatePaid"]]] forKey:@"DatePaid"];
    return  [[NSString alloc] initWithData:[NSJSONSerialization dataWithJSONObject:tempDict options:0 error:nil] encoding:NSUTF8StringEncoding];
}

- (NSString *)jsonForWebServicesWithAppDelegate:(AppDelegate*)appDelegate {
    
    NSMutableString *json = [NSMutableString string];
    [json appendString:@"{"];
    [json appendFormat:@"\"Active\": %@,", ([[_claimItem objectForKey:@"Active"] boolValue])?@"true":@"false"];
    [json appendFormat:@"\"Amount\": %.1f,", [self amountForeing]];
    [json appendFormat:@"\"AmountInLC\": %.1f,", [self amountLocal]];
    NSString *jsonizeAttachment = [[NSString alloc] initWithData:[NSJSONSerialization dataWithJSONObject:[self attachment] options:0 error:nil] encoding:NSUTF8StringEncoding];
    [json appendFormat:@"\"Attachment\": %@,", jsonizeAttachment];
    [json appendFormat:@"\"AttachmentNote\":\"%@\",", [self attachmentNote]];
    NSString *jsonizeAttendees = [[NSString alloc] initWithData:[NSJSONSerialization dataWithJSONObject:[self attendees] options:0 error:nil] encoding:NSUTF8StringEncoding];
    [json appendFormat:@"\"Attendees\": %@,", jsonizeAttendees];
    [json appendFormat:@"\"CategoryID\": %d,", [self categoryID]];
    [json appendFormat:@"\"CategoryName\":\"%@\",", [_claimItem objectForKey:@"CategoryName"]];
    [json appendFormat:@"\"ClaimID\": %d,", [[_claimItem objectForKey:@"ClaimID"] intValue]];
    [json appendFormat:@"\"ClaimLineItemID\":%d,", [[_claimItem objectForKey:@"ClaimLineItemID"] intValue]];
    [json appendFormat:@"\"ClaimLineItemNumber\":\"%@\",", [_claimItem objectForKey:@"ClaimLineItemNumber"]];
    [json appendFormat:@"\"ClaimStatus\": %d,", [self statusID]];
    [json appendFormat:@"\"CompanyToChargeID\" :%d,", [[_claimItem objectForKey:@"CompanyToChargeID"] intValue]];
    [json appendFormat:@"\"CompanyToChargeName\":\"%@\",", [self companyToChargeName]];
    [json appendFormat:@"\"CostCenterName\":\"%@\",", [_claimItem objectForKey:@"CostCenterName"]];
    [json appendFormat:@"\"Currency\": %d,", [[_claimItem objectForKey:@"Currency"] intValue]];
    [json appendFormat:@"\"CurrencyName\":\"%@\",", [_claimItem objectForKey:@"CurrencyName"]];
    [json appendFormat:@"\"DateApprovedByApprover\":\"%@\",", [self dateApprovedByApprover]];
    [json appendFormat:@"\"DateApprovedByDirector\":\"%@\",", [self dateApprovedByCM]];
    [json appendFormat:@"\"DateRejected\":\"%@\",", [self dateRejected]];
    [json appendFormat:@"\"DateCancelled\":\"%@\",", [self dateCancelled]];
    [json appendFormat:@"\"DatePaid\":\"%@\",", [appDelegate.propGatewayOnline convertToUnixDate:[appDelegate.propDateFormatByProcessed dateFromString:[_claimItem objectForKey:@"DatePaid"]]]];
    [json appendFormat:@"\"DateCreated\":\"%@\",", [appDelegate.propGatewayOnline convertToUnixDate:[appDelegate.propDateFormatByProcessed dateFromString:[_claimItem objectForKey:@"DateCreated"]]]];
    [json appendFormat:@"\"ExpenseDate\":\"%@\",", [appDelegate.propGatewayOnline convertToUnixDate:[appDelegate.propDateFormatByProcessed dateFromString:[_claimItem objectForKey:@"ExpenseDate"]]]];
    [json appendFormat:@"\"DateModified\":\"%@\",", [appDelegate.propGatewayOnline epochizeDate:[NSDate date]]];
    [json appendFormat:@"\"Description\":\"%@\",", [_claimItem objectForKey:@"Description"]];
    [json appendFormat:@"\"ExchangeRate\": %.1f,", [[_claimItem objectForKey:@"ExchangeRate"] floatValue]];
    [json appendFormat:@"\"HasAttachment\": %d,", [self attachmentFlag]];
    [json appendFormat:@"\"IsRechargable\": %@,", ([[_claimItem objectForKey:@"IsRechargable"] boolValue])?@"true":@"false"];
    [json appendFormat:@"\"IsTaxRate\": %@,", ([[_claimItem objectForKey:@"IsTaxRate"] boolValue])?@"true":@"false"];
    [json appendFormat:@"\"IsWithReceipt\": %@,", ([[_claimItem objectForKey:@"IsWithReceipt"] boolValue])?@"true":@"false"];
    [json appendFormat:@"\"LocalCurrency\": %d,", [[_claimItem objectForKey:@"LocalCurrency"] intValue]];
    [json appendFormat:@"\"LocalCurrencyName\":\"%@\",", [_claimItem objectForKey:@"LocalCurrencyName"]];
    [json appendFormat:@"\"Mileage\": %.1f,", [[_claimItem objectForKey:@"Mileage"] floatValue]];
    [json appendFormat:@"\"MileageFrom\":\"%@\",", [_claimItem objectForKey:@"MileageFrom"]];
    [json appendFormat:@"\"MileageRate\":%.1f,", [[_claimItem objectForKey:@"MileageRate"] floatValue]];
    [json appendFormat:@"\"MileageReturn\":%@,", ([self hasMileageReturn])?@"true":@"false"];
    [json appendFormat:@"\"MileageTo\":\"%@\",", [_claimItem objectForKey:@"MileageTo"]];
    [json appendFormat:@"\"MileageType\": %d,", [[_claimItem objectForKey:@"MileageType"] intValue]];
    [json appendFormat:@"\"ModifiedBy\": %d,", [self modifiedBy]];
    [json appendFormat:@"\"Notes\":\"%@\",", [self notes]];
    [json appendFormat:@"\"ProjectCodeID\":%d,", [[_claimItem objectForKey:@"ProjectCodeID"] intValue]];
    [json appendFormat:@"\"ProjectName\":\"%@\",", [self projectName]];
    [json appendFormat:@"\"RejectedBy\":%d,", [self rejectedBy]];
    [json appendFormat:@"\"RejectedByName\":\"%@\",", [self rejectedByName]];
    [json appendFormat:@"\"SapTaxCode\":\"%@\",",[self sapTaxCode]];
    [json appendFormat:@"\"ServiceOrderCode\":\"%@\",",[self serviceOrderCode]];
    [json appendFormat:@"\"StaffID\":%d,", [[_claimItem objectForKey:@"StaffID"] intValue]];
    [json appendFormat:@"\"StaffName\":\"%@\",", [_claimItem objectForKey:@"StaffName"]];
    [json appendFormat:@"\"StatusName\":\"%@\",", [self statusName]];
    [json appendFormat:@"\"StdExchangeRate\": %.1f,", [[_claimItem objectForKey:@"StdExchangeRate"] floatValue]];
    [json appendFormat:@"\"TaxAmount\": %.1f,", [self taxAmount]];
    [json appendFormat:@"\"WBSCode\":\"%@\"",[self wbsCode]];
    [json appendString:@"}"];
    return json;
}

+ (NSString *)JsonFromNewEmptyClaimHeader{
    NSMutableString *json = [NSMutableString string];
    [json appendString:@"{"];
    [json appendString:@"\"Active\": true"];
    [json appendString:@",\"Amount\": 0"];
    [json appendString:@",\"AmountInLC\": 0"];
    [json appendString:@",\"Attachment\": []"];
    [json appendString:@",\"AttachmentNote\":\"\""];
    [json appendString:@",\"Attendees\": []"];
    [json appendString:@",\"CategoryID\": 0"];
    [json appendString:@",\"CategoryName\": \"\""];
    [json appendString:@",\"ClaimID\": 0"];
    [json appendString:@",\"ClaimLineItemID\": 0"];
    [json appendString:@",\"ClaimLineItemNumber\": \"\""];
    [json appendString:@",\"ClaimStatus\": 0"];
    [json appendString:@",\"CompanyToChargeID\": 0"];
    [json appendString:@",\"CompanyToChargeName\": \"\""];
    [json appendString:@",\"CostCenterName\": \"\""];
    [json appendString:@",\"Currency\": 0"];
    [json appendString:@",\"CurrencyName\": \"\""];
    [json appendString:@",\"DateApprovedByApprover\": \"/Date(-2208988800000+0000)/\""];
    [json appendString:@",\"DateApprovedByDirector\": \"/Date(-2208988800000+0000)/\""];
    [json appendString:@",\"DateCancelled\": \"/Date(-2208988800000+0000)/\""];
    [json appendString:@",\"DateCreated\": \"/Date(-2208988800000+0000)/\""];
    [json appendString:@",\"DateModified\": \"/Date(-2208988800000+0000)/\""];
    [json appendString:@",\"DatePaid\": \"/Date(-2208988800000+0000)/\""];
    [json appendString:@",\"DateRejected\": \"/Date(-2208988800000+0000)/\""];
    [json appendString:@",\"Description\": \"\""];
    [json appendString:@",\"ExchangeRate\": 0.0"];
    [json appendString:@",\"ExpenseDate\": \"/Date(-2208988800000+0000)/\""];
    [json appendString:@",\"HasAttachment\": 3"];
    [json appendString:@",\"IsRechargable\": false"];
    [json appendString:@",\"IsTaxRate\": false"];
    [json appendString:@",\"IsWithReceipt\": false"];
    [json appendString:@",\"LocalCurrency\": 0"];
    [json appendString:@",\"LocalCurrencyName\": \"\""];
    [json appendString:@",\"Mileage\": 0.0"];
    [json appendString:@",\"MileageFrom\": \"\""];
    [json appendString:@",\"MileageRate\": 0"];
    [json appendString:@",\"MileageReturn\": false"];
    [json appendString:@",\"MileageTo\": \"\""];
    [json appendString:@",\"MileageType\": 0"];
    [json appendString:@",\"ModifiedBy\": 0"];
    [json appendString:@",\"Notes\": \"\""];
    [json appendString:@",\"ProjectCodeID\": 0"];
    [json appendString:@",\"ProjectName\": \"\""];
    [json appendString:@",\"SapTaxCode\": \"\""];
    [json appendString:@",\"ServiceOrderCode\": \"\""];
    [json appendString:@",\"RejectedBy\": 0"];
    [json appendString:@",\"RejectedByName\": \"\""];
    [json appendString:@",\"StaffID\": 0"];
    [json appendString:@",\"StaffName\": \"\""];
    [json appendString:@",\"StatusName\": \"\""];
    [json appendString:@",\"StdExchangeRate\": 0.0"];
    [json appendString:@",\"TaxAmount\": 0.0"];
    [json appendString:@",\"WBSCode\": \"\""];
    [json appendString:@"}"];
    return json;
}

@end
