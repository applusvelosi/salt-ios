//
//  HomeOverviewItem.h
//  Salt iOS
//
//  Created by Rick Royd Aban on 5/20/15.
//  Copyright (c) 2015 Rick Royd Aban. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface HomeOverviewItem : NSObject

typedef enum{
    HomeOverviewItem_HEADER,
    HomeOverviewItem_ITEM1,
    HomeOverviewItem_ITEM2
}HomeOverviewItemCellType;

- (HomeOverviewItem *)initWithName:(NSString *)name label1:(NSString *)label1 label2:(NSString *)label2 label3:(NSString *)label3 cellType:(HomeOverviewItemCellType)cellType;

- (NSString *)propName;
- (NSString *)propLabel1;
- (NSString *)propLabel2;
- (NSString *)propLabel3;
- (HomeOverviewItemCellType)propCellType;

@end
