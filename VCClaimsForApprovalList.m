//
//  VCClaimsForApprovalList.m
//  Salt
//
//  Created by Rick Royd Aban on 02/05/2017.
//  Copyright © 2017 Applus Velosi. All rights reserved.
//

#import "VCClaimsForApprovalList.h"
#import "VCClaimsForApproval.h"

@interface VCClaimsForApprovalList ()

@end

@implementation VCClaimsForApprovalList

- (void)viewDidLoad {
    [super viewDidLoad];
}

-(void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [self reloadClaimsForApprovalList];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"embedVCClaims"]) {
        VCClaimsForApproval *destVC = segue.destinationViewController;
        destVC.parentVC = self;
    }
}

- (void)reloadClaimsForApprovalList {
    [AppDelegate showGlogalHUDWithView:self.view];
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
        id result = [self.propAppDelegate.propGatewayOnline claimsForApproval];
        dispatch_async(dispatch_get_main_queue(), ^{
            [AppDelegate hideGlogalHUDWithView:self.view];
            if([result isKindOfClass:[NSString class]])
                [self.delegate loadFailedWithError:result];
            else
                [self.propAppDelegate updateClaimsForApproval:result];
            [self.delegate loadFinished];
        });
        
    });
}


#pragma mark -
#pragma mark - Event Handler
#pragma mark -
- (IBAction)toggleList:(id)sender {
    [self.propAppDelegate.propSlider toggleSidebar];
}


@end
