//
//  OnlineGateway.h
//  Salt iOS
//
//  Created by Rick Royd Aban on 5/18/15.
//  Copyright (c) 2015 Rick Royd Aban. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
@class AppDelegate;
@class ClaimItem;

#define GET 1
#define POST 2

@interface OnlineGateway : NSObject

- (OnlineGateway *)initWithAppDelegate:(AppDelegate *)appDelegate;
- (id)makeWebServiceCall:(NSString*)url requestMethod:(int)requestMethod postBody:(NSString*)jsonString;
- (NSString *)epochizeDate:(NSDate *)date;
- (NSString *)deserializeJsonDateString: (NSString *)jsonDateString;
- (NSString *)deserializeUnixDate: (NSString *)jsonDateString;
- (NSString *)authenticateUsername:(NSString *)username password:(NSString *)password;
- (id)initializeDataWithStaffID:(int)staffID securityLevel:(int)securityLevel officeID:(int)officeID;
- (NSString *)updateAppStaffDataWithStaffID:(int)staffID securityLevel:(int)securityLevel officeID:(int)officeID;
- (id)allOffices;
- (id)allProjectsForCostCenterID:(int)costCenterID;
- (id)allCostCentersForThisOffice;
- (id)dashboard;
- (id)projectsByCostCenterID:(int)costCenterID;
- (id)myLeaves;
- (id)leavesForApproval;
- (NSString *)convertToUnixLeaveDate:(NSDate*)date;
- (NSString *)followupLeave:(int)leaveID;
//holidays
- (id)localHolidays;
- (id)monthlyholidays;
- (id)weeklyholiday;
//claims
- (id)myClaimsWithStaffID:(int)staffID requestingPerson:(int)requestingPersonID;
- (id)claimsForApproval;
- (id)claimItemsForClaimID:(int)claimID;
- (id)claimItemCategoriesByOffice;
- (id)giftsAndHospitalitiesForApproval;
- (id)giftHeaderForID:(int)giftHeaderID;
- (id)contractsTenderForApproval;
- (id)contractHeaderForId:(int)contractheaderId;
- (id)recruitmentsForApproval;
- (id)recruitmentForID:(int)recruitmentID;
- (id)capexesForApproval;
- (id)capexHeaderForID:(int)capexheaderID;
- (id)capexLineItemsForCapexID:(int)capexID;
- (id)capexLineItemQoutationForLineItemID:(int)lineItemID;
- (id)claimByID:(int)claimID andStaffID:(int)staffID;
- (NSString *)convertToUnixDate:(NSDate *)date;
- (NSString *)saveLeaveWithNewLeaveJSON:(NSString *)newLeaveJSON oldLeaveJSON:(NSString *)oldLeaveJSON;
- (NSString *)processLeaveJSON:(int)leaveID forStatusID:(int)statusID;
- (id)saveClaimWithNewClaimHeaderJSON:(NSString *)newClaimHeaderJSON oldClaimHeaderID:(int)oldClaimID document:(NSString *)doc base64File:(NSString *)base64;
-(NSString *)processClaimForApprovalWithClaimId:(int)claimId claimStatusId:(int)statusId approversId:(int)approversId approversNote:(NSString*)note;
- (NSString *)saveCapexHeader:(NSString *)newCapexHeaderJSON oldCapexHeaderJSON:(NSString *)oldCapexHeaderJSON;
- (NSString *)saveRecruitment:(NSString *)newRecruitmentJSON oldRecruitmentJSON:(NSString *)oldRecruitmentJSON;
- (NSString *)saveGift:(NSString *)newGiftJSON oldGiftJSON:(NSString *)oldGiftJSON;
- (NSString *)saveContract:(NSString *)newContractJSON oldContractId:(int)contractId;
- (id)saveNewClaimItemJSON:(NSString *)newClaimItemJSON oldClaimItemID:(int)oldClaimItemID document:(NSString *)doc base64:(NSString *)base64;

- (id)forexRateFrom:(NSString *)currFrom to:(NSString *)currTo;
- (NSString *)encodeURLString:(NSString*)url;
@end
