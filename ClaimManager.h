//
//  ClaimHeaderSplitter.h
//  Salt
//
//  Created by Rick Royd Aban on 16/02/2017.
//  Copyright © 2017 Applus Velosi. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "VCClaimsForApproval.h"

@interface ClaimManager : NSObject <ClaimHeaderDelegate>

@end
