//
//  VCCapexesForApproval.h
//  Salt
//
//  Created by Rick Royd Aban on 10/5/15.
//  Copyright © 2015 Applus Velosi. All rights reserved.
//

#import "VCPage.h"
#import "LoaderDelegate.h"

@interface VCCapexesForApproval : VCPage

@property(nonatomic, weak) id<LoaderDelegate> delegate;
- (void)reloadCapexesForApproval;
@end
