//
//  VCContractForApprovalList.h
//  Salt
//
//  Created by Rick Royd Aban on 09/06/2017.
//  Copyright © 2017 Applus Velosi. All rights reserved.
//

#import "VCPage.h"
#import "LoaderDelegate.h"

@interface VCContractForApprovalList : VCPage

@property (nonatomic, weak) id<LoaderDelegate> delegate;

- (void)reloadContractsForApprovalList;

@end
