//
//  DSCurrencyList.h
//  Salt
//
//  Created by Rick Royd Aban on 2015/09/10.
//  Copyright (c) 2015年 Applus Velosi. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DSCurrencyList : NSObject

- (DSCurrencyList *)initCurrencyList;

- (NSArray *)propCurrencies;
- (NSArray *)propCurrencyNames;
- (NSArray *)propCurrencyThrees;

@end
