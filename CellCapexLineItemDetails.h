//
//  CellCapexForApprovalDetailSupplier.h
//  Salt
//
//  Created by Rick Royd Aban on 10/16/15.
//  Copyright © 2015 Applus Velosi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CellCapexLineItemDetails : UITableViewCell

@property (strong, nonatomic) IBOutlet UILabel *propDesc;
@property (strong, nonatomic) IBOutlet UILabel *propCategory;
@property (strong, nonatomic) IBOutlet UILabel *propQuantity;
@property (strong, nonatomic) IBOutlet UILabel *propCurrency;
@property (strong, nonatomic) IBOutlet UILabel *propUnitCost;
@property (strong, nonatomic) IBOutlet UILabel *propLC;
@property (strong, nonatomic) IBOutlet UILabel *propLocalCurrencyAmount;
@property (strong, nonatomic) IBOutlet UILabel *propUSDRate;
@property (strong, nonatomic) IBOutlet UILabel *propAmountInUSD;
@property (strong, nonatomic) IBOutlet UILabel *propUsefulLife;

@end
