//
//  CostCenter.h
//  Salt
//
//  Created by Rick Royd Aban on 2/2/16.
//  Copyright © 2016 Applus Velosi. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CostCenter : NSObject

- (CostCenter *)initWithID:(int)costCenterID name:(NSString *)costCenterName;
- (int)propCostCenterID;
- (NSString *)propCostCenterName;

@end
