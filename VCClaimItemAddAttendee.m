//
//  VCClaimItemAddAttendee.m
//  Salt
//
//  Created by Rick Royd Aban on 2/29/16.
//  Copyright © 2016 Applus Velosi. All rights reserved.
//

#import "VCClaimItemAddAttendee.h"
#import "ClaimItemAttendee.h"

@interface VCClaimItemAddAttendee (){
    
    IBOutlet UITextField *_propFieldName;
    IBOutlet UITextField *_propFieldJob;
    IBOutlet UITextView *_propFieldNotes;
}

@end

@implementation VCClaimItemAddAttendee

- (void)viewDidLoad {
    [super viewDidLoad];
}

- (IBAction)done:(id)sender {
    [_propClaimItemAttendees addObject:[[ClaimItemAttendee alloc] initWithName:_propFieldName.text jobTitle:_propFieldJob.text desc:_propFieldNotes.text]];
    [self.navigationController popViewControllerAnimated:YES];
}
@end
