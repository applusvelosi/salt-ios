//
//  VCClaimItemCategorySelection.h
//  Salt
//
//  Created by Rick Royd Aban on 2015/09/08.
//  Copyright (c) 2015年 Applus Velosi. All rights reserved.
//

#import "VCPage.h"
#import "ClaimHeader.h"

@interface VCClaimItemCategorySelection : VCPage<UITableViewDataSource, UITableViewDelegate>

@property (strong, nonatomic) ClaimHeader *propClaimHeader;

@end
