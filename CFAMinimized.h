//
//  CFAMinimized.h
//  Salt
//
//  Created by Rick Royd Aban on 2015/08/26.
//  Copyright (c) 2015年 Applus Velosi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CFAMinimized : UITableViewCell

@property (strong, nonatomic) IBOutlet UILabel *propFieldType;
@property (strong, nonatomic) IBOutlet UILabel *propFieldStatus;
@property (strong, nonatomic) IBOutlet UILabel *propFieldTotal;

@end
