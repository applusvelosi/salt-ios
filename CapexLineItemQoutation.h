//
//  CapexLineItemQoutation.h
//  Salt
//
//  Created by Rick Royd Aban on 10/7/15.
//  Copyright © 2015 Applus Velosi. All rights reserved.
//

#define CAPEX_DELIMITER @"~"

#import <Foundation/Foundation.h>
#import "OnlineGateway.h"

@interface CapexLineItemQoutation : NSObject

- (CapexLineItemQoutation *)initWithDictionary:(NSDictionary *)jsonCapexLineItemQoutation onlineGateway:(OnlineGateway *)onlineGateway;
-(CapexLineItemQoutation *)initWithData;

- (float)propAmount;
- (float)propAmountInUSD;
- (int)propCapexLineItemID;
- (int)propCapexLineItemQoutationID;
- (int)propCurrencyID;
- (NSString *)propCurrencyThree;
- (NSMutableArray *)propDocuments;
- (float)propExchangeRate;
- (int)propFinancingSchemeID;
- (NSString *)propFinancingSchemenName;
- (BOOL)propIsPrimary;
- (NSString *)propNote;
- (NSString *)propPaymentTerm;
- (NSString *)propSupplierName;

@end
