//
//  VCGiftsHospitalityForApprovalList.m
//  Salt
//
//  Created by Rick Royd Aban on 02/05/2017.
//  Copyright © 2017 Applus Velosi. All rights reserved.
//

#import "VCGiftsHospitalityForApprovalList.h"
#import "VCGiftsHospitalityForApproval.h"

@interface VCGiftsHospitalityForApprovalList ()

@end

@implementation VCGiftsHospitalityForApprovalList

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

-(void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [self reloadGiftsForApprovalList];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"embedVCGifts"]) {
        VCGiftsHospitalityForApproval *destVC = segue.destinationViewController;
        destVC.parentVC = self;
    }
}

-(void)reloadGiftsForApprovalList {
    [AppDelegate showGlogalHUDWithView:self.view];
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
        id result = [self.propAppDelegate.propGatewayOnline giftsAndHospitalitiesForApproval];
        dispatch_async(dispatch_get_main_queue(), ^{
            [AppDelegate hideGlogalHUDWithView:self.view];
            if([result isKindOfClass:[NSString class]])
                [self.delegate loadFailedWithError:result];
            else
                [self.propAppDelegate updateGiftsForApproval:result];
            [self.delegate loadFinished];
        });
        
    });

}

#pragma mark -
#pragma mark - Event Handler
#pragma mark -
- (IBAction)toggleList:(id)sender {
    [self.propAppDelegate.propSlider toggleSidebar];
}

@end
