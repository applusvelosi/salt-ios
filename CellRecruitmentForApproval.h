//
//  CellRecruitmentForApproval.h
//  Salt
//
//  Created by Rick Royd Aban on 9/30/15.
//  Copyright © 2015 Applus Velosi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CellRecruitmentForApproval : UITableViewCell

@property (strong, nonatomic) IBOutlet UILabel *propFieldApplicantName;
@property (strong, nonatomic) IBOutlet UILabel *propFieldPositionType;
@property (strong, nonatomic) IBOutlet UILabel *propFieldOfficeName;
@property (strong, nonatomic) IBOutlet UILabel *propFieldStatus;

@end
