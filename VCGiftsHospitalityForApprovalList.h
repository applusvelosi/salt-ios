//
//  VCGiftsHospitalityForApprovalList.h
//  Salt
//
//  Created by Rick Royd Aban on 02/05/2017.
//  Copyright © 2017 Applus Velosi. All rights reserved.
//

#import "VCPage.h"
#import "LoaderDelegate.h"

@interface VCGiftsHospitalityForApprovalList : VCPage
@property(nonatomic, weak) id<LoaderDelegate> delegate;

-(void)reloadGiftsForApprovalList;
@end
