//
//  VCHome.m
//  Salt iOS
//
//  Created by Rick Royd Aban on 5/19/15.
//  Copyright (c) 2015 Rick Royd Aban. All rights reserved.
//

#import "VCHome.h"
#import "VCHomeLeaves.h"
#import "VCHomeClaims.h"
#import "VCHomeAdvances.h"
#import "VelosiColors.h"
#import "Dashboard.h"
#import "HomeOverviewItem.h"
#import "CellHomeHeader.h"
#import "CellHomeItem1.h"
#import "CellHomeItem2.h"

@interface VCHome ()<UITableViewDelegate, UITableViewDataSource>{
    UIBarButtonItem *_buttonRefresh, *_buttonNewLeaveRequest; /**_buttonNewClaimHeader;*/
    
    IBOutlet UILabel *propLabelName;
    IBOutlet UILabel *propLabelToday;
    IBOutlet UIView *approvalView;
    IBOutlet UILabel *_labelForApproval;
    IBOutlet UILabel *cntLeavesForApproval;
    IBOutlet UILabel *labelLeavesForApproval;
    IBOutlet UIView *separator1;
    IBOutlet UILabel *cntClaimsForApproval;
    IBOutlet UILabel *labelClaimsForApproval;
    IBOutlet UIView *separator2;
    IBOutlet UILabel *cntRcmtsForApproval;
    IBOutlet UILabel *labelRcmtForApproval;
    IBOutlet UIView *separator3;
    IBOutlet UILabel *cntCapexForApproval;
    IBOutlet UILabel *labelCapexForApproval;
    IBOutlet UITableView *propTable;
    
    UITapGestureRecognizer *_leavesForApprovalTap, *_claimsForApprovalTap, *_recruitmentsForApprovalTap, *_capexForApprovalTap;
    
    UITableView *_lvLeaves;
    NSDateFormatter *_homeDateFormat;
    NSMutableArray *_items;
    MBProgressHUD *_hud;
}

@end

@implementation VCHome

- (void)viewDidLoad {
    [super viewDidLoad];

    propLabelName.text = @"";
    _homeDateFormat = [[NSDateFormatter alloc] init];
    _homeDateFormat.dateFormat = @"dd-MMM-yyyy HH:mm:ss";
    _buttonRefresh = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemRefresh target:self action:@selector(refresh)];
    _buttonNewLeaveRequest = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"actionbar_newleaverequest"] style:UIBarButtonItemStylePlain target:self action:@selector(newLeaveRequest)];
//    _buttonNewClaimHeader = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"actionbar_newclaim"] style:UIBarButtonItemStylePlain target:self action:@selector(newClaimHeader)];
    _items = [[NSMutableArray alloc] init];
    propTable.delegate = self;
    propTable.dataSource = self;
    
    self.navigationItem.rightBarButtonItems = @[_buttonRefresh,/* _buttonNewClaimHeader,*/ _buttonNewLeaveRequest];
    if(![[self.propAppDelegate getStaff] isUser]){ //current user is an approver
        _leavesForApprovalTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onLeavesForApprovalButtonTapped)];
        _claimsForApprovalTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onClaimsForApprovalButtonTapped)];
        _capexForApprovalTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onCapexForApprovalButtonTapped)];
        _recruitmentsForApprovalTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onRecruitmentsForApprovalButtonTapped)];
        
        [approvalView  addGestureRecognizer:_leavesForApprovalTap];
        [cntLeavesForApproval addGestureRecognizer:_leavesForApprovalTap];
        [cntClaimsForApproval addGestureRecognizer:_claimsForApprovalTap];
        [cntCapexForApproval addGestureRecognizer:_capexForApprovalTap];
        [cntRcmtsForApproval addGestureRecognizer:_recruitmentsForApprovalTap];
    }
//    _hud = [self.propAppDelegate initializeProgressHUDWithView:self.view];
    [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(updateDateTime) userInfo:nil repeats:YES];
    
}

- (void)layoutSubviews
{
//    [super layoutSubviews];
//    self.preferredMaxLayoutWidth = self.bounds.size.width;
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self refresh];
}

- (void)onLeavesForApprovalButtonTapped{
    [self.propAppDelegate.propSlider switchToLeavesForApprovalPage];
}

- (void)onClaimsForApprovalButtonTapped{    
    [self.propAppDelegate.propSlider switchToClaimsForApprovalPage];
}

- (void)onRecruitmentsForApprovalButtonTapped{
    [self.propAppDelegate.propSlider switchToRecruitmentsForApprovalPage];
}

- (void)onCapexForApprovalButtonTapped{
    [self.propAppDelegate.propSlider switchToCapexForApprovalPage];
}

- (void)refresh{
    UIWindow *window = [[[UIApplication sharedApplication] delegate] window];
    [AppDelegate showGlogalHUDWithView:window];
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
        id result = [self.propAppDelegate.propGatewayOnline updateAppStaffDataWithStaffID:[[self.propAppDelegate getStaff] propStaffID] securityLevel:[[self.propAppDelegate getStaff] propSecurityLevel] officeID:[[self.propAppDelegate getStaff] propOfficeID]];
        
        if(result == nil)
            result = [self.propAppDelegate.propGatewayOnline dashboard];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [AppDelegate hideGlogalHUDWithView:window];
            if([result isKindOfClass:[NSString class]])
                [[[UIAlertView alloc] initWithTitle:@"" message:result delegate:nil cancelButtonTitle:@"Dismiss" otherButtonTitles:nil, nil] show];
            else{
                self.propAppDelegate.dashboard = [[Dashboard alloc] initWithDictionary:result];
                propLabelName.text =  [NSString stringWithFormat:@"%@, %@",[[self.propAppDelegate getStaff] propLastName], [[self.propAppDelegate getStaff] propFirstName]]; //[[self.propAppDelegate getStaff] propFullName];
                if([[self.propAppDelegate getStaff] isUser] || [[self.propAppDelegate getStaff] isAccount] || [[self.propAppDelegate getStaff] isAM]){
                    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:approvalView attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0 constant:0]];
                    _labelForApproval.hidden = YES;
                    cntLeavesForApproval.hidden = YES;
                    labelLeavesForApproval.hidden = YES;
                    separator1.hidden = YES;
                    cntClaimsForApproval.hidden = YES;
                    labelClaimsForApproval.hidden = YES;
                    separator2.hidden = YES;
                    cntRcmtsForApproval.hidden = YES;
                    labelRcmtForApproval.hidden = YES;
                    separator3.hidden = YES;
                    cntCapexForApproval.hidden = YES;
                    labelCapexForApproval.hidden = YES;
                }else{
                    int leaveApproval = self.propAppDelegate.dashboard.propLeaveApproval;
                    int claimApproval = self.propAppDelegate.dashboard.propClaimApproval+self.propAppDelegate.dashboard.propAdvanceApproval+self.propAppDelegate.dashboard.propLiquidationApproval;
                    int rcmtApproval = self.propAppDelegate.dashboard.propRecruitmentApproval;
                    int capexApproval = self.propAppDelegate.dashboard.propCapexApproval;

                    cntLeavesForApproval.text = [NSString stringWithFormat:@"%d",leaveApproval];
                    cntClaimsForApproval.text = [NSString stringWithFormat:@"%d",claimApproval];
                    cntRcmtsForApproval.text = [NSString stringWithFormat:@"%d",rcmtApproval];
                    cntCapexForApproval.text = [NSString stringWithFormat:@"%d",capexApproval];
                    
                    cntLeavesForApproval.textColor = (leaveApproval>0)?[VelosiColors redRejection]:[VelosiColors lightGrayDisabled];
                    cntClaimsForApproval.textColor = (claimApproval>0)?[VelosiColors redRejection]:[VelosiColors lightGrayDisabled];
                    cntRcmtsForApproval.textColor = (rcmtApproval>0)?[VelosiColors redRejection]:[VelosiColors lightGrayDisabled];
                    cntCapexForApproval.textColor = (capexApproval>0)?[VelosiColors redRejection]:[VelosiColors lightGrayDisabled];
                }
                
                Dashboard *dashboard = self.propAppDelegate.dashboard;
                [_items removeAllObjects];
                [_items addObject:[[HomeOverviewItem alloc] initWithName:@"Leaves" label1:@"Used" label2:@"Pending" label3:@"Remaining" cellType:HomeOverviewItem_HEADER]];
                [_items addObject:[[HomeOverviewItem alloc] initWithName:@"Vacation" label1:[NSString stringWithFormat:@"%d",[dashboard propUsedVL]] label2:[NSString stringWithFormat:@"%d",[dashboard propPendingVL]] label3:[NSString stringWithFormat:@"%.2f",[dashboard propRemVL]] cellType:HomeOverviewItem_ITEM1]];
                [_items addObject:[[HomeOverviewItem alloc] initWithName:@"Sick" label1:[NSString stringWithFormat:@"%d",[dashboard propUsedSL]] label2:[NSString stringWithFormat:@"%d",[dashboard propPendingSL]] label3:[NSString stringWithFormat:@"%.2f",[dashboard propRemSL]] cellType:HomeOverviewItem_ITEM2]];
                [_items addObject:[[HomeOverviewItem alloc] initWithName:@"Unpaid" label1:[NSString stringWithFormat:@"%d",[dashboard propUsedUnpaid]] label2:[NSString stringWithFormat:@"%d",[dashboard propPendingUnpaid]] label3:@"-" cellType:HomeOverviewItem_ITEM1]];
                [_items addObject:[[HomeOverviewItem alloc] initWithName:@"Hospitalization" label1:[NSString stringWithFormat:@"%d",[dashboard propUsedHospitalization]] label2:[NSString stringWithFormat:@"%d",[dashboard propPendingHospitalization]] label3:@"-" cellType:HomeOverviewItem_ITEM2]];
                [_items addObject:[[HomeOverviewItem alloc] initWithName:@"Business Trip" label1:[NSString stringWithFormat:@"%d",[dashboard propUsedBT]] label2:[NSString stringWithFormat:@"%d",[dashboard propPendingBT]] label3:@"-" cellType:HomeOverviewItem_ITEM1]];
                [_items addObject:[[HomeOverviewItem alloc] initWithName:@"Expenses" label1:@"Claims" label2:@"Advances" label3:@"Liquidation" cellType:HomeOverviewItem_HEADER]];
                [_items addObject:[[HomeOverviewItem alloc] initWithName:@"Open" label1:[NSString stringWithFormat:@"%d",[dashboard propClaimOpen]] label2:[NSString stringWithFormat:@"%d",[dashboard propAdvanceOpen]] label3:[NSString stringWithFormat:@"%d",[dashboard propLiquidationOpen]] cellType:HomeOverviewItem_ITEM1]];
                [_items addObject:[[HomeOverviewItem alloc] initWithName:@"Submitted" label1:[NSString stringWithFormat:@"%d",[dashboard propClaimSubmitted]] label2:[NSString stringWithFormat:@"%d",[dashboard propAdvanceSubmitted]] label3:[NSString stringWithFormat:@"%d",[dashboard propLiquidationSubmitted]] cellType:HomeOverviewItem_ITEM2]];
                [_items addObject:[[HomeOverviewItem alloc] initWithName:@"Approved by Manager" label1:[NSString stringWithFormat:@"%d",[dashboard propClaimApprovedByManager]] label2:[NSString stringWithFormat:@"%d",[dashboard propAdvanceApprovedByManager]] label3:[NSString stringWithFormat:@"%d",[dashboard propLiquidationApprovedByManager]] cellType:HomeOverviewItem_ITEM1]];
                [_items addObject:[[HomeOverviewItem alloc] initWithName:@"Approved by CM" label1:@"-" label2:[NSString stringWithFormat:@"%d",[dashboard propAdvanceApprovedByCM]] label3:@"-" cellType:HomeOverviewItem_ITEM2]];
                [_items addObject:[[HomeOverviewItem alloc] initWithName:@"Approved by Accounts" label1:[NSString stringWithFormat:@"%d",[dashboard propClaimApprovedByAccounts]] label2:[NSString stringWithFormat:@"%d",[dashboard propAdvanceApprovedByAccounts]] label3:[NSString stringWithFormat:@"%d",[dashboard propLiquidationApprovedByAccounts]] cellType:HomeOverviewItem_ITEM1]];
                [_items addObject:[[HomeOverviewItem alloc] initWithName:@"Paid" label1:[NSString stringWithFormat:@"%d",[dashboard propClaimPaid]] label2:[NSString stringWithFormat:@"%d",[dashboard propAdvancePaid]] label3:@"-" cellType:HomeOverviewItem_ITEM2]];
                [_items addObject:[[HomeOverviewItem alloc] initWithName:@"Liquidated" label1:@"-" label2:[NSString stringWithFormat:@"%d",[dashboard propAdvanceLiquidated]] label3:[NSString stringWithFormat:@"%d",[dashboard propLiquidationLiquidated]] cellType:HomeOverviewItem_ITEM1]];
                
                [propTable reloadData];
            }
        });
    });
}

- (void)showWeeklyHoliday{
    [self performSegueWithIdentifier:@"hometoweeklyholidays" sender:_buttonNewLeaveRequest];
}

- (void)newLeaveRequest{
    [self.navigationController pushViewController:[self.propAppDelegate.propPageNavigator vcLeaveInput] animated:YES];
}

- (void)newClaimHeader{
    NSLog(@"New Claim Header");
}

- (void)updateDateTime{
    propLabelToday.text = [_homeDateFormat stringFromDate:[NSDate date]];
}

- (IBAction)toggleSidebar:(id)sender {
    [self.propAppDelegate.propSlider toggleSidebar];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    HomeOverviewItem *item = [_items objectAtIndex:indexPath.row];
    if([item propCellType] == HomeOverviewItem_HEADER){
        CellHomeHeader *cell = (CellHomeHeader *)[tableView dequeueReusableCellWithIdentifier:@"cell_header"];
        
        cell.propLabelName.text = item.propName;
        cell.propLabel1.text = item.propLabel1;
        cell.propLabel2.text = item.propLabel2;
        cell.propLabel3.text = item.propLabel3;
        
        return cell;
    }else if([item propCellType] == HomeOverviewItem_ITEM1){
        CellHomeItem1 *cell = (CellHomeItem1 *)[tableView dequeueReusableCellWithIdentifier:@"cell_item1"];
        
        cell.propLabelName.text = item.propName;
        cell.propLabel1.text = item.propLabel1;
        cell.propLabel2.text = item.propLabel2;
        cell.propLabel3.text = item.propLabel3;
        
        return cell;
    }else{
        CellHomeItem2 *cell = (CellHomeItem2 *)[tableView dequeueReusableCellWithIdentifier:@"cell_item2"];
        
        cell.propLabelName.text = item.propName;
        cell.propLabel1.text = item.propLabel1;
        cell.propLabel2.text = item.propLabel2;
        cell.propLabel3.text = item.propLabel3;
        
        return cell;
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return _items.count;
}

@end
