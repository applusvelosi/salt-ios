//
//  VCContractForApprovalDetail.m
//  Salt
//
//  Created by Rick Royd Aban on 14/06/2017.
//  Copyright © 2017 Applus Velosi. All rights reserved.
//

#import "VCContractForApprovalDetail.h"
#import "Contract.h"
#import "VelosiColors.h"
#import "Document.h"

@interface VCContractForApprovalDetail ()<UIAlertViewDelegate, UIDocumentInteractionControllerDelegate> {
    Contract *_contract;
    int _staffId;
    NSMutableString *_approversNotes, *_stringNote;
    UITapGestureRecognizer *_attachmentTapRecognizer;
    UIDocumentInteractionController *_dic;
}

@property (weak, nonatomic) IBOutlet UILabel *propReferenceNo;
@property (weak, nonatomic) IBOutlet UILabel *propRequestor;
@property (weak, nonatomic) IBOutlet UILabel *propCompany;
@property (weak, nonatomic) IBOutlet UILabel *propRequestDate;
@property (weak, nonatomic) IBOutlet UILabel *propRequestStatus;
@property (weak, nonatomic) IBOutlet UILabel *propContractType;
@property (weak, nonatomic) IBOutlet UILabel *propServiceLines;
@property (weak, nonatomic) IBOutlet UILabel *propProjectExecutionLocation;
@property (weak, nonatomic) IBOutlet UILabel *propClient;
@property (weak, nonatomic) IBOutlet UILabel *propCompanyCurrency;
@property (weak, nonatomic) IBOutlet UILabel *propBillingCurrency;
@property (weak, nonatomic) IBOutlet UILabel *propProjectValue;
@property (weak, nonatomic) IBOutlet UILabel *propProjValEuro;
@property (weak, nonatomic) IBOutlet UILabel *propCapexRequired;
@property (weak, nonatomic) IBOutlet UILabel *propCapexEuro;
@property (weak, nonatomic) IBOutlet UILabel *propTenderSubmissionDeadline;
@property (weak, nonatomic) IBOutlet UILabel *propStartDateEst;
@property (weak, nonatomic) IBOutlet UILabel *propGrossMargin;
@property (weak, nonatomic) IBOutlet UILabel *propStatus;
@property (weak, nonatomic) IBOutlet UILabel *propAwardProbability;
@property (weak, nonatomic) IBOutlet UILabel *propProjectDuration;
@property (weak, nonatomic) IBOutlet UILabel *propIsAgentRequired;
@property (weak, nonatomic) IBOutlet UILabel *propDocument;
@property (weak, nonatomic) IBOutlet UILabel *propCm;
@property (weak, nonatomic) IBOutlet UILabel *propRm;
@property (weak, nonatomic) IBOutlet UILabel *propLm;
@property (weak, nonatomic) IBOutlet UILabel *propCfo;
@property (weak, nonatomic) IBOutlet UILabel *propEvp;

@end

@implementation VCContractForApprovalDetail

#pragma mark -
#pragma mark === View Life Cycle Management ===
#pragma mark -
- (void)viewDidLoad {
    [super viewDidLoad];
    [AppDelegate showGlogalHUDWithView:self.view];
    _staffId = [[self.propAppDelegate getStaff] propStaffID];
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        id result = [self.propAppDelegate.propGatewayOnline contractHeaderForId:self.contactId];
        dispatch_async(dispatch_get_main_queue(), ^{
            [AppDelegate hideGlogalHUDWithView:self.view];
            if ([result isKindOfClass:[NSString class]]) {
                [[[UIAlertView alloc] initWithTitle:@"" message:result delegate:self cancelButtonTitle:@"Dismiss" otherButtonTitles:nil, nil] show];
            } else {
                _contract = [[Contract alloc] initWithJSONDictionary:result withOnlineGateway:self.propAppDelegate.propGatewayOnline];
                self.propReferenceNo.text = _contract.propReferenceNumber;
                self.propRequestor.text = _contract.propRequestor;
                self.propCompany.text = _contract.propCompany;
                self.propRequestDate.text = [_contract processedDateSubmitted:self.propAppDelegate];
                self.propRequestStatus.text = _contract.propRequestStatusName;
                self.propContractType.text = [_contract getContractType:_contract.propContractType];
                self.propServiceLines.text = [_contract getServiceLine:_contract.propServiceLines];
                
                self.propProjectExecutionLocation.text = _contract.propProjectExecutionLocation;
                self.propClient.text = _contract.propEndClient;
                self.propCompanyCurrency.text = _contract.propCompanyCurrency;
                self.propBillingCurrency.text = _contract.propBillingCurrency;
                self.propProjectValue.text = [NSString stringWithFormat:@"%d", _contract.propProjectValue];
                self.propProjValEuro.text = [NSString stringWithFormat:@"%d", _contract.propProjectValueEquiv];
                self.propCapexRequired.text = [NSString stringWithFormat:@"%d", _contract.propCapexRequired];
                self.propCapexEuro.text = [NSString stringWithFormat:@"%d", _contract.propCapexRequiredEquiv];
                self.propTenderSubmissionDeadline.text = [_contract getDateTenderSubmission:self.propAppDelegate];
                self.propStartDateEst.text = [_contract getDateStart:self.propAppDelegate];
                self.propGrossMargin.text = [NSString stringWithFormat:@"%@ %%",[self.propAppDelegate.decimalFormat stringFromNumber:[NSNumber numberWithFloat:_contract.propGrossMargin]]];
                self.propStatus.text = [_contract getStatus:_contract.propStatus];
                self.propAwardProbability.text = [NSString stringWithFormat:@"%@ %%",[self.propAppDelegate.decimalFormat stringFromNumber:[NSNumber numberWithFloat:_contract.propProbabilityOfAward]]];
                self.propProjectDuration.text = [_contract getDurationRange:_contract.propProjectDuration];
                self.propIsAgentRequired.text = (_contract.propIsAgentRequired) ? @"Yes" : @"No";
                if ([[_contract propBusinessCaseDoc] count] > 0) {
                    self.propDocument.text = [[[_contract propBusinessCaseDoc] objectAtIndex:0] propDocName];
                    self.propDocument.textColor = [VelosiColors orangeVelosi];
                    _attachmentTapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onAttachmentClicked)];
                    [self.propDocument addGestureRecognizer:_attachmentTapRecognizer];
                } else {
                    self.propDocument.text = @"NO ATTACHMENT";
                }
                _dic = [[UIDocumentInteractionController alloc] init];
                _dic.delegate = self;
                self.propCm.text = _contract.propCmName;
                self.propRm.text = _contract.propRmName;
                self.propLm.text = _contract.propLmName;
                self.propCfo.text = _contract.propCfoName;
                self.propEvp.text = _contract.propEvpName;
                
                NSLog(@"Hello World");
                _approversNotes = [[NSMutableString alloc] init];
                _stringNote = [[NSMutableString alloc] init];
                
                if (_contract.propApproverNote.length > 0) {
                    NSArray *tempStr = [_contract.propApproverNote componentsSeparatedByString:@";"];
                    for (NSString *str in tempStr) {
                        [_stringNote appendString:str];
                        [_stringNote appendString:@"\n"];
                    }
                } else {
                    [_approversNotes appendString:_contract.propApproverNote];
                    [_stringNote appendString:_contract.propApproverNote];
                }
            }
        });
    });
}


#pragma mark -
#pragma mark === Helper Method ===
#pragma mark -
- (void)alertTextFieldDidChange:(UITextField *)sender {
    UIAlertController *alertController = (UIAlertController *)self.presentedViewController;
    if (alertController) {
        UITextField *noteField = alertController.textFields.firstObject;
        UIAlertAction *confirmAction = alertController.actions.lastObject;
        confirmAction.enabled = ([[noteField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]] length] == 0)?NO:YES;
    }
}

- (void)updateContractStatus:(ContractStatusId)statusId keyForUpdatableDate:(NSString *)keyForUpdatableDate withApproversNote:(NSString *)note {
//    [AppDelegate showGlogalHUDWithView:self.view];
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        id result = [self.propAppDelegate.propGatewayOnline saveContract:[_contract updateJSONGiftWithStatusId:statusId keyForUpdatableDate:keyForUpdatableDate withApproverNote:note appDelegate:self.propAppDelegate] oldContractId:_contract.propContractId];
        dispatch_async(dispatch_get_main_queue(), ^{
            [AppDelegate hideGlogalHUDWithView:self.view];
            [[[UIAlertView alloc] initWithTitle:@"" message:([result isEqualToString:@"OK"])?@"Updated Successfully":result delegate:self cancelButtonTitle:@"Dismiss" otherButtonTitles:nil, nil] show];
        });
    });
}

- (void)onAttachmentClicked {
    [AppDelegate showGlogalHUDWithView:self.tableView.superview];
    Document *doc = [[_contract propBusinessCaseDoc] objectAtIndex:0];
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
        NSString *urlStr = [NSString stringWithFormat:@"%@?dID=%d&refID=%d&obTypeID=%d", NSLocalizedString(@"ImageViewerLink", @""), [doc propDocID], [doc propRefID], [doc propObjectTypeID]];
        id urlData = [self.propAppDelegate.propGatewayOnline makeWebServiceCall:urlStr requestMethod:GET postBody:nil];
        if ( ![urlData isKindOfClass:[NSString class]]) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [AppDelegate hideGlogalHUDWithView:self.tableView.superview];
                NSString *filePath = [NSTemporaryDirectory() stringByAppendingPathComponent:[doc propDocName]];
                [urlData writeToFile:filePath atomically:YES];
                NSURL *filePathURL = [NSURL fileURLWithPath:filePath];
                _dic.URL = filePathURL;
                [_dic presentPreviewAnimated:YES];
            });
        } else {
            dispatch_async(dispatch_get_main_queue(), ^{
                [AppDelegate hideGlogalHUDWithView:self.tableView.superview];
                UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"" message:urlData preferredStyle:UIAlertControllerStyleAlert];
                UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"Dismiss" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
                    
                }];
                [alertController addAction:cancelAction];
                [self presentViewController:alertController animated:YES completion:nil];
            });
        }
        
    });
}


#pragma mark -
#pragma mark === UIDocument Interaction Controller Delegate ===
#pragma mark -
- (UIViewController *)documentInteractionControllerViewControllerForPreview:(UIDocumentInteractionController *)controller{
    return self;
}


#pragma mark -
#pragma mark === UI Event Handler Management ===
#pragma mark -
- (IBAction)approveContractRequest:(id)sender {
    NSString *alertTitle = @"Approver's Note";
    UIAlertController *approveAlertController = [UIAlertController alertControllerWithTitle:alertTitle
                                                                                    message:nil
                                                                             preferredStyle:UIAlertControllerStyleAlert];
    [approveAlertController addTextFieldWithConfigurationHandler:^(UITextField *textField) {
        textField.placeholder = @"Note";
        [textField addTarget:nil
                      action:@selector(self)
            forControlEvents:UIControlEventEditingChanged];
    }];
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"Cancel"
                                                           style:UIAlertActionStyleCancel
                                                         handler:^(UIAlertAction *action) {
                                                             NSLog(@"Canceled");
                                                         }];
    UIAlertAction *approveAction = [UIAlertAction actionWithTitle:@"Proceed"
                                                            style:UIAlertActionStyleDefault
                                                          handler:^(UIAlertAction *action) {
                                                              UITextField *approveNote = approveAlertController.textFields[0];
                                                              if (_contract.propRequestStatus == ContractStatusIdSubmitted) {
                                                                  if (approveNote.text.length > 0) {
                                                                      if (_contract.propOfficeId != 41)
                                                                          [_approversNotes appendFormat:@"CM: %@",approveNote.text];
                                                                      else
                                                                          [_approversNotes appendFormat:@"CFO: %@",approveNote.text];
                                                                  }else{
                                                                      if (_contract.propOfficeId != 41)
                                                                          [_approversNotes appendString:@"CM: Approved"];
                                                                      else
                                                                          [_approversNotes appendString:@"CFO: Approved"];
                                                                  }
                                                                  [_approversNotes appendString:@";"];
                                                                  [self updateContractStatus:ContractStatusIdApprovedByCm keyForUpdatableDate:@"DateProcessedByCM" withApproversNote:_approversNotes];
                                                              } else if (_contract.propRequestStatus == ContractStatusIdApprovedByCm && _contract.propRfmId == _staffId) {
                                                                  if (approveNote.text.length > 0) {
                                                                      [_approversNotes appendFormat:@"RFM: %@", approveNote.text];
                                                                  } else {
                                                                      [_approversNotes appendString:@"RFM: Approved"];
                                                                  }
                                                                  [_approversNotes appendString:@";"];
                                                                  [self updateContractStatus:ContractStatusIdApprovedByRfm keyForUpdatableDate:@"DateProcessedByRFM" withApproversNote:_approversNotes];
                                                              } else if ((_contract.propRequestStatus == ContractStatusIdApprovedByCm && _contract.propRfmId == 0) || _contract.propRequestStatus == ContractStatusIdApprovedByRfm) {
                                                                  if (approveNote.text.length > 0) {
                                                                      if (_contract.propOfficeId != 41)
                                                                          [_approversNotes appendFormat:@"RM: %@",approveNote.text];
                                                                      else
                                                                          [_approversNotes appendFormat:@"CEO: %@",approveNote.text];
                                                                  } else {
                                                                      if (_contract.propOfficeId != 41)
                                                                          [_approversNotes appendString:@"RM: Approved"];
                                                                      else
                                                                          [_approversNotes appendString:@"CEO: Approved"];
                                                                  }
                                                                  [_approversNotes appendString:@";"];
                                                                  [self updateContractStatus:ContractStatusIdApprovedByRm keyForUpdatableDate:@"DateProcessedByRM" withApproversNote:_approversNotes];
                                                              } else if (_contract.propRequestStatus == ContractStatusIdApprovedByRm && _contract.propProjectValueEquiv >= 500) {
                                                                  if (approveNote.text.length > 0) {
                                                                      [_approversNotes appendFormat:@"LM: %@", approveNote.text];
                                                                  } else {
                                                                      [_approversNotes appendString:@"LM: Approved"];
                                                                  }
                                                                  [_approversNotes appendString:@";"];
                                                                  [self updateContractStatus:ContractStatusIdApprovedByLm keyForUpdatableDate:@"DateProcessedByLM" withApproversNote:_approversNotes];
                                                              } else if (_contract.propRequestStatus == ContractStatusIdApprovedByLm && _contract.propProjectValueEquiv >= 500) {
                                                                  if (approveNote.text.length > 0) {
                                                                      [_approversNotes appendFormat:@"CFO: %@", approveNote.text];
                                                                  } else {
                                                                      [_approversNotes appendString:@"CFO: Approved"];
                                                                  }
                                                                  [_approversNotes appendString:@";"];
                                                                  [self updateContractStatus:ContractStatusIdApprovedByCfo keyForUpdatableDate:@"DateProcessedByCFO" withApproversNote:_approversNotes];
                                                              } else if (_contract.propRequestStatus == ContractStatusIdApprovedByCfo && _contract.propProjectValueEquiv >= 500) {
                                                                  if (approveNote.text.length > 0) {
                                                                      [_approversNotes appendFormat:@"EVP: %@", approveNote.text];
                                                                  } else {
                                                                      [_approversNotes appendString:@"EVP: Approved"];
                                                                  }
                                                                  [_approversNotes appendString:@";"];
                                                                  [self updateContractStatus:ContractStatusIdApprovedByEvp keyForUpdatableDate:@"DateProcessedByEVP" withApproversNote:_approversNotes];
                                                              }
                                                          }];
    [approveAlertController addAction:cancelAction];
    [approveAlertController addAction:approveAction];
    [self presentViewController:approveAlertController animated:YES completion:nil];
}

- (IBAction)rejectContractRequest:(id)sender {
    NSString *alertTitle = @"Reason for Rejection";
    
    UIAlertController *rejectAlertController = [UIAlertController alertControllerWithTitle:alertTitle
                                                                                   message:nil
                                                                            preferredStyle:UIAlertControllerStyleAlert];
    [rejectAlertController addTextFieldWithConfigurationHandler:^(UITextField *textField) {
        textField.placeholder = @"Required";
        [textField addTarget:self
                      action:@selector(alertTextFieldDidChange:)
            forControlEvents:UIControlEventEditingChanged];
    }];
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"Cancel"
                                                           style:UIAlertActionStyleCancel
                                                         handler:^(UIAlertAction *action) {
                                                             NSLog(@"Canceled");
                                                         }];
    UIAlertAction *rejectAction = [UIAlertAction actionWithTitle:@"Reject"
                                                           style:UIAlertActionStyleDefault
                                                         handler:^(UIAlertAction *action){
                                                             UITextField *rejectNote = rejectAlertController.textFields.firstObject;
                                                             
                                                             if (_contract.propRequestStatus == ContractStatusIdSubmitted) {
                                                                 if (_contract.propOfficeId != 41 ) //office 41 is the headquarter
                                                                     [_approversNotes appendFormat:@"CM: %@", rejectNote.text];
                                                                 else
                                                                     [_approversNotes appendFormat:@"CFO: %@", rejectNote.text];
                                                                 [_approversNotes appendString:@";"];
                                                                 [self updateContractStatus:ContractStatusIdRejectedByCm keyForUpdatableDate:@"DateProcessedByCM" withApproversNote:_approversNotes];
                                                             } else if (_contract.propRequestStatus == ContractStatusIdApprovedByCm && _contract.propRfmId == _staffId) {
                                                                 [_approversNotes appendFormat:@"RFM: %@;", rejectNote.text];
                                                                 [self updateContractStatus:ContractStatusIdApprovedByRfm keyForUpdatableDate:@"DateProcessedByRFM" withApproversNote:_approversNotes];
                                                             } else if ((_contract.propRequestStatus == ContractStatusIdApprovedByCm && _contract.propRfmId == 0) || _contract.propRequestStatus == ContractStatusIdApprovedByRfm) {
                                                                 if (_contract.propOfficeId != 41 ) //office 41 is the headquarter
                                                                     [_approversNotes appendFormat:@"RM: %@", rejectNote.text];
                                                                 else
                                                                     [_approversNotes appendFormat:@"EVP: %@", rejectNote.text];
                                                                 [_approversNotes appendString:@";"];
                                                                 [self updateContractStatus:ContractStatusIdRejectedByRm keyForUpdatableDate:@"DateProcessedByRM" withApproversNote:_approversNotes];
                                                             } else if (_contract.propRequestStatus == ContractStatusIdApprovedByRm && _contract.propProjectValueEquiv >= 500) {
                                                                 [_approversNotes appendFormat:@"LM: %@;", rejectNote.text];
                                                                 [self updateContractStatus:ContractStatusIdRejectedByLm keyForUpdatableDate:@"DateProcessedByLM" withApproversNote:_approversNotes];
                                                             } else if (_contract.propRequestStatus == ContractStatusIdApprovedByLm && _contract.propProjectValueEquiv >= 500) {
                                                                 [_approversNotes appendFormat:@"CFO: %@;", rejectNote.text];
                                                                 [self updateContractStatus:ContractStatusIdRejectedByCfo keyForUpdatableDate:@"DateProcessedByCFO" withApproversNote:_approversNotes];
                                                             } else if (_contract.propRequestStatus == ContractStatusIdApprovedByCfo && _contract.propProjectValueEquiv >= 500) {
                                                                 [_approversNotes appendFormat:@"EVP: %@;", rejectNote.text];
                                                                 [self updateContractStatus:ContractStatusIdRejectedByEvp keyForUpdatableDate:@"DateProcessedByEVP" withApproversNote:_approversNotes];
                                                             }
                                                             
                                                         }];
    rejectAction.enabled = NO;
    [rejectAlertController addAction:cancelAction];
    [rejectAlertController addAction:rejectAction];
    [self presentViewController:rejectAlertController animated:YES completion:nil];
}

#pragma mark -
#pragma mark === Alert View Delegate ===
#pragma mark -
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    //this delegate is only intended for listening button clicked after leave submission success message will be dismissed
    [self.navigationController popViewControllerAnimated:YES];
}

@end
