    //
//  VCContractForApprovalDetail.h
//  Salt
//
//  Created by Rick Royd Aban on 14/06/2017.
//  Copyright © 2017 Applus Velosi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

@interface VCContractForApprovalDetail : UITableViewController

@property (assign, nonatomic) int contactId;
@property (nonatomic, strong) AppDelegate *propAppDelegate;

@end
