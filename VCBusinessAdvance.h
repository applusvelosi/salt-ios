//
//  VCBusinessAdvance.h
//  Salt
//
//  Created by Rick Royd Aban on 2015/07/31.
//  Copyright (c) 2015年 Applus Velosi. All rights reserved.
//

#import "VCPage.h"
#import "BusinessAdvance.h"

@interface VCBusinessAdvance : UITableViewController

@property (strong, nonatomic) Claim *baLiquidation;
@property (readwrite, copy) LCSumValuesProvider lcSumValuesProvider;
@property (nonatomic, strong) AppDelegate *propAppDelegate;

@end
