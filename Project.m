//
//  Project.m
//  Salt
//
//  Created by Rick Royd Aban on 2015/09/11.
//  Copyright (c) 2015年 Applus Velosi. All rights reserved.
//

#import "Project.h"

@interface Project(){
    int _projectID;
    NSString *_projectName, *_projectCode;
}
@end

@implementation Project

- (id)initWithDictionary:(NSDictionary *)dict{
    if([super init]){
        _projectID = [[dict objectForKey:@"ProjectID"] intValue];
        _projectName = [dict objectForKey:@"ProjectName"];
        _projectCode = [dict objectForKey:@"ProjectCode"];
    }
    
    return self;
}

- (int)propID{
    return _projectID;
}

- (NSString *)propName{
    return _projectName;
}

- (NSString *)propCode{
    return _projectCode;
}

@end
