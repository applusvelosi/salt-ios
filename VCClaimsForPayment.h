//
//  VCClaimsForPayment.h
//  Salt
//
//  Created by Rick Royd Aban on 2015/07/31.
//  Copyright (c) 2015年 Applus Velosi. All rights reserved.
//

#import "VCPage.h"

@interface VCClaimsForPayment : VCPage<UITableViewDataSource, UITableViewDelegate>

@end
