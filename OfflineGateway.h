//
//  OfflineGateway.h
//  Salt iOS
//
//  Created by Rick Royd Aban on 5/22/15.
//  Copyright (c) 2015 Rick Royd Aban. All rights reserved.
//

#import <Foundation/Foundation.h>
@class Staff;
#import "Office.h"
#import "Leave.h"
#import "Claim.h"
#import "Recruitment.h"
#import "CapexHeader.h"
#import "LocalHoliday.h"
#import "Holiday.h"
#import "AppDelegate.h"
#import "GiftHospitality.h"
#import "Contract.h"

@interface OfflineGateway : NSObject

- (OfflineGateway *) initWithAppDelegate:(AppDelegate *)appDelegate;
- (void)updatePreviouslyUsedUsername:(NSString *)username;
- (BOOL)isLoggedIn;
- (void)logout;
- (NSString *)getPrevUsername;

//data serialization
- (void)serializeStaff:(Staff *)staff office:(Office *)office;
- (void)serializeMyLeaves:(NSMutableArray *)myLeaves;
- (void)serializeLeavesForApproval:(NSMutableArray *)leavesForApproval;
- (void)serializeLocalHolidays:(NSMutableArray *)localHolidays;
- (void)serializeMonthlyHolidays:(NSMutableArray *)monthlyHolidays;
- (void)serializeRecruitmentsForApproval:(NSMutableArray *)recruitmentsForApproval;
- (void)serializeCapexesForApproval:(NSMutableArray *)capexesForApproval;
- (void)serializeGiftsForApproval;
- (void)serializeContractsForApproval;
- (Staff *)deserializeStaff;
- (Office *)deserializeStaffOffice;
- (void)serializeMyClaims:(NSMutableArray *)myClaims;
- (void)serializeClaimsForApproval:(NSMutableArray *)claimsForApproval;
//- (void)serializeClaimsForPayment:(NSMutableArray *)claimsForPayment;

- (NSMutableArray *)deserializeMyLeaves;
- (NSMutableArray *)deserializeLeavesForApproval;
- (NSMutableArray *)deserializeLocalHolidays;
- (NSMutableArray *)deserializeMonthlyHolidays;
- (NSMutableArray *)deserializeMyClaims;
- (NSMutableArray *)deserializeClaimsForApproval;
//- (NSMutableArray *)deserializeClaimsForPayment;
- (NSMutableArray *)deserializeRecruitmentForApproval;
- (NSMutableArray *)deserializeCapexesForApproval;
- (NSMutableArray *)deserializeGiftsForApproval;
- (NSMutableArray *)deserializeContractsForApproval;

@end
