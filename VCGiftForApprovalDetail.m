//
//  VCGiftForApprovalDetail.m
//  Salt
//
//  Created by Rick Royd Aban on 11/05/2017.
//  Copyright © 2017 Applus Velosi. All rights reserved.
//

#import "VCGiftForApprovalDetail.h"

@interface VCGiftForApprovalDetail () <UIAlertViewDelegate> {
    GiftHospitality *_giftHeader;
    NSMutableString *_stringNotes;
}

@property (weak, nonatomic) IBOutlet UILabel *propReferenceNum;
@property (weak, nonatomic) IBOutlet UILabel *propRequestor;
@property (weak, nonatomic) IBOutlet UILabel *propCountryManager;
@property (weak, nonatomic) IBOutlet UILabel *propRegionalManager;
@property (weak, nonatomic) IBOutlet UILabel *propCEO;
@property (weak, nonatomic) IBOutlet UILabel *propApplusCSR;
@property (weak, nonatomic) IBOutlet UILabel *propStatus;
@property (weak, nonatomic) IBOutlet UILabel *propOffice;

@property (weak, nonatomic) IBOutlet UILabel *propFieldSubmitted;
@property (weak, nonatomic) IBOutlet UILabel *propFieldDateProcessedByCM;
@property (weak, nonatomic) IBOutlet UILabel *propFieldDateProcessedByRM;
@property (weak, nonatomic) IBOutlet UILabel *propFieldDateProcessedByCEO;
@property (weak, nonatomic) IBOutlet UILabel *propFieldDateProcessedByCSR;

@property (weak, nonatomic) IBOutlet UILabel *propTotalAmount;
@property (weak, nonatomic) IBOutlet UILabel *propEuroRate;
@property (weak, nonatomic) IBOutlet UILabel *propAmountInEuro;
@property (weak, nonatomic) IBOutlet UILabel *propGiftType;
@property (weak, nonatomic) IBOutlet UILabel *propIsAlreadyReceived;
@property (weak, nonatomic) IBOutlet UILabel *propDateReceived;
@property (weak, nonatomic) IBOutlet UILabel *propCountry;
@property (weak, nonatomic) IBOutlet UILabel *propCompanyName;
@property (weak, nonatomic) IBOutlet UILabel *propCity;
@property (weak, nonatomic) IBOutlet UILabel *propIsFormOfDonation;
@property (weak, nonatomic) IBOutlet UILabel *propIsGiverAPublicOfficial;

@property (weak, nonatomic) IBOutlet UILabel *propDescription;
@property (weak, nonatomic) IBOutlet UILabel *propReasonForGifts;
@property (weak, nonatomic) IBOutlet UILabel *propApproverNote;

@end

@implementation VCGiftForApprovalDetail

#pragma mark -
#pragma mark === View Life Cycle Management ===
#pragma mark -
- (void)viewDidLoad {
    [super viewDidLoad];
    [AppDelegate showGlogalHUDWithView:self.view];
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        id result = [self.propAppDelegate.propGatewayOnline giftHeaderForID:self.propGiftHeaderId];
        dispatch_async(dispatch_get_main_queue(), ^{
            [AppDelegate hideGlogalHUDWithView:self.view];
            if ([result isKindOfClass:[NSString class]]) {
                [[[UIAlertView alloc] initWithTitle:@"" message:result delegate:self cancelButtonTitle:@"Dismiss" otherButtonTitles:nil, nil] show];
            } else {
                _giftHeader = [[GiftHospitality alloc] initWithJSONDictionary:result withOnlineGateway:self.propAppDelegate.propGatewayOnline];
                self.propReferenceNum.text = _giftHeader.propReferenceNum;
                self.propRequestor.text = _giftHeader.propStaffName;
                self.propCountryManager.text = _giftHeader.propCountryManagerName;
                self.propRegionalManager.text = _giftHeader.propRegionalManagerName;
                self.propCEO.text = _giftHeader.propCeoName;
                self.propApplusCSR.text = _giftHeader.propCsrName;
                self.propStatus.text = _giftHeader.propStatusName;
                self.propOffice.text = _giftHeader.propOfficeName;
                
                self.propFieldSubmitted.text = [_giftHeader processedDateSubmitted:self.propAppDelegate];
                self.propFieldDateProcessedByCM.text = [_giftHeader processedDateByCM:self.propAppDelegate];
                self.propFieldDateProcessedByRM.text = [_giftHeader processedDateByRM:self.propAppDelegate];
                self.propFieldDateProcessedByCEO.text = [_giftHeader processedDateByCEO:self.propAppDelegate];
                self.propFieldDateProcessedByCSR.text = [_giftHeader processedDateByCSR:self.propAppDelegate];
                
                self.propTotalAmount.text = [NSString stringWithFormat:@"%@ %@", _giftHeader.propCurrencyThree, [self.propAppDelegate.decimalFormat stringFromNumber:[NSNumber numberWithFloat:_giftHeader.propAmountInLc]]];
                self.propEuroRate.text = [NSString stringWithFormat:@"%@", [NSNumber numberWithFloat:_giftHeader.propEuroRate]];
                self.propAmountInEuro.text = [NSString stringWithFormat:@"%@", [self.propAppDelegate.decimalFormat stringFromNumber:[NSNumber numberWithFloat:_giftHeader.propAmountInEuro]]];
                self.propGiftType.text = _giftHeader.propGiftType;
                self.propIsAlreadyReceived.text = (_giftHeader.propIsReceivedGiven)?@"Yes":@"No";
                self.propDateReceived.text = [_giftHeader processedDateIsReceived:self.propAppDelegate];
                self.propCountry.text = _giftHeader.propCountry;
                self.propCompanyName.text = _giftHeader.propCompanyName;
                self.propCity.text = _giftHeader.propCity;
                self.propIsFormOfDonation.text = (_giftHeader.propIsDonation)?@"Yes":@"No";
                self.propIsGiverAPublicOfficial.text = (_giftHeader.propIsPublicOfficial)?@"Yes":@"No";
                
                self.propDescription.text = _giftHeader.propDescription;
                self.propReasonForGifts.text = _giftHeader.propReason;
                self.propApproverNote.text = _giftHeader.propApproverNote;
                
                _stringNotes = [[NSMutableString alloc] init];
                NSString *tempString;
                NSScanner *stringScanner = [NSScanner scannerWithString:[_giftHeader propApproverNote]];
                NSCharacterSet *separator = [NSCharacterSet newlineCharacterSet];
                while ([stringScanner isAtEnd] == NO) {
                    if([stringScanner scanUpToCharactersFromSet:separator intoString:&tempString])
                        [_stringNotes appendString:tempString];
                    [_stringNotes appendFormat:@"\n"];
                }
            }
        });
    });
}

#pragma mark -
#pragma mark === Helper Method ===
#pragma mark -
- (void)alertTextFieldDidChange:(UITextField *)sender {
    UIAlertController *alertController = (UIAlertController *)self.presentedViewController;
    if (alertController) {
        UITextField *noteField = alertController.textFields.firstObject;
        UIAlertAction *confirmAction = alertController.actions.lastObject;
        confirmAction.enabled = ([[noteField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]] length] == 0) ? NO : YES;
    }
}

- (void)changeGiftStatus:(GiftStatusId)statusID keyForUpdatableDate:(NSString *)keyForUpdatableDate withApproversNote:(NSString *)approverNote{
    [AppDelegate showGlogalHUDWithView:self.view];
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
        id result = [self.propAppDelegate.propGatewayOnline saveGift:[_giftHeader jSONForUpdatingGiftWithStatusId:statusID keyForUpdatableDate:keyForUpdatableDate withApproverNote:approverNote appDelegate:self.propAppDelegate] oldGiftJSON:[_giftHeader jsonizeWithappDelegate:self.propAppDelegate]];
        dispatch_async(dispatch_get_main_queue(), ^{
            [AppDelegate hideGlogalHUDWithView:self.view];
            [[[UIAlertView alloc] initWithTitle:@"" message:([result isEqualToString:@"OK"])?@"Updated Successfully":result delegate:self cancelButtonTitle:@"Dismiss" otherButtonTitles:nil, nil] show];
        });
    });
}


#pragma mark -
#pragma mark === UI Event Handler Management ===
#pragma mark -
- (IBAction)approveGiftRequest:(id)sender {
    NSString *alertTitle = @"Approver's Note";
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:alertTitle
                                                                             message:nil
                                                                      preferredStyle:UIAlertControllerStyleAlert];
    
    [alertController addTextFieldWithConfigurationHandler:^(UITextField *textField)
     {
         textField.placeholder = @"Note";
         [textField addTarget:nil
                       action:@selector(self)
             forControlEvents:UIControlEventEditingChanged];
     }];
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"Cancel"
                                                           style:UIAlertActionStyleCancel
                                                         handler:^(UIAlertAction *action) {
                                                             NSLog(@"Canceled");
                                                         }];
    UIAlertAction *approveAction = [UIAlertAction actionWithTitle:@"Proceed"
                                                            style:UIAlertActionStyleDefault
                                                          handler:^(UIAlertAction *action) {
                                                              UITextField *approveNote = alertController.textFields[0];
                                                              
                                                              switch(_giftHeader.propStatusId){
                                                                  case GiftStatusIdSubmitted:
                                                                      if (approveNote.text.length > 0) {
                                                                          if (_giftHeader.propRequestorOfficeId != 41)
                                                                              [_stringNotes appendFormat:@"CM: %@",approveNote.text];
                                                                          else
                                                                              [_stringNotes appendFormat:@"CFO: %@",approveNote.text];
                                                                      }else{
                                                                          if (_giftHeader.propRequestorOfficeId != 41)
                                                                              [_stringNotes appendString:@"CM: Approved"];
                                                                          else
                                                                              [_stringNotes appendString:@"CFO: Approved"];
                                                                      }
                                                                      [_stringNotes appendString:@";"];
                                                                      [self changeGiftStatus:GiftStatusIdApprovedByCm keyForUpdatableDate:@"DateProcessedByCountryManager"
                                                                           withApproversNote:_stringNotes];
                                                                      break;
                                                                  case GiftStatusIdApprovedByCm:
                                                                      if (_giftHeader.propProcessedByRmId == _giftHeader.propProcessedByCeoId) {
                                                                          if (approveNote.text.length > 0)
                                                                              [_stringNotes insertString:[NSString stringWithFormat:@"EVP: %@;",approveNote.text] atIndex:0];
                                                                          else
                                                                              [_stringNotes insertString:@"EVP: Approved;" atIndex:0];
                                                                          [self changeGiftStatus:GiftStatusIdApprovedByCeo
                                                                             keyForUpdatableDate:@"1"
                                                                               withApproversNote:_stringNotes];
                                              
                                                                      } else {
                                                                          if (approveNote.text.length > 0)
                                                                              [_stringNotes insertString:[NSString stringWithFormat:@"RM: %@",approveNote.text] atIndex:0];
                                                                          else
                                                                              [_stringNotes insertString:@"RM: Approved;" atIndex:0];
                                                                          [self changeGiftStatus:GiftStatusIdApprovedByRm
                                                                             keyForUpdatableDate:@"DateProcessedByRegionalManager"
                                                                               withApproversNote:_stringNotes];
                                                                      }
                                                                      break;
                                                                  case GiftStatusIdApprovedByRm:
                                                                      if (approveNote.text.length > 0) {
                                                                          [_stringNotes insertString:@"\n" atIndex:0];
                                                                          [_stringNotes insertString:[NSString stringWithFormat:@"EVP: %@;",approveNote.text] atIndex:0];
                                                                      }else{
                                                                          [_stringNotes insertString:@"\n" atIndex:0];
                                                                          [_stringNotes insertString:@"EVP: Approved;" atIndex:0];
                                                                      }
                                                                      [self changeGiftStatus:GiftStatusIdApprovedByCeo
                                                                         keyForUpdatableDate:@"DateProcessedByCEO"
                                                                           withApproversNote:_stringNotes];
                                                                      break;
                                                              }
                                                              
                                                          }];
    [alertController addAction:cancelAction];
    [alertController addAction:approveAction];
    
    [self presentViewController:alertController animated:YES completion:nil];
}


- (IBAction)rejectGiftRequest:(id)sender {
    NSString *alertTitle = @"Reason for Rejection";
    
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:alertTitle
                                                                             message:nil
                                                                      preferredStyle:UIAlertControllerStyleAlert];
    [alertController addTextFieldWithConfigurationHandler:^(UITextField *textField)
     {
         textField.placeholder = @"Required";
         [textField addTarget:self
                       action:@selector(alertTextFieldDidChange:)
             forControlEvents:UIControlEventEditingChanged];
     }];
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"Cancel"
                                                           style:UIAlertActionStyleCancel
                                                         handler:^(UIAlertAction *action) {
                                                             NSLog(@"Canceled");
                                                         }];
    UIAlertAction *rejectAction = [UIAlertAction actionWithTitle:@"Reject"
                                                           style:UIAlertActionStyleDefault
                                                         handler:^(UIAlertAction *action){
                                                             UITextField *rejectNote = alertController.textFields.firstObject;
                                                             NSLog(@"Reject Text Value: %@",rejectNote.text);
                                                             
                                                             switch(_giftHeader.propStatusId){
                                                                 case GiftStatusIdSubmitted:
                                                                     if (_giftHeader.propRequestorOfficeId != 41 ) //office 41 is the headquarter
                                                                         [_stringNotes appendFormat:@"CM: %@", rejectNote.text];
                                                                     else
                                                                         [_stringNotes appendFormat:@"CFO: %@", rejectNote.text];
                                                                     [_stringNotes appendString:@";"];
                                                                     [self changeGiftStatus:GiftStatusIdRejectedByCm keyForUpdatableDate:@"DateProcessedByCountryManager" withApproversNote:_stringNotes];
                                                                     break;
                                                                 case GiftStatusIdApprovedByCm:
                                                                     [_stringNotes insertString:@"\n" atIndex:0];
                                                                     if (_giftHeader.propProcessedByRmId == _giftHeader.propProcessedByCeoId) {
                                                                         [_stringNotes appendFormat:@"CEO: %@;", rejectNote.text];
                                                                         [self changeGiftStatus:GiftStatusIdRejectedByCeo
                                                                            keyForUpdatableDate:@"DateProcessedByCEO"
                                                                              withApproversNote:_stringNotes];
                                                                     } else {
                                                                         [_stringNotes appendFormat:@"RM: %@;", rejectNote.text];
                                                                         [self changeGiftStatus:GiftStatusIdRejectedByRm
                                                                            keyForUpdatableDate:@"DateProcessedByRegionalManager"
                                                                              withApproversNote:_stringNotes];
                                                                     }
                                                                     break;
                                                                 case GiftStatusIdApprovedByRm: {
                                                                     [_stringNotes insertString:@"\n" atIndex:0];
                                                                     NSString *stringFormat = [NSString stringWithFormat:@"CEO: %@;",rejectNote.text];
                                                                     [_stringNotes insertString:stringFormat atIndex:0];
                                                                     [self changeGiftStatus:GiftStatusIdRejectedByCeo keyForUpdatableDate:@"DateProcessedByCEO" withApproversNote:_stringNotes];
                                                                     break;
                                                                 }
                                                                 default:
                                                                     break;
                                                             }
                                                         }];
    rejectAction.enabled = NO;
    [alertController addAction:cancelAction];
    [alertController addAction:rejectAction];
    
    [self presentViewController:alertController animated:YES completion:nil];
}


#pragma mark -
#pragma mark === Alert View Delegate ===
#pragma mark -
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    //this delegate is only intended for listening button clicked after leave submission success message will be dismissed
    [self.navigationController popViewControllerAnimated:YES];
}

@end
