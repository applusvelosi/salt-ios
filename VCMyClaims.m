//
//  MyClaims.m
//  Salt
//
//  Created by Rick Royd Aban on 2015/07/27.
//  Copyright (c) 2015年 Applus Velosi. All rights reserved.
//

#import "VCMyClaims.h"
#import "CellMyClaim.h"
#import "ClaimHeader.h"
#import "Claim.h"
#import "ClaimPaidByCC.h"
#import "ClaimNotPaidByCC.h"
#import "BusinessAdvance.h"
#import "Liquidation.h"
#import "VCClaimPaidByCC.h"
#import "VCClaimNotPaidByCC.h"
#import "VCBusinessAdvance.h"
#import "VCLiquidation.h"
#import "VelosiColors.h"

@interface VCMyClaims (){
    
    IBOutlet UITableView *_propLV;
    ClaimHeader *_selectedClaimHeader;
    UIBarButtonItem *_buttonRefresh;
    int _lastFirstClaimID;
}

@end

@implementation VCMyClaims


#pragma mark -
#pragma mark === View Life Cycle Management ===
#pragma mark -
- (void)viewDidLoad {
    [super viewDidLoad];
    
    _lastFirstClaimID = -1;
    _propLV.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    _propLV.dataSource = self;
    _propLV.delegate = self;
    
    _buttonRefresh = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemRefresh target:self action:@selector(refresh)];
    self.navigationItem.rightBarButtonItems = @[_buttonRefresh];
}

- (void)viewDidAppear:(BOOL)animated{
    [self refresh];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    if([sender isKindOfClass:[UITableViewCell class]]) {
        UIViewController *destVC = segue.destinationViewController;
        if([_selectedClaimHeader propTypeID] == CLAIMTYPEID_CLAIMS) {
            Claim *selectedClaim = [[Claim alloc] initWithDictionary:[[_selectedClaimHeader savableDictionary] mutableCopy]];
            if([segue.destinationViewController isKindOfClass:[VCClaimPaidByCC class]]) {
                VCClaimPaidByCC *claim = (VCClaimPaidByCC*)destVC;
                claim.claimPaidByCC = [[ClaimPaidByCC alloc] initWithDictionary:[[selectedClaim savableDictionary] mutableCopy]];
                claim.propAppDelegate = self.propAppDelegate;
            }else {
                VCClaimNotPaidByCC *claim = (VCClaimNotPaidByCC*)destVC;
                claim.claimNotPaidByCC = [[ClaimNotPaidByCC alloc] initWithDictionary:[[selectedClaim savableDictionary] mutableCopy]];
                claim.propAppDelegate = self.propAppDelegate;
            }
        } else {
            VCBusinessAdvance *baLiq = (VCBusinessAdvance*)destVC;
            baLiq.baLiquidation = [[Claim alloc] initWithDictionary:[[_selectedClaimHeader savableDictionary] mutableCopy]];
            baLiq.propAppDelegate = self.propAppDelegate;
        
        }
    }
}

#pragma mark -
#pragma mark - Table view data source delegate
#pragma mark -
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    ClaimHeader *myClaim = [[self.propAppDelegate myClaims] objectAtIndex:indexPath.row];
    CellMyClaim *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    cell.propFieldClaimNumber.text = [myClaim propClaimNumber];
    cell.propFieldDateSubmitted.text = [myClaim propDateCreated:self.propAppDelegate];

    NSString *statusName;
    int statusID = [myClaim propStatusID];
    statusName = [ClaimHeader statusDescForStatusKey:statusID];
    
    if(statusID==CLAIMSTATUSID_APPROVEDBYACCOUNTS || statusID==CLAIMSTATUSID_APPROVEDBYAPPROVER || statusID==CLAIMSTATUSID_APPROVEDBYCM || statusID==CLAIMSTATUSID_LIQUIDATED || statusID==CLAIMSTATUSID_PAID || statusID==CLAIMSTATUSID_PAIDUNDERCOMPANYCARD)
        cell.propFieldStatus.textColor = [VelosiColors greenAcceptance];
    else if(statusID==CLAIMSTATUSID_REJECTEDBYACCOUNTS || statusID==CLAIMSTATUSID_REJECTEDBYAPPROVER || statusID==CLAIMSTATUSID_REJECTEDBYCM || statusID==CLAIMSTATUSID_REJECTEDFORSALARYDEDUCTION || statusID==CLAIMSTATUSID_RETURN || statusID == CLAIMSTATUSID_CANCELLED)
        cell.propFieldStatus.textColor = [UIColor redColor];
    else
        cell.propFieldStatus.textColor = [UIColor grayColor];
    cell.propFieldStatus.text = statusName;
    cell.propFieldTotalClaim.text = [NSString stringWithFormat:@"%@ %@",[myClaim propCurrencyThree], [self.propAppDelegate.decimalFormat stringFromNumber:[NSNumber numberWithFloat:[myClaim propTotalClaim]]]];

    return cell;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [self.propAppDelegate myClaims].count;
}


#pragma mark -
#pragma mark - Event Handler
#pragma mark -
- (IBAction)toggleList:(id)sender {
    [self.propAppDelegate.propSlider toggleSidebar];
}

- (void)refresh{
    UIWindow *window = [[[UIApplication sharedApplication] delegate] window];
    [AppDelegate showGlogalHUDWithView:window];
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
        id result = [self.propAppDelegate.propGatewayOnline myClaimsWithStaffID:[[self.propAppDelegate getStaff] propStaffID] requestingPerson:[[self.propAppDelegate getStaff] propStaffID]];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [AppDelegate hideGlogalHUDWithView:window];
            if([result isKindOfClass:[NSString class]])
                [[[UIAlertView alloc] initWithTitle:@"" message:result delegate:nil cancelButtonTitle:@"Dismiss" otherButtonTitles:nil, nil] show];
            else{
                NSMutableArray *myClaims = [[result sortedArrayUsingComparator:^NSComparisonResult(ClaimHeader *c1, ClaimHeader *c2){
                    return c2.propClaimID-c1.propClaimID;
                }] mutableCopy];
                [self.propAppDelegate updateMyClaims:myClaims];
            }
            [_propLV reloadData];
            
            if ([self.propAppDelegate.myClaims count] > 0) {
                int nowFirstClaimID = ((ClaimHeader *)[self.propAppDelegate.myClaims objectAtIndex:0]).propClaimID;
                NSLog(@"My Claim List: %d",((ClaimHeader*)[self.propAppDelegate.myClaims objectAtIndex:0]).propClaimID);
                
                if(_lastFirstClaimID>0 && _lastFirstClaimID != nowFirstClaimID) {
                    [_propLV selectRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0] animated:YES scrollPosition:UITableViewScrollPositionTop];
                    [_propLV.delegate tableView:_propLV didSelectRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
                }
                _lastFirstClaimID = nowFirstClaimID;
            }
        });
    });
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    ClaimHeader *selectedClaimHeader = [[self.propAppDelegate myClaims] objectAtIndex:indexPath.row];
    _selectedClaimHeader = selectedClaimHeader;
    
    if([selectedClaimHeader propTypeID] == CLAIMTYPEID_CLAIMS) {
        Claim *selectedClaim = [[Claim alloc] initWithDictionary:[[selectedClaimHeader savableDictionary] mutableCopy]];
        [self performSegueWithIdentifier:([selectedClaim isPaidByCompanyCard])?@"myclaimtoclaimpaidbycc":@"myclaimtoclaimnotpaidbycc" sender:[tableView cellForRowAtIndexPath:indexPath]];
    } else {
        [self performSegueWithIdentifier:@"myclaimtobusinessadvanceliq" sender:[tableView cellForRowAtIndexPath:indexPath]];
    }
}

@end
