//
//  CellLFAProcessed.h
//  Salt iOS
//
//  Created by Rick Royd Aban on 6/15/15.
//  Copyright (c) 2015 Rick Royd Aban. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "VCLeavesForApprovalProcessed.h"

@interface CellLFAProcessed : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *propLabelType;
@property (weak, nonatomic) IBOutlet UILabel *propLabelDuration;
@property (weak, nonatomic) IBOutlet UILabel *propLabelStaff;
@property (weak, nonatomic) IBOutlet UIButton *propButtonCancel;

- (void)assignLeavesForApprovalProcessed:(VCLeavesForApprovalProcessed *)vcLeavesForApprovalProcessed;

@end
