//
//  Document.m
//  Salt
//
//  Created by Rick Royd Aban on 2/10/16.
//  Copyright © 2016 Applus Velosi. All rights reserved.
//

#import "Document.h"
#import "AppDelegate.h"
#import <MobileCoreServices/MobileCoreServices.h>

@interface Document(){
    BOOL _isActive;
    NSString *_contentType, *_dateCreated;
    int _docID, _objectTypeID, _refID, _staffID;
    float _fileSize;
    NSString *_docName, *_ext, *_origDocName;
}
@end

@implementation Document

- (Document *)initWithAppDelegate:(AppDelegate *)appDelegate{
    if([super init]){
        _isActive = YES;
        _contentType = @"";
        _dateCreated = [appDelegate.propGatewayOnline epochizeDate:[NSDate date]];
        _docID = 0;
        _docName = @"test.jpg";
        _ext = @".jpg";
        _fileSize = 0;
        _objectTypeID = 0;
        _origDocName = @"test.jpg";
        _refID = 0;
        _staffID = 0;
    }
    
    return self;
}

- (Document *)initWithDictionary:(NSDictionary *)dictionary{
    if([super init]){
        _isActive = [[dictionary objectForKey:@"Active"] boolValue];
        _contentType = [dictionary objectForKey:@"ContentType"];
        _dateCreated = [dictionary objectForKey:@"DateCreated"];
        _docID = [[dictionary objectForKey:@"DocID"] intValue];
        _docName = [dictionary objectForKey:@"DocName"];
        _ext = [dictionary objectForKey:@"Ext"];
        _fileSize = [[dictionary objectForKey:@"FileSize"] floatValue];
        _objectTypeID = [[dictionary objectForKey:@"ObjectType"] intValue];
        _origDocName = [dictionary objectForKey:@"OrigDocName"];
        _refID = [[dictionary objectForKey:@"RefID"] intValue];
        _staffID = [[dictionary objectForKey:@"StaffID"] intValue];
    }
    
    return self;
}

- (Document *)initWithFileName:(NSString *)fileName dateCreated:(NSString *)dateCreated fileSize:(NSInteger)fileSize claimHeader:(ClaimHeader *)claimHeader nameDate:(NSString *)nameDate objectTypeID:(int)objectTypeID{
    
    if([super init]){
        _isActive = true;
        _origDocName = fileName;
        _ext = [fileName pathExtension];
        _contentType = (__bridge NSString *)(UTTypeCopyPreferredTagWithClass (UTTypeCreatePreferredIdentifierForTag(kUTTagClassFilenameExtension, (__bridge CFStringRef)_ext, NULL), kUTTagClassMIMEType));
        _dateCreated = dateCreated;
        _docID = 0;
        _docName = [NSString stringWithFormat:@"%d_ClaimLineItem_%@.%@",[claimHeader propClaimID], nameDate, _ext];
        _fileSize = fileSize;
        _objectTypeID = objectTypeID;
        _staffID = [claimHeader propStaffID];
        _refID = 0;
    }
    
    return self;
}

- (int)propDocID{
    return _docID; }
- (int)propRefID{ return _refID; }
- (int)propObjectTypeID{ return _objectTypeID; }
- (NSString *)propDocName{ return _docName; }
- (NSString *)propOrigDocName{ return _origDocName; }

- (void)propRefID:(int)refID{ _refID = refID; }

- (NSString *)jsonObject{
    NSMutableString *str = [NSMutableString stringWithFormat:@"{"];
    [str appendFormat:@"\"Active\": %@", (_isActive)?@"true":@"false"];
    [str appendFormat:@",\"ContentType\":\"%@\"", _contentType];
    [str appendFormat:@",\"DateCreated\":\"%@\"", _dateCreated];
    [str appendFormat:@",\"DocID\": %d", [[NSNumber numberWithInt:_docID] intValue]];
    [str appendFormat:@",\"DocName\":\"%@\"", _docName];
    [str appendFormat:@",\"Ext\":\"%@\"", _ext];
    [str appendFormat:@",\"FileSize\":%@", [NSNumber numberWithInt:_fileSize]];
    [str appendFormat:@",\"ObjectType\":%@", [NSNumber numberWithInt:_objectTypeID]];
    [str appendFormat:@",\"OrigDocName\":\"%@\"", _origDocName];
    [str appendFormat:@",\"RefID\":%@", [NSNumber numberWithInt:_refID]];
    [str appendFormat:@",\"StaffID\":%@", [NSNumber numberWithInt:_staffID]];
    [str appendString:@"}"];
    
    return str;
}

+ (int)objecttype_CLAIMID{ return 1; }
+ (int)objecttype_LEAVEID{ return 2; }
+ (int)objecttype_CLAIMLINEITEMID{ return 3; }
+ (int)objecttype_STAFFID{ return 4; }
+ (int)objecttype_RECRUITMENTID{ return 5; }
+ (int)objecttype_TRAVELID{ return 6; }
+ (int)objecttype_CAPEXID{ return 7; }
+ (int)objecttype_CAPEXLINEITEMQOUTATIONID{ return 8; }

@end
