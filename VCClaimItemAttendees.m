//
//  VCClaimItemAttendeesViewController.m
//  Salt
//
//  Created by Rick Royd Aban on 2/29/16.
//  Copyright © 2016 Applus Velosi. All rights reserved.
//

#import "VCClaimItemAttendees.h"
#import "VCClaimItemAddAttendee.h"
#import "CellClaimItemInputAttendee.h"
#import "ClaimItemAttendee.h"

@interface VCClaimItemAttendees (){
    
    IBOutlet UITableView *_propLV;
}

@end

@implementation VCClaimItemAttendees

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _propLV.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    _propLV.dataSource = self;
    _propLV.delegate = self;
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    self.navigationItem.title = [NSString stringWithFormat:@"Attendees (%lu)", _claimitemAttendees.count];
    [_propLV reloadData];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    CellClaimItemInputAttendee *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    ClaimItemAttendee *attendee = [_claimitemAttendees objectAtIndex:indexPath.row];
    cell.propLabelName.text = attendee.propName;
    cell.propLabelJobDesc.text = attendee.propJobTitle;
    cell.propLabelNotes.text = attendee.propDesc;
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath{
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return _claimitemAttendees.count;
}


- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    ((VCClaimItemAddAttendee *)segue.destinationViewController).propClaimItemAttendees = _claimitemAttendees;
}

@end
