//
//  VCCapexesForApprovalSearchResult.h
//  Salt
//
//  Created by Rick Royd Aban on 30/08/2016.
//  Copyright © 2016 Applus Velosi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"
#import "VCCapexesForApproval.h"

@interface VCCapexesForApprovalSearchResult : UITableViewController

@property(nonatomic, weak) VCCapexesForApproval *parentVC;

@end
