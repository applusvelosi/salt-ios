//
//  CellClaimsForApproval.h
//  Salt
//
//  Created by Rick Royd Aban on 2015/08/25.
//  Copyright (c) 2015年 Applus Velosi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CellClaimsForApproval : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *propFieldType;
@property (weak, nonatomic) IBOutlet UILabel *propFieldDateExpensed;
@property (weak, nonatomic) IBOutlet UILabel *propFieldStatus;
@property (weak, nonatomic) IBOutlet UILabel *propFieldClaimant;
@property (weak, nonatomic) IBOutlet UILabel *propFieldAmount;
@end
