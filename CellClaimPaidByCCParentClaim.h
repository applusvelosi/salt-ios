//
//  CellClaimPaidByCCParentClaim.h
//  Salt
//
//  Created by Rick Royd Aban on 2015/07/27.
//  Copyright (c) 2015年 Applus Velosi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CellClaimPaidByCCParentClaim : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *propFieldParentClaimNumber;
@end
