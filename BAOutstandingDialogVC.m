//
//  VCBusinessAdvance.m
//  Salt
//
//  Created by Rick Royd Aban on 2015/07/31.
//  Copyright (c) 2015年 Applus Velosi. All rights reserved.
//

#import "BAOutstandingDialogVC.h"
#import <QuartzCore/QuartzCore.h>
#import <dispatch/dispatch.h>
#import "CellForBAOutstanding.h"
#import "CellForBAOutstandingHeader.h"

@interface BAOutstandingDialogVC () {
    NSMutableArray *selectedItems;
}

@end

@implementation BAOutstandingDialogVC

#pragma mark -
#pragma mark === View Life Cycle Management ===
#pragma mark -
-(void)viewDidLoad {
    [super viewDidLoad];
    self.alertTableView = [[UITableView alloc] initWithFrame:self.propTableFrame style:UITableViewStylePlain];
    [self.alertTableView registerClass:[CellForBAOutstanding class] forCellReuseIdentifier:@"outstandingcell"];
    [self.alertTableView registerClass:[CellForBAOutstanding class] forCellReuseIdentifier:@"outstandingheadercell"];
    self.alertTableView.delegate = self;
    self.alertTableView.dataSource = self;
    self.alertTableView.tableFooterView = [[UIView alloc]initWithFrame:CGRectZero];
    self.alertTableView.separatorInset = UIEdgeInsetsMake(0, 0, 0, 0);
    [self.view addSubview:self.alertTableView];
    [self.alertTableView setAllowsSelection:NO];
    
    self.alertTableView.rowHeight = UITableViewAutomaticDimension;
    self.alertTableView.estimatedRowHeight = 140;
}

#pragma mark -
#pragma mark - Table view data source delegate
#pragma mark -
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    return self.cellForRow(self,indexPath);
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.numberOfRowsInTableAlert(self);
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row == 0)
        return 22.0f;
    else
        return self.alertTableView.rowHeight;
}

@end
