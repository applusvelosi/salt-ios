//
//  Currency.h
//  Salt
//
//  Created by Rick Royd Aban on 2/19/16.
//  Copyright © 2016 Applus Velosi. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Currency : NSObject

- (Currency *)initWithID:(int)currencyID name:(NSString *)currencyName currThree:(NSString *)currThree;

- (int)propCurrID;
- (NSString *)propCurrencyName;
- (NSString *)propCurrencyThree;

@end
