//
//  CellClaimItemDetailOverview.h
//  Salt
//
//  Created by Rick Royd Aban on 2/10/16.
//  Copyright © 2016 Applus Velosi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CellClaimItemDetailOverview : UITableViewCell

@property (strong, nonatomic) IBOutlet UILabel *propLabelCategoryName;
@property (strong, nonatomic) IBOutlet UILabel *propLabelItemNumber;
@property (strong, nonatomic) IBOutlet UILabel *propLabelProjectName;
@property (strong, nonatomic) IBOutlet UILabel *propLabelDate;
@property (strong, nonatomic) IBOutlet UILabel *propLabelAttachment;

@end
