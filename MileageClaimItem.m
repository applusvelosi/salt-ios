//
//  MileageClaimItem.m
//  Salt
//
//  Created by Rick Royd Aban on 2015/08/05.
//  Copyright (c) 2015年 Applus Velosi. All rights reserved.
//

#import "MileageClaimItem.h"

@implementation MileageClaimItem

+ (int)MILEAGETYPE_KEY_KILOMETER{
    return 1;
}

+ (int)MILEAGETYPE_KEY_MILE{
    return 2;
}

+ (NSString *)MILEAGETYPE_VAL_KILOMETER{
    return @"KM";
}

+ (NSString *)MILEAGETYPE_VAL_MILE{
    return @"MI";
}

- (int)propMileageTypeID{
    return [super mileageType]; //[[[super claimItem] objectForKey:@"MileageType"] intValue];
}

- (NSString *)propMileageTypeName{
    return ([self propMileageTypeID] == [MileageClaimItem MILEAGETYPE_KEY_KILOMETER])?[MileageClaimItem MILEAGETYPE_VAL_KILOMETER]:[MileageClaimItem MILEAGETYPE_VAL_MILE];
}

- (float)propMileageRate{
    return [super mileageRate]; //[[[super claimItem] objectForKey:@"MileageRate"] floatValue];
}

- (float)propMileage{
    return [super mileage]; //[[[super claimItem] objectForKey:@"Mileage"] floatValue];
}

- (NSString *)propMileageFrom{
    return  [super mileageFrom]; //[[super claimItem] objectForKey:@"MileageFrom"];
}

- (NSString *)propMileageTo{
    return [[super claimItem] objectForKey:@"MileageTo"];
}

- (BOOL)isMileageReturn{
    return  [super hasMileageReturn]; //[[[super claimItem] objectForKey:@"MileageReturn"] boolValue];
}

@end
