//
//  CellContractsForApproval.h
//  Salt
//
//  Created by Rick Royd Aban on 13/06/2017.
//  Copyright © 2017 Applus Velosi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CellContractsForApproval : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *propFieldOffice;
@property (weak, nonatomic) IBOutlet UILabel *propFieldLocalCurrency;
@property (weak, nonatomic) IBOutlet UILabel *propFieldRequestorName;
@property (nonatomic, weak) IBOutlet UILabel *propFieldStatus;

@end
