//
//  CellLFACancelled.h
//  Salt iOS
//
//  Created by Rick Royd Aban on 6/15/15.
//  Copyright (c) 2015 Rick Royd Aban. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "VCLeavesForApprovalCancelled.h"

@interface CellLFACancelled : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *propLabelType;
@property (weak, nonatomic) IBOutlet UILabel *propLabelDuration;
@property (weak, nonatomic) IBOutlet UILabel *propLabelStaff;

- (void)assignLeavesForApprovalCancelled:(VCLeavesForApprovalCancelled *)vcLeavesForApprovalCancelled;

@end
