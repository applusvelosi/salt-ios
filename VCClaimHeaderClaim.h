//
//  VCClaimHeaderClaim.h
//  Salt
//
//  Created by Rick Royd Aban on 2015/08/12.
//  Copyright (c) 2015年 Applus Velosi. All rights reserved.
//

#import "VCClaimHeader.h"
#import "Claim.h"
#import "ClaimPaidByCC.h"
#import "ClaimNotPaidByCC.h"
#import "CostCenter.h"

@interface VCClaimHeaderClaim : VCClaimHeader

@property (strong, nonatomic) Claim *propClaim;

@end
