//
//  VCLiquidationBARefs.h
//  Salt
//
//  Created by Rick Royd Aban on 2/3/16.
//  Copyright © 2016 Applus Velosi. All rights reserved.
//

#import "VCPage.h"
#import "VCClaimHeaderLiquidation.h"

@interface VCLiquidationBARefs : VCPage<UITableViewDataSource, UITableViewDelegate>

@property (strong, nonatomic) VCClaimHeaderLiquidation *vcClaimHeaderLiq;

@end
