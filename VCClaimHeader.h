//
//  VCClaimHeader.h
//  Salt
//
//  Created by Rick Royd Aban on 2015/08/12.
//  Copyright (c) 2015年 Applus Velosi. All rights reserved.
//

#import "VCDetail.h"

@interface VCClaimHeader : VCDetail<UIAlertViewDelegate>

@property (strong, nonatomic) CostCenter *propCostCenter;

- (void)updateCostCenter:(CostCenter *)costCenter;

@end
