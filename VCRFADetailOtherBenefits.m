//
//  VCRFADetailOtherBenefits.m
//  Salt
//
//  Created by Rick Royd Aban on 10/5/15.
//  Copyright © 2015 Applus Velosi. All rights reserved.
//

#import "VCRFADetailOtherBenefits.h"
#import "CellTitleAboveDetailBelow.h"

@interface VCRFADetailOtherBenefits(){
    
    IBOutlet UITableView *_propLV;
}
@end

@implementation VCRFADetailOtherBenefits

- (void)viewDidLoad{
    [super viewDidLoad];
    _propLV.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    _propLV.dataSource = self;
    _propLV.delegate = self;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    NSArray *benefitDataParts = [[[_propRecruitment propOtherBenefits] objectAtIndex:indexPath.row] componentsSeparatedByString:RECRUITMENT_OTHERBENEFIT_DELIMITER];
    
    CellTitleAboveDetailBelow *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    cell.propName.text = [benefitDataParts objectAtIndex:0];
    cell.propAnnualCost.text  = [benefitDataParts objectAtIndex:1];

    return cell;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [_propRecruitment propOtherBenefits].count;
}

@end
