//
//  VCBusinessAdvance.h
//  Salt
//
//  Created by Rick Royd Aban on 2015/07/31.
//  Copyright (c) 2015年 Applus Velosi. All rights reserved.
//

#import "VCPage.h"

@class BAOutstandingDialogVC;

typedef void (^TableAlertIndexBlock)(BAOutstandingDialogVC*,NSInteger);
typedef void (^TableAlertGeneralBlock)(BAOutstandingDialogVC*);
typedef NSUInteger (^TableAlertNumberOfRowsBlock)(BAOutstandingDialogVC*);
typedef UITableViewCell* (^TableAlertCellSourceBlock)(BAOutstandingDialogVC*,NSIndexPath*);

@interface BAOutstandingDialogVC : VCPage <UITableViewDelegate, UITableViewDataSource>

@property (nonatomic, strong) UITableView *alertTableView;
@property (nonatomic,copy)	TableAlertIndexBlock didSelectRow;
@property (nonatomic,copy)	TableAlertIndexBlock willDismissWithButtonIndex;
@property (nonatomic,copy)	TableAlertGeneralBlock willPresent;
@property (nonatomic,copy)	TableAlertCellSourceBlock cellForRow;
@property (nonatomic,copy)	TableAlertNumberOfRowsBlock numberOfRowsInTableAlert;
@property (nonatomic, assign) NSInteger maxSelection;
@property (weak, nonatomic, readonly) NSArray* indexPathsForSelectedRows;
@property (nonatomic, assign) CGRect propTableFrame;

@end
