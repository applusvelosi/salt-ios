//
//  AppDelegate.m
//  Salt iOS
//
//  Created by Rick Royd Aban on 5/18/15.
//  Copyright (c) 2015 Rick Royd Aban. All rights reserved.
//

#import "AppDelegate.h"
//#import <Google/Analytics.h>
//#import <Parse/Parse.h>
@import Firebase;
#import "VelosiColors.h"
#import <Fabric/Fabric.h>
#import <Crashlytics/Crashlytics.h>
#import "ClaimItem.h"
#import <Objection/Objection.h>
#import "SaltModuleDI.h"

@interface AppDelegate ()<UIAlertViewDelegate>{
    Staff *_staff; //must not be visible as interface so we can serialize this object for every staff and office update
    Office *_office;
    NSMutableArray *_myLeaves, *_leavesForApproval, *_monthlyHolidays, *_localHolidays;
    NSMutableArray *_myClaims, *_claimsForApproval, *_claimsForPayment;
    NSMutableDictionary *_myClaimItems, *_forApprovalClaimItems, *_forPaymentClaimItems;
    NSMutableArray *_recruitmentForApproval, *_capexesForApproval;
    NSMutableArray *_giftsForApproval, *_contractsForApproval;
}

@end

@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
//    [Parse setApplicationId:@"Lsnfv65XD5V1RlgoYnWX8DEB06EW9BCTOdwBDWRb" clientKey:@"CiYC18jb5FXSdjJgvJbixG0wqoC252dnR7YAwgBd"];
    UIUserNotificationType userNotificationTypes = (UIUserNotificationTypeAlert |
                                                    UIUserNotificationTypeBadge |
                                                    UIUserNotificationTypeSound);
    UIUserNotificationSettings *settings = [UIUserNotificationSettings settingsForTypes:userNotificationTypes
                                                                             categories:nil];
    [application registerUserNotificationSettings:settings];
    [application registerForRemoteNotifications];
    
    //Initializing Firebase
    [FIRApp configure];
    _databaseRef = [[FIRDatabase database] reference];
    
    @try {
        [_databaseRef observeEventType:FIRDataEventTypeValue withBlock:^(FIRDataSnapshot * _Nonnull snapshot){
            NSDictionary *contents = snapshot.value;
            NSString *appVersion = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleShortVersionString"];
            NSString *latestVersion = [contents valueForKey:@"iOSVersion"];
//            NSString *latestVersion = [contents valueForKey:@"devVersion"];
            if ([latestVersion compare:appVersion options:NSNumericSearch] == NSOrderedDescending)
                [[[UIAlertView alloc] initWithTitle:@"Update Required" message:@"New version available. Please update with the latest version" delegate:self cancelButtonTitle:@"Update" otherButtonTitles:nil, nil] show];
        }];
    } @catch (NSException *exception) {
        NSLog(@"Exception: %@",[exception callStackSymbols]);
    }
    
    //Initailizing FabricCrashLatex
    [Fabric with:@[[Crashlytics class]]];
    
    // Configure tracker from GoogleService-Info.plist.
//    NSError *configureError;
//    [[GGLContext sharedInstance] configureWithError:&configureError];
//    NSAssert(!configureError, @"Error configuring Google services: %@", configureError);
//    if (configureError != nil) {⁄
//        NSLog(@"Error configuring the Google context: %@", configureError);
//    }
    
    // Optional: configure GAI options.
//    GAI *gai = [GAI sharedInstance];
//    gai.trackUncaughtExceptions = YES;  // report uncaught exceptions
//    gai.logger.logLevel = kGAILogLevelVerbose;  // remove before app release
    
//    self.propPageNavigator = [PageNavigatorFactory sharedNavigators];
    // Override point for customization after application launch
    
    JSObjectionInjector *injector = [JSObjection createInjector:[[SaltModuleDI alloc] init]];
    [JSObjection setDefaultInjector:injector];
    
    UIPageControl *pageControl = [UIPageControl appearance];
    pageControl.pageIndicatorTintColor = [UIColor lightGrayColor];
    pageControl.currentPageIndicatorTintColor = [VelosiColors orangeVelosi];
    pageControl.backgroundColor = [VelosiColors white];
    _propPageNavigator = [PageNavigatorFactory sharedNavigators];
    
    self.propFormatVelosiDate = [[NSDateFormatter alloc] init];
    self.propFormatVelosiDate.dateFormat = @"dd-MMM-yyyy";
    self.propFormatVelosiDate.timeZone = [NSTimeZone timeZoneWithAbbreviation:@"BST"];
    
    self.propDateFormatLeaveDuration = [[NSDateFormatter alloc] init];
    self.propDateFormatLeaveDuration.dateFormat = @"dd-MMM-yyyy";
    self.propDateFormatLeaveDuration.timeZone = [NSTimeZone systemTimeZone];
    
    self.propDateFormatMonthyear = [[NSDateFormatter alloc] init];
    self.propDateFormatMonthyear.dateFormat = @"MMMM-yyyy";
    self.propDateFormatDateTime = [[NSDateFormatter alloc] init];
    self.propDateFormatDateTime.dateFormat = @"yyyy-MM-dd HH:mm:ss";
    self.propDateFormatByProcessed = [[NSDateFormatter alloc] init]; //for approval date format set to timezone BST(British Summer Time) since the server came from London
    [self.propDateFormatByProcessed setDateFormat:@"dd-MMM-yyyy HH:mm:ss"];
//    self.propDateFormatByProcessed.timeZone = [NSTimeZone timeZoneWithAbbreviation:@"BST"];
    
    self.propGatewayOnline = [[OnlineGateway alloc] initWithAppDelegate:self];
    self.propGatewayOffline = [[OfflineGateway alloc] initWithAppDelegate:self];
    _decimalFormat = [[NSNumberFormatter alloc] init];
    [_decimalFormat setNumberStyle:NSNumberFormatterDecimalStyle];
    [_decimalFormat setMinimumFractionDigits:2];
    [_decimalFormat setMaximumFractionDigits:2];
    
    NSDateFormatter *yearOnlyFormatter = [[NSDateFormatter alloc] init];
    yearOnlyFormatter.dateFormat = @"yyyy";
    _currYear = [[yearOnlyFormatter stringFromDate:[NSDate date]] intValue];
    _filterYears = @[[NSString stringWithFormat:@"%d",_currYear+1], [NSString stringWithFormat:@"%d",_currYear], [NSString stringWithFormat:@"%d",_currYear-1], [NSString stringWithFormat:@"%d",_currYear-2], [NSString stringWithFormat:@"%d",_currYear-3], [NSString stringWithFormat:@"%d",_currYear-4]];
    
    _propCurrencies = [[DSCurrencyList alloc] initCurrencyList];
    
    _myClaimItems = [NSMutableDictionary dictionary];
    _forApprovalClaimItems = [NSMutableDictionary dictionary];
    _forPaymentClaimItems = [NSMutableDictionary dictionary];
    
    if([_propGatewayOffline isLoggedIn]) {
        _staff = [_propGatewayOffline deserializeStaff];
        _myLeaves = [_propGatewayOffline deserializeMyLeaves];
        _leavesForApproval = [_propGatewayOffline deserializeLeavesForApproval];
        _monthlyHolidays = [_propGatewayOffline deserializeMonthlyHolidays];
        _localHolidays = [_propGatewayOffline deserializeLocalHolidays];
        _myClaims = [_propGatewayOffline deserializeMyClaims];
        _claimsForApproval = [_propGatewayOffline deserializeClaimsForApproval];
        _recruitmentForApproval = [_propGatewayOffline deserializeRecruitmentForApproval];
        _capexesForApproval = [_propGatewayOffline deserializeCapexesForApproval];
        _giftsForApproval = [_propGatewayOffline deserializeGiftsForApproval];
        _contractsForApproval = [_propGatewayOffline deserializeContractsForApproval];
    } else {
        _myLeaves = [NSMutableArray array];
        _leavesForApproval = [NSMutableArray array];
        _monthlyHolidays = [NSMutableArray array];
        _localHolidays = [NSMutableArray array];
        _myClaims = [NSMutableArray array];
        _claimsForApproval = [NSMutableArray array];
        _claimsForPayment = [NSMutableArray array];
        _recruitmentForApproval = [NSMutableArray array];
        _capexesForApproval = [NSMutableArray array];
        _giftsForApproval = [NSMutableArray array];
        _contractsForApproval = [NSMutableArray array];
    }
    return YES;
}

- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken {
    // Store the deviceToken in the current installation and save it to Parse.
//    PFInstallation *currentInstallation = [PFInstallation currentInstallation];
//    [currentInstallation setDeviceTokenFromData:deviceToken];
//    currentInstallation.channels = @[ @"global" ];
//    [currentInstallation saveInBackground];
}

+ (MBProgressHUD*)showGlogalHUDWithView:(UIView *)view {
//    UIWindow *window = [[[UIApplication sharedApplication] windows] firstObject];
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:view animated:YES];
    hud.bezelView.color = [UIColor clearColor];
    hud.bezelView.style = MBProgressHUDBackgroundStyleSolidColor;
    return hud;
}

+ (void)hideGlogalHUDWithView:(UIView*)view {
//    UIWindow *window = [[[UIApplication sharedApplication] windows] firstObject];
    [MBProgressHUD hideHUDForView:view animated:YES];
}

- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo {

}

- (Staff *)getStaff{
    return _staff;
}

- (Office *)office{
    return _office;
}

- (void)updateStaffDataWithStaff:(Staff *)staff office:(Office *)office key:(OnlineGateway *)onlineGateway {
    _staff = staff;
    _office = office;
    
    //determine user position
    if ([office countryManagerID] == [staff propStaffID])
        [staff setUserPosition:([office isHQ])?USERPOSITION_CFO:USERPOSITION_CM];
    else if ([office regionalManagerID] == [staff propStaffID])
        [staff setUserPosition:([office isHQ])?USERPOSITION_CEO:USERPOSITION_RM];
    else if ([staff propStaffID] == LMid)
        [staff setUserPosition:USERPOSITION_LM];
    else
        [staff setUserPosition:USERPOSITION_DEFAULT];

    [_propGatewayOffline serializeStaff:staff office:office];
    
//    PFObject *pObject = [[PFObject alloc] initWithClassName:@"Usage"];
//    [pObject setObject:[staff propFullName] forKey:@"name"];
//    [pObject setObject:[office officeName] forKey:@"office"];
//    [pObject saveInBackground];
    
//    PFInstallation *installation = [PFInstallation currentInstallation];
//    [installation setObject:@([staff propStaffID]) forKey:@"staffID"];
//    [installation setObject:[staff propFullName] forKey:@"staffName"];
//    [installation saveInBackground];
}

- (void)initMyLeaves:myLeaves leavesForApproval:leavesForApproval{
    [_myLeaves removeAllObjects];
    [_leavesForApproval removeAllObjects];
    [_myLeaves addObjectsFromArray:myLeaves];
    [_leavesForApproval addObjectsFromArray:leavesForApproval];
    
    [_propGatewayOffline serializeMyLeaves:_myLeaves];
    [_propGatewayOffline serializeLeavesForApproval:_leavesForApproval];
    //replace this dummy empty string when found a solution to retrieve the my claim list
    [_propGatewayOffline serializeMyClaims:[NSMutableArray array]];
    [_propGatewayOffline serializeClaimsForApproval:[NSMutableArray array]];
//    [_propGatewayOffline serializeClaimsForPayment:[NSMutableArray array]];
    [_propGatewayOffline serializeRecruitmentsForApproval:[NSMutableArray array]];
    [_propGatewayOffline serializeCapexesForApproval:[NSMutableArray array]];
    [_propGatewayOffline serializeGiftsForApproval];
    [_propGatewayOffline serializeContractsForApproval];
}

//- (void)initMyLeaves:(NSArray *)myLeaves leavesForApproval:(NSArray *)leavesForApproval myClaims:(NSArray *)myClaims claimsForApproval:(NSArray *)claimsForApproval claimsForAccount:(NSArray *)claimsForPayment{
//    [_myLeaves removeAllObjects];
//    [_leavesForApproval removeAllObjects];
////    [_myClaims removeAllObjects];
////    [_claimsForApproval removeAllObjects];
////    [_claimsForPayment removeAllObjects];
//    [_myLeaves addObjectsFromArray:myLeaves];
//    [_leavesForApproval addObjectsFromArray:leavesForApproval];
////    [_myClaims addObjectsFromArray:myClaims];
////    [_claimsForApproval addObjectsFromArray:claimsForApproval];
////    [_claimsForPayment addObjectsFromArray:claimsForPayment];
//    
//    [_propGatewayOffline serializeMyLeaves:_myLeaves];
//    NSLog(@"initmyleaves");
//    [_propGatewayOffline serializeLeavesForApproval:_leavesForApproval];
////    [_propGatewayOffline serializeMyClaims:_myClaims];
////    [_propGatewayOffline serializeClaimsForApproval:_claimsForApproval];
////    [_propGatewayOffline serializeClaimsForPayment:_claimsForPayment];
//}

- (void)updateMyLeaves:(NSMutableArray *)myLeaves{
    [_myLeaves removeAllObjects];
    [_myLeaves addObjectsFromArray:myLeaves];
    [_propGatewayOffline serializeMyLeaves:myLeaves];
}

- (void)updateLeavesForApproval:(NSMutableArray *)leavesForApproval{
    [_leavesForApproval removeAllObjects];
    [_leavesForApproval addObjectsFromArray:leavesForApproval];
    [_propGatewayOffline serializeLeavesForApproval:leavesForApproval];
}

- (void)updateMonthlyHolidays:(NSMutableArray *)monthlyHolidays{
    [_monthlyHolidays removeAllObjects];
    [_monthlyHolidays addObjectsFromArray:monthlyHolidays];
    [_propGatewayOffline serializeMonthlyHolidays:monthlyHolidays];
}

- (void)updateLocalHolidays:(NSMutableArray *)localHolidays{
    [_localHolidays removeAllObjects];
    [_localHolidays addObjectsFromArray:localHolidays];
    [_propGatewayOffline serializeLocalHolidays:localHolidays];
}

- (void)updateMyClaims:(NSMutableArray *)myClaims{
    [_myClaims removeAllObjects];
    [_myClaims addObjectsFromArray:myClaims];
    [_propGatewayOffline serializeMyClaims:myClaims];
}

- (void)updateClaimsForApproval:(NSMutableArray *)claimsForApproval{
    [_claimsForApproval removeAllObjects];
    [_claimsForApproval addObjectsFromArray:claimsForApproval];
    [_propGatewayOffline serializeClaimsForApproval:claimsForApproval];
}

//- (void)updateClaimsForPayment:(NSMutableArray *)claimsForPayment{
//    [_claimsForPayment removeAllObjects];
//    [_claimsForPayment addObjectsFromArray:claimsForPayment];
//    [_propGatewayOffline serializeClaimsForPayment:claimsForPayment];
//}

- (void)updateMyClaimItems:(NSMutableArray *)myClaimItems forClaimID:(int)claimID{
    [_myClaimItems setObject:myClaimItems forKey:@(claimID)];
}

- (void)updateForApprovalClaimItems:(NSArray *)forApprovalClaimItems forClaimID:(int)claimID{
    if(claimID != 0)
        [_forApprovalClaimItems setObject:forApprovalClaimItems forKey:@(claimID)];
    else
        [_forApprovalClaimItems removeAllObjects];
}

- (void)updateForPaymentClaimItems:(NSMutableArray *)forPaymentClaimItems forClaimID:(int)claimID{

    [_forApprovalClaimItems setObject:forPaymentClaimItems forKey:@(claimID)];
}

- (void)updateRecruitmentsForApproval:(NSMutableArray *)recruitmentsForApproval {
    [_recruitmentForApproval removeAllObjects];
    [_recruitmentForApproval addObjectsFromArray:recruitmentsForApproval];
    [_propGatewayOffline serializeRecruitmentsForApproval:recruitmentsForApproval];
}

- (void)updateCapexesForApproval:(NSMutableArray *)capexesForApproval {
    [_capexesForApproval removeAllObjects];
    [_capexesForApproval addObjectsFromArray:capexesForApproval];
    [_propGatewayOffline serializeCapexesForApproval:capexesForApproval];
}

- (void)updateGiftsForApproval: (NSMutableArray *)giftsForApproval {
    [_giftsForApproval removeAllObjects];
    [_giftsForApproval addObjectsFromArray:giftsForApproval];
    [_propGatewayOffline serializeGiftsForApproval];
}

- (void)updateContractsForApproval:(NSMutableArray *)contractsForApproval {
    [_contractsForApproval removeAllObjects];
    [_contractsForApproval addObjectsFromArray:contractsForApproval];
    [_propGatewayOffline serializeContractsForApproval];
}

- (NSArray *)capexesForApproval{
    return _capexesForApproval;
}

- (NSArray *)giftsForApproval {
    return _giftsForApproval;
}

- (NSArray *)contractsForApproval {
    return _contractsForApproval;
}

- (NSArray *)myLeaves{
    return _myLeaves;
}

- (NSArray *)leavesForApproval {
    return _leavesForApproval;
}

- (NSArray *)monthlyHolidays{
    return _monthlyHolidays;
}

- (NSArray *)localHolidays{
    return _localHolidays;
}

- (NSArray *)myClaims{
    return _myClaims;
}

- (NSArray *)claimsForApproval{
    return _claimsForApproval;
}

- (NSArray *)claimsForPayment{
    return _claimsForPayment;
}

- (NSArray *)recruitmentsForApproval{
    return _recruitmentForApproval;
}


- (NSArray *)myClaimItemsForMyClaimID:(int)myClaimID{
    return [_myClaimItems objectForKey:@(myClaimID)];
}

- (NSArray *)forApprovalClaimItemsForClaimID:(int)claimID{
    NSArray *tempClaimForApproval = [_forApprovalClaimItems objectForKey:@(claimID)];
    return [tempClaimForApproval sortedArrayUsingComparator:^NSComparisonResult(id itemA, id itemB){
        NSString *firstItem = [(ClaimItem*)itemA claimLineItemNumber];
        NSString *secondItem = [(ClaimItem*)itemB claimLineItemNumber];
        return [firstItem compare:secondItem options:NSCaseInsensitiveSearch];
    }];
}

- (NSArray *)forPaymentClaimItemsForClaimID:(int)claimID{
    return [_forPaymentClaimItems objectForKey:@(claimID)];
}


- (void)setSlider:(VCSlider *)slider{
    _propSlider = slider;
}

+ (NSString *)all{
    return @"All";
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"https://salttest.velosi.com/app/download.html"]];
}

@end
