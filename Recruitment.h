//
//  Recruitment.h
//  Salt
//
//  Created by Rick Royd Aban on 10/1/15.
//  Copyright © 2015 Applus Velosi. All rights reserved.
//

#define RECRUITMENT_STATUSID_OPEN 26
#define RECRUITMENT_STATUSID_SUBMITTED 27
#define RECRUITMENT_STATUSID_APPROVEDBYCM 28
#define RECRUITMENT_STATUSID_REJECTEDBYCM 29
#define RECRUITMENT_STATUSID_CANCELLED 30
#define RECRUITMENT_STATUSID_APPROVEDBYRM 54
#define RECRUITMENT_STATUSID_REJECTEDBYRM 55
#define RECRUITMENT_STATUSID_APPROVEDBYMHR 56
#define RECRUITMENT_STATUSID_REJECTEDBYMHR 57
#define RECRUITMENT_STATUSID_APPROVEDBYCEO 58
#define RECRUITMENT_STATUSID_REJECTEDBYCEO 59
#define RECRUITMENT_STATUSID_APPROVEDBYRHM 142
#define RECRUITMENT_STATUSID_REJECTEDBYRHM 143

#define RECRUITMENT_OTHERBENEFIT_DELIMITER @"~"

#import <Foundation/Foundation.h>
#import "OfflineGatewaySavable.h"
#import "OnlineGateway.h"

typedef NS_ENUM(NSUInteger, RecruitmentPositionTypeId) {
    RecruitmentPositionTypeIdNew = 1,
    RecruitmentPositionTypeIdReplacement,
    RecruitmentPositionTypeIdPackageRevision,
    RecruitmentPositionTypeIdDismissal
};

@interface Recruitment : NSObject<OfflineGatewaySavable>

- initWithDictionary:(NSDictionary *)jsonRecruitment onlineGateway:(OnlineGateway *)onlineGateway;
- (Recruitment *)initWithDictionary:(NSMutableDictionary *)recruitment;

- (float)propAnnualRevenue;
- (NSString *)propApproverNote;
- (int)propCMID;
- (NSString *)propCMName;
- (NSString *)propCMEmail;
- (NSString *)propDateRequested:(AppDelegate *)app;
- (NSString *)propDateRejected:(AppDelegate *)app;
- (NSString *)propDateProcessedByCM:(AppDelegate *)app;
- (NSString *)propDateProcessedByRHM:(AppDelegate *)app;
- (NSString *)propDateProcessedByRM:(AppDelegate *)app;
- (NSString *)propDateProcessedByHR:(AppDelegate *)app;
- (NSString *)propDateProcessedByCEO:(AppDelegate *)app;
- (int)propDepartmentToBeAssignedID;
- (NSString *)propDepartmentToBeAssignedName;
- (NSString *)propEmail;
- (int)propEmployeeCategoryID;
- (NSString *)propEmployeeCategoryName;
- (int)propEmploymentTypeID;
- (NSString *)propEmploymentTypeName;
- (float)propGrossBaseBonus;
- (float)propHoursPerWeek;
- (BOOL)propIsBudgetedCost;
- (BOOL)propIsPositionMayBePermanent;
- (BOOL)propIsSpecificPerson;
- (BOOL)propIsWithAttachment;
- (BOOL)propIsRequiredReplacement;
- (NSString *)propJobTitle;
- (int)propOfficeOfDeploymentID;
- (NSString *)propOfficeOfDeploymentName;
- (NSMutableArray *)propOtherBenefits;
- (NSMutableArray *)propDocuments;
- (int)propPositionTypeID;
- (NSString *)propPositionTypeName;
- (NSString *)propReason;
- (NSString *)propRecruitmentNumber;
- (int)propRecruitmentRequestID;
- (int)propRegionalHRManager;
- (int)propRMID;
- (NSString *)propRMName;
- (NSString *)propRMEmail;
- (NSString *)propReplacementFor;
- (NSString *)propRequesterDepartmentName;
- (int)propRequesterID;
- (NSString *)propRequesterName;
- (int)propRequesterOfficeID;
- (NSString *)propRequesterOfficeName;
- (NSString *)propRequesterPhoneNumber;
- (float)propSalaryRangeFrom;
- (float)propSalaryRangeTo;
- (NSString *)propSeverancePayment;
- (int)propStatusID;
- (NSString *)propStatusName;
- (NSString *)propTargettedStartDate;
- (int)propTimeBaseTypeID;
- (NSString *)propTimeBaseTypeName;

- (NSString *)JSONForUpdatingRecruitmentWithStatusID:(int)statusID keyForUpdatableDate:(NSString *)keyForUpdatableDate approverNotes:(NSString *)approverNotes appDelegate:(AppDelegate *)appDelegate;
- (NSString *)jsonizeWithAppDelegate:(AppDelegate *)appDelegate;


@end
