//
//  OnlineGateway.m
//  Salt iOS
//
//  Created by Rick Royd Aban on 5/18/15.
//  Copyright (c) 2015 Rick Royd Aban. All rights reserved.
//

#import "OnlineGateway.h"
#import "AppDelegate.h"
#import "Dashboard.h"
#import "Leave.h"
#import "ClaimHeader.h"
#import "ClaimCategory.h"
#import "MileageClaimItem.h"
#import "LocalHoliday.h"
#import "Holiday.h"
#import "Project.h"
#import "Recruitment.h"
#import "ClaimHeader.h"
#import "CapexForApprovalLineItem.h"
#import "CapexLineItemQoutation.h"
#import "CostCenter.h"
#import "ClaimPaidByCC.h"
#import "ClaimNotPaidByCC.h"
#import "BusinessAdvance.h"
#import "Liquidation.h"
#import "GiftHospitality.h"
#import "Contract.h"

@interface OnlineGateway(){
    AppDelegate *_appDelegate;
    NSString *_apiUrl, *_root;
    NSDateFormatter *_dateTimeFormat;
}
@end

@implementation OnlineGateway

static NSString const *AUTHKEY = @"ak0ayMa+apAn6";
static NSString const *ERROR_MSG = @"There was an error with the internet connection. Please try again.";

- (OnlineGateway *)initWithAppDelegate:(AppDelegate *)appDelegate {
    self = [super init];
    if(self) {
        _appDelegate = appDelegate;
//        _apiUrl = @"https://saltapi.velosi.com/SALTService.svc/";    //PROD WEB SERVICE
        _apiUrl = @"https://salttestapi.velosi.com/SALTService.svc/"; //TEST WEB SERVICE
//        _apiUrl = @"http://10.63.201.6:90/SALTService.svc/";       //LOCAL WEB SERVICE
        
//        _root = @"https://salt.velosi.com/";                         //PROD WEB
        _root = @"https://salttest.velosi.com/";                      //TEST WEB
//          _root = @"http://10.63.201.6:91/";                       //LOCAL WEB
        _dateTimeFormat = [[NSDateFormatter alloc] init];
        _dateTimeFormat.dateFormat = @"yyyy-MM-dd HH:mm:ss";
    }
    return self;
}

-(id)makeWebServiceCall:(NSString*)url requestMethod:(int)requestMethod postBody:(NSString*)jsonString {
    NSLog(@"URL: %@",url);
    NSURLSessionConfiguration *requestConfig = [NSURLSessionConfiguration defaultSessionConfiguration];
    [requestConfig setTimeoutIntervalForResource:15];
    requestConfig.HTTPAdditionalHeaders = @{@"AuthKey": AUTHKEY,
                                            @"Accept": @"application/json",
                                            @"Content-Type": @"application/json"};
    NSURL *apiURL = [NSURL URLWithString:url];
//    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:apiURL cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:30];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:apiURL];
    NSURLSessionDataTask *dataTask = nil;
    __block NSURLResponse *response = [[NSURLResponse alloc] init];
    __block NSError *error = [[NSError alloc] init];
    __block NSData *data = nil;
    
    dispatch_semaphore_t semaphore = dispatch_semaphore_create(0);
    if (requestMethod == GET) {
        [request setHTTPMethod:@"GET"];
    } else {
        NSLog(@"JSON: %@",jsonString);
        [request setHTTPMethod:@"POST"];
        [request setHTTPBody:[NSData dataWithBytes:[jsonString UTF8String] length:[jsonString length]]];
    }
    NSURLSession *urlSession = [NSURLSession sessionWithConfiguration:requestConfig];
    dataTask = [urlSession dataTaskWithRequest:request completionHandler:^(NSData *taskData, NSURLResponse *taskResponse, NSError *taskError){
        data = taskData;
        if (response)
            response = taskResponse;
        if (error)
            error = taskError;
        dispatch_semaphore_signal(semaphore);
    }];
    [dataTask resume];
    dispatch_semaphore_wait(semaphore, DISPATCH_TIME_FOREVER);
    
    if([(NSHTTPURLResponse*)response statusCode] != 200) {
        NSLog(@"ERROR DESC: %@", [self responseErrorDescriptionFromStatusCode:error.code]);
        return ERROR_MSG;
    } else
        return data;
}

//DEPRECATED METHOD
//- (id)httpPostFrom:(NSString *)url withBody:(NSString *)jsonString{
//    NSLog(@"%@",url);
//    NSLog(@"%@",jsonString);
//    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:url]];
//    request.HTTPMethod = @"POST";
//    [request setValue:[NSString stringWithFormat:@"%d",(int)jsonString.length] forHTTPHeaderField:@"Content-Length"];
//    [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
//    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
//    [request setValue:@"ak0ayMa+apAn6" forHTTPHeaderField:@"AuthKey"];
//    request.HTTPBody = [NSData dataWithBytes:[jsonString UTF8String] length:[jsonString length]];
//    
//    NSError *error = [[NSError alloc] init];
//    NSHTTPURLResponse *responseCode = nil;
//    
//    NSData *responseData = [NSURLConnection sendSynchronousRequest:request returningResponse:&responseCode error:&error];
//    if(responseCode.statusCode != 200)
//        return [self responseErrorDescriptionFromStatusCode:(int)responseCode.statusCode];
//    
//    return responseData;
//}

//- (id)httpsGetFrom:(NSString *)url{
//    NSLog(@"%@",url);
//    @try {
//        NSURLSessionConfiguration *requestConfig = [NSURLSessionConfiguration defaultSessionConfiguration];
//        requestConfig.HTTPAdditionalHeaders = @{@"AuthKey":AUTHKEY};
//        NSURLSession *httpSession = [NSURLSession sessionWithConfiguration:requestConfig];
//        NSURL *apiUrl = [NSURL URLWithString:url];
//        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:apiUrl];
//        [request setHTTPMethod:@"GET"];
//        NSError *error = nil;
//        NSHTTPURLResponse *responseCode = nil;
//        NSData *responseData = [httpSession sendSynchronousDataTaskWithRequest:request returningResponse:&responseCode error:&error];
//        if(responseCode.statusCode != 200)
//            return [self responseErrorDescriptionFromStatusCode:(int)responseCode.statusCode];
//        return responseData;
//    } @catch (NSException *exception) {
//        return exception.reason;
//    }
//}

//returns nsdata if posted successfully otherwise returns a string message
- (NSString *)responseErrorDescriptionFromStatusCode:(long)statusCode{
    NSLog(@"RESPONSE CODE: %ld",statusCode);
    // 4XX: client error
    // 5XX: server error
    switch (statusCode) {
        case 204: return @"No Response";
        case 400: return @"Bad Request";
        case 401: return @"Unauthorized";
        case 402: return @"Payment Required";
        case 403: return @"Not Authorized";
        case 404: return @"Not Found";
        case 405: return @"Method Not Allowed";
        case 406: return @"Not Acceptable";
        case 407: return @"Proxy Authentication Required";
        case 409: return @"Conflict";
        case 410: return @"Gone";
        case 411: return @"Length Required";
        case 412: return @"Precondition Failed";
        case 413: return @"Request Entity Too Large";
        case 414: return @"Request-URI Too Long";
        case 415: return @"Unsupported Media Type";
        case 416: return @"Request Range Not Satisfiable";
        case 417: return @"Expectation Failed";
        case 500: return @"Internal Server Error";
        case 501: return @"Not Implemented";
        case 502: return @"Service temporarily overloaded";
        case NSURLErrorNotConnectedToInternet: return @"No Internet Connection";
        case NSURLErrorTimedOut: return @"Connection Timed Out";
        case NSURLErrorNetworkConnectionLost: return @"Connection Lost";
        case NSURLErrorCannotConnectToHost: return @"Cannot connect to Host";
        case NSURLErrorCannotFindHost: return @"Cannot find to Host";
        case NSURLErrorBadServerResponse: return @"Bad Server Response";
        case NSURLErrorResourceUnavailable: return @"Resource unavailable";
        default: return @"Server Connection Failed";
    }
}

- (NSString *)epochizeDate:(NSDate *)date {
//    NSTimeInterval todayDateInMilleseconds = [date timeIntervalSince1970]*1000; //1000 to convert the seconds to milliseconds
    NSInteger localToGmtOffset = [[NSTimeZone defaultTimeZone] secondsFromGMTForDate:date];
    NSInteger bstToGmtOffset = [[NSTimeZone timeZoneWithAbbreviation:@"BST"] secondsFromGMT];
    NSTimeInterval timeOffset = localToGmtOffset - bstToGmtOffset;
    NSDate *timeStamp = [date dateByAddingTimeInterval:timeOffset];
    double processedDate = [timeStamp timeIntervalSince1970] * 1000;
    return [NSString stringWithFormat:@"/Date(%.0lf+0000)/",processedDate];
}

-(NSString *)convertToUnixDate:(NSDate *)date{
    NSTimeInterval todayDateInMilleseconds = [date timeIntervalSince1970]*1000;
    return [NSString stringWithFormat:@"/Date(%.0f+0000)/",todayDateInMilleseconds];
}

-(NSString *)convertToUnixLeaveDate:(NSDate *)date {
    NSTimeZone *sourceTimeZone = _appDelegate.propDateFormatLeaveDuration.timeZone;
    NSTimeInterval timeOffset = [sourceTimeZone secondsFromGMTForDate:date];
    NSDate *leaveTimeStamp = [date dateByAddingTimeInterval:timeOffset];
    NSTimeInterval leaveDateOffsetInMS = [leaveTimeStamp timeIntervalSince1970] * 1000;
    return [NSString stringWithFormat:@"/Date(%.0lf+0000)/", leaveDateOffsetInMS];
}

- (NSString *)deserializeUnixDate: (NSString *)jsonDateString {
    @try {
        NSInteger startEpochPos = [jsonDateString rangeOfString:@"("].location + 1; //start of the date value
        NSInteger endEpochPos = [jsonDateString rangeOfString:@"+" options:NSBackwardsSearch].location - 6; //end of the date value
        NSTimeInterval unixTime = [[jsonDateString substringWithRange:NSMakeRange(startEpochPos, endEpochPos)] doubleValue] / 1000; //WCF will send 13 digit-long value for the time interval since 1970 (millisecond precision) whereas iOS works with 10 digit-long values (second precision), hence the divide by 1000
        NSTimeInterval timeZone = [[NSTimeZone defaultTimeZone] secondsFromGMT];
        NSDate *leaveUnixDate = [[NSDate dateWithTimeIntervalSince1970:unixTime] dateByAddingTimeInterval:timeZone];
        return [_appDelegate.propFormatVelosiDate stringFromDate:leaveUnixDate];
//        return [NSDate dateWithTimeIntervalSinceNow:unixTime];
    }@catch(NSException *e) {
        NSLog(@"%@",[e reason]);
    }
}

- (NSString *)deserializeJsonDateString: (NSString *)jsonDateString {
    @try {
        NSInteger startEpochPos = [jsonDateString rangeOfString:@"("].location + 1; //start of the date value
        NSInteger endEpochPos = [jsonDateString rangeOfString:@"+" options:NSBackwardsSearch].location - 6; //end of the date value
        NSTimeInterval unixTime = [[jsonDateString substringWithRange:NSMakeRange(startEpochPos, endEpochPos)] doubleValue] / 1000; //WCF will send 13 digit-long value for the time interval since 1970 (millisecond precision) whereas iOS works with 10 digit-long values (second precision), hence the divide by 1000
        NSDate *epochDate = [NSDate dateWithTimeIntervalSince1970:unixTime];
        return [_appDelegate.propDateFormatByProcessed stringFromDate:epochDate];
    }@catch(NSException *e) {
        return e.reason;
    }
}

- (NSString *)dateToday{
    return [_dateTimeFormat stringFromDate:[NSDate date]];
}

- (NSString *)authenticateUsername:(NSString *)username password:(NSString *)password {
    NSString *urlLogin = [NSString stringWithFormat:@"%@AuthenticateLogin?userName=%@&password=%@",_apiUrl,username,password];
//    NSString *urlLogin = [NSString stringWithFormat:@"%@AuthenticateLogin?userName=%@&password=%@&datetime=%@",_apiUrl,username,password,[self dateToday]];
    id data = [self makeWebServiceCall:[self encodeURLString:urlLogin] requestMethod:POST postBody:nil];
    if(![data isKindOfClass:[NSString class]]) {
        NSDictionary *result = [[NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:nil] objectForKey:@"AuthenticateLoginResult"];
        if([NSArray arrayWithArray:[result objectForKey:@"SystemErrors"]].count > 0) {
            NSString *authError = [[[NSArray arrayWithArray:[result objectForKey:@"SystemErrors"]] objectAtIndex:0] objectForKey:@"Message"];
            NSString *ldapError = [[authError componentsSeparatedByString:@" "] objectAtIndex:0];
            if ([ldapError isEqualToString:@"LDAP"] || [ldapError isEqualToString:@"Server"])
                return @"Username or password is invalid";
            else
                return authError;
        } else
            return [NSString stringWithFormat:@"%@-%@-%@", [result objectForKey:@"StaffID"], [result objectForKey:@"SecurityLevel"], [result objectForKey:@"OfficeID"]];
    }else
        return data;
}

- (id)initializeDataWithStaffID:(int)staffID securityLevel:(int)securityLevel officeID:(int)officeID {
    NSString *updateStaffResultError = [self updateAppStaffDataWithStaffID:staffID securityLevel:securityLevel officeID:officeID];
    if(updateStaffResultError)
        return updateStaffResultError;
    id myLeaveResult = [self myLeaves];
    if([myLeaveResult isKindOfClass:[NSString class]])
        return myLeaveResult;
    id leavesForApprovalResult;
    if([[_appDelegate getStaff] isAdmin] || [[_appDelegate getStaff] isCM] || [[_appDelegate getStaff] isManager] || [[_appDelegate getStaff] isAM]) {
        leavesForApprovalResult = [NSMutableArray array];
        if([leavesForApprovalResult isKindOfClass:[NSString class]])
            return leavesForApprovalResult;
    }
    [_appDelegate initMyLeaves:myLeaveResult leavesForApproval:leavesForApprovalResult];
    return nil;
}

- (NSString *)updateAppStaffDataWithStaffID:(int)staffID securityLevel:(int)securityLevel officeID:(int)officeID{
    NSString *urlStaff = [NSString stringWithFormat:@"%@GetStaffByID?staffID=%d&requestingPerson=%d&datetime=%@",_apiUrl, staffID, staffID, [self dateToday]];
    NSString *urlOffice = [NSString stringWithFormat:@"%@GetOfficeByID?officeID=%d&requestingPerson=%d&datetime=%@",_apiUrl, officeID, staffID, [self dateToday]];
    id staffResultString = [self makeWebServiceCall:[self encodeURLString:urlStaff] requestMethod:GET postBody:nil];
    id officeResultString = [self makeWebServiceCall:[self encodeURLString:urlOffice] requestMethod:GET postBody:nil];
    NSError *error;
    @try{
        if([staffResultString isKindOfClass:[NSString class]] || [officeResultString isKindOfClass:[NSString class]])
            return staffResultString;
        else{
            NSDictionary *staffResult = [[NSJSONSerialization JSONObjectWithData:staffResultString options:0 error:&error] objectForKey:@"GetStaffByIDResult"];
            NSDictionary *officeResult = [[NSJSONSerialization JSONObjectWithData:officeResultString options:0 error:&error] objectForKey:@"GetOfficeByIDResult"];
            
            if([NSArray arrayWithArray:[staffResult objectForKey:@"SystemErrors"]].count > 0)
                return [[[NSArray arrayWithArray:[staffResult objectForKey:@"SystemErrors"]] objectAtIndex:0] objectForKey:@"Message"];
            
            if([NSArray arrayWithArray:[officeResult objectForKey:@"SystemErrors"]].count > 0)
                return [[[NSArray arrayWithArray:[officeResult objectForKey:@"SystemErrors"]] objectAtIndex:0] objectForKey:@"Message"];
            [_appDelegate updateStaffDataWithStaff:[[Staff alloc] initWithOnlineGateway:self staffDictionary:[staffResult objectForKey:@"Staff"]] office:[[Office alloc] initWithDictionary:[officeResult objectForKey:@"Office"] isFullDetail:true] key:self];
        }        
        return nil;
    }@catch(NSException *e){
        return [NSString stringWithFormat:@"Server connection failed %@",e.reason];
    }
}


- (id)dashboard{
    @try{
        int staffID = [[_appDelegate getStaff] propStaffID];
        int staffSecurityLevel = [[_appDelegate getStaff] propSecurityLevel];
        NSString *role;
        if([[_appDelegate getStaff] getUserPosition] == USERPOSITION_CM) role = @"CM";
        else if([[_appDelegate getStaff] getUserPosition] == USERPOSITION_RM) role = @"RM";
        else if([[_appDelegate getStaff] getUserPosition] == USERPOSITION_CFO) role = @"CFO";
        else if([[_appDelegate getStaff] getUserPosition] == USERPOSITION_CEO) role = @"CEO";
        else role = @"";
        
        NSString *urlDashboardCnt = [NSString stringWithFormat:@"%@DashboardCount?staffID=%d&userRole=%@&securityLevel=%d&requestingPerson=%d",_apiUrl, staffID, role, staffSecurityLevel, staffID];
        id data = [self makeWebServiceCall:[self encodeURLString:urlDashboardCnt] requestMethod:GET postBody:nil];
        if([data isKindOfClass:[NSString class]])
            return data;
        else{
            NSDictionary *result =[[NSJSONSerialization JSONObjectWithData:data options:0 error:nil] objectForKey:@"DashboardCountResult"];
            return ([NSArray arrayWithArray:[result objectForKey:@"SystemErrors"]].count > 0)?[[[NSArray arrayWithArray:[result objectForKey:@"SystemErrors"]] objectAtIndex:0] objectForKey:@"Message"] : [result objectForKey:@"Dashboards"];
        }
    }@catch(NSException *e){
        return [NSString stringWithFormat:@"Server connection failed %@",e.reason];
    }
}

- (id)projectsByCostCenterID:(int)costCenterID{
    @try{
        NSString *urlProjectByCostCenter = [NSString stringWithFormat:@"%@GetProjectByCostCenter?costCenterID=%d&requestingperson=%d&datetime=%@",_apiUrl,costCenterID,[[_appDelegate getStaff] propStaffID], [self dateToday]];
        id data = [self makeWebServiceCall:[self encodeURLString:urlProjectByCostCenter] requestMethod:GET postBody:nil];
        if([data isKindOfClass:[NSString class]]){
            return data;
        }else{
            NSDictionary *result = [[NSJSONSerialization JSONObjectWithData:data options:0 error:nil] objectForKey:@"GetProjectByCostCenterResult"];
            NSMutableArray *projects = [NSMutableArray array];
            NSArray *jsonProjects = [result objectForKey:@"Projects"];
            
            if(jsonProjects){
                for(id jsonProject in jsonProjects)
                    [projects addObject:[[Project alloc] initWithDictionary:jsonProject]];
            }
            return projects;
        }
    }@catch(NSException * exception){
        return exception.reason;
    }
}

- (id)myLeaves{
    @try{
        NSString *filter = [NSString stringWithFormat:@"WHERE year(DateFrom)=Year(getdate()) AND StaffID=%d AND ((LeaveStatus=18)OR(LeaveStatus=20)) ORDER BY DateSubmitted DESC",[[_appDelegate getStaff] propStaffID]];
        NSString *urlLeaves = [NSString stringWithFormat:@"%@GetAllLeave?filter=%@&requestingperson=%d&datetime=%@",_apiUrl, filter,[[_appDelegate getStaff] propStaffID],[self dateToday]];
        id data = [self makeWebServiceCall:[self encodeURLString:urlLeaves] requestMethod:GET postBody:nil];
        
        if([data isKindOfClass:[NSString class]])
            return data;
        else{
            NSDictionary *result = [[NSJSONSerialization JSONObjectWithData:data options:0 error:nil] objectForKey:@"GetAllLeaveResult"];
            if([NSArray arrayWithArray:[result objectForKey:@"SystemErrors"]].count > 0)
                return [[[NSArray arrayWithArray:[result objectForKey:@"SystemErrors"]] objectAtIndex:0] objectForKey:@"Message"];
            else{
                NSError *error = [[NSError alloc] init];
                
                NSMutableArray *leaves = [NSMutableArray array];
                NSArray *jsonLeaves = [result objectForKey:@"Leaves"];
                
                if(jsonLeaves){
                    for(id jsonLeave in jsonLeaves)
                        [leaves addObject:[[Leave alloc] initWithJSONDictionary:jsonLeave appDelegate:_appDelegate]];
                    return leaves;
                }else
                    return error.localizedFailureReason;
                //                if(jsonLeaves){
                //                    for(id jsonLeave in jsonLeaves){
                //                        if(!([[jsonLeave objectForKey:@"LeaveStatus"] intValue] == LEAVESTATUSID_CANCELLED))
                //                            [leaves addObject:[[Leave alloc] initWithJSONDictionary:jsonLeave onlineGateway:self]];
                //                    }
                //                    return leaves;
                //                }else
                //                    return error.localizedFailureReason;
            }
        }
    }@catch(NSException *exception){
        return @"Server Connection Failed";
    }
}

- (id)leavesForApproval{
    @try{
        NSString *filter = [NSString stringWithFormat:@"WHERE (year(DateSubmitted)=Year(getdate()) OR year(DateFrom)=Year(getdate()-1)) AND (LeaveApprover1=%d OR LeaveApprover2=%d OR LeaveApprover3=%d) ORDER BY DateSubmitted DESC",[[_appDelegate getStaff] propStaffID], [[_appDelegate getStaff] propStaffID], [[_appDelegate getStaff] propStaffID]];
        NSString *urlGetAllLeave = [NSString stringWithFormat:@"%@GetAllLeave?filter=%@&requestingperson=%d&datetime=%@",_apiUrl, filter,[[_appDelegate getStaff] propStaffID], [self dateToday]];
        id data = [self makeWebServiceCall:[self encodeURLString:urlGetAllLeave] requestMethod:GET postBody:nil];
        
        if([data isKindOfClass:[NSString class]])
            return data;
        else{
            NSDictionary *result = [[NSJSONSerialization JSONObjectWithData:data options:0 error:nil] objectForKey:@"GetAllLeaveResult"];
            if([NSArray arrayWithArray:[result objectForKey:@"SystemErrors"]].count > 0)
                return [[[NSArray arrayWithArray:[result objectForKey:@"SystemErrors"]] objectAtIndex:0] objectForKey:@"Message"];
            else{
                NSError *error = [[NSError alloc] init];
                
                NSMutableArray *leavesForApproval = [NSMutableArray array];
                NSArray *jsonLeaves = [result objectForKey:@"Leaves"];
                
                if(jsonLeaves){
                    for(id jsonLeave in jsonLeaves)
                        [leavesForApproval addObject:[[Leave alloc] initWithJSONDictionary:jsonLeave appDelegate:_appDelegate]];
                    return leavesForApproval;
                }else
                    return error.localizedFailureReason;
            }
        }
    }@catch(NSException *exception){
        return @"Server Connection Failed";
    }
}

//claims
//- (id)myClaims{
//    @try{
//        NSString *filter = [NSString stringWithFormat:@"WHERE year(DateSubmitted)=year(getdate()) AND StaffID=%d ORDER BY DateSubmitted DESC",[[_appDelegate staff] staffID]];
//        id data = [self httpsGetFrom:[NSString stringWithFormat:@"%@GetFilterClaims?filter=%@&requestingperson=%d&datetime=%@",_apiUrl,[filter stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding],[[_appDelegate staff] staffID], [self dateToday]]];
//
//        NSDictionary *result = [[NSJSONSerialization JSONObjectWithData:data options:0 error:nil] objectForKey:@"GetFilterClaimsResult"];
//        if([NSArray arrayWithArray:[result objectForKey:@"SystemErrors"]].count > 0)
//            return [[[NSArray arrayWithArray:[result objectForKey:@"SystemErrors"]] objectAtIndex:0] objectForKey:@"Message"];
//        else{
//            NSMutableArray *myClaims = [NSMutableArray array];
//            NSArray *jsonMyClaims = [result objectForKey:@"Claims"];
//
//            if(jsonMyClaims){
//                for(id jsonMyClaim in jsonMyClaims)
//                    [myClaims addObject:[[ClaimHeader alloc] initWithJSONDictionary:jsonMyClaim onlineGateway:self]];
//                return myClaims;
//            }
//        }
//    }@catch(NSException *exception){
//        return @"Server Connection Failed";
//    }
//}

- (id)myClaimsWithStaffID:(int)staffID requestingPerson:(int)requestingPersonID {
    @try{
        NSString *urlClaimsByStaff = [NSString stringWithFormat:@"%@GetClaimsByStaff?staffID=%d&requestingPerson=%d&datetime=%@", _apiUrl, staffID, requestingPersonID, [self dateToday]];
        id data = [self makeWebServiceCall:[self encodeURLString:urlClaimsByStaff] requestMethod:GET postBody:nil];
        if([data isKindOfClass:[NSString class]])
            return data;
        else{
            NSDictionary *result = [[NSJSONSerialization JSONObjectWithData:data options:0 error:nil] objectForKey:@"GetClaimsByStaffResult"];
            if([NSArray arrayWithArray:[result objectForKey:@"SystemErrors"]].count > 0)
                return [[[NSArray arrayWithArray:[result objectForKey:@"SystemErrors"]] objectAtIndex:0] objectForKey:@"Message"];
            else{
                NSMutableArray *myClaims = [NSMutableArray array];
                NSArray *jsonMyClaims = [result objectForKey:@"Claims"];
                
                if(jsonMyClaims){
                    for(id jsonMyClaim in jsonMyClaims)
                        [myClaims addObject:[[ClaimHeader alloc] initWithJSONDictionary:jsonMyClaim withAppDelegate:_appDelegate]];
                    
                    return myClaims;
                }
            }
        }
    }@catch(NSException *exception){
        return @"Server Connection Failed";
    }
}

- (id)claimsForApproval{
    @try{
        NSMutableString *filter = [NSMutableString string];
        int staffID = [[_appDelegate getStaff] propStaffID];
        if([[_appDelegate getStaff] propIsApprover]){
            if([[_appDelegate getStaff] getUserPosition] == USERPOSITION_CM || [[_appDelegate getStaff] getUserPosition] == USERPOSITION_CFO)
                [filter appendFormat:@"WHERE (ClaimStatus=%d AND ClaimCountryManagerID = %d) OR (ClaimStatus=%d AND ApproverID=%d)", CLAIMSTATUSID_APPROVEDBYAPPROVER, staffID, CLAIMSTATUSID_SUBMITTED, staffID];
            else
                [filter appendFormat:@"WHERE ClaimStatus=%d AND ApproverID=%d", CLAIMSTATUSID_SUBMITTED, staffID];
        }

        [filter appendString:@" ORDER BY DateCreated DESC"];
        NSString *urlClaimForApproval = [NSString stringWithFormat:@"%@GetFilterClaims?filter=%@&requestingperson=%d&datetime=%@", _apiUrl, filter, [[_appDelegate getStaff] propStaffID], [self dateToday]];
        id data = [self makeWebServiceCall:[self encodeURLString:urlClaimForApproval] requestMethod:GET postBody:nil];
        if([data isKindOfClass:[NSString class]])
            return data;
        else {
            NSDictionary *result = [[NSJSONSerialization JSONObjectWithData:data options:0 error:nil] objectForKey:@"GetFilterClaimsResult"];
            if([NSArray arrayWithArray:[result objectForKey:@"SystemErrors"]].count > 0)
                return [[[NSArray arrayWithArray:[result objectForKey:@"SystemErrors"]] objectAtIndex:0] objectForKey:@"Message"];
            else {
                NSMutableArray *claimsForApproval = [NSMutableArray array];
                NSArray *jsonClaimsForApproval = [result objectForKey:@"Claims"];
                if(jsonClaimsForApproval) {
                    for(id jsonClaimForApproval in jsonClaimsForApproval)
                        [claimsForApproval addObject:[[ClaimHeader alloc] initWithJSONDictionary:jsonClaimForApproval withAppDelegate:_appDelegate]];
                    return claimsForApproval;
//                    for (ClaimHeader *claim in claimsForAccountsApproval) {
//                        if (claim.claimCMId == staffID)
//                            [claimsForCMApproval addObject:claim];
//                    }
//                    if ([[_appDelegate getStaff] getUserPosition] == USERPOSITION_CM || [[_appDelegate getStaff] getUserPosition] == USERPOSITION_CFO) {
//                        return claimsForCMApproval;
//                    } else {
//                        return claimsForAccountsApproval;
//                    }
                }
            }
        }
    }@catch(NSException *exception){
        return @"Server Connection Failed";
    }
}

-(id)claimByID:(int)claimID andStaffID:(int)staffID {
    @try{
        NSString *urlClaimByID = [NSString stringWithFormat:@"%@GetByClaimID?claimID=%d&requestingPerson=%d&datetime=%@", _apiUrl, claimID, staffID, [self dateToday]];
        id data = [self makeWebServiceCall:[self encodeURLString:urlClaimByID] requestMethod:GET postBody:nil];
        if ([data isKindOfClass:[NSString class]])
            return data;
        else{
            NSDictionary *result = [[NSJSONSerialization JSONObjectWithData:data options:0 error:nil] objectForKey:@"GetByClaimIDResult"];
            if ([NSArray arrayWithArray:[result objectForKey:@"SystemErrors"]].count > 0)
                return [[[NSArray arrayWithArray:[result objectForKey:@"SystemErrors"]] objectAtIndex:0] objectForKey:@"Message"];
            return [result objectForKey:@"Claim"];
        }
    }@catch(NSException *exception){
        NSLog(@"%@",[exception reason]);
        return @"Server Connection Failed";
    }
}

- (id)claimItemsForClaimID:(int)claimID{
    //loaded data will be cached within this scope so that data wont be reloaded again and again
    NSMutableDictionary *cachedCategories = [NSMutableDictionary dictionary];
    @try{
        NSString *urlClaimLineItems = [NSString stringWithFormat:@"%@GetClaimLineItems?claimID=%d&requestingPerson=%d&datetime=%@", _apiUrl, claimID, [[_appDelegate getStaff] propStaffID], [self dateToday]];
        id data = [self makeWebServiceCall:[self encodeURLString:urlClaimLineItems] requestMethod:GET postBody:nil];
        
        if([data isKindOfClass:[NSString class]])
            return data;
        else{
            NSDictionary *result = [[NSJSONSerialization JSONObjectWithData:data options:0 error:nil] objectForKey:@"GetClaimLineItemsResult"];
            if([NSArray arrayWithArray:[result objectForKey:@"SystemErrors"]].count > 0)
                return [[[NSArray arrayWithArray:[result objectForKey:@"SystemErrors"]] objectAtIndex:0] objectForKey:@"Message"];
            else{
                NSMutableArray *myClaimItems = [NSMutableArray array];
                NSArray *jsonMyClaimItems = [result objectForKey:@"ClaimLineItems"];
                if(jsonMyClaimItems){
                    for(id jsonMyClaimItem in jsonMyClaimItems){
                        if([cachedCategories objectForKey:[jsonMyClaimItem objectForKey:[ClaimItem KEY_CATEGORYID]]] == nil){
                            NSString *urlCategoryID = [NSString stringWithFormat:@"%@GetByCategoryID?categoryID=%d&requestingperson=%d&datetime=%@",_apiUrl, [[jsonMyClaimItem objectForKey:[ClaimItem KEY_CATEGORYID]] intValue], [[_appDelegate getStaff] propStaffID], [self dateToday]];
                            id catData = [self makeWebServiceCall:[self encodeURLString:urlCategoryID] requestMethod:GET postBody:nil];
                            
                            NSDictionary *catResult = [[NSJSONSerialization JSONObjectWithData:catData options:0 error:nil] objectForKey:@"GetByCategoryIDResult"];
                            if([NSArray arrayWithArray:[result objectForKey:@"SystemErrors"]].count > 0)
                                return [[[NSArray arrayWithArray:[result objectForKey:@"SystemErrors"]] objectAtIndex:0] objectForKey:@"Message"];
                            
                            NSMutableDictionary *jsonCategory = [catResult objectForKey:@"Category"];
                            [cachedCategories setObject:jsonCategory forKey:[jsonMyClaimItem objectForKey:[ClaimItem KEY_CATEGORYID]]];
                        }
                        NSMutableDictionary *category = [cachedCategories objectForKey:[jsonMyClaimItem objectForKey:[ClaimItem KEY_CATEGORYID]]];
                        int categoryType = [[category objectForKey:[ClaimItem KEY_CATEGORYTYPEID]] intValue];
                        if(categoryType != [ClaimCategory TYPE_ASSET]){ //exclude categories of type asset
                            if(categoryType == [ClaimCategory TYPE_MILEAGE])
                                [myClaimItems addObject:[[MileageClaimItem alloc] initWithJSONClaimItem:jsonMyClaimItem JSONCategory:category appDelegate:_appDelegate]];
                            else
                                [myClaimItems addObject:[[ClaimItem alloc] initWithJSONClaimItem:jsonMyClaimItem JSONCategory:category appDelegate:_appDelegate]];
                        }
                    }
                }
                return myClaimItems;
            }
        }
    }@catch(NSException *exception){
        NSLog(@"Exception %@",exception.reason);
        return @"Server Connection Failed";
    }
}

- (id)claimItemCategoriesByOffice{
    @try{
        NSString *urlOfficeCategory = [NSString stringWithFormat:@"%@GetCategoryByOffice?officeID=%d&requestingperson=%d&datetime=%@",_apiUrl, [[_appDelegate getStaff] propOfficeID], [[_appDelegate getStaff] propStaffID], [self dateToday]];
//        id data = [self httpsGetFrom:[self stringByAddingPercentEncodingForFormData:urlOfficeCategory]];
        id data = [self makeWebServiceCall:[self encodeURLString:urlOfficeCategory] requestMethod:GET postBody:nil];
        if([data isKindOfClass:[NSString class]])
            return data;
        else{
            NSDictionary *result = [[NSJSONSerialization JSONObjectWithData:data options:0 error:nil] objectForKeyedSubscript:@"GetCategoryByOfficeResult"];
            if([NSArray arrayWithArray:[result objectForKeyedSubscript:@"SystemErrors"]].count > 0)
                return [[[NSArray arrayWithArray:[result objectForKeyedSubscript:@"SystemErrors"]] objectAtIndex:0] objectForKeyedSubscript:@"Message"];
            else{
                NSMutableArray *categories = [NSMutableArray array];
                NSArray *jsonCategories = [result objectForKey:@"Categories"];
                
                if(jsonCategories){
                    for(id jsonCategory in jsonCategories)
                        [categories addObject:[[ClaimCategory alloc] initWithDictionary:jsonCategory]];
                }
                return categories;
            }
        }
    }@catch(NSException *e){
        return @"Server Connection Failed";
    }
}

#pragma mark TODO HOLIDAY METHODS

- (id)localHolidays{
    NSDateFormatter *dayFormatter = [[NSDateFormatter alloc] init];
    NSDateFormatter *monthFormatter =  [[NSDateFormatter alloc] init];
    dayFormatter.dateFormat = @"EEEE"; //getting the day name
    monthFormatter.dateFormat = @"LLLL"; //getting the month name
    @try{
        NSString *urlNationalHolidayByOffice = [NSString stringWithFormat:@"%@GetNationalHolidaysByOffice?officeID=%d&requestingperson=%d&datetime=%@", _apiUrl,[[_appDelegate getStaff] propOfficeID], [[_appDelegate getStaff] propStaffID], [self dateToday]];
//        id data = [self httpsGetFrom:[self stringByAddingPercentEncodingForFormData:urlNationalHolidayByOffice]];
        id data = [self makeWebServiceCall:[self encodeURLString:urlNationalHolidayByOffice] requestMethod:GET postBody:nil];
        if([data isKindOfClass:[NSString class]])
            return data;
        else{
            NSDictionary *result = [[NSJSONSerialization JSONObjectWithData:data options:0 error:nil] objectForKey:@"GetNationalHolidaysByOfficeResult"];
            if([NSArray arrayWithArray:[result objectForKey:@"SystemErros"]].count > 0)
                return [[[NSArray arrayWithArray:[result objectForKey:@"SystemErrors"]] objectAtIndex:0] objectForKey:@"Message"];
            else{
                NSError *error = [[NSError alloc] init];
                NSMutableArray *holidays = [NSMutableArray array];
                NSArray *jsonHolidays = [result objectForKey:@"NationalHolidays"];
                if(jsonHolidays) {
                    for(id jsonHoliday in jsonHolidays){
                        if([[jsonHoliday objectForKey:@"Active"] boolValue]){ //only active holidays must be retrieved
                            NSArray *jsonApplicableOffices =  [jsonHoliday objectForKey:@"OfficesApplied"];
                            for(id jsonApplicableOffice in jsonApplicableOffices){ //not all offices must observe a holiday so must recheck
                                if([[jsonApplicableOffice objectForKey:@"OfficeID"] intValue] == [[_appDelegate getStaff] propOfficeID]){
                                    NSString *dateStr = [self deserializeUnixDate:[jsonHoliday objectForKey:@"Date"]];
//                                    NSDate *holidayDate = [self deserializeUnixDate:[jsonHoliday objectForKey:@"Date"]];
//                                    NSTimeInterval timeZone = [[NSTimeZone defaultTimeZone] secondsFromGMT];
//                                    NSString *dateStr = [_appDelegate.propFormatVelosiDate stringFromDate:[holidayDate dateByAddingTimeInterval:timeZone]];
                                    if([[dateStr componentsSeparatedByString:@"-"][2] intValue] == [_appDelegate currYear]){
                                        NSDate *date = [_appDelegate.propFormatVelosiDate dateFromString:dateStr];
                                        [holidays addObject:[[LocalHoliday alloc] initWithName:[jsonHoliday objectForKey:@"Name"] date:dateStr day:[dayFormatter stringFromDate:date] month:[monthFormatter stringFromDate:date]]];
                                    }
                                    break;
                                }
                            }
                        }
                    }
                }else
                    return error.localizedFailureReason;
                
                return holidays;
            }
        }
    }@catch(NSException *e){
        return @"Server Connection Failed";
    }
}

- (id)monthlyholidays{
    //    NSMutableDictionary *flagDics = [NSMutableDictionary dictionary];
    @try{
        NSString *urlNationalHoliday = [NSString stringWithFormat:@"%@GetAllNationalHoliday?requestingperson=%d&datetime=%@", _apiUrl, [[_appDelegate getStaff] propOfficeID], [self dateToday]];
//        id data = [self httpsGetFrom:[self stringByAddingPercentEncodingForFormData:urlNationalHoliday]];
        id data = [self makeWebServiceCall:[self encodeURLString:urlNationalHoliday] requestMethod:GET postBody:nil];
        if([data isKindOfClass:[NSString class]])
            return data;
        else{
            NSDictionary *result = [[NSJSONSerialization JSONObjectWithData:data options:0 error:nil] objectForKey:@"GetAllNationalHolidayResult"];
            //            NSLog(@"Key Size: %lu",[result count]);
            if([NSArray arrayWithArray:[result objectForKey:@"SystemErrors"]].count > 0)
                return [[[NSArray arrayWithArray:[result objectForKey:@"SystemErrors"]] objectAtIndex:0] objectForKey:@"Message"];
            else{
                NSMutableArray *holidays = [NSMutableArray array];
                NSArray *jsonHolidays = [result objectForKey:@"NationalHoliday"];
                
                if(jsonHolidays) {
                    for(id jsonHoliday in jsonHolidays) {
                        if([[jsonHoliday objectForKey:@"Active"] boolValue]){
                            NSString *dateStr = [self deserializeUnixDate:[jsonHoliday objectForKey:@"Date"]];
//                            NSDate *holidayDate = [self deserializeUnixDate:[jsonHoliday objectForKey:@"Date"]];
//                            NSTimeInterval timeZone = [[NSTimeZone defaultTimeZone] secondsFromGMT];
//                            NSString *dateStr = [_appDelegate.propFormatVelosiDate stringFromDate:[holidayDate dateByAddingTimeInterval:timeZone]];
                            NSDate *date = [_appDelegate.propFormatVelosiDate dateFromString:dateStr];
                            Holiday *holiday = [[Holiday alloc] initWithName:[jsonHoliday objectForKey:@"Name"]
                                                                     country:[jsonHoliday objectForKey:@"Country"]
                                                               velosiDateStr:dateStr
                                                            monthYearDateStr:[_appDelegate.propDateFormatMonthyear stringFromDate:date]
                                                                        date:date];
                            for(id jsonApplicableOffice in [jsonHoliday objectForKey:@"OfficesApplied"]) {
                                [holiday addOfficeName:[jsonApplicableOffice objectForKey:@"OfficeName"]];
                            }
                            [holidays addObject:holiday];
                        }
                    }
                }
                return holidays;
            }
        }
    }@catch(NSException *e){
        NSLog(@"exception %@",e);
        return @"Server Connection Failed";
    }
}

- (id)weeklyholiday{
    NSString *urlWeeklyHoliday = [NSString stringWithFormat:@"%@GetNationalHolidaysForTheWeek?requestingPerson=%d&datetime=%@", _apiUrl,[[_appDelegate getStaff] propStaffID], [self dateToday]];
//    id data = [self httpsGetFrom:[self stringByAddingPercentEncodingForFormData:urlWeeklyHoliday]];
    id data = [self makeWebServiceCall:[self encodeURLString:urlWeeklyHoliday] requestMethod:GET postBody:nil];
    if([data isKindOfClass:[NSString class]])
        return data;
    else{
        NSDictionary *result = [[NSJSONSerialization JSONObjectWithData:data options:0 error:nil] objectForKey:@"GetNationalHolidaysForTheWeekResult"];
        if([NSArray arrayWithArray:[result objectForKey:@"SystemErrors"]].count > 0)
            return [[[NSArray arrayWithArray:[result objectForKey:@"SystemErrors"]] objectAtIndex:0] objectForKey:@"Message"];
        else{
            NSMutableArray *holidays = [NSMutableArray array];
            NSArray *jsonHolidays = [result objectForKey:@"NationalHoliday"];
            
            if(jsonHolidays){
                for(id jsonHoliday in jsonHolidays){
                    if([[jsonHoliday objectForKey:@"Active"] boolValue]){
                        NSString *dateStr = [self deserializeUnixDate:[jsonHoliday objectForKey:@"Date"]];
//                        NSDate *holidayDate = [self deserializeUnixDate:[jsonHoliday objectForKey:@"Date"]];
//                        NSTimeInterval timeZone = [[NSTimeZone defaultTimeZone] secondsFromGMT];
//                        NSString *dateStr = [_appDelegate.propFormatVelosiDate stringFromDate:[holidayDate dateByAddingTimeInterval:timeZone]];
                        NSDate *date = [_appDelegate.propFormatVelosiDate dateFromString:dateStr];
                        Holiday *holiday = [[Holiday alloc] initWithName:[jsonHoliday objectForKey:@"Name"] country:[jsonHoliday objectForKey:@"Country"] velosiDateStr:dateStr monthYearDateStr:[_appDelegate.propDateFormatMonthyear stringFromDate:date] date:date];
                        for(id jsonApplicableOffice in [jsonHoliday objectForKey:@"OfficesApplied"])
                            [holiday addOfficeName:[jsonApplicableOffice objectForKey:@"OfficeName"]];
                        [holidays addObject:holiday];
                    }
                }
            }
            return holidays;
        }
    }
}

- (id)recruitmentsForApproval{
    @try{
        NSMutableString *filter = [NSMutableString string];
        int staffId = [_appDelegate.getStaff propStaffID];
    
        [filter appendFormat:@"WHERE (CountryManager = %d AND StatusID = %d) OR (RegionalHRManagerId = %d AND StatusID = %d) OR ", staffId, RECRUITMENT_STATUSID_SUBMITTED, staffId, RECRUITMENT_STATUSID_APPROVEDBYCM];
        [filter appendFormat:@"(RegionalManager = %d AND ((StatusID = %d AND RegionalHRManagerId = 0) OR StatusID = %d)) OR ", staffId, RECRUITMENT_STATUSID_APPROVEDBYCM, RECRUITMENT_STATUSID_APPROVEDBYRHM];
        [filter appendFormat:@"(EVPId = %d AND StatusID = %d) ", staffId, RECRUITMENT_STATUSID_APPROVEDBYMHR];
        [filter appendFormat:@"ORDER BY DateRequested DESC"];
        
        
        NSString *urlHrForApproval = [NSString stringWithFormat:@"%@GetFilterRecruitment?filter=%@&requestingPerson=%d&datetime=%@", _apiUrl, filter, [_appDelegate.getStaff propStaffID], [self dateToday]];
        id data = [self makeWebServiceCall:[self encodeURLString:urlHrForApproval] requestMethod:GET postBody:nil];
        if([data isKindOfClass:[NSString class]])
            return data;
        else{
            NSDictionary *result = [[NSJSONSerialization JSONObjectWithData:data options:0 error:nil] objectForKey:@"GetFilterRecruitmentResult"];
            if([NSArray arrayWithArray:[result objectForKey:@"SystemErrors"]].count > 0)
                return [[[NSArray arrayWithArray:[result objectForKey:@"SystemErrors"]] objectAtIndex:0] objectForKey:@"Message"];
            
            NSMutableArray *recruitments = [NSMutableArray array];
            NSArray *jsonRecruitments = [result objectForKey:@"RecruitmentRequests"];
            for(id jsonRecruitment in jsonRecruitments)
                [recruitments addObject:[[Recruitment alloc] initWithDictionary:jsonRecruitment onlineGateway:self]];
            
            return recruitments;
        }
    }@catch(NSException *exception){
        return exception.reason;
    }
}

- (id)giftsAndHospitalitiesForApproval {
    @try {
        NSMutableString *filter = [NSMutableString string];
        int staffId = [_appDelegate.getStaff propStaffID];
        if ([_appDelegate.getStaff isCM] && [_appDelegate.getStaff propIsCorporateApprover]) {
            if([_appDelegate.getStaff getUserPosition] == USERPOSITION_CEO)
                [filter appendFormat:@"WHERE (CeoId = %d AND StatusId = %d) OR (CeoId = %d AND StatusID = %d)", staffId, GiftStatusIdApprovedByRm, staffId, GiftStatusIdApprovedByCm];
            else
                [filter appendFormat:@"WHERE (CountryManager = %d AND StatusID = %d) OR (RegionalManager = %d AND StatusID = %d)", staffId, GiftStatusIdSubmitted, staffId, GiftStatusIdApprovedByCm];
        } else {
            [filter appendFormat:@"WHERE (CountryManager = %d AND StatusID = %d)", staffId, GiftStatusIdSubmitted];
        }
        [filter appendString:@"ORDER BY DateSubmitted DESC"];
        NSString *url = [NSString stringWithFormat:@"%@GetFilterGiftsHospitality?filter=%@&requestingPerson=%d", _apiUrl, filter, staffId];
        id data = [self makeWebServiceCall:[self encodeURLString:url] requestMethod:GET postBody:nil];
        if ([data isKindOfClass:[NSString class]]) {
            return data;
        }else{
            NSDictionary *result = [[NSJSONSerialization JSONObjectWithData:data options:0 error:nil] objectForKey:@"GetFilterGiftsHospitalityResult"];
            if ([NSArray arrayWithArray:[result objectForKey:@"SystemErrors"]].count > 0)
                return [[[NSArray arrayWithArray:[result objectForKey:@"SystemErrors"]] objectAtIndex:0] objectForKey:@"Message"];
            NSMutableArray *giftsAndHospitalities = [NSMutableArray array];
            NSArray *jsonGifts = [result objectForKey:@"GiftsHospitalities"];
            for (NSDictionary *jsonGift in jsonGifts)
                [giftsAndHospitalities addObject:[[GiftHospitality alloc] initWithJSONDictionary:jsonGift withOnlineGateway:self]];
            return giftsAndHospitalities;
        }
    } @catch (NSException *exception) {
        return exception.reason;
    }
}

- (id)contractsTenderForApproval {
    @try {
        int staffId = [_appDelegate.getStaff propStaffID];
        NSMutableString *filter = [NSMutableString string];
        
        [filter appendFormat:@"WHERE (CMId = %d AND RequestStatus = %d) OR (RfmId = %d AND RequestStatus = %d) OR ", staffId, ContractStatusIdSubmitted, staffId, ContractStatusIdApprovedByCm];
        [filter appendFormat:@"(RMId = %d AND ((RequestStatus = %d AND RfmId = 0) OR RequestStatus = %d)) OR ", staffId, ContractStatusIdApprovedByCm, ContractStatusIdApprovedByRfm];
        [filter appendFormat:@"(LMId = %d AND RequestStatus = %d AND ProjectValueEquiv >= 500) OR ", staffId, ContractStatusIdApprovedByRm];
        [filter appendFormat:@"(CfoId = %d AND RequestStatus = %d) OR ", staffId, ContractStatusIdApprovedByLm];
        [filter appendFormat:@"(EVPId = %d AND RequestStatus = %d) ", staffId, ContractStatusIdApprovedByCfo];
        [filter appendFormat:@"ORDER BY RequestDate DESC"];
        
        NSString *url = [NSString stringWithFormat:@"%@GetFilterContract?filter=%@&requestingPerson=%d", _apiUrl, filter, staffId];
        id data =  [self makeWebServiceCall:[self encodeURLString:url] requestMethod:GET postBody:nil];
        if ([data isKindOfClass:[NSString class]])
            return data;
        else {
            NSDictionary *result = [[NSJSONSerialization JSONObjectWithData:data options:0 error:nil] objectForKey:@"GetFilterContractResult"];
            if ([NSArray arrayWithArray:[result objectForKey:@"SystemErrors"]].count > 0)
                return [[[NSArray arrayWithArray:[result objectForKey:@"SystemErrors"]] objectAtIndex:0] objectForKey:@"Message"];
            NSMutableArray *contractsTenders = [NSMutableArray array];
            NSArray *jsonContracts = [result objectForKey:@"Contracts"];
            for (NSDictionary *jsonContract in jsonContracts)
                [contractsTenders addObject:[[Contract alloc] initWithJSONDictionary:jsonContract withOnlineGateway:self]];
            return contractsTenders;
        }
    } @catch (NSException *exception) {
        return exception.reason;
    }
}

- (id)capexesForApproval{
    @try{
        NSMutableString *filter = [NSMutableString string];
        int staffId = [_appDelegate.getStaff propStaffID];
        
        [filter appendFormat:@"WHERE (CMId = %d AND StatusID = %d) OR (RfmId = %d AND StatusID = %d) OR ", staffId, CapexStatusIdSubmitted, staffId, CapexStatusIdApprovedByCm];
        [filter appendFormat:@"(RMId = %d AND ((StatusID = %d AND RfmId = 0) OR StatusID = %d)) OR ", staffId, CapexStatusIdApprovedByCm, CapexStatusIdApprovedByRfm];
        [filter appendFormat:@"(CfoId = %d AND TotalAmountInUSD >= 3000 AND StatusID = %d) OR ", staffId, CapexStatusIdApprovedByRm];
        [filter appendFormat:@"(EvpId = %d AND TotalAmountInUSD >= 3000 AND StatusID = %d) ", staffId, CapexStatusIdApprovedByCfo];
        [filter appendFormat:@"ORDER BY DateCreated DESC"];
        
        NSString *urlCapexForApproval = [NSString stringWithFormat:@"%@GetFilterCapex?filter=%@&requestingPerson=%d&datetime=%@",_apiUrl, filter, [_appDelegate.getStaff propStaffID], [self dateToday]];
        id data = [self makeWebServiceCall:[self encodeURLString:urlCapexForApproval] requestMethod:GET postBody:nil];
        if([data isKindOfClass:[NSString class]])
            return data;
        else{
            NSDictionary *result = [[NSJSONSerialization JSONObjectWithData:data options:0 error:nil] objectForKey:@"GetFilterCapexResult"];
            if([NSArray arrayWithArray:[result objectForKey:@"SystemErrors"]].count > 0)
                return [[[NSArray arrayWithArray:[result objectForKey:@"SystemErrors"]] objectAtIndex:0] objectForKey:@"Message"];
            
            NSMutableArray *capexHeaders = [NSMutableArray array];
            NSArray *jsonCapexHeaders = [result objectForKey:@"Capexes"];
            for(id jsonCapexHeader in jsonCapexHeaders)
                [capexHeaders addObject:[[CapexHeader alloc] initWithDictionary:jsonCapexHeader onlineGateway:self]];
            
            return capexHeaders;
        }
        
    }@catch(NSException *e){
        return e.reason;
    }
}

- (id)capexLineItemsForCapexID:(int)capexID{
    @try{
        NSString *urlCapexLineItems = [NSString stringWithFormat:@"%@GetCapexLineItems?CapexID=%d&requestingPerson=%d&datetime=%@",_apiUrl,capexID,[_appDelegate.getStaff propStaffID], [self dateToday]];
        id data = [self makeWebServiceCall:[self encodeURLString:urlCapexLineItems] requestMethod:GET postBody:nil];
        if([data isKindOfClass:[NSString class]])
            return data;
        else{
            NSDictionary *result = [[NSJSONSerialization JSONObjectWithData:data options:0 error:nil] objectForKey:@"GetCapexLineItemsResult"];
            if([NSArray arrayWithArray:[result objectForKey:@"SystemErrors"]].count > 0)
                return [[[NSArray arrayWithArray:[result objectForKey:@"SystemErrors"]] objectAtIndex:0] objectForKey:@"Message"];
            
            NSMutableArray *capexLineItems = [NSMutableArray array];
            NSArray *jsonCapexLineItems = [result objectForKey:@"CapexLineItems"];
            for(id jsonCapexLineItem in jsonCapexLineItems)
                [capexLineItems addObject:[[CapexForApprovalLineItem alloc] initWithDictionary:jsonCapexLineItem onlineGateway:self]];
            
            NSLog(@"ITEM IN LINEITEM: %ld",[capexLineItems count]);
            return capexLineItems;
        }
    }@catch(NSException *e){
        return e.reason;
    }
}

- (id)capexLineItemQoutationForLineItemID:(int)lineItemID{
    @try{
        NSString *urlCapexItemQuotation = [NSString stringWithFormat:@"%@GetCapexLinteItemQuotation?capexLineItemID=%d&requestingPerson=%d&datetime=%@",_apiUrl,lineItemID,[_appDelegate.getStaff propStaffID],[self dateToday]];
        id data = [self makeWebServiceCall:[self encodeURLString:urlCapexItemQuotation] requestMethod:GET postBody:nil];
        if([data isKindOfClass:[NSString class]])
            return data;
        else{
            NSDictionary *result = [[NSJSONSerialization JSONObjectWithData:data options:0 error:nil] objectForKey:@"GetCapexLinteItemQuotationResult"];
            if([NSArray arrayWithArray:[result objectForKey:@"SystemErrors"]].count > 0)
                return [[[NSArray arrayWithArray:[result objectForKey:@"SystemErrors"]] objectAtIndex:0] objectForKey:@"Message"];
            
            NSMutableArray *capexLineItemQoutations = [NSMutableArray array];
            NSArray *jsonCapexLineItemQoutations = [result objectForKey:@"CapexLineItemQuotations"];
            for(id jsonCapexLineItemQoutation in jsonCapexLineItemQoutations)
                [capexLineItemQoutations addObject:[[CapexLineItemQoutation alloc] initWithDictionary:jsonCapexLineItemQoutation onlineGateway:self]];
            
            return capexLineItemQoutations;
        }
    }@catch(NSException *e){
        return e.reason;
    }
}

- (id)recruitmentForID:(int)recruitmentID {
    @try{
        NSString *urlRecruitmentByID = [NSString stringWithFormat:@"%@GetRecruitmentByID?RecruitmentID=%d&requestingPerson=%d&datetime=%@",_apiUrl,recruitmentID,[_appDelegate.getStaff propStaffID],[self dateToday]];
        id data = [self makeWebServiceCall:[self encodeURLString:urlRecruitmentByID] requestMethod:GET postBody:nil];
        if([data isKindOfClass:[NSString class]])
            return data;
        else{
            NSDictionary *result = [[NSJSONSerialization JSONObjectWithData:data options:0 error:nil] objectForKey:@"GetRecruitmentByIDResult"];
            
            if([NSArray arrayWithArray:[result objectForKey:@"SystemErrors"]].count > 0)
                return [[[NSArray arrayWithArray:[result objectForKey:@"SystemErrors"]] objectAtIndex:0] objectForKey:@"Message"];
            return [[result objectForKey:@"RecruitmentRequests"] objectAtIndex:0];
        }
    }@catch(NSException *e){
        return e.reason;
    }
}

- (id)capexHeaderForID:(int)capexheaderID {
    @try{
        NSString *urlCapexByID = [NSString stringWithFormat:@"%@GetByCapexID?CapexID=%d&requestingPerson=%d&datetime=%@",_apiUrl,capexheaderID,[_appDelegate.getStaff propStaffID],[self dateToday]];
        id data = [self makeWebServiceCall:[self encodeURLString:urlCapexByID] requestMethod:GET postBody:nil];
        if([data isKindOfClass:[NSString class]])
            return data;
        else{
            NSDictionary *result = [[NSJSONSerialization JSONObjectWithData:data options:0 error:nil] objectForKey:@"GetByCapexIDResult"];
            if([NSArray arrayWithArray:[result objectForKey:@"SystemErrors"]].count > 0)
                return [[[NSArray arrayWithArray:[result objectForKey:@"SystemErrors"]] objectAtIndex:0] objectForKey:@"Message"];
            
            return [result objectForKey:@"Capex"];
        }
    }@catch(NSException *e){
        return e.reason;
    }
}

- (id)contractHeaderForId:(int)contractheaderId {
    @try {
        NSString *urlContractById = [NSString stringWithFormat:@"%@GetContractByID?contractID=%d&requestingPerson=%d",_apiUrl, contractheaderId, [_appDelegate.getStaff propStaffID]];
        id data = [self makeWebServiceCall:[self encodeURLString:urlContractById] requestMethod:GET postBody:nil];
        if ([data isKindOfClass:[NSString class]])
            return data;
        else {
            NSDictionary *result = [[NSJSONSerialization JSONObjectWithData:data options:0 error:nil] objectForKey:@"GetContractByIDResult"];
            if([NSArray arrayWithArray:[result objectForKey:@"SystemErrors"]].count > 0)
                return [[[NSArray arrayWithArray:[result objectForKey:@"SystemErrors"]] objectAtIndex:0] objectForKey:@"Message"];
            return [result objectForKey:@"Contracts"];
        }
    } @catch (NSException *exception) {
        return exception.reason;
    }
}

- (id)giftHeaderForID:(int)giftHeaderID {
    @try {
        NSString *urlGiftById = [NSString stringWithFormat:@"%@GetGiftsHospitalityByID?giftsID=%d&requestingPerson=%d", _apiUrl, giftHeaderID, [_appDelegate.getStaff propStaffID]];
        id data = [self makeWebServiceCall:[self encodeURLString:urlGiftById] requestMethod:GET postBody:nil];
        if ([data isKindOfClass:[NSString class]])
            return data;
        else {
            NSDictionary *result = [[NSJSONSerialization JSONObjectWithData:data options:0 error:nil] objectForKey:@"GetGiftsHospitalityByIDResult"];
            if ( [NSArray arrayWithArray:[result objectForKey:@"SystemErrors"]].count > 0)
                return [[[NSArray arrayWithArray:[result objectForKey:@"SystemErrors"]] objectAtIndex:0] objectForKey:@"Message"];
            return [result objectForKey:@"GiftHospitality"];
        }
    } @catch (NSException *e) {
        return e.reason;
    }
}

- (id)allOffices{
    @try{
        NSString *urlAllOffice = [NSString stringWithFormat:@"%@GetAllOffice?requestingperson=%d&datetime=%@",_apiUrl, [[_appDelegate getStaff] propStaffID], [self dateToday]];
        id data = [self makeWebServiceCall:[self encodeURLString:urlAllOffice] requestMethod:GET postBody:nil];
        if([data isKindOfClass:[NSString class]])
            return data;
        else{
            NSDictionary *result = [[NSJSONSerialization JSONObjectWithData:data options:0 error:nil] objectForKey:@"GetAllOfficeResult"];
            if([NSArray arrayWithArray:[result objectForKey:@"SystemErrors"]].count > 0)
                return [[[NSArray arrayWithArray:[result objectForKey:@"SytemErrors"]] objectAtIndex:0] objectForKey:@"Message"];
            NSMutableArray *offices = [NSMutableArray array];
            NSArray *jsonOffices = [result objectForKey:@"Offices"];
            for(id jsonOffice in jsonOffices)
                [offices addObject:[[Office alloc] initWithDictionary:jsonOffice isFullDetail:NO]];
            return offices;
        }
    }@catch(NSException *exception){
        return exception.reason;
    }
}

- (id)allProjectsForCostCenterID:(int)costCenterID{
    @try{
        NSString *urlProjectByCostCenter = [NSString stringWithFormat:@"%@GetProjectByCostCenter?costCenterID=%d&requestingPerson=%d&datetime=%@", _apiUrl, costCenterID, [[_appDelegate getStaff] propStaffID], [self dateToday]];
        id data = [self makeWebServiceCall:[self encodeURLString:urlProjectByCostCenter] requestMethod:GET postBody:nil];
        if([data isKindOfClass:[NSString class]])
            return data;
        else{
            NSDictionary *result = [[NSJSONSerialization JSONObjectWithData:data options:0 error:nil] objectForKey:@"GetProjectByCostCenterResult"];
            if([NSArray arrayWithArray:[result objectForKey:@"SystemErrors"]].count > 0)
                return [[[NSArray arrayWithArray:[result objectForKey:@"SystemErrors"]] objectAtIndex:0] objectForKey:@"Message"];
            
            NSMutableArray *projects = [NSMutableArray array];
            NSArray *jsonProjects = [result objectForKey:@"Projects"];
            for(id jsonProject in jsonProjects)
                [projects addObject:[[Project alloc] initWithDictionary:jsonProject]];
            return projects;
        }
    }@catch(NSException *exception){
        return exception.reason;
    }
}

- (id)allCostCentersForThisOffice{
    @try{
        NSString *urlCostCenterByOffice = [NSString stringWithFormat:@"%@GetCostCenterByOFfice?officeID=%d&requestingPerson=%d&datetime=%@", _apiUrl, [[_appDelegate getStaff] propOfficeID], [[_appDelegate getStaff] propStaffID], [self dateToday]];
//        id data = [self httpsGetFrom:[self stringByAddingPercentEncodingForFormData:urlCostCenterByOffice]];
        id data = [self makeWebServiceCall:[self encodeURLString:urlCostCenterByOffice] requestMethod:GET postBody:nil];
        if(([data isKindOfClass:[NSString class]]))
            return data;
        else{
            NSDictionary *result = [[NSJSONSerialization JSONObjectWithData:data options:0 error:nil] objectForKey:@"GetCostCenterByOfficeResult"];
            if([NSArray arrayWithArray:[result objectForKey:@"SystemErrors"]].count > 0)
                return [[[NSArray arrayWithArray:[result objectForKey:@"SystemErrors"]] objectAtIndex:0] objectForKey:@"Message"];
            
            NSMutableArray *costCenters = [NSMutableArray array];
            NSArray *jsonCostCenters = [result objectForKey:@"CostCenters"];
            for(id jsonCostCenter in jsonCostCenters)
                [costCenters addObject:[[CostCenter alloc] initWithID:[[jsonCostCenter objectForKey:@"CostCenterID"] intValue] name:[jsonCostCenter objectForKey:@"Description"]]];
            return costCenters;
        }
    }@catch(NSException *exception){
        return exception.reason;
    }
}

- (NSString *)saveLeaveWithNewLeaveJSON:(NSString *)newLeaveJSON oldLeaveJSON:(NSString *)oldLeaveJSON{
    @try{
        NSString *urlSaveLeave = [NSString stringWithFormat:@"%@SaveLeave?newLeave=%@&oldLeave=%@&requestingperson=%d&datetime=%@",_apiUrl, newLeaveJSON, oldLeaveJSON, [[_appDelegate getStaff] propStaffID], [self dateToday]];
//        id data = [self httpsGetFrom:[[self stringByAddingPercentEncodingForFormData:urlSaveLeave] stringByReplacingOccurrencesOfString:@"+" withString:@"%2b"]];
        id data = [self makeWebServiceCall:[[self encodeURLString:urlSaveLeave] stringByReplacingOccurrencesOfString:@"+" withString:@"%2b"] requestMethod:GET postBody:nil];
        NSDictionary *result = [[NSJSONSerialization JSONObjectWithData:data options:0 error:nil] objectForKey:@"SaveLeaveResult"];
        if([NSArray arrayWithArray:[result objectForKey:@"SystemErrors"]].count > 0)
            return [[[NSArray arrayWithArray:[result objectForKey:@"SystemErrors"]] objectAtIndex:0] objectForKey:@"Message"];
        return @"OK";
    }@catch(NSException *exception){
        NSLog(@"Exception in save leave %@", exception);
        return exception.reason;
    }
}

- (NSString *)followupLeave:(int)leaveID{
    @try{
        NSString *urlFollowUpLeave = [NSString stringWithFormat:@"%@EmailLeave?leave=%d&requestingPerson=%d",_apiUrl, leaveID, [[_appDelegate getStaff] propOfficeID]];
//        id data = [self httpsGetFrom:[[self stringByAddingPercentEncodingForFormData:urlFollowUpLeave] stringByReplacingOccurrencesOfString:@"+" withString:@"%2b"]];
        id data = [self makeWebServiceCall:[[self encodeURLString:urlFollowUpLeave] stringByReplacingOccurrencesOfString:@"+" withString:@"%2b"] requestMethod:POST postBody:nil];
        if([data isKindOfClass:[NSString class]])
            return data;
        else{
            NSDictionary *result = [[NSJSONSerialization JSONObjectWithData:data options:0 error:nil] objectForKey:@"EmailLeaveResult"];
            if([NSArray arrayWithArray:[result objectForKey:@"SystemErrors"]].count > 0)
                return [[[NSArray arrayWithArray:[result objectForKey:@"SystemErrors"]] objectAtIndex:0] objectForKey:@"Message"];
        }
        return nil;
    }@catch(NSException *exception){
        NSLog(@"Exception in follow up leaves %@",exception);
        return exception.reason;
    }
}

- (NSString *)processLeaveJSON:(int)leaveID forStatusID:(int)statusID{
    @try{
        NSString *urlApprovedLeave = [NSString stringWithFormat:@"%@ApproveLeave?leave=%d&status=%d&requestingPerson=%d", _apiUrl, leaveID, statusID, [[_appDelegate getStaff] propStaffID]];
//        id data = [self httpsGetFrom:[self stringByAddingPercentEncodingForFormData:urlApprovedLeave]];
        id data = [self makeWebServiceCall:[self encodeURLString:urlApprovedLeave] requestMethod:POST postBody:nil];
        NSDictionary *result = [[NSJSONSerialization JSONObjectWithData:data options:0 error:nil] objectForKey:@"ApproveLeaveResult"];
        if([NSArray arrayWithArray:[result objectForKey:@"SystemErrors"]].count > 0)
            return [[[NSArray arrayWithArray:[result objectForKey:@"SystemErrors"]] objectAtIndex:0] objectForKey:@"Message"];
        return @"OK";
    }@catch(NSException *exception) {
        NSLog(@"Exception in follow up leaves %@", exception);
        return exception.reason;
    }
}

- (id)saveClaimWithNewClaimHeaderJSON:(NSString *)newClaimHeaderJSON oldClaimHeaderID:(int)oldClaimID document:(NSString *)doc base64File:(NSString *)base64 {

    NSMutableString *jsonString = [NSMutableString stringWithString:@"{"];
    [jsonString appendFormat:@"\"newClaim\":%@,", newClaimHeaderJSON];
    [jsonString appendFormat:@"\"oldClaimId\":%d,", oldClaimID];
    if(base64 != nil){
        [jsonString appendFormat:@"\"document\":%@,",doc];
        int cSharpMaxStringLength = 30000;
        NSMutableArray *base64StrBodyParts = [NSMutableArray array];
        int len = 0;
        for(; (len+cSharpMaxStringLength) < base64.length; len+=cSharpMaxStringLength)
            [base64StrBodyParts addObject:[base64 substringWithRange:NSMakeRange(len, cSharpMaxStringLength)]];
        [base64StrBodyParts addObject:[base64 substringFromIndex:len]];
        [jsonString appendFormat:@"\"base64File\":\"%@\",", [base64StrBodyParts componentsJoinedByString:@","]];
    }else{
        [jsonString appendString:@"\"document\":null,"];
        [jsonString appendString:@"\"base64File\":\"\","];
    }
    [jsonString appendFormat:@"\"requestingPerson\":%d", [_appDelegate.getStaff propStaffID]];
    [jsonString appendString:@"}"];
    
    id data = [self makeWebServiceCall:[NSString stringWithFormat:@"%@SaveClaim",_apiUrl] requestMethod:POST postBody:jsonString];
    if (![data isKindOfClass:[NSString class]]){
        @try{
        NSDictionary *result = [[NSJSONSerialization JSONObjectWithData:data options:0 error:nil] objectForKey:@"SaveClaimResult"];
            
        if([NSArray arrayWithArray:[result objectForKey:@"SystemErrors"]].count > 0)
            return [[[NSArray arrayWithArray:[result objectForKey:@"SystemErrors"]] objectAtIndex:0] objectForKey:@"Message"];
        if([[result objectForKey:@"ProcessedClaimID"] intValue] != 0)
            return [NSNumber numberWithInteger:[[result objectForKey:@"ProcessedClaimID"] intValue]];
        }@catch(NSException *e){
            return e.reason;
        }
    }else{
        return data;
    }
}

- (id)saveNewClaimItemJSON:(NSString *)newClaimItemJSON oldClaimItemID:(int)oldClaimItemID document:(NSString *)doc base64:(NSString *)base64 {
    NSMutableString *sb = [NSMutableString stringWithString:@"{"];
    [sb appendFormat:@"\"newClaimLineItem\":%@,", newClaimItemJSON];
    [sb appendFormat:@"\"oldClaimLineItemId\":%d,", oldClaimItemID];
    if(base64 != nil) {
//        [sb appendFormat:@",\"document\":%@", ([[newClaimItem propAttachments] count] > 0) ? [[[newClaimItem propAttachments] objectAtIndex:0] jsonObject] : @"\"\""];
        [sb appendFormat:@"\"document\":%@,",doc];
        int cSharpMaxStringLength = 30000;
        //        NSMutableString *base64SB = [NSMutableString string];
        //        for(int ctr=0; ctr<base64.length; ctr+=cSharpMaxStringLength){
        //            cSharpMaxStringLength = (ctr+cSharpMaxStringLength > base64.length)?base64.length-ctr:30000;
        //            NSLog(@"calculating at range %d - %d -- max %lu",(ctr==0)?0:ctr+1, cSharpMaxStringLength, base64.length);
        //            [base64SB appendFormat:@"%@", [base64 substringWithRange:NSMakeRange((ctr==0)?0:ctr, cSharpMaxStringLength)]];
        //            if(ctr+cSharpMaxStringLength < base64.length)
        //                [base64SB appendString:@","];
        //        }
        //        [sb appendFormat:@",\"base64File\": \"%@\"", base64SB];
        NSMutableArray *base64SBParts = [NSMutableArray array];
        int len=0;
        for(; (len+cSharpMaxStringLength) < base64.length; len+=cSharpMaxStringLength)
            [base64SBParts addObject:[base64 substringWithRange:NSMakeRange(len, cSharpMaxStringLength)]];
        [base64SBParts addObject:[base64 substringFromIndex:len]];
        [sb appendFormat:@"\"base64File\":\"%@\",", [base64SBParts componentsJoinedByString:@","]];
    }else {
        [sb appendString:@"\"document\":null,"];
        [sb appendString:@"\"base64File\":\"\","];
    }
    [sb appendFormat:@"\"requestingPerson\":%d", _appDelegate.getStaff.propStaffID];
    [sb appendString:@"}"];
    //    int i = 0;
    id data = [self makeWebServiceCall:[NSString stringWithFormat:@"%@SaveClaimLineItem",_apiUrl] requestMethod:POST postBody:sb];
    if(![data isKindOfClass:[NSString class]]) {
        @try {
            NSDictionary *result = [[NSJSONSerialization JSONObjectWithData:data options:0 error:nil] objectForKey:@"SaveClaimLineItemResult"];
            if([NSArray arrayWithArray:[result objectForKey:@"SystemErrors"]].count > 0)
                return [[[NSArray arrayWithArray:[result objectForKey:@"SystemErrors"]] objectAtIndex:0] objectForKey:@"Message"];
            
            NSMutableArray *claimItems = [NSMutableArray array];
            NSMutableDictionary *cachedCategories = [NSMutableDictionary dictionary];
            NSArray *jsonClaimItems = [result objectForKey:@"ClaimLineItems"];
            for(id jsonMyClaimItem in jsonClaimItems) {
                if( [cachedCategories objectForKey: [jsonMyClaimItem objectForKey: [ClaimItem KEY_CATEGORYID]]] == nil ) {
                    NSString *urlCategoryByID = [NSString stringWithFormat:@"%@GetByCategoryID?categoryID=%d&requestingperson=%d&datetime=%@",_apiUrl, [[jsonMyClaimItem objectForKey:[ClaimItem KEY_CATEGORYID]] intValue], [[_appDelegate getStaff] propStaffID], [self dateToday]];
                    id catData = [self makeWebServiceCall:[self encodeURLString:urlCategoryByID] requestMethod:GET postBody:nil];
                    NSDictionary *catResult = [[NSJSONSerialization JSONObjectWithData:catData options:0 error:nil] objectForKey:@"GetByCategoryIDResult"];
                    if([NSArray arrayWithArray:[result objectForKey:@"SystemErrors"]].count > 0)
                        return [[[NSArray arrayWithArray:[result objectForKey:@"SystemErrors"]] objectAtIndex:0] objectForKey:@"Message"];
                    NSMutableDictionary *jsonCategory = [catResult objectForKey:@"Category"];
                    [cachedCategories setObject:jsonCategory forKey:[jsonMyClaimItem objectForKey:[ClaimItem KEY_CATEGORYID]]];
                }
                NSMutableDictionary *category = [cachedCategories objectForKey:[jsonMyClaimItem objectForKey:[ClaimItem KEY_CATEGORYID]]];
                int categoryType = [[category objectForKey:[ClaimItem KEY_CATEGORYTYPEID]] intValue];
                if( categoryType != [ClaimCategory TYPE_ASSET] ) { //exclude categories of type asset
                    if(categoryType == [ClaimCategory TYPE_MILEAGE])
                        [claimItems addObject:[[MileageClaimItem alloc] initWithJSONClaimItem:jsonMyClaimItem JSONCategory:category appDelegate:_appDelegate]];
                    else
                        [claimItems addObject:[[ClaimItem alloc] initWithJSONClaimItem:jsonMyClaimItem JSONCategory:category appDelegate:_appDelegate]];
                }
            }
            return claimItems;
        }@catch(NSException *e){
            return e.reason;
        }
    }else{
        return data;
    }
}

-(NSString *)processClaimForApprovalWithClaimId:(int)claimId claimStatusId:(int)statusId approversId:(int)approversId approversNote:(NSString*)note {
    NSMutableString *sb = [NSMutableString string];
    [sb appendString:@"{"];
    [sb appendFormat:@"\"claimId\":%d,", claimId];
    [sb appendFormat:@"\"claimStatusId\":%d,", statusId];
    [sb appendFormat:@"\"nextApproverId\":%d,", approversId];
    [sb appendFormat:@"\"requestingPerson\":%d,", [[_appDelegate getStaff] propStaffID]];
    [sb appendFormat:@"\"approversNote\":\"%@\"", note];
    [sb appendString:@"}"];
    
    id data = [self makeWebServiceCall:[NSString stringWithFormat:@"%@ProcessClaim",_apiUrl] requestMethod:POST postBody:sb];
    if (![data isKindOfClass:[NSString class]]) {
        NSDictionary *result = [[NSJSONSerialization JSONObjectWithData:data options:0 error:nil] objectForKey:@"ProcessClaimResult"];
        if ([[NSArray arrayWithArray:[result objectForKey:@"SystemErrors"]] count] > 0)
            return [[[NSArray arrayWithArray:[result objectForKey:@"SystemErrors"]] objectAtIndex:0] objectForKey:@"Message"];
        return nil;
    } else {
        if ([data isEqualToString:@"\"success\""]) {
            return nil;
        }else{
            return data;
        }
    }
}

- (NSString *)saveGift:(NSString *)newGiftJSON oldGiftJSON:(NSString *)oldGiftJSON {
    NSMutableString *strBody = [NSMutableString stringWithString:@"{"];
    [strBody appendFormat:@"\"newGifts\":%@,", newGiftJSON];
    [strBody appendFormat:@"\"oldGifts\":%@,", oldGiftJSON];
    [strBody appendFormat:@"\"requestingPerson\":%d", [[_appDelegate getStaff] propStaffID]];
    [strBody appendString:@"}"];
    
    @try {
        id data = [self makeWebServiceCall:[NSString stringWithFormat:@"%@SaveGiftsHospitality", _apiUrl] requestMethod:POST postBody:strBody];
        if ([data isKindOfClass:[NSData class]]) {
            NSDictionary *result = [[NSJSONSerialization JSONObjectWithData:data options:0 error:nil] objectForKey:@"SaveGiftsHospitalityResult"];
            if ([NSArray arrayWithArray:[result objectForKey:@"SystemErrors"]].count > 0)
                return [[[NSArray arrayWithArray:[result objectForKey:@"SystemErrors"]] objectAtIndex:0] objectForKey:@"Message"];
            if ([result objectForKey:@"ProcessedGiftsHospitalityID"] < 0)
                return @"No changes were made";
            return @"OK";
        } else {
            return data;
        }
    } @catch (NSException *e) {
        return e.reason;
    }
}

- (NSString *)saveContract:(NSString *)newContractJSON oldContractId:(int)contractId {
    NSMutableString *strBody = [NSMutableString stringWithFormat:@"{"];
    [strBody appendFormat:@"\"newContracts\":%@,", newContractJSON];
    [strBody appendFormat:@"\"oldContractId\":%d,", contractId];
    [strBody appendFormat:@"\"requestingPerson\":%d", [[_appDelegate getStaff] propStaffID]];
    [strBody appendString:@"}"];
    
    @try {
        id data = [self makeWebServiceCall:[NSString stringWithFormat:@"%@SaveContract", _apiUrl] requestMethod:POST postBody:strBody];
        if ([data isKindOfClass:[NSData class]]) {
            NSDictionary *result = [[NSJSONSerialization JSONObjectWithData:data options:0 error:nil] objectForKey:@"SaveContractResult"];
            if ([NSArray arrayWithArray:[result objectForKey:@"SystemErrors"]].count > 0)
                return [[[NSArray arrayWithArray:[result objectForKey:@"SystemErrors"]] objectAtIndex:0] objectForKey:@"Message"];
            return @"OK";
        } else {
            return data;
        }
    } @catch (NSException *exception) {
        return exception.reason;
    }
}

- (NSString *)saveCapexHeader:(NSString *)newCapexHeaderJSON oldCapexHeaderJSON:(NSString *)oldCapexHeaderJSON{
    NSString *body = [NSString stringWithFormat:@"{\"newCapex\":%@,\"oldCapex\":%@,\"requestingPerson\":%d,\"datetime\":\"%@\"}",newCapexHeaderJSON,oldCapexHeaderJSON,[[_appDelegate getStaff] propStaffID], [_appDelegate.propDateFormatDateTime stringFromDate:[NSDate date]]];
    @try{
        NSLog(@"SAVE CAPEX: %@SaveCapex%@",_apiUrl,body);
        id data = [self makeWebServiceCall:[NSString stringWithFormat:@"%@SaveCapex",_apiUrl] requestMethod:POST postBody:body];
        if ([data isKindOfClass:[NSData class]]) {
            NSDictionary *result = [[NSJSONSerialization JSONObjectWithData:data options:0 error:nil] objectForKey:@"SaveCapexResult"];
            if([NSArray arrayWithArray:[result objectForKey:@"SystemErrors"]].count > 0)
                return [[[NSArray arrayWithArray:[result objectForKey:@"SystemErrors"]] objectAtIndex:0] objectForKey:@"Message"];
            if([[result objectForKey:@"ProcessedCapexID"] intValue] == -1)
                return @"No changes were made";
            return @"OK";
        }else{
            return data;
        }
    }@catch(NSException *e){
        return e.reason;
    }
}

- (NSString *)saveRecruitment:(NSString *)newRecruitmentJSON oldRecruitmentJSON:(NSString *)oldRecruitmentJSON {
    NSString *body = [NSString stringWithFormat:@"{\"newRecruitment\":%@,\"oldRecruitment\":%@,\"requestingPerson\":%d,\"datetime\":\"%@\"}", newRecruitmentJSON, oldRecruitmentJSON, [[_appDelegate getStaff] propStaffID], [_appDelegate.propDateFormatDateTime stringFromDate:[NSDate date]]];
    @try{
//        id data = [self httpPostFrom:[NSString stringWithFormat:@"%@SaveRecruitment",_apiUrl] withBody:body];
        id data = [self makeWebServiceCall:[NSString stringWithFormat:@"%@SaveRecruitment",_apiUrl] requestMethod:POST postBody:body];
        if ([data isKindOfClass:[NSData class]]) {
            NSDictionary *result = [[NSJSONSerialization JSONObjectWithData:data options:0 error:nil] objectForKey:@"SaveRecruitmentResult"];
            if([NSArray arrayWithArray:[result objectForKey:@"SystemErrors"]].count > 0)
                return [[[NSArray arrayWithArray:[result objectForKey:@"SystemErrors"]] objectAtIndex:0] objectForKey:@"Message"];
            if([[result objectForKey:@"ProcessedRecruitmentID"] intValue] == -1)
                return @"No changes were made";
            return @"OK";
        }else{
            return data;
        }
    }@catch(NSException *e) {
        return e.reason;
    }
}

- (id)forexRateFrom:(NSString *)currFrom to:(NSString *)currTo {
    NSString *url = [NSString stringWithFormat:@"http://www.google.com/finance/converter?a=1&from=%@&to=%@",currFrom ,currTo];
//    int i = 0;
    NSString *html = [[NSString alloc] initWithData:[self makeWebServiceCall:url requestMethod:GET postBody:nil] encoding:NSASCIIStringEncoding];
    NSInteger parentDivStartPos = [html rangeOfString:@"<div id=currency_converter_result>"].location;
    NSInteger parentDivEndPos = [[html substringFromIndex:parentDivStartPos] rangeOfString:@"<input type=submit value=\"Convert\">\n</div>"].location;
    if(parentDivStartPos>=0 &&parentDivEndPos>=0){
        NSString *parentDivSubstring = [html substringWithRange:NSMakeRange(parentDivStartPos, parentDivEndPos)];
        //check if div has a child, if it does not have then there is something wrong with the parameters
        if(![parentDivSubstring containsString:@"><input"]) {
            //get the span that contains the text of the rate
            NSInteger spanStartPos = [parentDivSubstring rangeOfString:@"<span class=bld>"].location;
            NSInteger spanEndPos = [parentDivSubstring rangeOfString:@"</span>"].location;
            if(spanStartPos>=0 && spanEndPos>=0) {
                NSString *tempSpanSubStr = [parentDivSubstring substringWithRange:NSMakeRange(spanStartPos, spanEndPos-spanStartPos)];
                return [NSNumber numberWithFloat:[[[tempSpanSubStr substringFromIndex:[tempSpanSubStr rangeOfString:@">"].location+1] componentsSeparatedByString:@" "][0] floatValue]];
            }else
                return @"HTML Parsing error. Target source code of container span for rate string might have been changed. Please contact developer";
        }else
            return @"Currency Not Found";
    }else
        return @"HTML Parsing error. Target source code of container div for rate string might have been changed. Please contact developer";
}

- (NSString *)encodeURLString:(NSString*)url {
    return [url stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
}

@end
