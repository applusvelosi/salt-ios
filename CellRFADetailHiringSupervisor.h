//
//  CellRFADetailMain.h
//  Salt
//
//  Created by Rick Royd Aban on 10/2/15.
//  Copyright © 2015 Applus Velosi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CellRFADetailHiringSupervisor : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *propFieldName;
@property (weak, nonatomic) IBOutlet UILabel *propFieldEmail;
@property (weak, nonatomic) IBOutlet UILabel *propFieldOffice;
@property (weak, nonatomic) IBOutlet UILabel *propFieldDepartment;
@property (weak, nonatomic) IBOutlet UILabel *propFieldPhoneNumber;
@property (weak, nonatomic) IBOutlet UILabel *propFieldCMName;
@property (weak, nonatomic) IBOutlet UILabel *propFieldDateRequested;
@property (weak, nonatomic) IBOutlet UILabel *propFieldProcessedByCM;
@property (weak, nonatomic) IBOutlet UILabel *propFieldProcessedByRHM;
@property (weak, nonatomic) IBOutlet UILabel *propFieldProcessedByRM;
@property (weak, nonatomic) IBOutlet UILabel *propFieldProcessedByHRM;
@property (weak, nonatomic) IBOutlet UILabel *propFieldProcessedByCEO;
@end
