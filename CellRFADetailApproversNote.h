//
//  CellRFADetailApproversNote.h
//  Salt
//
//  Created by Rick Royd Aban on 28/09/2016.
//  Copyright © 2016 Applus Velosi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CellRFADetailApproversNote : UITableViewCell
@property (strong,nonatomic) IBOutlet UILabel *propFieldApproversNote;
@end
