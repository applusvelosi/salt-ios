//
//  VCMileageClaimItemDetail.m
//  Salt
//
//  Created by Rick Royd Aban on 2015/08/06.
//  Copyright (c) 2015年 Applus Velosi. All rights reserved.
//

#import "VCMileageClaimItemDetail.h"
#import "CellClaimItemDetailOverview.h"
#import "CellClaimItemDetailMileage.h"
#import "CellClaimItemDetailAmount.h"
#import "CellClaimItemDetailOtherDetails.h"
#import "CellClaimItemDetailAttendee.h"
#import "ClaimItemAttendee.h"

@interface VCMileageClaimItemDetail (){
    
    IBOutlet UITableView *_propLV;
}
@end

@implementation VCMileageClaimItemDetail

- (void)viewDidLoad{
    [super viewDidLoad];
    
    _propLV.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    _propLV.estimatedRowHeight = 30;
    _propLV.rowHeight = UITableViewAutomaticDimension;
    _propLV.dataSource = self;
    _propLV.delegate = self;
}

- (void)viewDidAppear:(BOOL)animated{
    [_propLV reloadData];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    switch (indexPath.row) {
        case 0:{
            CellClaimItemDetailOverview *cellClaimItemDetailOverview = [tableView dequeueReusableCellWithIdentifier:@"cell_overview"];
            cellClaimItemDetailOverview.propLabelCategoryName.text = [_propClaimItem propCategoryName];
            cellClaimItemDetailOverview.propLabelItemNumber.text = [_propClaimItem propItemNumber];
            cellClaimItemDetailOverview.propLabelProjectName.text = [_propClaimItem propProjectName];
            cellClaimItemDetailOverview.propLabelDate.text = [_propClaimItem propDateExpensedWithAppDelegate:self.propAppDelegate];
            cellClaimItemDetailOverview.propLabelAttachment.text = ([_propClaimItem propHasReceipt])?[[[_propClaimItem propAttachments] objectAtIndex:0] propDocName]:@"No";
            
            return cellClaimItemDetailOverview;
        }
            
        case 1:{
            CellClaimItemDetailMileage *cellClaimItemDetailMileage = [tableView dequeueReusableCellWithIdentifier:@"cell_mileage"];
            cellClaimItemDetailMileage.propLabelMileageFrom.text = [_propClaimItem propMileageFrom];
            cellClaimItemDetailMileage.propLabelMileageTo.text = [_propClaimItem propMileageTo];
            cellClaimItemDetailMileage.propLabelMileageMileage.text = [NSString stringWithFormat:@"%.2f",[_propClaimItem propMileage]];
            cellClaimItemDetailMileage.propLabelMileageRate.text = [NSString stringWithFormat:@"%.2f",[_propClaimItem propMileageRate]];
            cellClaimItemDetailMileage.propCboxReturn.image = [UIImage imageNamed:([_propClaimItem propHasReceipt])?@"icon_checkbox_sel":@"icon_checkbox_normal"];

            return cellClaimItemDetailMileage;
        }
            
        case 2:{
            CellClaimItemDetailAmount *cellClaimItemDetailAmount = [tableView dequeueReusableCellWithIdentifier:@"cell_amount"];
            cellClaimItemDetailAmount.propLabelAmountForeign.text = [NSString stringWithFormat:@"%.2f %@", [_propClaimItem propForeignAmount], [_propClaimItem propForeignCurrencyName]];
            cellClaimItemDetailAmount.propLabelAmountLocal.text = [NSString stringWithFormat:@"%.2f %@", [_propClaimItem propLocalAmount], [_propClaimItem propLocalCurrencyName]];
            cellClaimItemDetailAmount.propLabelAmountForex.text = [NSString stringWithFormat:@"%.2f", [_propClaimItem propOnActualConversionExchangeRate]];
            cellClaimItemDetailAmount.propLabelAmountTax.text = [NSString stringWithFormat:@"%.2f", [_propClaimItem propTaxedAmount]];
            
            return cellClaimItemDetailAmount;
        }
            
        case 3:{
            CellClaimItemDetailOtherDetails *cellClaimItemDetailOtherDetails = [tableView dequeueReusableCellWithIdentifier:@"cell_otherdetails"];
            cellClaimItemDetailOtherDetails.propLabelBillTo.text = ([_propClaimItem propCompanyChargeToID] > 0)?[_propClaimItem propCompanyChargeToName]:@"No";
//            cellClaimItemDetailOtherDetails.propLabelNotesToBill.text = [_propClaimItem propNotes];
            cellClaimItemDetailOtherDetails.propLabelDescription.text = [_propClaimItem description];
            
            return cellClaimItemDetailOtherDetails;
        }
            
        default:{
            CellClaimItemDetailAttendee *cellClaimItemAttendee = [tableView dequeueReusableCellWithIdentifier:@"cell_attendee"];
            ClaimItemAttendee *attendee = [[_propClaimItem propAttendees] objectAtIndex:indexPath.row-4];
            cellClaimItemAttendee.propLabelName.text = [attendee propName];
            cellClaimItemAttendee.propLabelJobTitle.text = [attendee propJobTitle];
            cellClaimItemAttendee.propLabelNotes.text = [attendee propDesc];
            
            return cellClaimItemAttendee;
        }
    }
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 4+[_propClaimItem propAttendees].count;
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
}

@end
