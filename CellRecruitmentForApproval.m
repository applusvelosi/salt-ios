//
//  CellRecruitmentForApproval.m
//  Salt
//
//  Created by Rick Royd Aban on 9/30/15.
//  Copyright © 2015 Applus Velosi. All rights reserved.
//

#import "CellRecruitmentForApproval.h"
#import "VelosiColors.h"

@implementation CellRecruitmentForApproval

-(void)awakeFromNib{
    [super awakeFromNib];
    self.propFieldStatus.textColor = [VelosiColors lightGrayDisabled];
}

@end
