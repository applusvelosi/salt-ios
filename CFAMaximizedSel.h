//
//  CFAMaximizedSel.h
//  Salt
//
//  Created by Rick Royd Aban on 2015/09/07.
//  Copyright (c) 2015年 Applus Velosi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CFAMaximizedSel : UITableViewCell

@property (strong, nonatomic) IBOutlet UIImageView *propCbox;
@property (strong, nonatomic) IBOutlet UIImageView *propImageReceipt;
@property (strong, nonatomic) IBOutlet UILabel *propFieldCategory;
@property (strong, nonatomic) IBOutlet UILabel *propFieldDate;
@property (strong, nonatomic) IBOutlet UILabel *propFieldStatus;
@property (strong, nonatomic) IBOutlet UILabel *propFieldLocalAmount;
@property (strong, nonatomic) IBOutlet UILabel *propFieldVAT;
@property (strong, nonatomic) IBOutlet UILabel *propFieldTax;
@property (strong, nonatomic) IBOutlet UILabel *propFieldForeignAmount;
@property (strong, nonatomic) IBOutlet UILabel *propFieldDesc;

@end
