//
//  VCClaimsForApproval.m
//  Salt
//
//  Created by Rick Royd Aban on 2015/07/31.
//  Copyright (c) 2015年 Applus Velosi. All rights reserved.
//

#import "VCClaimsForApproval.h"
#import "MBProgressHUD.h"
#import "CellClaimsForApproval.h"
#import "ClaimHeader.h"
#import "Claim.h"
#import "BusinessAdvance.h"
#import "Liquidation.h"
#import "VCClaimPaidByCC.h"
#import "VCClaimNotPaidByCC.h"
#import "VCBusinessAdvance.h"
#import "VCLiquidation.h"
#import "AppDelegate.h"
#import "LoaderDelegate.h"
#import "VCClaimsForApprovalList.h"
#import "VelosiColors.h"

@interface VCClaimsForApproval () <LoaderDelegate, UISearchResultsUpdating, UIScrollViewDelegate, UISearchBarDelegate, UISearchControllerDelegate> {
    //    IBOutlet UITableView *_propLV;
    ClaimHeader *_selectedClaimHeader;
    AppDelegate *_appDelegate;
    UISearchController *_searchController;
    NSMutableArray *_tempClaimsForApproval;
    NSString *_stringFilter;
    BOOL _searchWasCancelled;
}
@end

@implementation VCClaimsForApproval

#pragma mark -
#pragma mark === View Life Cycle Management ===
#pragma mark -
- (void)viewDidLoad {
    [super viewDidLoad];
    _appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    _tempClaimsForApproval = [NSMutableArray array];
    _stringFilter = @"";
    self.parentVC.delegate = self;
    _searchWasCancelled = NO;
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(didChangePrefeeredContentSize:)
                                                 name:UIContentSizeCategoryDidChangeNotification object:nil];
    
    _searchController = [[UISearchController alloc] initWithSearchResultsController:nil];
    _searchController.searchResultsUpdater = self;
    _searchController.dimsBackgroundDuringPresentation = NO;
    _searchController.hidesNavigationBarDuringPresentation = NO;
    
    _searchController.searchBar.tintColor = [UIColor blueColor];
    _searchController.searchBar.placeholder = @"Staff name or cost center";
    
    self.tableView.tableHeaderView = _searchController.searchBar;
    self.definesPresentationContext = YES;
    [_searchController.searchBar sizeToFit];
    _searchController.searchBar.delegate = self;
    
    self.tableView.rowHeight = UITableViewAutomaticDimension;
    self.tableView.estimatedRowHeight = 140;
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    //    NSIndexPath *indexPath = [_propLV indexPathForCell:sender];
    if([sender isKindOfClass:[UITableViewCell class]]) {
        UIViewController *destVC = segue.destinationViewController;
        if([_selectedClaimHeader propTypeID] == CLAIMTYPEID_CLAIMS) {
            Claim *selectedClaim = [[Claim alloc] initWithDictionary:[[_selectedClaimHeader savableDictionary] mutableCopy]];
            if( [segue.destinationViewController isKindOfClass:[VCClaimPaidByCC class]] ) {
                VCClaimPaidByCC *claim = (VCClaimPaidByCC*)destVC;
                claim.claimPaidByCC = [[ClaimPaidByCC alloc] initWithDictionary:[[selectedClaim savableDictionary] mutableCopy]];
                claim.propAppDelegate = _appDelegate;
            } else {
                ((VCClaimNotPaidByCC *)segue.destinationViewController).claimNotPaidByCC = [[ClaimNotPaidByCC alloc] initWithDictionary:[[selectedClaim savableDictionary] mutableCopy]];
                ((VCClaimNotPaidByCC *)segue.destinationViewController).propAppDelegate = _appDelegate;
            }
        } else {
            VCBusinessAdvance *baLiq = (VCBusinessAdvance*)destVC;
            baLiq.baLiquidation = [[Claim alloc] initWithDictionary:[[_selectedClaimHeader savableDictionary] mutableCopy]];
            baLiq.propAppDelegate = _appDelegate;
            //            ((VCLiquidation *)segue.destinationViewController).liquidation = [[Liquidation alloc] initWithDictionary:[[_selectedClaimHeader savableDictionary] mutableCopy]];
        }
    }
}


#pragma mark -
#pragma mark === Loader delegate ===
#pragma mark -
-(void)loadFinished {
    [self reloadClaimsForApproval:_stringFilter];
}

-(void)loadFailedWithError:(NSString *)error {
    [[[UIAlertView alloc] initWithTitle:@"" message:error delegate:nil cancelButtonTitle:@"Dismiss" otherButtonTitles:nil, nil] show];
}

#pragma mark -
#pragma mark - === scroll view delegate ===
#pragma mark -
-(void)scrollViewWillBeginDragging:(UIScrollView *)scrollView {
    [_searchController.searchBar setShowsCancelButton:NO animated:YES];
    [_searchController.searchBar resignFirstResponder];
}


#pragma mark -
#pragma mark === Search results updating delegate ===
#pragma mark -
- (void)updateSearchResultsForSearchController:(UISearchController *)searchController {
    if (searchController.active)
        [searchController.searchBar setShowsCancelButton:YES animated:YES];
}


#pragma mark -
#pragma mark === Search bar delegate ===
#pragma mark -
- (void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar {
    _searchWasCancelled = NO;
}

- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar {
    _searchWasCancelled = YES;
}

-(void)searchBarTextDidEndEditing:(UISearchBar *)searchBar {
    if (_searchWasCancelled) {
        searchBar.text = _stringFilter;
    } else {
        _stringFilter = searchBar.text;
    }
}

-(void)searchBarSearchButtonClicked:(UISearchBar *)searchBar {
    [searchBar setShowsCancelButton:NO animated:YES];
}

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText {
    _stringFilter = searchText;
    [self reloadClaimsForApproval:_stringFilter];
}

#pragma mark -
#pragma mark === Helper methods ===
#pragma mark -
- (ClaimHeader *)claimForApprovalIndexPath:(NSIndexPath *)indexPath {
    ClaimHeader *claimForApproval = nil;
    if (indexPath)
        claimForApproval = [[_appDelegate claimsForApproval] objectAtIndex:indexPath.row];
    return claimForApproval;
}

-(void)didChangePrefeeredContentSize:(NSNotification*)notification{
    [self.tableView reloadData];
}

- (void)reloadClaimsForApproval:(NSString *) searchString {
    [_tempClaimsForApproval removeAllObjects];
    if (searchString.length > 0) {
        for (ClaimHeader *claimApproval in [_appDelegate claimsForApproval]) {
            if ([[claimApproval propStaffName] localizedCaseInsensitiveContainsString:searchString] || [[claimApproval propCostCenterName] localizedCaseInsensitiveContainsString:searchString] || searchString.length < 1)
                [_tempClaimsForApproval addObject:claimApproval];
        }
    } else {
        [_tempClaimsForApproval addObjectsFromArray:[_appDelegate claimsForApproval]];
    }
    [self.tableView reloadData];
}

#pragma mark -
#pragma mark - Table view data source delegate
#pragma mark -
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    ClaimHeader *claimForApproval = [_tempClaimsForApproval objectAtIndex:indexPath.row];
    CellClaimsForApproval *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    
    cell.propFieldType.text = [claimForApproval propClaimNumber];
    cell.propFieldDateExpensed.text = [claimForApproval propDateCreated:_appDelegate];
    cell.propFieldStatus.text = [claimForApproval propStatusName];
    
    switch (claimForApproval.propStatusID) {
        case CLAIMSTATUSID_SUBMITTED:
            cell.propFieldStatus.textColor = [UIColor lightGrayColor];
            break;
        case CLAIMSTATUSID_APPROVEDBYAPPROVER:
            cell.propFieldStatus.textColor = [VelosiColors greenAcceptance];
            break;
        case CLAIMSTATUSID_REJECTEDBYAPPROVER:
            cell.propFieldStatus.textColor = [VelosiColors redRejection];
            break;
        case CLAIMSTATUSID_CANCELLED:
            cell.propFieldStatus.textColor = [VelosiColors redRejection];
            break;
        case CLAIMSTATUSID_APPROVEDBYCM:
            cell.propFieldStatus.textColor = [VelosiColors greenAcceptance];
            break;
        case CLAIMSTATUSID_REJECTEDBYCM:
            cell.propFieldStatus.textColor = [VelosiColors redRejection];
            break;
        case CLAIMSTATUSID_RETURN:
            cell.propFieldStatus.textColor = [VelosiColors redRejection];
            break;
        case CLAIMSTATUSID_LIQUIDATED:
            cell.propFieldStatus.textColor = [VelosiColors greenAcceptance];
            break;
        case CLAIMSTATUSID_PAIDUNDERCOMPANYCARD:
            cell.propFieldStatus.textColor = [VelosiColors greenAcceptance];
            break;
    }
    cell.propFieldClaimant.text = [claimForApproval propRequesterName];
    cell.propFieldAmount.text = [NSString stringWithFormat:@"%@ %@",[claimForApproval propCurrencyThree], [_appDelegate.decimalFormat stringFromNumber:[NSNumber numberWithFloat:[claimForApproval propTotalAmountInLC]]]];
    
    cell.bounds = CGRectMake(0, 0, CGRectGetWidth(tableView.bounds), 99999);
    cell.contentView.bounds = cell.bounds;
    [cell layoutIfNeeded];
    
    cell.propFieldType.preferredMaxLayoutWidth = CGRectGetWidth(cell.propFieldType.frame);
    cell.propFieldDateExpensed.preferredMaxLayoutWidth = CGRectGetWidth(cell.propFieldDateExpensed.frame);
    cell.propFieldStatus.preferredMaxLayoutWidth = CGRectGetWidth(cell.propFieldStatus.frame);
    cell.propFieldClaimant.preferredMaxLayoutWidth = CGRectGetWidth(cell.propFieldClaimant.frame);
    
    return cell;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return _tempClaimsForApproval.count;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    ClaimHeader *selectedClaimHeader = [[_appDelegate claimsForApproval] objectAtIndex:indexPath.row];
    _selectedClaimHeader = selectedClaimHeader;
    
    if([selectedClaimHeader propTypeID] == CLAIMTYPEID_CLAIMS){
        Claim *selectedClaim = [[Claim alloc] initWithDictionary:[[selectedClaimHeader savableDictionary] mutableCopy]];
        [self performSegueWithIdentifier:([selectedClaim isPaidByCompanyCard])?@"cfatoclaimpaidbycc":@"cfatoclaimnotpaidbycc" sender:[tableView cellForRowAtIndexPath:indexPath]];
    }else{
        [self performSegueWithIdentifier:@"cfatobusinessadvance" sender:[tableView cellForRowAtIndexPath:indexPath]];
    }
}



//#pragma mark -
//#pragma mark - search results delegate
//#pragma mark -
//-(void)updateSearchResultsForSearchController:(UISearchController *)searchController {
//    _stringFilter = [searchController.searchBar text];
//    if ([_stringFilter length] > 0)
//        [self reloadClaimsForApproval:_stringFilter];
////    else
////        [self.parentVC reloadClaimsForApprovalList];
//}

@end
