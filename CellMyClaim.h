//
//  CellMyClaim.h
//  Salt
//
//  Created by Rick Royd Aban on 2015/07/27.
//  Copyright (c) 2015年 Applus Velosi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CellMyClaim : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *propFieldClaimNumber;
@property (weak, nonatomic) IBOutlet UILabel *propFieldDateSubmitted;
@property (weak, nonatomic) IBOutlet UILabel *propFieldTotalClaim;
@property (weak, nonatomic) IBOutlet UILabel *propFieldCostCenter;
@property (weak, nonatomic) IBOutlet UILabel *propFieldStatus;

@end
