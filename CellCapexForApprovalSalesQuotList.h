//
//  CellCapexForApprovalLineItemQoutationDetailNote.h
//  Salt
//
//  Created by Rick Royd Aban on 10/16/15.
//  Copyright © 2015 Applus Velosi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CellCapexForApprovalSalesQuotList : UITableViewCell

@property (strong, nonatomic) IBOutlet UILabel *propSalesQuot;
@end
