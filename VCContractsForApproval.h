//
//  VCContractsForApproval.h
//  Salt
//
//  Created by Rick Royd Aban on 13/06/2017.
//  Copyright © 2017 Applus Velosi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "VCContractForApprovalList.h"

@interface VCContractsForApproval : UITableViewController

@property (nonatomic, weak) VCContractForApprovalList *parentVC;

@end
