//
//  VCHomeLeaves.m
//  Salt
//
//  Created by Rick Royd Aban on 8/19/14.
//  Copyright (c) 2014 applusvelosi. All rights reserved.
//


#import "VCHomeLeaves.h"
#import "VCHomeScrollPage.h"
#import "VelosiColors.h"
#import "HomeOverviewItem.h"
#import "CellHomeHeader.h"
#import "CellHomeItem1.h"
#import "CellHomeItem2.h"

@interface VCHomeLeaves (){
    NSMutableArray *_items;
    AppDelegate *_appDelegate;
}

@end

@implementation VCHomeLeaves

- (void)viewDidLoad{
    [super viewDidLoad];

    _items = [[NSMutableArray alloc] init];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    HomeOverviewItem *item = [_items objectAtIndex:indexPath.row];
    if([item propCellType] == HomeOverviewItem_HEADER){
        CellHomeHeader *cell = (CellHomeHeader *)[tableView dequeueReusableCellWithIdentifier:@"cell_header"];

        NSLog(@"HEADER @ %lu",indexPath.row);
        cell.propLabelName.text = item.propName;
        cell.propLabel1.text = item.propLabel1;
        cell.propLabel2.text = item.propLabel2;
        cell.propLabel3.text = item.propLabel3;
        
        return cell;
    }else if([item propCellType] == HomeOverviewItem_ITEM1){
        CellHomeItem1 *cell = (CellHomeItem1 *)[tableView dequeueReusableCellWithIdentifier:@"cell_item1"];

        NSLog(@"ITEM1 @ %lu",indexPath.row);
        cell.propLabelName.text = item.propName;
        cell.propLabel1.text = item.propLabel1;
        cell.propLabel2.text = item.propLabel2;
        cell.propLabel3.text = item.propLabel3;
        
        return cell;
    }else{
        CellHomeItem2 *cell = (CellHomeItem2 *)[tableView dequeueReusableCellWithIdentifier:@"cell_item2"];

        NSLog(@"ITEM2 @ %lu",indexPath.row);
        cell.propLabelName.text = item.propName;
        cell.propLabel1.text = item.propLabel1;
        cell.propLabel2.text = item.propLabel2;
        cell.propLabel3.text = item.propLabel3;
        
        return cell;
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return _items.count;
}

- (void)refresh:(Dashboard *)dashboard{
    [_items removeAllObjects];
    [_items addObject:[[HomeOverviewItem alloc] initWithName:@"Leaves" label1:@"Used" label2:@"Pending" label3:@"Remaining" cellType:HomeOverviewItem_HEADER]];
    [_items addObject:[[HomeOverviewItem alloc] initWithName:@"Vacation" label1:[NSString stringWithFormat:@"%d",[dashboard propUsedVL]] label2:[NSString stringWithFormat:@"%d",[dashboard propPendingVL]] label3:[NSString stringWithFormat:@"%.2f",[dashboard propRemVL]] cellType:HomeOverviewItem_ITEM1]];
    [_items addObject:[[HomeOverviewItem alloc] initWithName:@"Sick" label1:[NSString stringWithFormat:@"%d",[dashboard propUsedSL]] label2:[NSString stringWithFormat:@"%d",[dashboard propPendingSL]] label3:[NSString stringWithFormat:@"%.2f",[dashboard propRemSL]] cellType:HomeOverviewItem_ITEM2]];
    [_items addObject:[[HomeOverviewItem alloc] initWithName:@"Unpaid" label1:[NSString stringWithFormat:@"%d",[dashboard propUsedUnpaid]] label2:[NSString stringWithFormat:@"%d",[dashboard propPendingUnpaid]] label3:@"-" cellType:HomeOverviewItem_ITEM1]];
    [_items addObject:[[HomeOverviewItem alloc] initWithName:@"Hospitalization" label1:[NSString stringWithFormat:@"%d",[dashboard propUsedHospitalization]] label2:[NSString stringWithFormat:@"%d",[dashboard propPendingHospitalization]] label3:@"-" cellType:HomeOverviewItem_ITEM1]];
    [_items addObject:[[HomeOverviewItem alloc] initWithName:@"Business Trip" label1:[NSString stringWithFormat:@"%d",[dashboard propUsedBT]] label2:[NSString stringWithFormat:@"%d",[dashboard propPendingBT]] label3:@"-" cellType:HomeOverviewItem_ITEM2]];
    [_items addObject:[[HomeOverviewItem alloc] initWithName:@"Expenses" label1:@"Claims" label2:@"Advances" label3:@"Liquidation" cellType:HomeOverviewItem_HEADER]];
    [_items addObject:[[HomeOverviewItem alloc] initWithName:@"Open" label1:[NSString stringWithFormat:@"%d",[dashboard propClaimOpen]] label2:[NSString stringWithFormat:@"%d",[dashboard propAdvanceOpen]] label3:[NSString stringWithFormat:@"%d",[dashboard propLiquidationOpen]] cellType:HomeOverviewItem_ITEM1]];
    [_items addObject:[[HomeOverviewItem alloc] initWithName:@"Submitted" label1:[NSString stringWithFormat:@"%d",[dashboard propClaimSubmitted]] label2:[NSString stringWithFormat:@"%d",[dashboard propAdvanceSubmitted]] label3:[NSString stringWithFormat:@"%d",[dashboard propLiquidationSubmitted]] cellType:HomeOverviewItem_ITEM2]];
    [_items addObject:[[HomeOverviewItem alloc] initWithName:@"Approved by Manager" label1:[NSString stringWithFormat:@"%d",[dashboard propClaimApprovedByManager]] label2:[NSString stringWithFormat:@"%d",[dashboard propAdvanceApprovedByManager]] label3:[NSString stringWithFormat:@"%d",[dashboard propLiquidationApprovedByManager]] cellType:HomeOverviewItem_ITEM1]];
    [_items addObject:[[HomeOverviewItem alloc] initWithName:@"Approved by CM" label1:@"-" label2:[NSString stringWithFormat:@"%d",[dashboard propAdvanceApprovedByCM]] label3:@"-" cellType:HomeOverviewItem_ITEM2]];
    [_items addObject:[[HomeOverviewItem alloc] initWithName:@"Approved by Accounts" label1:[NSString stringWithFormat:@"%d",[dashboard propClaimApprovedByAccounts]] label2:[NSString stringWithFormat:@"%d",[dashboard propAdvanceApprovedByAccounts]] label3:[NSString stringWithFormat:@"%d",[dashboard propLiquidationApprovedByAccounts]] cellType:HomeOverviewItem_ITEM1]];
    [_items addObject:[[HomeOverviewItem alloc] initWithName:@"Paid" label1:[NSString stringWithFormat:@"%d",[dashboard propClaimPaid]] label2:[NSString stringWithFormat:@"%d",[dashboard propAdvancePaid]] label3:@"-" cellType:HomeOverviewItem_ITEM2]];
    [_items addObject:[[HomeOverviewItem alloc] initWithName:@"Liquidated" label1:@"-" label2:[NSString stringWithFormat:@"%d",[dashboard propAdvanceLiquidated]] label3:[NSString stringWithFormat:@"%d",[dashboard propLiquidationLiquidated]] cellType:HomeOverviewItem_ITEM1]];

    [self.tableView reloadData];
}

@end
