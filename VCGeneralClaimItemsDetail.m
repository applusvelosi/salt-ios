//
//  VCGeneralClaimItemsDetail.m
//  Salt
//
//  Created by Rick Royd Aban on 17/11/2016.
//  Copyright © 2016 Applus Velosi. All rights reserved.
//

#import "VCGeneralClaimItemsDetail.h"
#import "VCGeneralClaimItemsDetailsPage.h"
#import "ClaimItem.h"

@interface VCGeneralClaimItemsDetail () <UIPageViewControllerDataSource>
@property (nonatomic) UIDocumentInteractionController *_document;
@property (strong,nonatomic) NSArray *arrayPage;
@end

@implementation VCGeneralClaimItemsDetail


#pragma mark -
#pragma mark === View Life Cycle Management ===
#pragma mark -
- (void)viewDidLoad {
    
    [super viewDidLoad];
    self.arrayPage = [self.propAppDelegate forApprovalClaimItemsForClaimID:[self.propClaimHeader propClaimID]];
    self.pageVC = [[UIStoryboard storyboardWithName:@"SBClaimsForApproval" bundle:nil] instantiateViewControllerWithIdentifier:@"PageViewController"];
    self.pageVC.dataSource = self;
    VCGeneralClaimItemsDetailsPage *vcPage = [self viewControllerAtIndex:self.pageIndex];
    
    NSArray *viewControllers = @[vcPage];
    [self.pageVC setViewControllers:viewControllers direction:UIPageViewControllerNavigationDirectionReverse animated:NO completion:nil];
    [self addChildViewController:self.pageVC];
    [self.view addSubview:self.pageVC.view];
    [self.pageVC didMoveToParentViewController:self];
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];

}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {

}

#pragma mark -
#pragma mark - Helper Method
#pragma mark -
-(VCGeneralClaimItemsDetailsPage*)viewControllerAtIndex:(NSUInteger)index{
    if(([self.arrayPage count] == 0) || (index >= [self.arrayPage count]))
        return nil;
    VCGeneralClaimItemsDetailsPage *pageViewController = [[UIStoryboard storyboardWithName:@"SBClaimsForApproval" bundle:nil] instantiateViewControllerWithIdentifier:@"VCGeneralClaimItemsDetailsPage"];
    ClaimItem *claimItem = [self.arrayPage objectAtIndex:index];
    pageViewController.pageIndex = index;
    
    pageViewController.propClaimItem = claimItem;
    pageViewController.propClaimHeader = self.propClaimHeader;
    pageViewController.propAppDelegate = self.propAppDelegate;
    return pageViewController;
}


#pragma mark -
#pragma mark - Page view data source delegate
#pragma mark -
- (nullable UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerBeforeViewController:(UIViewController *)viewController{
    NSUInteger index = ((VCGeneralClaimItemsDetailsPage*) viewController).pageIndex;
    if ((index == 0) || (index == NSNotFound))
        return nil;
    index--;
    return [self viewControllerAtIndex:index];
}

- (nullable UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerAfterViewController:(UIViewController *)viewController{
    NSUInteger index = ((VCGeneralClaimItemsDetailsPage*) viewController).pageIndex;
    if (index == NSNotFound)
        return nil;
    index++;
    if (index == [self.arrayPage count])
        return nil;
    return [self viewControllerAtIndex:index];
}

- (NSInteger)presentationCountForPageViewController:(UIPageViewController *)pageViewController{
    return [self.arrayPage count];
}

- (NSInteger)presentationIndexForPageViewController:(UIPageViewController *)pageViewController{
    return self.pageIndex;
}


@end
