//
//  CellHomeItem1.h
//  Salt iOS
//
//  Created by Rick Royd Aban on 5/20/15.
//  Copyright (c) 2015 Rick Royd Aban. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CellHomeItem1 : UITableViewCell

@property (strong, nonatomic) IBOutlet UILabel *propLabelName;
@property (strong, nonatomic) IBOutlet UILabel *propLabel1;
@property (strong, nonatomic) IBOutlet UILabel *propLabel2;
@property (strong, nonatomic) IBOutlet UILabel *propLabel3;

@end
