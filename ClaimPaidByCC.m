//
//  ClaimPaidByCC.m
//  Salt
//
//  Created by Rick Royd Aban on 2015/07/27.
//  Copyright (c) 2015年 Applus Velosi. All rights reserved.
//

#import "ClaimPaidByCC.h"
#import "Staff.h"
#import "Office.h"

@implementation ClaimPaidByCC

- (ClaimHeader *)initWithCostCenterID:(int)costCenterID costCenterName:(NSString *)costCenterName claimTypeID:(int)claimTypeID isPaidByCC:(BOOL)isPaidByCC baIDCharged:(int)baIDCharged bacNumber:(NSString *)bacNumber appDelegate:(AppDelegate *)appDelegate {
    
    return [super initWithCostCenterID:costCenterID costCenterName:costCenterName claimTypeID:claimTypeID isPaidByCC:isPaidByCC baIDCharged:0 bacNumber:@"" appDelegate:appDelegate];
}

- (NSString *)propParentClaimNumber{
    return [[super claimHeader] objectForKey:@"ParentClaimNumber"];
}

- (float)propForDeductionAmount{
    return [[[super claimHeader] objectForKey:@"TotalComputedForDeductionInLC"] floatValue];
}

- (float)propForPaymentAmount{
    return [[[super claimHeader] objectForKey:@"TotalComputedForPaymentInLC"] floatValue];
}

@end
