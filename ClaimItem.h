//
//  ClaimItem.h
//  Salt
//
//  Created by Rick Royd Aban on 2015/07/29.
//  Copyright (c) 2015年 Applus Velosi. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "OnlineGateway.h"
#import "OfflineGatewaySavable.h"
#import "Document.h"
#import "ClaimCategory.h"
#import "Currency.h"
#import "Project.h"
#import "AppDelegate.h"

//#define NO_ATTACHMENT @"NO ATTACHMENT"
#define NOT_APPLICABLE @"NOT APPLICABLE"
typedef NS_ENUM(NSInteger, ClaimItemHasAttachment) {
    ClaimItemHasAttachmentYes = 1,
    ClaimItemHasAttachmentNo,
    ClaimItemHasAttachmentNA
};

typedef NS_ENUM(NSInteger, ClaimItemUpdatableDate) {
    ClaimItemUpdatableDateReoppened = 1,
    ClaimItemUpdatableDateSubmitted,
    ClaimItemUpdatableDateApprovedByApprover,
    ClaimItemUpdatableDateApprovedByCm,
    ClaimItemUpdatableDateApprovedByDirector,
    ClaimItemUpdatableDateCancelled,
    ClaimItemUpdatableDatePaid,
    ClaimItemUpdatableDateRejected,
};

@interface ClaimItem : NSObject<OfflineGatewaySavable>

+ (NSString *)KEY_CATEGORYID;
+ (NSString *)KEY_CATEGORYTYPEID;
+ (NSString *)KEY_ATTENDEE_REQUIREMENT;
+ (NSString *)KEY_SPENDLIMIT;
//+ (NSString *)statusDescForStatusKey:(int)statusKey;
//+ (NSString *)typeDescForTypeKey:(int)typeKey;

@property (strong, nonatomic) NSMutableDictionary *claimItem;

@property (nonatomic, assign) float amountForeing;
@property (nonatomic, assign) float amountLocal;
@property (nonatomic) NSArray *attachment;
@property (nonatomic, strong) NSString * attachmentNote;
@property (nonatomic) NSArray *attendees;
@property (nonatomic) int categoryID;
@property (nonatomic) NSString *categoryName;
@property (nonatomic) int claimID;
@property (nonatomic) int claimLineItemID;
@property (nonatomic) NSString *claimLineItemNumber;
@property (nonatomic, getter=statusID) int claimStatus;
@property (nonatomic, assign) int companyToChargeID;
@property (nonatomic) NSString *companyToChargeName;
@property (nonatomic) NSString *costCenterName;
@property (nonatomic, assign) int currency;
@property (nonatomic) NSString *foreignCurrency;
@property (nonatomic) NSString *dateApprovedByApprover;
@property (nonatomic) NSString *dateApprovedByCM;
@property (nonatomic) NSString *dateCancelled;
@property (nonatomic) NSString *dateRejected;
@property (nonatomic, getter=description) NSString *desc;
@property (nonatomic, assign) float exchangeRate;
@property (nonatomic, assign) int attachmentFlag;
@property (nonatomic, getter=isRechargable) BOOL rechargable;
@property (nonatomic, getter=isTaxRate) BOOL taxRate;
@property (nonatomic, getter=hasReceipt) BOOL withReceipt;
@property (nonatomic, assign) int localCurrency;
@property (nonatomic) NSString *localCurrencyName;
@property (nonatomic) float mileage;
@property (nonatomic) NSString *mileageFrom;
@property (nonatomic) float mileageRate;
@property (nonatomic, getter=hasMileageReturn) BOOL mileageReturn;
@property (nonatomic) NSString *mileageTo;
@property (nonatomic, assign) int mileageType;
@property (nonatomic, assign) int modifiedBy;
@property (nonatomic) NSString *notes;
@property (nonatomic) int projectCodeID;
@property (nonatomic) NSString *projectName;
@property (nonatomic) int rejectedBy;
@property (nonatomic) NSString *rejectedByName;
@property (nonatomic) NSString *sapTaxCode;
@property (nonatomic) NSString *serviceOrderCode;
@property (nonatomic, assign) int staffID;
@property (nonatomic) NSString *staffName;
@property (nonatomic) NSString *statusName;
@property (nonatomic, assign) float standardExRate;
@property (nonatomic, assign) float taxAmount;
@property (nonatomic) NSString *wbsCode;


- (instancetype)initWithJSONClaimItem:(NSDictionary *)claimItemDict JSONCategory:(NSDictionary *)categoryDict appDelegate:(AppDelegate*)appDelegate;
- (instancetype)initWithDictionary:(NSDictionary *)dict;
- (instancetype)initWithClaimHeader:(ClaimHeader *)claimHeader;
- (NSString *)propDateExpensedWithAppDelegate:(AppDelegate *)appdelegate;
- (NSString *)propDateApprovedbyApproverWithAppDelegate:(AppDelegate *)appdelegate;
- (NSString *)propDateApprovedbyDirectorWithAppDelegate:(AppDelegate *)appdelegate;
- (NSString *)propDateCancelledWithAppDelegate:(AppDelegate *)appdelegate;
- (NSString *)propDateCreatedWithAppDelegate:(AppDelegate *)appdelegate;
- (NSString *)propDateModifiedWithAppDelegate:(AppDelegate *)appdelegate;
- (NSString *)propDatePaidWithAppDelegate:(AppDelegate *)appdelegate;
- (NSString *)propDateRejecteddWithAppDelegate:(AppDelegate *)appdelegate;

- (void)updatableDate:(ClaimItemUpdatableDate)date withAppDelegate:(AppDelegate*) appDelegate;
- (NSDictionary *)savableDictionaryWithAppDelegate:(AppDelegate *)appDelegate;
//- (NSString *)propAttachmentCert;
- (NSString *)propAttachmentNote;
- (NSString *)propStatusName;
- (int)propCategoryTypeID;
- (float)propCategorySpendLimit;
- (int)propItemID;
- (NSString *)propItemNumber;
- (int)propProjectCodeID;
- (NSString *)propProjectName;
- (int)propCategoryID;
- (NSString *)propCategoryName;
- (float)propForeignAmount;
- (float)propLocalAmount;
- (float)propTaxedAmount;
- (float)propOnFileExchangeRate;
- (float)propOnActualConversionExchangeRate;
- (int)propForeignCurrencyID;
- (NSString *)propForeignCurrencyName;
- (int)propLocalCurrencyID;
- (NSString *)propLocalCurrencyName;
- (int)propCompanyChargeToID;
- (NSString *)propCompanyChargeToName;
- (BOOL)propHasReceipt;
- (NSString *)propProjectCode;
- (NSMutableArray *)propAttendees;
- (id)propAttachments;

- (void)propAmount:(float)amount;
- (void)propAmounLC:(float)amountLC;
- (void)propCategory:(ClaimCategory *)category;
- (void)propCompanyChargeTo:(Office *)companyToCharge;
- (void)propCurrency:(Currency *)currency;
- (void)propForex:(float)forex;
- (void)propExpenseDate:(NSString *)expenseDate;
- (void)propIsRechargeable:(BOOL)isRechargeable;
- (void)propIsTaxRated:(BOOL)isTaxRated;
- (void)propHasReceipt:(BOOL)hasRecreipt;
- (void)propLocalCurrency:(Currency *)localCurrency;
- (void)propNotes:(NSString *)notes;
- (void)propProject:(Project *)project;
- (void)propTaxAmount:(float)amount;
- (void)addAttachment:(Document *)document;
- (void)clearAndUpdateAttendees:(NSMutableArray *)attendees;
- (void)propDateCreated:(NSString *)dateCreated;

- (NSString *)jsonForWebServicesWithAppDelegate:(AppDelegate*)appDelegate;
+ (NSString *)JsonFromNewEmptyClaimHeader;


@end
